#!/bin/bash

# This will pop your new commits off, add the commits
# from upstream and then apply your new commits on top of those.
git-pull-under() {
    local branchName=${1-develop}
    git pull --rebase origin $branchName
}
