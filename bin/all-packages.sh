#!/bin/bash

#==================================================
# Get the directory of the script file
#==================================================
# Using pwd alone will not work if you are not running the script from
# the directory it is contained in.
#
# $() acts as a kind of quoting for commands but they're run in their own context.
# dirname gives you the path portion of the provided argument (removing the file key).
#
# This command gets the script's source file pathname, strips it to just
# the path portion, cds to that path, then uses pwd to return the (effectively)
# full path of the script. This is assigned to the variable. After all of
# that, the context is unwound so you end up back in the directory you started
# at but with an environment variable containing the script's path.
dirScript="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function updateAllModules() {
    ls;
#    sudo ncu -u;
#    sudo npm i --force;
#    npm run build;
#    npm run bump;
}

### Print a list of directories 1 level below the starting directory that contain package.json files.
function printPackageDirectoryAll() {
    cd $dirScript/../../;
    find . -name 'package.json' -maxdepth 2 -print0 | xargs -0 -n1 dirname | sort --unique | echo ${1}/boo
}

### Create a .npmrc file in all package directories and write the text 'package-lock=false' into it.
### This will prevent npm from automatically creating package-lock.json files.
function disablePackageLockAll() {
    cd $dirScript/../../;
    find . -name 'package.json' -maxdepth 2 -execdir sh -c 'echo "package-lock=false" >> .npmrc' \;
}

### Remove package-lock.json files from all package directories.
function removePackageLockFileAll() {
    cd $dirScript/../packages/;
    find . -name 'package.json' -maxdepth 2 -execdir sh -c 'sudo rm "package-deps.json" "yarn.lock"' \;
}

function npmPruneAll() {
    cd $dirScript/../../;
    find . -name 'package.json' -maxdepth 2 -execdir sh -c 'sudo npm prune' \;
}

function npmInstallAll() {
    cd $dirScript/../../;
    find . -name 'package.json' -maxdepth 2 -execdir sh -c 'sudo npm install' \;
}

function removeNodeModulesAll() {
    cd $dirScript/../../;
    find . -name 'package.json' -maxdepth 2 -execdir sh -c 'sudo rm -fr node_modules' \;
}

### Find all the folders that have a package.json file and execute the given script within each.
function nested() {
    cd $dirScript/../../;
    find . -name package.json -maxdepth 2 -execdir sudo ncu -u \;
    cd $dirScript/../../;
    find . -name package.json -maxdepth 2 -execdir sudo npm update \;
}

function gitCommitAll() {
    local message="$(read -p 'Commit message: ')";
    cd $dirScript/../packages/;
    find . -name 'package.json' -maxdepth 2 -execdir sh -c 'sudo git add . && sudo git add -u && sudo git commit -m "${message}" && sudo git push' \;
}

function buildAll() {
    cd $dirScript/../packages/;
    find . -name 'package.json' -maxdepth 3 -execdir sh -c 'sudo yarn build' \;
}

function npmUpdateScriptsAll() {
    local script="";
    local path="../../bin";

    ### Remove any existing scripts
    cd $dirScript/../packages/;
    find . -name 'package.json' -maxdepth 2 -execdir sh -c 'jq ".scripts = { }" package.json > package_temp.json && mv package_temp.json package.json' \;

    ### These scripts use node-jq to parse json files. Must be installed globally
    ### https://www.npmjs.com/package/node-jq

    ### clean
    cd $dirScript/../packages/;
    script="jq '.scripts.clean = \"source ${path}/clean.sh; clean;\"' package.json > package_temp.json && mv package_temp.json package.json";
    find . -name 'package.json' -maxdepth 2 -execdir sh -c "${script}" \;

    ### permit
    cd $dirScript/../packages/;
    script="jq '.scripts.permit = \"source ${path}/permit.sh; permit;\"' package.json > package_temp.json && mv package_temp.json package.json";
    find . -name 'package.json' -maxdepth 2 -execdir sh -c "${script}" \;

    ### pretest
    cd $dirScript/../packages/;
    script="jq '.scripts.pretest = \"source ${path}/pretest.sh; pretest;\"' package.json > package_temp.json && mv package_temp.json package.json";
    find . -name 'package.json' -maxdepth 2 -execdir sh -c "${script}" \;

    ### test
    cd $dirScript/../packages/;
    script="jq '.scripts.test = \"source ${path}/test/test.sh; test;\"' package.json > package_temp.json && mv package_temp.json package.json";
    find . -name 'package.json' -maxdepth 2 -execdir sh -c "${script}" \;

    ### bump
    cd $dirScript/../packages/;
    script="jq '.scripts.bump = \"source ${path}/npm-bump.sh; bump;\"' package.json > package_temp.json && mv package_temp.json package.json";
    find . -name 'package.json' -maxdepth 2 -execdir sh -c "${script}" \;

    ### build
    cd $dirScript/../packages/;
    script="jq '.scripts.build = \"source ${path}/build.sh; build;\"' package.json > package_temp.json && mv package_temp.json package.json";
    find . -name 'package.json' -maxdepth 2 -execdir sh -c "${script}" \;

    ### publish
    cd $dirScript/../packages/;
    script="jq '.scripts.publish = \"source ${path}/npm-publish.sh; publish;\"' package.json > package_temp.json && mv package_temp.json package.json";
    find . -name 'package.json' -maxdepth 2 -execdir sh -c "${script}" \;
}

function npmUpdateTsconfigAll() {
    local script="";

    ### experimentalDecorators
    cd $dirScript/../packages/;
    script="jq '.compilerOptions.experimentalDecorators = true' tsconfig.json > tsconfig_temp.json && mv tsconfig_temp.json tsconfig.json";
    find . -name 'package.json' -maxdepth 2 -execdir sh -c "${script}" \;
}
