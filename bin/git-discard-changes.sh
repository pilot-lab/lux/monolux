#!/bin/bash

function gitDiscardChanges() {
    git stash save --keep-index;
    git stash drop;
}
