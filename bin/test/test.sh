#!/usr/bin/env bash

dirScript="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
dirWorking="$( pwd )"

function testTypescript() {
    if [[ -n $(find . -name "test") ]]
    then
        ### We found a sub-directory named 'test', so look there for test files.
        ### Add the --tap option to get console.log output.
        ${dirScript}/../../node_modules/.bin/ts-node --project ${dirScript} ${dirScript}/../../node_modules/alsatian/cli/alsatian-cli.js --tap "./test/*.test.ts";
    else
        ${dirScript}/../../node_modules/.bin/ts-node ${dirScript}/../../node_modules/alsatian/cli/alsatian-cli.js --tap "./*.test.ts";
    fi

    return 0;
}

function testJavascript() {
    node ${dirScript}/../../node_modules/alsatian/cli/alsatian-cli.js "./**/*.test.js";
    return 0;
}

function test() {
    testTypescript;
    return 0;
}
