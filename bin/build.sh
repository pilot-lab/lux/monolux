#!/bin/bash

dirScript="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function tsBuild() {
    ### Delete index.d.ts file to avoid error:
    ### error TS5055: Cannot write file because it would overwrite input file.
    find . -type f -name "*.d.ts" -maxdepth 2 -delete;
    ### Use tsc to transpile project
    sudo ${dirScript}/../node_modules/typescript/bin/tsc --project ./;
    return 0;
}

function build() {
    tsBuild;
    return 0;
}

