#!/bin/bash

function watch() {
    tsc --project ./ --watch;
    return 0;
}
