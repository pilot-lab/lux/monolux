#!/bin/bash

#==================================================
# Spinner
# See: http://fitnr.com/showing-a-bash-spinner.html
#==================================================
spinner() {
    pid=$! # Process Id of the previous running command

    spin='-\|/'

    i=0
    while kill -0 $pid 2>/dev/null
    do
      i=$(( (i+1) %4 ))
      printf "\r${spin:$i:1} "
      sleep .1
    done
    printf "\b\b"
}

#==================================================
# Get the directory of the script file
#==================================================
# Using pwd alone will not work if you are not running the script from
# the directory it is contained in.
#
# $() acts as a kind of quoting for commands but they're run in their own context.
# dirname gives you the path portion of the provided argument (removing the file key).
#
# This command gets the script's source file pathname, strips it to just
# the path portion, cds to that path, then uses pwd to return the (effectively)
# full path of the script. This is assigned to the variable. After all of
# that, the context is unwound so you end up back in the directory you started
# at but with an environment variable containing the script's path.
dirScript="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#==================================================
# Declare other variables
#==================================================

#==================================================
# Make sure all the necessary tools are installed
#==================================================
installCommon() {
    echo "Installing common components"

    # Install brew, if it's not already installed.
    # Otherwise, make sure it's updated
    which -s brew > /dev/null 2>&1
    if [ $? != 0 ]
    then
        echo "Installing Homebrew"
        /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" > /dev/null 2>&1 & spinner $!
    else
        echo "Updating Homebrew"
        brew update > /dev/null 2>&1 & spinner $!
    fi

    # Make sure git is installed
    echo "Making sure git is installed"
    brew install git > /dev/null 2>&1 & spinner $!
    brew upgrade git > /dev/null 2>&1 & spinner $!

    # Make sure git-flow-avh is installed
    echo "Installing/updating git-flow"
    brew uninstall git-flow > /dev/null 2>&1 & spinner $!
    brew install git-flow-avh > /dev/null 2>&1 & spinner $!
    brew upgrade git-flow-avh > /dev/null 2>&1 & spinner $!

    # Install bash-completion
    echo "Installing/updating bash completion for git-flow"
    brew install bash-completion > /dev/null 2>&1 & spinner $!

    # Add bash-completion shortcut to usr/local/etc
    if [ -f $(brew --prefix)/etc/bash_completion ]; then
    . $(brew --prefix)/etc/bash_completion & spinner $!
    fi

    # Add line to ~/.bash_profile:
    echo '[ -f /usr/local/etc/bash_completion ] && . /usr/local/etc/bash_completion' >>~/.bash_profile & spinner $!

    # Copy git-flow-completion.bash to appropriate directory
    sudo cp git-flow-completion.bash /usr/local/etc/bash_completion.d > /dev/null 2>&1 & spinner $!

    # Initialize git-flow-avh
    echo "Initializing git-flow"
    git add -A . && git commit --allow-empty -m "Initial commit" > /dev/null 2>&1 & spinner $!
    git flow init -fd > /dev/null 2>&1 & spinner $!
}

installCommon
