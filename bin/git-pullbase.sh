#!/bin/bash

branchName=${1-master}
git checkout $branchName && git pull && git checkout - && git rebase -i $branchName
