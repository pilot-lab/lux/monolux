#!/bin/bash

### Amend the previous commit, reusing the same commit message
function gitAmend() {
    git add -A . && git add -u && git commit --amend -C HEAD;
}
