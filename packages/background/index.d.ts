import { IPromise } from '@pilotlab/result';
export declare class BackgroundProcess {
    constructor(src: string);
    private _worker;
    private _result;
    initialize(src: string): void;
    start(dataIn: any): IPromise<any>;
    destroy(): void;
}
export default BackgroundProcess;
