import is from '@pilotlab/is';
import { IPromise, Result } from '@pilotlab/result';


export class BackgroundProcess {
    constructor(src:string) {
        this.initialize(src);
    }


    private _worker:Worker;
    private _result:IPromise<any>;


    initialize(src:string):void {
        this._worker = new Worker(src);
        this._result = new Result<any>();
        this._worker.onmessage = (e:any) => {
            this._result.resolve(e.data)
        };
    }


    start(dataIn:any):IPromise<any> {
        this._worker.postMessage({'command': 'start', 'data': dataIn}); // Start the worker.
        return this._result;
    }


    destroy():void {
        if (is.notEmpty(this._worker)) this._worker.postMessage({'command': 'stop'});
        this._worker.terminate();
        this._worker = null;
    }
} // End class


export default BackgroundProcess;
