import is from '@pilotlab/is';
import Debug from '@pilotlab/debug';


import {
    AsyncTest,
    Setup,
    SetupFixture,
    Teardown,
    TeardownFixture,
    Test,
    TestCase,
    TestFixture,
    Timeout,
    Expect
} from 'alsatian';


import {
    BackgroundProcess
} from '../index';


@TestFixture("BacgroundProcess Tests")
export class Tests {
    @AsyncTest("background process")
    @Timeout(5000) // Alsatian will now wait 5 seconds before failing
    public async asyncBackground(testValue:any) {
        const promise:Promise<any> = new Promise((resolve, reject) => {
            let worker:BackgroundProcess = new BackgroundProcess('work.js');
            worker.start('hey there');
            resolve();
        });

        const result:any = await promise;
    }
}
