import {
    AsyncTest,
    Setup,
    SetupFixture,
    Teardown,
    TeardownFixture,
    Test,
    TestCase,
    TestFixture,
    Timeout,
    Expect
} from 'alsatian';

import { SignalBinding, ISignalBinding } from '@pilotlab/signals';
import { Numbers } from '@pilotlab/numbers';
import Strings from '@pilotlab/strings';


import {
    AnimationEaseBack,
    AnimationEaseBounce,
    AnimationEaseCircular,
    AnimationEaseCubic,
    AnimationEaseElastic,
    AnimationEaseExponential,
    AnimationEaseLinear,
    AnimationEaseQuadratic,
    AnimationEaseQuartic,
    AnimationEaseQuintic,
    AnimationEaseSine,
    IAnimationBatch,
    IAnimation,
    Animation,
    AnimationEase,
    IAnimationEaseFunction,
    SpeedType,
    AnimationEaseCategory,
    AnimationEaseType,
    AnimationEventArgs,
    AnimationBatch,
    AnimationManager,
    AnimationValue,
    AnimationOptions,
    IAnimationOptions,
    ISpeed,
    Speed
} from '../index';


@TestFixture('Attribute Tests')
export class Tests {
    @AsyncTest('timer intervalPassed event')
    @Timeout(5000) // Alsatian will now wait 5 seconds before failing
    public async asyncTest() {
        console.log('Pausing for 2 seconds');
        const animation = Animation.pause(2);
        animation.then(() => {
            console.log('Continuing with animation.');
            let animation:IAnimation = Animation.go(0, 100, 2);

            const signalBindingTicked:ISignalBinding<AnimationEventArgs> = animation.ticked.listen((args:AnimationEventArgs) => {
                console.log(Strings.padDigits(Numbers.round(args.values[0].current, 2).toFixed(2), 6), 'animation progress');
            });
            const signalBindingCompleted:ISignalBinding<AnimationEventArgs> = animation.completed.listen((args:AnimationEventArgs) => {
                console.log('animation complete');
                signalBindingTicked.detach();
                signalBindingCompleted.detach();
                Expect(args.progress).toBe(1);
            });
        });
    }
} // End Tests
