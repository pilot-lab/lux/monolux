export declare enum SpeedType {
    DURATION = 0,
    UNITS_PER_SECOND = 1,
    DIFFERENCE_FACTOR = 2,
}
export declare enum AnimationEaseCategory {
    LINEAR = 0,
    QUADRATIC = 1,
    CUBIC = 2,
    QUARTIC = 3,
    QUINTIC = 4,
    SINE = 5,
    EXPONENTIAL = 6,
    CIRCULAR = 7,
    ELASTIC = 8,
    BACK = 9,
    BOUNCE = 10,
}
export declare enum AnimationEaseType {
    NONE = 0,
    IN = 1,
    OUT = 2,
    INOUT = 3,
}
