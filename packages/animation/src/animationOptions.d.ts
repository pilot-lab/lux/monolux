import { Initializable } from '@pilotlab/initializable';
import { IPromise, Result } from '@pilotlab/result';
import IAnimationOptions from './interfaces/iAnimationOptions';
import ISpeed from './interfaces/iSpeed';
import IAnimationEaseFunction from './interfaces/iAnimationEaseFunction';
export declare class AnimationOptions extends Initializable implements IAnimationOptions {
    constructor(durationSpeed?: (number | ISpeed), ease?: IAnimationEaseFunction, configuration?: Object, isInitialize?: boolean);
    protected p_onInitializeStarted(result: Result<any>, args: any[]): IPromise<any>;
    static readonly default: IAnimationOptions;
    static readonly zero: IAnimationOptions;
    durationSpeed: (number | ISpeed);
    setDuration(value: (number | ISpeed)): IAnimationOptions;
    readonly durationZero: IAnimationOptions;
    readonly durationDefault: IAnimationOptions;
    protected p_durationSpeed: (number | ISpeed);
    ease: IAnimationEaseFunction;
    setEase(value: IAnimationEaseFunction): this;
    protected p_ease: IAnimationEaseFunction;
    repeatCount: number;
    isHideOnCompletion: boolean;
    configuration: Object;
    readonly copy: IAnimationOptions;
}
export default AnimationOptions;
