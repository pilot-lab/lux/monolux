import { List } from '@pilotlab/collections';
import { IInitializable } from '@pilotlab/initializable';
import { IPromise } from '@pilotlab/result';
import { Signal } from '@pilotlab/signals';
import IAnimation from './iAnimation';
import IAnimationEventArgs from './iAnimationEventArgs';
import AnimationManager from '../animationManager';


export interface IAnimationBatch extends IInitializable {
    animationManager:AnimationManager;
    isCompleted:boolean;
    isInterrupted:boolean;
    animations:List<IAnimation>;
    result:IPromise<IAnimationBatch>;
    initialize():IPromise<any>;
    then(onDone:(value:IAnimationBatch) => any, onError?:(error:Error) => void):IPromise<any>;
    interrupt():void;


    ticked:Signal<IAnimationEventArgs[]>;
    completed:Signal<IAnimationEventArgs[]>;
} // End class


export default IAnimationBatch;
