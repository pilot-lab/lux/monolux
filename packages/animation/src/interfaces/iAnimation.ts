import { IPromise } from '@pilotlab/result';
import { Signal } from '@pilotlab/signals';
import IAnimationValue from './iAnimationValue';
import IAnimationEventArgs from './iAnimationEventArgs';
import AnimationManager from '../animationManager';


export interface IAnimation {
    /*====================================================================*
     START: Properties and Fields
     *====================================================================*/
    readonly values:IAnimationValue[];


    readonly result:IPromise<IAnimation>;


    then(onDone:(value:IAnimation) => void, onError?:(error:Error) => void):IPromise<IAnimation>;

    isTimed:boolean;


    readonly isCompleted:boolean;
    readonly isInterrupted:boolean;


    readonly animationManager:AnimationManager;
    internalAnimationManager:AnimationManager;


    readonly progress:number;
    readonly key:string;


    /*====================================================================*
     START: Signals
     *====================================================================*/
    ticked:Signal<IAnimationEventArgs>;
    completed:Signal<IAnimationEventArgs>;


    /*====================================================================*
     START: Methods
     *====================================================================*/
    internalTick(elapsedMilliseconds:number):void;


    /**
     * Fire event from an external class.
     */
    completeAnimation(isInterrupted?:boolean):void;



    interrupt():void;
} // End interface


export default IAnimation;
