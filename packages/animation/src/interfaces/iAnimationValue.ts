export interface IAnimationValue {
    isTargetReached:boolean;
    start:number;
    target:number;
    current:number;
} // End class


export default IAnimationValue;
