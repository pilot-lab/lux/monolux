import ISpeed from './iSpeed';
import IAnimationEaseFunction from './iAnimationEaseFunction';
export interface IAnimationOptions {
    readonly durationZero: IAnimationOptions;
    readonly durationDefault: IAnimationOptions;
    durationSpeed: (number | ISpeed);
    setDuration(value: (number | ISpeed)): IAnimationOptions;
    ease: IAnimationEaseFunction;
    repeatCount: number;
    isHideOnCompletion: boolean;
    configuration: Object;
    copy: IAnimationOptions;
}
export default IAnimationOptions;
