import { IPromise } from '@pilotlab/result';
import { Signal } from '@pilotlab/signals';
import IAnimationValue from './iAnimationValue';
import IAnimationEventArgs from './iAnimationEventArgs';
import AnimationManager from '../animationManager';
export interface IAnimation {
    readonly values: IAnimationValue[];
    readonly result: IPromise<IAnimation>;
    then(onDone: (value: IAnimation) => void, onError?: (error: Error) => void): IPromise<IAnimation>;
    isTimed: boolean;
    readonly isCompleted: boolean;
    readonly isInterrupted: boolean;
    readonly animationManager: AnimationManager;
    internalAnimationManager: AnimationManager;
    readonly progress: number;
    readonly key: string;
    ticked: Signal<IAnimationEventArgs>;
    completed: Signal<IAnimationEventArgs>;
    internalTick(elapsedMilliseconds: number): void;
    completeAnimation(isInterrupted?: boolean): void;
    interrupt(): void;
}
export default IAnimation;
