export interface IAnimationValue {
    isTargetReached: boolean;
    start: number;
    target: number;
    current: number;
}
export default IAnimationValue;
