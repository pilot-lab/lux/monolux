import IAnimationValue from './interfaces/iAnimationValue';
export declare class AnimationValue implements IAnimationValue {
    constructor(start: number, target: number);
    readonly isTargetReached: boolean;
    private _isTargetReached;
    readonly start: number;
    private _start;
    readonly target: number;
    private _target;
    current: number;
    private _current;
}
export default AnimationValue;
