"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
class AnimationEventArgs {
    constructor(animation, progress, elapsedMilliseconds = -1, values) {
        this.progress = 0;
        this.animation = animation;
        this.progress = progress;
        this.elapsedMilliseconds = elapsedMilliseconds;
        this.values = is_1.default.notEmpty(values) ? values : [];
    }
}
exports.AnimationEventArgs = AnimationEventArgs;
exports.default = AnimationEventArgs;
//# sourceMappingURL=animationEventArgs.js.map