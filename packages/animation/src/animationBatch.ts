import { List } from '@pilotlab/collections';
import { Initializable } from '@pilotlab/initializable';
import { IPromise, Result } from '@pilotlab/result';
import { Signal, SignalMonitor } from '@pilotlab/signals';
import IAnimation from './interfaces/iAnimation';
import IAnimationBatch from './interfaces/iAnimationBatch';
import IAnimationEventArgs from './interfaces/iAnimationEventArgs';
import AnimationManager from './animationManager';
import Animation from './animation';


export class AnimationBatch<TAnimationBatch extends IAnimationBatch>
extends Initializable implements IAnimationBatch {
    constructor(
        animationManager:AnimationManager = Animation.animate,
        animations:IAnimation[] = [],
        isInitialize:boolean = true
    ) {
        super();
        this.p_animationManager = animationManager;
        this.p_animations = List.fromArray<IAnimation>(animations);
        this.p_result = new Result<IAnimationBatch>();
        this.ticked = new Signal<IAnimationEventArgs[]>(false);
        this.completed = new Signal<IAnimationEventArgs[]>(true);
        if (isInitialize && this.p_animations.size > 0) this.initialize();
    }


    initialize():IPromise<any> {
        if (this.p_animations.size > 0) {
            this._signalMonitorTicked = new SignalMonitor<IAnimationEventArgs>(false);
            this._signalMonitorCompleted = new SignalMonitor<IAnimationEventArgs>();
            for (let i:number = 0; i < this.p_animations.size; i++) {
                const animation:IAnimation = this.p_animations.item(i);

                this._signalMonitorCompleted.add(animation.completed);

                if (!animation.isCompleted) {
                    this._signalMonitorTicked.add(animation.ticked);
                }

                animation.completed.listenOnce((args:IAnimationEventArgs) => {
                    this._signalMonitorTicked.remove(args.animation.ticked);
                    if (!this.p_isInterrupted && animation.isInterrupted) this.p_isInterrupted = true;
                }, this);

                this.animationManager.run(animation);
            }

            this._signalMonitorTicked.allSignalsDispatched.listen((args:IAnimationEventArgs[]) => {
                this.ticked.dispatch(args);
            }, this);

            this._signalMonitorCompleted.allSignalsDispatched.listenOnce((args:IAnimationEventArgs[]) => {
                this.p_isCompleted = true;
                this.completed.dispatch(args);
                this.p_result.resolve(this);
            }, this);

            this._signalMonitorTicked.start();
            this._signalMonitorCompleted.start();
        } else { this.p_result.resolve(this); }

        return this.p_result;
    }


    private _signalMonitorTicked:SignalMonitor<IAnimationEventArgs>;
    private _signalMonitorCompleted:SignalMonitor<IAnimationEventArgs>;


    clear():void {
        this.interrupt();
        this.p_isInterrupted = false;
        this.p_result = new Result<IAnimationBatch>();
        this.p_animations.clear();
        this._signalMonitorTicked.clear();
        this._signalMonitorCompleted.clear();
    }


    get animationManager():AnimationManager { return this.p_animationManager; }
    protected p_animationManager:AnimationManager;


    get isCompleted():boolean { return this.p_isCompleted; }
    protected p_isCompleted:boolean = false;


    get isInterrupted():boolean { return this.p_isInterrupted; }
    protected p_isInterrupted:boolean = false;


    get animations():List<IAnimation> { return this.p_animations; }
    protected p_animations:List<IAnimation>;


    get result():Result<IAnimationBatch> { return this.p_result; }
    protected p_result:Result<IAnimationBatch>;


    /*====================================================================*
     START: Signals
     *====================================================================*/
    ticked:Signal<IAnimationEventArgs[]>;
    completed:Signal<IAnimationEventArgs[]>;


    /*====================================================================*
     START: Methods
     *====================================================================*/
    then(onDone:(value:TAnimationBatch) => any, onError?:(error:Error) => void):IPromise<any> {
        return this.p_result.then(onDone, onError);

    }


    interrupt():void {
        if (this.p_isInterrupted) return;
        for (let i:number = 0; i < this.p_animations.size; i++) {
            this.p_animations.item(i).interrupt();
        }
        this.p_isInterrupted = true;
    }
} // End class


export default AnimationBatch;
