"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const initializable_1 = require("@pilotlab/initializable");
class AnimationOptions extends initializable_1.Initializable {
    constructor(durationSpeed, ease, configuration, isInitialize = true) {
        super();
        this.repeatCount = 0;
        this.isHideOnCompletion = false;
        this.configuration = {};
        if (isInitialize)
            this.initialize(durationSpeed, ease, configuration);
    }
    p_onInitializeStarted(result, args) {
        const durationSpeed = args[0];
        const ease = args[1];
        const configuration = args[2];
        if (is_1.default.notEmpty(durationSpeed))
            this.durationSpeed = durationSpeed;
        if (is_1.default.notEmpty(ease))
            this.ease = ease;
        if (is_1.default.notEmpty(configuration))
            this.configuration = configuration;
        return result.resolve();
    }
    static get default() { return new AnimationOptions(); }
    static get zero() { return new AnimationOptions().durationZero; }
    get durationSpeed() { return this.p_durationSpeed; }
    set durationSpeed(value) { this.p_durationSpeed = value; }
    setDuration(value) {
        this.p_durationSpeed = value;
        return this;
    }
    get durationZero() { return this.copy.setDuration(0); }
    get durationDefault() { return this.copy.setDuration(null); }
    get ease() { return this.p_ease; }
    set ease(value) { this.p_ease = value; }
    setEase(value) {
        this.p_ease = value;
        return this;
    }
    get copy() {
        let animationOptionsCopy = new AnimationOptions(this.durationSpeed, this.ease, this.configuration);
        animationOptionsCopy.repeatCount = this.repeatCount;
        return animationOptionsCopy;
    }
}
exports.AnimationOptions = AnimationOptions;
exports.default = AnimationOptions;
//# sourceMappingURL=animationOptions.js.map