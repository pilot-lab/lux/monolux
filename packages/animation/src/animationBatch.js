"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const collections_1 = require("@pilotlab/collections");
const initializable_1 = require("@pilotlab/initializable");
const result_1 = require("@pilotlab/result");
const signals_1 = require("@pilotlab/signals");
const animation_1 = require("./animation");
class AnimationBatch extends initializable_1.Initializable {
    constructor(animationManager = animation_1.default.animate, animations = [], isInitialize = true) {
        super();
        this.p_isCompleted = false;
        this.p_isInterrupted = false;
        this.p_animationManager = animationManager;
        this.p_animations = collections_1.List.fromArray(animations);
        this.p_result = new result_1.Result();
        this.ticked = new signals_1.Signal(false);
        this.completed = new signals_1.Signal(true);
        if (isInitialize && this.p_animations.size > 0)
            this.initialize();
    }
    initialize() {
        if (this.p_animations.size > 0) {
            this._signalMonitorTicked = new signals_1.SignalMonitor(false);
            this._signalMonitorCompleted = new signals_1.SignalMonitor();
            for (let i = 0; i < this.p_animations.size; i++) {
                const animation = this.p_animations.item(i);
                this._signalMonitorCompleted.add(animation.completed);
                if (!animation.isCompleted) {
                    this._signalMonitorTicked.add(animation.ticked);
                }
                animation.completed.listenOnce((args) => {
                    this._signalMonitorTicked.remove(args.animation.ticked);
                    if (!this.p_isInterrupted && animation.isInterrupted)
                        this.p_isInterrupted = true;
                }, this);
                this.animationManager.run(animation);
            }
            this._signalMonitorTicked.allSignalsDispatched.listen((args) => {
                this.ticked.dispatch(args);
            }, this);
            this._signalMonitorCompleted.allSignalsDispatched.listenOnce((args) => {
                this.p_isCompleted = true;
                this.completed.dispatch(args);
                this.p_result.resolve(this);
            }, this);
            this._signalMonitorTicked.start();
            this._signalMonitorCompleted.start();
        }
        else {
            this.p_result.resolve(this);
        }
        return this.p_result;
    }
    clear() {
        this.interrupt();
        this.p_isInterrupted = false;
        this.p_result = new result_1.Result();
        this.p_animations.clear();
        this._signalMonitorTicked.clear();
        this._signalMonitorCompleted.clear();
    }
    get animationManager() { return this.p_animationManager; }
    get isCompleted() { return this.p_isCompleted; }
    get isInterrupted() { return this.p_isInterrupted; }
    get animations() { return this.p_animations; }
    get result() { return this.p_result; }
    then(onDone, onError) {
        return this.p_result.then(onDone, onError);
    }
    interrupt() {
        if (this.p_isInterrupted)
            return;
        for (let i = 0; i < this.p_animations.size; i++) {
            this.p_animations.item(i).interrupt();
        }
        this.p_isInterrupted = true;
    }
}
exports.AnimationBatch = AnimationBatch;
exports.default = AnimationBatch;
//# sourceMappingURL=animationBatch.js.map