import is from '@pilotlab/is';
import { Initializable } from '@pilotlab/initializable';
import { IPromise, Result } from '@pilotlab/result';
import IAnimationOptions from './interfaces/iAnimationOptions';
import ISpeed from './interfaces/iSpeed';
import IAnimationEaseFunction from './interfaces/iAnimationEaseFunction';


export class AnimationOptions extends Initializable implements IAnimationOptions {
    constructor(
        durationSpeed?:(number | ISpeed),
        ease?:IAnimationEaseFunction,
        configuration?:Object,
        isInitialize:boolean = true
    ) {
        super();
        if (isInitialize) this.initialize(durationSpeed, ease, configuration);
    }


    protected p_onInitializeStarted(result:Result<any>, args:any[]):IPromise<any> {
        const durationSpeed:(number | ISpeed) = args[0];
        const ease:IAnimationEaseFunction = args[1];
        const configuration:Object = args[2];

        if (is.notEmpty(durationSpeed)) this.durationSpeed = durationSpeed;
        if (is.notEmpty(ease)) this.ease = ease;
        if (is.notEmpty(configuration)) this.configuration = configuration;

        return result.resolve();
    }


    static get default():IAnimationOptions { return new AnimationOptions(); }
    static get zero():IAnimationOptions { return new AnimationOptions().durationZero; }


    get durationSpeed():(number | ISpeed) { return this.p_durationSpeed; }
    set durationSpeed(value:(number | ISpeed)) { this.p_durationSpeed = value; }
    setDuration(value:(number | ISpeed)):IAnimationOptions {
        this.p_durationSpeed = value;
        return this;
    }
    get durationZero():IAnimationOptions { return this.copy.setDuration(0); }
    get durationDefault():IAnimationOptions { return this.copy.setDuration(null); }
    protected p_durationSpeed:(number | ISpeed);


    get ease():IAnimationEaseFunction { return this.p_ease; }
    set ease(value:IAnimationEaseFunction) { this.p_ease = value; }
    setEase(value:IAnimationEaseFunction) {
        this.p_ease = value;
        return this;
    }
    protected p_ease:IAnimationEaseFunction;


    repeatCount:number = 0;
    isHideOnCompletion:boolean = false;
    configuration:Object = {};


    get copy():IAnimationOptions {
        let animationOptionsCopy:IAnimationOptions = new AnimationOptions(this.durationSpeed, this.ease, this.configuration);
        animationOptionsCopy.repeatCount = this.repeatCount;
        return animationOptionsCopy;
    }
} // End class


export default AnimationOptions;
