import IAnimationValue from './interfaces/iAnimationValue';


export class AnimationValue implements IAnimationValue {
    constructor(start:number, target:number) {
        this._start = start;
        this._target = target;
        this._current = start;
    }


    get isTargetReached():boolean { return this._isTargetReached; }
    private _isTargetReached:boolean = false;


    get start():number { return this._start; }
    private _start:number = 0.0;


    get target():number { return this._target; }
    private _target:number = 0.0;


    get current():number { return this._current; }
    set current(value:number) {
        if (this._isTargetReached) return;
        this._current = value;
        if (this._current === this._target) this._isTargetReached = true;
    }
    private _current:number = 0.0;
} // End class


export default AnimationValue;
