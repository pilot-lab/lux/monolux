"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const animationEnums_1 = require("./animationEnums");
const animation_1 = require("./animation");
const animationEase_1 = require("./animationEase");
class Speed {
    constructor(data) {
        this._isDefault = true;
        this.ease = animationEase_1.default.defaultEase;
        if (is_1.default.notEmpty(data))
            this._isDefault = false;
        this._speedType = animationEnums_1.SpeedType.DURATION;
        this._duration = 1.2;
        this._unitsPerSecond = 1000;
        this._differenceFactor = 0.15;
        this._unitValue = 1.0;
        this._normalizedUnitValue = 0.0005;
    }
    static validate(durationSpeed) {
        let speed = new Speed();
        if (typeof durationSpeed === 'number') {
            speed.type = animationEnums_1.SpeedType.DURATION;
            speed.duration = durationSpeed;
        }
        else if (durationSpeed instanceof Speed)
            speed = durationSpeed;
        return speed;
    }
    get isDefault() { return this._isDefault; }
    get type() { return this._speedType; }
    set type(value) { this._speedType = value; }
    get duration() { return this._duration; }
    set duration(value) { this._duration = value; }
    get unitsPerSecond() { return this._unitsPerSecond; }
    set unitsPerSecond(value) { this._unitsPerSecond = value; }
    get differenceFactor() { return this._differenceFactor; }
    set differenceFactor(value) { this._differenceFactor = value; }
    get unitValue() { return this._unitValue; }
    set unitValue(value) { this._unitValue = value; }
    get normalizedUnitValue() { return this._normalizedUnitValue; }
    set normalizedUnitValue(value) { this._normalizedUnitValue = value; }
    getDuration(startValue, targetValue, isNormalizedValue, fps) {
        return animation_1.default.getDuration(this, startValue, targetValue, isNormalizedValue, fps);
    }
}
exports.Speed = Speed;
exports.default = Speed;
//# sourceMappingURL=speed.js.map