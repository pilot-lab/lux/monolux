"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const collections_1 = require("@pilotlab/collections");
const is_1 = require("@pilotlab/is");
const signals_1 = require("@pilotlab/signals");
class AnimationManager {
    constructor(timer) {
        this.animations = null;
        this.p_completedAnimations = null;
        this._isAllAnimationCompleted = false;
        this._lengthCurrent = 0;
        this.ticked = new signals_1.Signal();
        this.allAnimationsCompleted = new signals_1.Signal(false);
        this.p_isRunning = false;
        this.animations = new collections_1.List();
        this.p_completedAnimations = new collections_1.List();
        this.p_timer = timer;
        this.start();
    }
    get isAllAnimationCompleted() { return this._isAllAnimationCompleted; }
    length() { if (this.animations != null)
        return this.animations.size;
    else
        return 0; }
    get timer() { return this.p_timer; }
    set timer(value) {
        this.stop();
        this.p_timer = value;
        this.start();
    }
    tick() {
        if (is_1.default.empty(this.p_timer) || !this.p_isRunning)
            return;
        let elapsedMilliseconds = this.p_timer.elapsedMilliseconds;
        this.ticked.dispatch(elapsedMilliseconds);
        let animation = null;
        let isAllCompleted = true;
        for (let i = 0; i < this.animations.size; i++) {
            animation = this.animations.item(i);
            if (animation.isCompleted === false) {
                isAllCompleted = false;
                animation.internalTick(elapsedMilliseconds);
            }
            else if (is_1.default.notEmpty(this.p_completedAnimations)) {
                this.p_completedAnimations.add(animation);
            }
        }
        if (isAllCompleted && !this._isAllAnimationCompleted) {
            this._isAllAnimationCompleted = true;
            this.allAnimationsCompleted.dispatch(true);
        }
        if (isAllCompleted)
            this.animations.clear();
        else {
            for (let i = 0; i < this.p_completedAnimations.size; i++) {
                this.animations.delete(this.p_completedAnimations.item(i));
            }
        }
        this.p_completedAnimations.clear();
        if (this._lengthCurrent !== this.animations.size) {
            this._lengthCurrent = this.animations.size;
        }
    }
    stop() { this.p_isRunning = false; }
    start() { this.p_isRunning = true; }
    get isRunning() { return this.p_isRunning; }
    run(animation) {
        if (is_1.default.notEmpty(this.animations)) {
            this.animations.add(animation);
            animation.internalAnimationManager = this;
        }
        return animation;
    }
    delete(animation) {
        this.animations.delete(animation);
    }
    clear() { this.animations.clear(); }
    interruptAll() {
        this.animations.forEach(function (animation) {
            animation.interrupt();
            return true;
        });
    }
}
exports.AnimationManager = AnimationManager;
exports.default = AnimationManager;
//# sourceMappingURL=animationManager.js.map