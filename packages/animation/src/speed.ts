import is from '@pilotlab/is';
import ISpeed from './interfaces/iSpeed';
import { SpeedType } from './animationEnums';
import Animation from './animation';
import AnimationEase from './animationEase';
import IAnimationEaseFunction from './interfaces/iAnimationEaseFunction'


export class Speed implements ISpeed {
    constructor(data?:(Speed | Object | string)) {
        if (is.notEmpty(data)) this._isDefault = false;

        this._speedType = SpeedType.DURATION;
        this._duration = 1.2;
        this._unitsPerSecond = 1000;
        this._differenceFactor = 0.15;
        this._unitValue = 1.0;
        this._normalizedUnitValue = 0.0005;
    }


    static validate(durationSpeed?:(number | ISpeed)):ISpeed {
        let speed:ISpeed = new Speed();

        if (typeof durationSpeed === 'number') {
            speed.type = SpeedType.DURATION;
            speed.duration = durationSpeed;
        } else if (durationSpeed instanceof Speed) speed = <ISpeed>durationSpeed;

        return speed;
    }


    get isDefault():boolean { return this._isDefault; }
    private _isDefault:boolean = true;


    get type():SpeedType { return this._speedType; }
    set type(value:SpeedType) { this._speedType = value; }
    private _speedType:SpeedType;


    ease:IAnimationEaseFunction = AnimationEase.defaultEase;


    get duration():number { return this._duration; }
    set duration(value:number) { this._duration = value; }
    private _duration:number;


    get unitsPerSecond():number { return this._unitsPerSecond; }
    set unitsPerSecond(value:number) { this._unitsPerSecond = value; }
    private _unitsPerSecond:number;


    get differenceFactor():number { return this._differenceFactor; }
    set differenceFactor(value:number) { this._differenceFactor = value; }
    private _differenceFactor:number;


    get unitValue():number { return this._unitValue; }
    set unitValue(value:number) { this._unitValue = value; }
    private _unitValue:number;


    get normalizedUnitValue():number { return this._normalizedUnitValue; }
    set normalizedUnitValue(value:number) { this._normalizedUnitValue = value; }
    private _normalizedUnitValue:number;


    getDuration(startValue?:number, targetValue?:number, isNormalizedValue?:boolean, fps?:number):number {
        return Animation.getDuration(this, startValue, targetValue, isNormalizedValue, fps);
    }
} // End class


export default Speed;
