"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class AnimationValue {
    constructor(start, target) {
        this._isTargetReached = false;
        this._start = 0.0;
        this._target = 0.0;
        this._current = 0.0;
        this._start = start;
        this._target = target;
        this._current = start;
    }
    get isTargetReached() { return this._isTargetReached; }
    get start() { return this._start; }
    get target() { return this._target; }
    get current() { return this._current; }
    set current(value) {
        if (this._isTargetReached)
            return;
        this._current = value;
        if (this._current === this._target)
            this._isTargetReached = true;
    }
}
exports.AnimationValue = AnimationValue;
exports.default = AnimationValue;
//# sourceMappingURL=animationValue.js.map