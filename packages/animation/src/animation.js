"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const initializable_1 = require("@pilotlab/initializable");
const is_1 = require("@pilotlab/is");
const result_1 = require("@pilotlab/result");
const signals_1 = require("@pilotlab/signals");
const ids_1 = require("@pilotlab/ids");
const collections_1 = require("@pilotlab/collections");
const timer_1 = require("@pilotlab/timer");
const animationEnums_1 = require("./animationEnums");
const animationEase_1 = require("./animationEase");
const animationManager_1 = require("./animationManager");
const animationEventArgs_1 = require("./animationEventArgs");
const animationValue_1 = require("./animationValue");
const linear_1 = require("./easing/linear");
const animationBatch_1 = require("./animationBatch");
const speed_1 = require("./speed");
class Animation extends initializable_1.Initializable {
    constructor(values, duration = 0.0, ease = animationEase_1.default.defaultEase, repeatCount = 0, key) {
        super();
        this.p_isTimed = true;
        this.p_isCompleted = false;
        this.p_isInterrupted = false;
        this.p_progress = 0;
        this.p_duration = 0.0;
        this.p_startElapsedMilliseconds = -1;
        this.p_elapsedMilliseconds = 0;
        this.p_isInitialized = false;
        this.p_repeatCount = 0;
        this._values = values;
        this.p_result = new result_1.default();
        this.p_duration = duration;
        if (is_1.default.notEmpty(ease))
            this.p_ease = ease;
        this.p_repeatCount = repeatCount;
        if (is_1.default.notEmpty(key))
            this.p_key = key;
        this.ticked = new signals_1.Signal();
        this.completed = new signals_1.Signal(true);
    }
    get values() { return this._values; }
    get result() { return this.p_result; }
    then(onDone, onError) {
        return this.p_result.then(onDone, onError);
    }
    get isTimed() { return this.p_isTimed; }
    set isTimed(value) { this.p_isTimed = value; }
    get isCompleted() { return this.p_isCompleted; }
    get isInterrupted() { return this.p_isInterrupted; }
    get animationManager() { return this.internalAnimationManager; }
    get progress() { return this.p_progress; }
    get key() {
        if (is_1.default.empty(this.p_key)) {
            this.p_key = `animation_internal_${ids_1.Identifier.getSessionUniqueInteger('animation_internal')}`;
        }
        return this.p_key;
    }
    internalTick(elapsedMilliseconds) {
        if (this.p_isCompleted)
            return;
        if (!this.isInitialized) {
            if (!this.p_isTimed) {
                this.initialize();
            }
            else if (is_1.default.empty(this.p_startElapsedMilliseconds) || this.p_startElapsedMilliseconds < 0) {
                this.initialize();
                this.p_startElapsedMilliseconds = elapsedMilliseconds;
            }
        }
        if (this.p_isTimed && is_1.default.notEmpty(this.p_startElapsedMilliseconds) && this.p_startElapsedMilliseconds > -1) {
            let quotient = 0.0;
            let quotientInt = 0.0;
            let remainder = 0.0;
            let progress = 0.0;
            let halfCycles = 0.0;
            if (this.p_duration > 0) {
                quotient = ((elapsedMilliseconds - this.p_startElapsedMilliseconds) / 1000) / this.p_duration;
                quotientInt = Math.floor(quotient);
                remainder = quotient - quotientInt;
                progress = remainder;
                halfCycles = quotientInt;
            }
            else {
                this._completeAnimation(1.0, elapsedMilliseconds - this.p_startElapsedMilliseconds);
                return;
            }
            if (halfCycles >= 1.0) {
                if (this.p_repeatCount === 0) {
                    this._completeAnimation(1.0, elapsedMilliseconds - this.p_startElapsedMilliseconds);
                    return;
                }
                else if (this.p_repeatCount === 1) {
                    if (halfCycles >= 2.0) {
                        this._completeAnimation(0.0, elapsedMilliseconds - this.p_startElapsedMilliseconds);
                        return;
                    }
                }
                else if (this.p_repeatCount > 1) {
                    if ((halfCycles / 2) >= this.p_repeatCount) {
                        this._completeAnimation(this.p_repeatCount % 1, elapsedMilliseconds - this.p_startElapsedMilliseconds);
                        return;
                    }
                }
                else if (this.p_repeatCount < 0) { }
            }
            this.p_elapsedMilliseconds = elapsedMilliseconds - this.p_startElapsedMilliseconds;
            this.p_progress = progress;
            this.p_onTicked(new animationEventArgs_1.default(this, progress, this.p_elapsedMilliseconds));
        }
    }
    p_onTicked(args) {
        let isAllTargetsReached = true;
        if (is_1.default.notEmpty(this.p_ease)) {
            this._values.forEach((value) => {
                value.current = this.p_ease(args.elapsedMilliseconds / 1000, value.start, value.target - value.start, this.p_duration);
                if (!value.isTargetReached)
                    isAllTargetsReached = false;
                return true;
            });
        }
        else {
            this._values.forEach((value) => {
                value.current = (value.start + (args.progress * (value.target - value.start)));
                if (!value.isTargetReached)
                    isAllTargetsReached = false;
                return true;
            });
        }
        this.ticked.dispatch(new animationEventArgs_1.default(this, args.progress, args.elapsedMilliseconds, this._values));
        if (this._values.length > 0 && isAllTargetsReached)
            this.completeAnimation();
    }
    _completeAnimation(progress, elapsedMilliseconds, isInterrupted = false) {
        let args = new animationEventArgs_1.default(this, progress, elapsedMilliseconds);
        if (isInterrupted) {
            this.p_isInterrupted = true;
        }
        this.p_isCompleted = true;
        this.p_onCompleted(args);
        this.completed.dispatch(args);
        this.p_result.resolve(this);
        this.ticked.deleteAll();
        this.completed.deleteAll();
    }
    completeAnimation(isInterrupted = false) { this._completeAnimation(1, this.p_elapsedMilliseconds, isInterrupted); }
    p_onCompleted(args) {
        if (this.isInterrupted)
            return;
        this._values.forEach((value) => {
            value.current = (value.start + (args.progress * (value.target - value.start)));
            return true;
        });
    }
    interrupt() { this.completeAnimation(true); }
    static get animate() { return this._animate; }
    static get animations() { return this._animations; }
    static get fpsActual() { return this._fpsActual; }
    static initialize() {
        if (this._isInitialized)
            return;
        this._timerFPS = new timer_1.HiResTimer(new timer_1.HiResTimerCounterBrowser());
        this._timerFPSActual = new timer_1.HiResTimer(new timer_1.HiResTimerCounterBrowser());
        try {
            window['requestAnimationFrame'](() => this._onTimerTick());
        }
        catch (e) {
            this._intervalReference = setInterval(() => this._onTimerTick(), 20);
            this._isRunningInWindow = false;
        }
        this._timerFPS.start();
        this._isInitialized = true;
    }
    static reset() {
        if (!this._isRunningInWindow && is_1.default.notEmpty(this._intervalReference, false)) {
            clearInterval(this._intervalReference);
            this._intervalReference = null;
        }
    }
    static _onTimerTick() {
        this._tick();
        if (this._isRunningInWindow)
            window.requestAnimationFrame(() => this._onTimerTick());
    }
    static _tick() {
        if (is_1.default.empty(this.animate.timer))
            return;
        this._timerFPSActual.start();
        if (this._timerFPS.elapsedMilliseconds >= 1000 / this.fps) {
            this._timerFPS.start();
            this.animate.tick();
        }
        if (this._frameCount === this._frameAverageCount) {
            this._fpsActual = Math.round(1000 / (this._fpsAccum / this._frameAverageCount));
            this._fpsAccum = 0;
            this._frameCount = 0;
        }
        else {
            this._frameCount++;
            this._fpsAccum += this._timerFPSActual.elapsedMilliseconds;
        }
    }
    static validateSpeed(durationSpeed) {
        let speed = speed_1.default.validate(durationSpeed);
        if (is_1.default.empty(speed) || speed.isDefault)
            if (is_1.default.notEmpty(this.speed))
                speed = this.speed;
        return speed;
    }
    static getDuration(durationSpeed, startValue, targetValue, isNormalizedValue = false, fps = 24) {
        let duration = 0;
        let speed;
        if (typeof durationSpeed === 'number' && durationSpeed >= 0)
            return durationSpeed;
        else {
            speed = this.validateSpeed(durationSpeed);
            if (speed.type === animationEnums_1.SpeedType.DURATION
                || (speed.type === animationEnums_1.SpeedType.UNITS_PER_SECOND && (is_1.default.empty(startValue) || is_1.default.empty(targetValue)))) {
                duration = speed.duration >= 0 ? speed.duration : this.speed.duration;
            }
            else if (speed.type === animationEnums_1.SpeedType.UNITS_PER_SECOND) {
                let diff = Math.abs(targetValue - startValue);
                let unitValue = isNormalizedValue ? speed.normalizedUnitValue : speed.unitValue;
                duration = (diff) / (speed.unitsPerSecond * unitValue);
            }
            else if (speed.type === animationEnums_1.SpeedType.DIFFERENCE_FACTOR) {
                if (speed.differenceFactor === 0)
                    duration = 0;
                else {
                    let ticks = 1 / speed.differenceFactor;
                    duration = ticks / fps;
                }
            }
        }
        return duration;
    }
    static go(startValue, targetValue, durationSpeed, ease, repeatCount = 0, animationKey) {
        let duration = this.getDuration(durationSpeed, startValue, targetValue);
        let animation = new Animation([new animationValue_1.default(startValue, targetValue)], duration, ease, repeatCount, animationKey);
        this.goAnimation(animation);
        return animation;
    }
    static goAnimation(animationIn) {
        if (is_1.default.empty(animationIn))
            return null;
        let animationCurrent = null;
        if (is_1.default.notEmpty(animationIn.key))
            animationCurrent = this._animations.get(animationIn.key);
        if (is_1.default.notEmpty(animationCurrent)) {
            animationCurrent.interrupt();
            this._animations.delete(animationCurrent.key);
            animationCurrent = null;
        }
        this._animations.set(animationIn.key, animationIn);
        animationIn.completed.listenOnce(() => {
            let animationOld = this.animations.get(animationIn.key);
            if (is_1.default.notEmpty(animationOld) && animationOld == animationIn && animationOld.isCompleted)
                this._animations.delete(animationIn.key);
            animationIn = null;
        });
        this.initialize();
        this.animate.run(animationIn);
        this.animate.allAnimationsCompleted.listenOnce(() => { this.reset(); });
    }
    static goAnimations(animationsIn) {
        const animationBatch = new animationBatch_1.default(this.animate, [], false);
        if (is_1.default.empty(animationsIn) || animationsIn.length === 0)
            return animationBatch;
        for (let i = 0; i < animationsIn.length; i++) {
            let animationIn = animationsIn[i];
            let animationCurrent = null;
            if (is_1.default.notEmpty(animationIn.key))
                animationCurrent = this._animations.get(animationIn.key);
            if (is_1.default.notEmpty(animationCurrent)) {
                animationCurrent.interrupt();
                this._animations.delete(animationCurrent.key);
                animationCurrent = null;
            }
            this._animations.set(animationIn.key, animationIn);
            animationIn.completed.listenOnce(() => {
                let animationOld = this.animations.get(animationIn.key);
                if (is_1.default.notEmpty(animationOld) && animationOld == animationIn && animationOld.isCompleted)
                    this._animations.delete(animationIn.key);
                animationIn = null;
            });
            animationBatch.animations.add(animationIn);
        }
        this.initialize();
        animationBatch.initialize();
        this.animate.allAnimationsCompleted.listenOnce(() => { this.reset(); });
        return animationBatch;
    }
    static pause(durationSpeed, ease = linear_1.default.none) {
        let duration = this.getDuration(durationSpeed, 0, 1);
        let animation = new Animation([new animationValue_1.default(0, 1)], duration, ease, 0);
        this.initialize();
        this.goAnimation(animation);
        return animation;
    }
}
Animation._animate = new animationManager_1.default(timer_1.HiResTimer.timerDefault);
Animation._animations = new collections_1.MapList();
Animation.speed = new speed_1.default();
Animation.fps = 30;
Animation._fpsActual = 0.0;
Animation._frameCount = 0;
Animation._fpsAccum = 0;
Animation._frameAverageCount = 30;
Animation._isRunningInWindow = true;
Animation._isInitialized = false;
exports.Animation = Animation;
exports.default = Animation;
//# sourceMappingURL=animation.js.map