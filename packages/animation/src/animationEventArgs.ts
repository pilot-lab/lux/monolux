import is from '@pilotlab/is';
import IAnimationValue from './interfaces/iAnimationValue';
import IAnimation from './interfaces/iAnimation';


/*====================================================================*
 ===== START: AnimationEventArgs
 *====================================================================*/
export class AnimationEventArgs {
    constructor(
        animation:IAnimation,
        progress:number,
        elapsedMilliseconds:number = -1,
        values?:IAnimationValue[]
    ) {
        this.animation = animation;
        this.progress = progress;
        this.elapsedMilliseconds = elapsedMilliseconds;
        this.values = is.notEmpty(values) ? values : [];
    }


    animation:IAnimation;


    /**
     * The normalized progress of the animation (0.0 <--> 1.0).
     */
    progress:number = 0;


    elapsedMilliseconds:number;


    /**
     * The list of values being animated.
     */
    values:IAnimationValue[];
} // End class


export default AnimationEventArgs;
