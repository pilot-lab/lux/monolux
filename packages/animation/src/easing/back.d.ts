export declare class AnimationEaseBack {
    static in(t: number, b: number, c: number, d: number): number;
    static inExtended(t: number, b: number, c: number, d: number, s: number): number;
    static out(t: number, b: number, c: number, d: number): number;
    static outExtended(t: number, b: number, c: number, d: number, s: number): number;
    static inOut(t: number, b: number, c: number, d: number): number;
    static inOutExtended(t: number, b: number, c: number, d: number, s: number): number;
}
export default AnimationEaseBack;
