export class AnimationEaseQuadratic {
    static in(t:number, b:number, c:number, d:number):number {
        return c * (t /= d) * t + b;
    }


    static out(t:number, b:number, c:number, d:number):number {
        return -c * (t /= d) * (t - 2) + b;
    }


    static inOut(t:number, b:number, c:number, d:number):number {
        if ((t /= d * 0.5) < 1.0) return c * 0.5 * t * t + b;
        return -c * 0.5 * ((--t) * (t - 2) - 1) + b;
    }
} // End class


export default AnimationEaseQuadratic;
