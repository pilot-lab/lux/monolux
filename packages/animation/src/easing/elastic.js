"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class AnimationEaseElastic {
    static in(t, b, c, d) {
        return AnimationEaseElastic.inExtended(t, b, c, d, 0.0, 0.0);
    }
    static inExtended(t, b, c, d, a, p) {
        if (t == 0.0)
            return b;
        if ((t /= d) == 1.0)
            return b + c;
        if (p == 0.0)
            p = d * 0.3;
        let s = 0.0;
        if (a == 0.0 || a < Math.abs(c)) {
            a = c;
            s = p * 0.25;
        }
        else
            s = p / (2 * Math.PI) * Math.asin(c / a);
        return -(a * Math.pow(2, 10 * (--t)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b;
    }
    static out(t, b, c, d) {
        return AnimationEaseElastic.outExtended(t, b, c, d, 0.0, 0.0);
    }
    static outExtended(t, b, c, d, a, p) {
        if (t == 0.0)
            return b;
        if ((t /= d) == 1)
            return b + c;
        if (p == 0.0)
            p = d * 0.3;
        let s = 0.0;
        if (a == 0.0 || a < Math.abs(c)) {
            a = c;
            s = p * 0.25;
        }
        else
            s = p / (2 * Math.PI) * Math.asin(c / a);
        return a * Math.pow(2, -10 * t) * Math.sin((t * d - s) * (2 * Math.PI) / p) + c + b;
    }
    static inOut(t, b, c, d) {
        return AnimationEaseElastic.inOutExtended(t, b, c, d, 0.0, 0.0);
    }
    static inOutExtended(t, b, c, d, a, p) {
        if (t == 0.0)
            return b;
        if ((t /= d * 0.5) == 2)
            return b + c;
        if (p == 0.0)
            p = d * (0.3 * 1.5);
        let s = 0.0;
        if (a == 0 || a < Math.abs(c)) {
            a = c;
            s = p * 0.25;
        }
        else
            s = p / (2 * Math.PI) * Math.asin(c / a);
        if (t < 1.0)
            return -0.5 * (a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b;
        return a * Math.pow(2, -10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p) * 0.5 + c + b;
    }
}
exports.AnimationEaseElastic = AnimationEaseElastic;
exports.default = AnimationEaseElastic;
//# sourceMappingURL=elastic.js.map