"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class AnimationEaseSine {
    static in(t, b, c, d) {
        return -c * Math.cos(t / d * (Math.PI * 0.5)) + c + b;
    }
    static out(t, b, c, d) {
        return c * Math.sin(t / d * (Math.PI * 0.5)) + b;
    }
    static inOut(t, b, c, d) {
        return -c * 0.5 * (Math.cos(Math.PI * t / d) - 1) + b;
    }
}
exports.AnimationEaseSine = AnimationEaseSine;
exports.default = AnimationEaseSine;
//# sourceMappingURL=sine.js.map