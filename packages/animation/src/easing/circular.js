"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class AnimationEaseCircular {
    static in(t, b, c, d) {
        return -c * (Math.sqrt(1 - (t /= d) * t) - 1) + b;
    }
    static out(t, b, c, d) {
        return c * Math.sqrt(1 - (t = t / d - 1) * t) + b;
    }
    static inOut(t, b, c, d) {
        if ((t /= d * 0.5) < 1)
            return -c * 0.5 * (Math.sqrt(1 - t * t) - 1) + b;
        return c * 0.5 * (Math.sqrt(1 - (t -= 2) * t) + 1) + b;
    }
}
exports.AnimationEaseCircular = AnimationEaseCircular;
exports.default = AnimationEaseCircular;
//# sourceMappingURL=circular.js.map