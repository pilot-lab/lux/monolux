export class AnimationEaseCircular {
    static in(t:number, b:number, c:number, d:number):number {
        return -c * (Math.sqrt(1 - (t /= d) * t) - 1) + b;
    }


    static out(t:number, b:number, c:number, d:number):number {
        return c * Math.sqrt(1 - (t = t / d - 1) * t) + b;
    }


    static inOut(t:number, b:number, c:number, d:number):number {
        if ((t /= d * 0.5) < 1) return -c * 0.5 * (Math.sqrt(1 - t * t) - 1) + b;
        return c * 0.5 * (Math.sqrt(1 - (t -= 2) * t) + 1) + b;
    }
} // End class


export default AnimationEaseCircular;
