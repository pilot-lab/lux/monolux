"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class AnimationEaseLinear {
    static none(t, b, c, d) {
        t /= d;
        return b + c * (t);
    }
    static in(t, b, c, d) {
        return c * t / d + b;
    }
    static out(t, b, c, d) {
        return c * t / d + b;
    }
    static inOut(t, b, c, d) {
        return c * t / d + b;
    }
}
exports.AnimationEaseLinear = AnimationEaseLinear;
exports.default = AnimationEaseLinear;
//# sourceMappingURL=linear.js.map