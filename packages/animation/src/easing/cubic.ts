export class AnimationEaseCubic {
    static in(t:number, b:number, c:number, d:number):number {
        let tc:number = (t /= d) * t * t;
        return b + c * (tc);
    }


    static out(t:number, b:number, c:number, d:number):number {
        let ts:number = (t /= d) * t;
        let tc:number = ts * t;
        return b + c * (tc + -3 * ts + 3 * t);
    }


    static inOut(t:number, b:number, c:number, d:number):number {
        let ts:number = (t /= d) * t;
        let tc:number = ts * t;
        return b + c * (4 * tc + -6 * ts + 3 * t);
    }
} // End class


export default AnimationEaseCubic;
