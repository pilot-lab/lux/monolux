export declare class AnimationEaseExponential {
    static in(t: number, b: number, c: number, d: number): number;
    static out(t: number, b: number, c: number, d: number): number;
    static inOut(t: number, b: number, c: number, d: number): number;
}
export default AnimationEaseExponential;
