"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class AnimationEaseCubic {
    static in(t, b, c, d) {
        let tc = (t /= d) * t * t;
        return b + c * (tc);
    }
    static out(t, b, c, d) {
        let ts = (t /= d) * t;
        let tc = ts * t;
        return b + c * (tc + -3 * ts + 3 * t);
    }
    static inOut(t, b, c, d) {
        let ts = (t /= d) * t;
        let tc = ts * t;
        return b + c * (4 * tc + -6 * ts + 3 * t);
    }
}
exports.AnimationEaseCubic = AnimationEaseCubic;
exports.default = AnimationEaseCubic;
//# sourceMappingURL=cubic.js.map