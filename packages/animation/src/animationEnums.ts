export enum SpeedType {
    DURATION,
    UNITS_PER_SECOND,
    DIFFERENCE_FACTOR
} // End enum


export enum AnimationEaseCategory {
    LINEAR,
    QUADRATIC,
    CUBIC,
    QUARTIC,
    QUINTIC,
    SINE,
    EXPONENTIAL,
    CIRCULAR,
    ELASTIC,
    BACK,
    BOUNCE
} // End enum


export enum AnimationEaseType {
    NONE,
    IN,
    OUT,
    INOUT
} // End enum
