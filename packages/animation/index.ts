import AnimationEaseBack from './src/easing/back';
import AnimationEaseBounce from './src/easing/bounce';
import AnimationEaseCircular from './src/easing/circular';
import AnimationEaseCubic from './src/easing/cubic';
import AnimationEaseElastic from './src/easing/elastic';
import AnimationEaseExponential from './src/easing/exponential';
import AnimationEaseLinear from './src/easing/linear';
import AnimationEaseQuadratic from './src/easing/quadratic';
import AnimationEaseQuartic from './src/easing/quartic';
import AnimationEaseQuintic from './src/easing/quintic';
import AnimationEaseSine from './src/easing/sine';
import Animation from './src/animation';
import AnimationEase from './src/animationEase';
import {
    SpeedType,
    AnimationEaseCategory,
    AnimationEaseType
} from './src/animationEnums';
import AnimationEventArgs from './src/animationEventArgs';
import AnimationBatch from './src/animationBatch';
import AnimationManager from './src/animationManager';
import AnimationValue from './src/animationValue';
import AnimationOptions from './src/animationOptions';
import IAnimationBatch from './src/interfaces/iAnimationBatch';
import IAnimation from './src/interfaces/iAnimation';
import IAnimationEaseFunction from './src/interfaces/iAnimationEaseFunction';
import IAnimationEventArgs from './src/interfaces/iAnimationEventArgs';
import IAnimationOptions from './src/interfaces/iAnimationOptions';
import ISpeed from './src/interfaces/iSpeed';
import Speed from './src/speed';


export {
    AnimationEaseBack,
    AnimationEaseBounce,
    AnimationEaseCircular,
    AnimationEaseCubic,
    AnimationEaseElastic,
    AnimationEaseExponential,
    AnimationEaseLinear,
    AnimationEaseQuadratic,
    AnimationEaseQuartic,
    AnimationEaseQuintic,
    AnimationEaseSine,
    IAnimationBatch,
    IAnimation,
    Animation,
    AnimationEase,
    IAnimationEaseFunction,
    IAnimationEventArgs,
    SpeedType,
    AnimationEaseCategory,
    AnimationEaseType,
    AnimationEventArgs,
    AnimationBatch,
    AnimationManager,
    AnimationValue,
    AnimationOptions,
    IAnimationOptions,
    ISpeed,
    Speed
};
