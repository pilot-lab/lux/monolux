import { Signal } from '@pilotlab/signals';
export declare class LogSpinner {
    constructor();
    private _intervalReference;
    private _characters;
    readonly isRunning: boolean;
    private _isRunning;
    ticked: Signal<string>;
    stopped: Signal<string>;
    start(): void;
    stop(): void;
}
export default LogSpinner;
