"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const detector_1 = require("@pilotlab/detector");
class LogFormat {
    constructor() {
        this._codesStart = `\u001B[`;
        this._codes = '';
        this._codesEnd = 'm';
        this._valueEnd = `\u001B[m`;
    }
    value(text) {
        if (detector_1.default.isNodeJs) {
            const codes = this._codes;
            this._codes = '';
            return `${this._codesStart}${codes}${this._codesEnd}${text}${this._valueEnd}`;
        }
        else
            return text;
    }
    _setCode(code) {
        if (this._codes !== '')
            this._codes += ';';
        this._codes += code;
        return this;
    }
    get reset() {
        this._codes = '';
        return this;
    }
    get bold() { return this._setCode('1'); }
    get dim() { return this._setCode('2'); }
    get italic() { return this._setCode('3'); }
    get underline() { return this._setCode('4'); }
    get inverse() { return this._setCode('7'); }
    get hidden() { return this._setCode('8'); }
    get strikethrough() { return this._setCode('9'); }
    get visible() { return this; }
    get black() { return this._setCode('30'); }
    get red() { return this._setCode('31'); }
    get green() { return this._setCode('32'); }
    get yellow() { return this._setCode('33'); }
    get blue() { return this._setCode('34'); }
    get magenta() { return this._setCode('35'); }
    get cyan() { return this._setCode('36'); }
    get white() { return this._setCode('37'); }
    get gray() { return this._setCode('90'); }
    get blackBright() { return this._setCode('90'); }
    get redBright() { return this._setCode('91'); }
    get greenBright() { return this._setCode('92'); }
    get yellowBright() { return this._setCode('93'); }
    get blueBright() { return this._setCode('94'); }
    get magentaBright() { return this._setCode('95'); }
    get cyanBright() { return this._setCode('96'); }
    get whiteBright() { return this._setCode('97'); }
    get orange() { return this._setCode('38;5;202'); }
    get bgBlack() { return this._setCode('40'); }
    get bgRed() { return this._setCode('41'); }
    get bgGreen() { return this._setCode('42'); }
    get bgYellow() { return this._setCode('43'); }
    get bgBlue() { return this._setCode('44'); }
    get bgMagenta() { return this._setCode('45'); }
    get bgCyan() { return this._setCode('46'); }
    get bgWhite() { return this._setCode('47'); }
    get bgBlackBright() { return this._setCode('100'); }
    get bgRedBright() { return this._setCode('101'); }
    get bgGreenBright() { return this._setCode('102'); }
    get bgYellowBright() { return this._setCode('103'); }
    get bgBlueBright() { return this._setCode('104'); }
    get bgMagentaBright() { return this._setCode('105'); }
    get bgCyanBright() { return this._setCode('106'); }
    get bgWhiteBright() { return this._setCode('107'); }
}
exports.LogFormat = LogFormat;
exports.default = LogFormat;
//# sourceMappingURL=logFormat.js.map