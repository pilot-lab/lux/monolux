/// <reference types="node" />
import WritableStream = NodeJS.WritableStream;
import LogCursor from './logCursor';
import { LogRegion } from './logEnums';
export declare class LogConsole {
    constructor(stream?: WritableStream);
    readonly stream: WritableStream;
    private _stream;
    readonly width: number;
    readonly height: number;
    readonly cursor: LogCursor;
    private _cursor;
    write(message: string): LogConsole;
    erase(region: LogRegion): this;
}
export default LogConsole;
