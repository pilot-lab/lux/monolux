"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const signals_1 = require("@pilotlab/signals");
class LogSpinner {
    constructor() {
        this._characters = [];
        this._isRunning = false;
        this.ticked = new signals_1.Signal(false);
        this.stopped = new signals_1.Signal(false);
        this._characters = '▉▊▋▌▍▎▏▎▍▌▋▊▉'.split('');
    }
    get isRunning() { return this._isRunning; }
    start() {
        if (this._isRunning)
            return;
        let current = 0;
        this._intervalReference = setInterval(() => {
            let spinnerText = this._characters[current];
            this.ticked.dispatch(spinnerText);
            current = ++current % this._characters.length;
        }, 100);
        this._isRunning = true;
    }
    ;
    stop() {
        clearInterval(this._intervalReference);
        this._intervalReference = null;
        this._isRunning = false;
        this.stopped.dispatch(' ');
    }
}
exports.LogSpinner = LogSpinner;
exports.default = LogSpinner;
//# sourceMappingURL=logSpinner.js.map