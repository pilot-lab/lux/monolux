"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const strings_1 = require("@pilotlab/strings");
const logEnums_1 = require("./logEnums");
const logger_1 = require("./logger");
const logMessage_1 = require("./logMessage");
class LoggerThread extends logger_1.default {
    constructor() {
        super();
        this.p_SPACE_FILLER_START = '┌';
        this.p_SPACE_FILLER = '─';
        this.p_INDENT_START = '│';
    }
    custom(message, label = '', labelColor = logEnums_1.LogColor.GRAY, tag, category, spinnerCharacter = ' ') {
        if (is_1.default.notEmpty(category, true) && !this.isCategoryOn(category))
            return new logMessage_1.default(this, label, labelColor, message, 0, tag, category);
        if (!message)
            message = '';
        if (!spinnerCharacter || spinnerCharacter === '')
            spinnerCharacter = ' ';
        let error = message instanceof Error ? message : new Error(message.toString());
        let messageString = error ? error.message : '';
        let timeStamp = logger_1.default.time;
        let logString = logger_1.default.format.gray.value(timeStamp);
        const headerLength = timeStamp.length + this.p_SPACE.length + this.p_LABEL_AREA_LENGTH_MAX + (this.p_SPACE.length * 2);
        let lineCount = 1;
        let labelFormatted = '';
        let labelLengthMax = this.p_LABEL_AREA_LENGTH_MAX - this.p_FILL_SPACES_MIN - this.p_SPACE.length;
        if (label && label !== '' && typeof label === 'string') {
            labelFormatted = label;
            if (labelFormatted.length > labelLengthMax) {
                labelFormatted = labelFormatted.slice(0, labelLengthMax - 3);
                while (labelFormatted.length < labelLengthMax)
                    labelFormatted += '.';
            }
        }
        let spaceFiller = this.p_SPACE;
        let labelFormattedLength = labelFormatted.length;
        while (spaceFiller.length < this.p_LABEL_AREA_LENGTH_MAX - labelFormattedLength)
            spaceFiller += this.p_SPACE_FILLER;
        if (labelFormatted && labelFormatted !== '')
            spaceFiller += this.p_SPACE;
        else {
            for (let i = 0; i < this.p_SPACE.length; i++) {
                spaceFiller += this.p_SPACE_FILLER;
            }
        }
        logString += logger_1.default.format.gray.value(spaceFiller);
        switch (labelColor) {
            case logEnums_1.LogColor.BLACK:
                logString += logger_1.default.format.bold.black.value(labelFormatted);
                break;
            case logEnums_1.LogColor.BLACK_BRIGHT:
                logString += logger_1.default.format.bold.blackBright.value(labelFormatted);
                break;
            case logEnums_1.LogColor.BLUE:
                logString += logger_1.default.format.bold.blue.value(labelFormatted);
                break;
            case logEnums_1.LogColor.BLUE_BRIGHT:
                logString += logger_1.default.format.bold.blueBright.value(labelFormatted);
                break;
            case logEnums_1.LogColor.CYAN:
                logString += logger_1.default.format.bold.cyan.value(labelFormatted);
                break;
            case logEnums_1.LogColor.CYAN_BRIGHT:
                logString += logger_1.default.format.bold.cyanBright.value(labelFormatted);
                break;
            case logEnums_1.LogColor.GRAY:
                logString += logger_1.default.format.bold.gray.value(labelFormatted);
                break;
            case logEnums_1.LogColor.GREEN:
                logString += logger_1.default.format.bold.green.value(labelFormatted);
                break;
            case logEnums_1.LogColor.GREEN_BRIGHT:
                logString += logger_1.default.format.bold.greenBright.value(labelFormatted);
                break;
            case logEnums_1.LogColor.MAGENTA:
                logString += logger_1.default.format.bold.magenta.value(labelFormatted);
                break;
            case logEnums_1.LogColor.MAGENTA_BRIGHT:
                logString += logger_1.default.format.bold.magentaBright.value(labelFormatted);
                break;
            case logEnums_1.LogColor.ORANGE:
                logString += logger_1.default.format.bold.orange.value(labelFormatted);
                break;
            case logEnums_1.LogColor.RED:
                logString += logger_1.default.format.bold.red.value(labelFormatted);
                break;
            case logEnums_1.LogColor.RED_BRIGHT:
                logString += logger_1.default.format.bold.redBright.value(labelFormatted);
                break;
            case logEnums_1.LogColor.YELLOW:
                logString += logger_1.default.format.bold.yellow.value(labelFormatted);
                break;
            case logEnums_1.LogColor.YELLOW_BRIGHT:
                logString += logger_1.default.format.bold.yellowBright.value(labelFormatted);
                break;
        }
        let tagFormatted = '';
        if (tag && typeof tag === 'string') {
            tagFormatted = tag;
            if (tagFormatted.length > this.tagLengthMax) {
                tagFormatted = tagFormatted.slice(0, this.tagLengthMax - 3);
                while (tagFormatted.length < this.tagLengthMax)
                    tagFormatted += '.';
            }
            tagFormatted = this.p_SPACE + this.p_SPACE + logger_1.default.format.gray.value(`${tagFormatted}`);
        }
        logString += ' ' + spinnerCharacter;
        if (messageString.length > this.lineLengthMax) {
            let messageLines = [];
            let tabSpace = '';
            while (tabSpace.length < headerLength)
                tabSpace += ' ';
            messageLines = strings_1.Strings.getLines(messageString, this.lineLengthMax);
            lineCount = messageLines.length;
            logString += this.p_SPACE + messageLines[0];
            logString += tagFormatted + '\n';
            for (let i = 1; i < messageLines.length; i++) {
                logString += tabSpace + messageLines[i] + '\n';
            }
        }
        else {
            logString += this.p_SPACE + (messageString && messageString !== '' ? logger_1.default.format.whiteBright.value(messageString) : logger_1.default.format.gray.value('null, undefined, or empty message'));
            logString += tagFormatted;
        }
        console.log(logString);
        if (this.isTrace && error.stack) {
            let traceString = error.stack.toString();
            let traceStringFormatted = '';
            if (traceString.length > this.lineLengthMax) {
                let tabSpace = '';
                while (tabSpace.length < headerLength)
                    tabSpace += ' ';
                let traceLines = strings_1.Strings.getLines(traceString, this.lineLengthMax);
                for (let i = 0; i < traceLines.length; i++) {
                    traceStringFormatted += tabSpace + logger_1.default.format.bgBlackBright.value(logger_1.default.format.white.value(traceLines[i])) + '\n';
                }
            }
            else {
                traceStringFormatted +=
                    this.p_SPACE + this.p_SPACE + (traceString && traceString !== '' ?
                        logger_1.default.format.whiteBright.value(traceString) :
                        logger_1.default.format.gray.value('null, undefined, or empty message'));
            }
            console.log('');
            console.log(traceStringFormatted);
            console.log('');
        }
        let logMessage = new logMessage_1.default(this, label, labelColor, message, lineCount, tag, category, headerLength);
        return logMessage;
    }
}
exports.LoggerThread = LoggerThread;
exports.default = LoggerThread;
//# sourceMappingURL=loggerThread.js.map