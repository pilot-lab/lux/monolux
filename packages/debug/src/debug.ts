import is from '@pilotlab/is';
import Logger from './logger';
import LogMessage from './logMessage';


export class Debug {
    static isProduction:boolean = false;
    static logger:Logger = new Logger();


    static setCategoryMode(category:string, isOn:boolean):void {
        this.logger.setCategoryMode(category, isOn);
    }


    static assert(isConditionMet:boolean, message?:string, tag?:string, category?:string):boolean {
        return this.logger.assert(isConditionMet, message, tag, category);
    }


    static data(data:any, tag?:string, category?:string):LogMessage { return this.logger.data(data, tag, category); }
    static error(message:any, tag?:string, category?:string):LogMessage { return this.logger.error(message, tag, category); }
    static info(message:any, tag?:string, category?:string):LogMessage { return this.logger.info(message, tag, category); }
    static warn(message:any, tag?:string, category?:string):LogMessage {
        if (!this.isProduction) return this.logger.warn(message, tag, category);
        else return new LogMessage(this.logger);
    }
    static log(message:any, tag?:string, category?:string):LogMessage {
        if (!this.isProduction) return this.logger.log(message, tag, category);
        else return new LogMessage(this.logger);
    }
    static test(message:any, tag?:string, category?:string):LogMessage {
        if (!this.isProduction) return this.logger.test(message, tag, category);
        else return new LogMessage(this.logger);
    }


    /**
     * Save the provided JSON object or string to a file in the downloads folder.
     */
    static save(data:any, fileName:string = 'console.json'):void {
        if (!data) return;

        if (typeof data === 'object') data = JSON.stringify(data, undefined, 4);

        let blob:Blob = new Blob([data], { type: 'text/json' });
        let mouseEvent:MouseEvent = document.createEvent('MouseEvents');
        let link:HTMLAnchorElement = document.createElement('a');

        link.setAttribute('download', fileName);
        link.href = window.URL.createObjectURL(blob);
        link.dataset['downloadurl'] =  ['text/json', fileName, link.href].join(':');

        mouseEvent.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);

        link.dispatchEvent(mouseEvent);
    }


    /**
     * Copy the provided data to the clipboard.
     */
    static copy(data:any, category?:string):void {
        if (!data) return;

        if (typeof data === 'object') data = JSON.stringify(data, undefined, 4);

        let listenerCopy = (e:ClipboardEvent):void => {
            if (is.empty(window)) return;
            let win:any = window; // To avoid TypeScript transpiler error
            const isIE:boolean = (
                is.notEmpty(navigator.userAgent.match(/msie/i))
                || is.notEmpty(navigator.userAgent.match(/trident/i))
            );
            if (isIE) win['clipboardData'].setData('text/plain', data);
            else e.clipboardData.setData('text/plain', data);
            e.preventDefault();
        };

        document.addEventListener('copy', listenerCopy);
        document.execCommand('copy');
        document.removeEventListener('copy', listenerCopy);

        console.log('%ccopied', 'color: blue');
        console.log(data);
    }


    /**
     * Given an object instance, return the associated class key.
     */
    static getClassName(instance:any):string {
        let results = (instance).constructor.name;
        return (results && results.length > 1) ? results : '';
    }
} // End class


export default Debug;
