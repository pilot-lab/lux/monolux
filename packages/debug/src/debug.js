"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const logger_1 = require("./logger");
const logMessage_1 = require("./logMessage");
class Debug {
    static setCategoryMode(category, isOn) {
        this.logger.setCategoryMode(category, isOn);
    }
    static assert(isConditionMet, message, tag, category) {
        return this.logger.assert(isConditionMet, message, tag, category);
    }
    static data(data, tag, category) { return this.logger.data(data, tag, category); }
    static error(message, tag, category) { return this.logger.error(message, tag, category); }
    static info(message, tag, category) { return this.logger.info(message, tag, category); }
    static warn(message, tag, category) {
        if (!this.isProduction)
            return this.logger.warn(message, tag, category);
        else
            return new logMessage_1.default(this.logger);
    }
    static log(message, tag, category) {
        if (!this.isProduction)
            return this.logger.log(message, tag, category);
        else
            return new logMessage_1.default(this.logger);
    }
    static test(message, tag, category) {
        if (!this.isProduction)
            return this.logger.test(message, tag, category);
        else
            return new logMessage_1.default(this.logger);
    }
    static save(data, fileName = 'console.json') {
        if (!data)
            return;
        if (typeof data === 'object')
            data = JSON.stringify(data, undefined, 4);
        let blob = new Blob([data], { type: 'text/json' });
        let mouseEvent = document.createEvent('MouseEvents');
        let link = document.createElement('a');
        link.setAttribute('download', fileName);
        link.href = window.URL.createObjectURL(blob);
        link.dataset['downloadurl'] = ['text/json', fileName, link.href].join(':');
        mouseEvent.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
        link.dispatchEvent(mouseEvent);
    }
    static copy(data, category) {
        if (!data)
            return;
        if (typeof data === 'object')
            data = JSON.stringify(data, undefined, 4);
        let listenerCopy = (e) => {
            if (is_1.default.empty(window))
                return;
            let win = window;
            const isIE = (is_1.default.notEmpty(navigator.userAgent.match(/msie/i))
                || is_1.default.notEmpty(navigator.userAgent.match(/trident/i)));
            if (isIE)
                win['clipboardData'].setData('text/plain', data);
            else
                e.clipboardData.setData('text/plain', data);
            e.preventDefault();
        };
        document.addEventListener('copy', listenerCopy);
        document.execCommand('copy');
        document.removeEventListener('copy', listenerCopy);
        console.log('%ccopied', 'color: blue');
        console.log(data);
    }
    static getClassName(instance) {
        let results = (instance).constructor.name;
        return (results && results.length > 1) ? results : '';
    }
}
Debug.isProduction = false;
Debug.logger = new logger_1.default();
exports.Debug = Debug;
exports.default = Debug;
//# sourceMappingURL=debug.js.map