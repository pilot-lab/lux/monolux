import is from '@pilotlab/is';
import { LogColor, LogType } from './logEnums';
import Logger from './logger';
import LogSpinner from './logSpinner';


export class LogMessage {
    constructor(
        logger?:Logger,
        label?:string,
        labelColor?:LogColor,
        message?:string,
        lineCount?:number,
        tag?:string,
        category?:string,
        headerLength?:number
    ) {
        if (
            is.notEmpty(logger) &&
            is.notEmpty(label) &&
            is.notEmpty(labelColor) &&
            is.notEmpty(lineCount)
        ) this._isEmpty = false;

        this._logger = logger;
        this._label = label;
        this._labelColor = labelColor;
        this._message = message;
        this._lineCount = lineCount;
        this._tag = tag;
        this._category = category;
        this._headerLength = headerLength;

        switch (label) {
            case LogType[LogType.LOG]:
                this._type = LogType.INFO;
                break;
            case LogType[LogType.TEST]:
                this._type = LogType.TEST;
                break;
            case LogType[LogType.INFO]:
                this._type = LogType.INFO;
                break;
            case LogType[LogType.WARNING]:
                this._type = LogType.WARNING;
                break;
            case LogType[LogType.ERROR]:
                this._type = LogType.ERROR;
                break;
            case LogType[LogType.ASSERT]:
                this._type = LogType.ASSERT;
                break;
            case LogType[LogType.DATA]:
                this._type = LogType.DATA;
                break;
            default:
                this._type = LogType.CUSTOM;
        }

        this._spinner.ticked.listen(this._spinner_ticked, this);
        this._spinner.stopped.listen(this._spinner_stopped, this);
    }


    get isEmpty():boolean { return this._isEmpty; }
    private _isEmpty:boolean = true;


    get logger():Logger { return this._logger; }
    private _logger:Logger;


    get type():LogType { return this._type; }
    private _type:LogType;


    get label():string { return this._label; }
    private _label:string;


    get labelColor():LogColor { return this._labelColor; }
    private _labelColor:LogColor;


    get message():string { return this._message; }
    private _message:string;


    get tag():string { return this._tag; }
    private _tag:string;


    get category():string { return this._category; }
    private _category:string;


    get lineCount():number { return this._lineCount; }
    private _lineCount:number;


    get headerLength():number { return this._headerLength; }
    private _headerLength:number;


    private _spinner:LogSpinner = new LogSpinner();


    startSpinner() {
        if (is.notEmpty(this.category, true) && !this.logger.isCategoryOn(this.category)) {
            return;
        }

        this.logger.console.cursor.hide();
        this.logger.console.cursor.up(this.lineCount + 1);
        this.logger.console.cursor.setColumn(this.headerLength - 2);
        this._spinner.start();
    }

    stopSpinner() {
        if (is.notEmpty(this.category, true) && !this.logger.isCategoryOn(this.category)) {
            return;
        }

        this._spinner.stop();
    }


    private _spinner_ticked(spinnerCharacter:string):void {
        process.stdout.write(Logger.setColor(spinnerCharacter, this.labelColor));
        this.logger.console.cursor.left(1);
    }


    private _spinner_stopped(spinnerCharacter:string):void {
        process.stdout.write(' ');
        this.logger.console.cursor.down(this.lineCount + 1);
        this.logger.console.cursor.show();
    }
} // End class


export default LogMessage;
