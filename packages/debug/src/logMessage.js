"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const logEnums_1 = require("./logEnums");
const logger_1 = require("./logger");
const logSpinner_1 = require("./logSpinner");
class LogMessage {
    constructor(logger, label, labelColor, message, lineCount, tag, category, headerLength) {
        this._isEmpty = true;
        this._spinner = new logSpinner_1.default();
        if (is_1.default.notEmpty(logger) &&
            is_1.default.notEmpty(label) &&
            is_1.default.notEmpty(labelColor) &&
            is_1.default.notEmpty(lineCount))
            this._isEmpty = false;
        this._logger = logger;
        this._label = label;
        this._labelColor = labelColor;
        this._message = message;
        this._lineCount = lineCount;
        this._tag = tag;
        this._category = category;
        this._headerLength = headerLength;
        switch (label) {
            case logEnums_1.LogType[logEnums_1.LogType.LOG]:
                this._type = logEnums_1.LogType.INFO;
                break;
            case logEnums_1.LogType[logEnums_1.LogType.TEST]:
                this._type = logEnums_1.LogType.TEST;
                break;
            case logEnums_1.LogType[logEnums_1.LogType.INFO]:
                this._type = logEnums_1.LogType.INFO;
                break;
            case logEnums_1.LogType[logEnums_1.LogType.WARNING]:
                this._type = logEnums_1.LogType.WARNING;
                break;
            case logEnums_1.LogType[logEnums_1.LogType.ERROR]:
                this._type = logEnums_1.LogType.ERROR;
                break;
            case logEnums_1.LogType[logEnums_1.LogType.ASSERT]:
                this._type = logEnums_1.LogType.ASSERT;
                break;
            case logEnums_1.LogType[logEnums_1.LogType.DATA]:
                this._type = logEnums_1.LogType.DATA;
                break;
            default:
                this._type = logEnums_1.LogType.CUSTOM;
        }
        this._spinner.ticked.listen(this._spinner_ticked, this);
        this._spinner.stopped.listen(this._spinner_stopped, this);
    }
    get isEmpty() { return this._isEmpty; }
    get logger() { return this._logger; }
    get type() { return this._type; }
    get label() { return this._label; }
    get labelColor() { return this._labelColor; }
    get message() { return this._message; }
    get tag() { return this._tag; }
    get category() { return this._category; }
    get lineCount() { return this._lineCount; }
    get headerLength() { return this._headerLength; }
    startSpinner() {
        if (is_1.default.notEmpty(this.category, true) && !this.logger.isCategoryOn(this.category)) {
            return;
        }
        this.logger.console.cursor.hide();
        this.logger.console.cursor.up(this.lineCount + 1);
        this.logger.console.cursor.setColumn(this.headerLength - 2);
        this._spinner.start();
    }
    stopSpinner() {
        if (is_1.default.notEmpty(this.category, true) && !this.logger.isCategoryOn(this.category)) {
            return;
        }
        this._spinner.stop();
    }
    _spinner_ticked(spinnerCharacter) {
        process.stdout.write(logger_1.default.setColor(spinnerCharacter, this.labelColor));
        this.logger.console.cursor.left(1);
    }
    _spinner_stopped(spinnerCharacter) {
        process.stdout.write(' ');
        this.logger.console.cursor.down(this.lineCount + 1);
        this.logger.console.cursor.show();
    }
}
exports.LogMessage = LogMessage;
exports.default = LogMessage;
//# sourceMappingURL=logMessage.js.map