import { Signal } from '@pilotlab/signals';


export class LogSpinner {
    constructor() {
        this._characters = '▉▊▋▌▍▎▏▎▍▌▋▊▉'.split('');
    }


    private _intervalReference:any;
    private _characters:string[] = [];


    get isRunning():boolean { return this._isRunning; }
    private _isRunning:boolean = false;


    ticked:Signal<string> = new Signal<string>(false);
    stopped:Signal<string> = new Signal<string>(false);


    start():void {
        if (this._isRunning) return;

        let current = 0;

        this._intervalReference = setInterval(() => {
            let spinnerText = this._characters[current];
            this.ticked.dispatch(spinnerText);
            current = ++current % this._characters.length;
        }, 100);

        this._isRunning = true;
    };


    stop():void {
        clearInterval(this._intervalReference);
        this._intervalReference = null;
        this._isRunning = false;
        this.stopped.dispatch(' ');
    }
} // End class


export default LogSpinner;
