import is from '@pilotlab/is';
import { Strings } from '@pilotlab/strings';
import { LogColor } from './logEnums';
import Logger from './logger';
import LogMessage from './logMessage';


export class LoggerThread extends Logger {
    constructor() {
        super();
        this.p_SPACE_FILLER_START = '┌';
        this.p_SPACE_FILLER = '─';
        this.p_INDENT_START = '│';
    }


    custom(message:any, label:string = '', labelColor:LogColor = LogColor.GRAY, tag?:string, category?:string, spinnerCharacter:string = ' '):LogMessage {
        if (is.notEmpty(category, true) && !this.isCategoryOn(category)) return new LogMessage(this, label, labelColor, message, 0, tag, category);
        if (!message) message = '';

        if (!spinnerCharacter || spinnerCharacter === '') spinnerCharacter = ' ';

        let error:any = message instanceof Error ? message : new Error(message.toString());
        let messageString:string = error ? error.message : '';
        let timeStamp:string = Logger.time;
        let logString:string = Logger.format.gray.value(timeStamp);
        const headerLength:number = timeStamp.length + this.p_SPACE.length + this.p_LABEL_AREA_LENGTH_MAX + (this.p_SPACE.length * 2);
        let lineCount:number = 1;

        /// Format label
        let labelFormatted:string = '';
        let labelLengthMax:number = this.p_LABEL_AREA_LENGTH_MAX - this.p_FILL_SPACES_MIN - this.p_SPACE.length;
        if (label && label !== '' && typeof label === 'string') {
            labelFormatted = label;
            if (labelFormatted.length > labelLengthMax) {
                labelFormatted = labelFormatted.slice(0, labelLengthMax - 3);
                while (labelFormatted.length < labelLengthMax) labelFormatted += '.';
            }
        }

        /// Format filler
        let spaceFiller:string = this.p_SPACE;
        let labelFormattedLength:number = labelFormatted.length;
        while (spaceFiller.length < this.p_LABEL_AREA_LENGTH_MAX - labelFormattedLength) spaceFiller += this.p_SPACE_FILLER;
        if (labelFormatted && labelFormatted !== '') spaceFiller += this.p_SPACE;
        else {
            for (let i =  0; i < this.p_SPACE.length; i++) {
                spaceFiller += this.p_SPACE_FILLER;
            }
        }
        logString += Logger.format.gray.value(spaceFiller);

        switch(labelColor) {
            case LogColor.BLACK:
                logString += Logger.format.bold.black.value(labelFormatted);
                break;
            case LogColor.BLACK_BRIGHT:
                logString += Logger.format.bold.blackBright.value(labelFormatted);
                break;
            case LogColor.BLUE:
                logString += Logger.format.bold.blue.value(labelFormatted);
                break;
            case LogColor.BLUE_BRIGHT:
                logString += Logger.format.bold.blueBright.value(labelFormatted);
                break;
            case LogColor.CYAN:
                logString += Logger.format.bold.cyan.value(labelFormatted);
                break;
            case LogColor.CYAN_BRIGHT:
                logString += Logger.format.bold.cyanBright.value(labelFormatted);
                break;
            case LogColor.GRAY:
                logString += Logger.format.bold.gray.value(labelFormatted);
                break;
            case LogColor.GREEN:
                logString += Logger.format.bold.green.value(labelFormatted);
                break;
            case LogColor.GREEN_BRIGHT:
                logString += Logger.format.bold.greenBright.value(labelFormatted);
                break;
            case LogColor.MAGENTA:
                logString += Logger.format.bold.magenta.value(labelFormatted);
                break;
            case LogColor.MAGENTA_BRIGHT:
                logString += Logger.format.bold.magentaBright.value(labelFormatted);
                break;
            case LogColor.ORANGE:
                logString += Logger.format.bold.orange.value(labelFormatted);
                break;
            case LogColor.RED:
                logString += Logger.format.bold.red.value(labelFormatted);
                break;
            case LogColor.RED_BRIGHT:
                logString += Logger.format.bold.redBright.value(labelFormatted);
                break;
            case LogColor.YELLOW:
                logString += Logger.format.bold.yellow.value(labelFormatted);
                break;
            case LogColor.YELLOW_BRIGHT:
                logString += Logger.format.bold.yellowBright.value(labelFormatted);
                break;
        }

        /// Format tag
        let tagFormatted:string = '';
        if (tag && typeof tag === 'string') {
            tagFormatted = tag;
            if (tagFormatted.length > this.tagLengthMax) {
                tagFormatted = tagFormatted.slice(0, this.tagLengthMax - 3);
                while (tagFormatted.length < this.tagLengthMax) tagFormatted += '.';
            }
            tagFormatted = this.p_SPACE + this.p_SPACE + Logger.format.gray.value(`${tagFormatted}`);
        }

        logString += ' ' + spinnerCharacter;

        /// Format message
        if (messageString.length > this.lineLengthMax) {
            let messageLines:string[] = [];
            let tabSpace:string = '';

            while (tabSpace.length < headerLength) tabSpace += ' ';

            messageLines = Strings.getLines(messageString, this.lineLengthMax);
            lineCount = messageLines.length;

            logString += this.p_SPACE + messageLines[0];
            logString += tagFormatted + '\n';

            for (let i = 1; i < messageLines.length; i++) {
                logString += tabSpace + messageLines[i] + '\n';
            }
        } else {
            logString += this.p_SPACE + (messageString && messageString !== '' ? Logger.format.whiteBright.value(messageString) : Logger.format.gray.value('null, undefined, or empty message'));
            logString += tagFormatted;
        }

        console.log(logString);

        /// Format trace info
        if (this.isTrace && error.stack) {
            let traceString:string = error.stack.toString();
            let traceStringFormatted:string = '';
            if (traceString.length > this.lineLengthMax) {
                let tabSpace:string = '';
                while (tabSpace.length < headerLength) tabSpace += ' ';
                let traceLines:string[] = Strings.getLines(traceString, this.lineLengthMax);

                for (let i = 0; i < traceLines.length; i++) {
                    traceStringFormatted += tabSpace + Logger.format.bgBlackBright.value(Logger.format.white.value(traceLines[i])) + '\n';
                }
            } else {
                traceStringFormatted +=
                    this.p_SPACE + this.p_SPACE + (traceString && traceString !== '' ?
                    Logger.format.whiteBright.value(traceString) :
                    Logger.format.gray.value('null, undefined, or empty message'));
            }

            console.log('');
            console.log(traceStringFormatted);
            console.log('');
        }

        let logMessage:LogMessage = new LogMessage(this, label, labelColor, message, lineCount, tag, category, headerLength);
        return logMessage;
    }
} // End class


export default LoggerThread;
