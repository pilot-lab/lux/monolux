import LogConsole from './logConsole';


export class LogCursor {
    constructor(console:LogConsole) {
        this._console = console;
    }


    private _console:LogConsole;


    setLine(value:number):LogCursor { return this.setPosition(undefined, value); }
    setColumn(value:number):LogCursor { return this.setPosition(value, undefined); }
    setPosition(x:number, y:number):LogCursor {
        const stream:any = this._console.stream;
        if (stream === null || stream === undefined) return this;
        if (typeof x !== 'number' && typeof y !== 'number') return this;

        const escape:string = '\x1b';
        function CSI(strings:any, ...args:any[]) {
            let ret = `${escape}[`;
            for (let n = 0; n < strings.length; n++) {
                ret += strings[n];
                if (n < args.length)
                    ret += args[n];
            }

            return ret;
        }

        if (typeof y !== 'number') stream.write(CSI`${x + 1}G`);
        else stream.write(CSI`${y + 1};${x + 1}H`);

        return this;
    }


    move(x:number, y:number):LogCursor {
        const stream:any = this._console.stream;
        if (stream === null || stream === undefined) return this;

        const escape:string = '\x1b';
        function CSI(strings:any, ...args:any[]) {
            let ret = `${escape}[`;
            for (let n = 0; n < strings.length; n++) {
                ret += strings[n];
                if (n < args.length)
                    ret += args[n];
            }

            return ret;
        }

        if (x < 0) stream.write(CSI`${-x}D`);
        else if (x > 0) stream.write(CSI`${x}C`);

        if (y < 0) stream.write(CSI`${-y}A`);
        else if (y > 0) stream.write(CSI`${y}B`);

        return this;
    }


    up(value:number = 1):LogCursor { return this.move(0, -Math.abs(value)); }
    down(value:number = 1):LogCursor { return this.move(0, Math.abs(value)); }
    left(value:number = 1):LogCursor { return this.move(-Math.abs(value), 0); }
    right(value:number = 1):LogCursor { return this.move(Math.abs(value), 0); }


    hide() {
        this._console.stream.write(this._encode('[?25l')); // From Charm
        return this;
    }


    show() {
        this._console.stream.write(this._encode('[?25h')); // From Charm
        return this;
    }

    /// From Charm
    private _encode(value:string):Buffer {
        return new Buffer([0x1b].concat(value.split('').map((value:string) => value.charCodeAt(0))));
    };
} // End class


export default LogCursor;
