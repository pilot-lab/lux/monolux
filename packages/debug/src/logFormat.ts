import Detector from '@pilotlab/detector';


export class LogFormat {
    constructor() {
        this._codesStart = `\u001B[`;
        this._codes = '';
        this._codesEnd = 'm';
        this._valueEnd = `\u001B[m`;
    }


    private _codesStart:string;
    private _codes:string;
    private _codesEnd:string;
    private _valueEnd:string;


    value(text:string):string {
        if (Detector.isNodeJs) {
            const codes: string = this._codes;
            this._codes = '';
            return `${this._codesStart}${codes}${this._codesEnd}${text}${this._valueEnd}`;
        } else return text;
    }


    private _setCode(code:string):LogFormat {
        if (this._codes !== '') this._codes += ';';
        this._codes += code;
        return this;
    }


    get reset():LogFormat {
        this._codes = '';
        return this;
    }
    get bold():LogFormat { return this._setCode('1'); }
    get dim():LogFormat { return this._setCode('2'); }
    get italic():LogFormat { return this._setCode('3'); }
    get underline():LogFormat { return this._setCode('4'); }
    get inverse():LogFormat { return this._setCode('7'); }
    get hidden():LogFormat { return this._setCode('8'); }
    get strikethrough():LogFormat { return this._setCode('9'); }


    get visible():LogFormat { return this; }


    get black():LogFormat { return this._setCode('30'); }
    get red():LogFormat { return this._setCode('31'); }
    get green():LogFormat { return this._setCode('32');}
    get yellow():LogFormat { return this._setCode('33'); }
    get blue():LogFormat { return this._setCode('34'); }
    get magenta():LogFormat { return this._setCode('35'); }
    get cyan():LogFormat { return this._setCode('36'); }
    get white():LogFormat { return this._setCode('37'); }
    get gray():LogFormat { return this._setCode('90'); }
    get blackBright():LogFormat { return this._setCode('90'); }
    get redBright():LogFormat { return this._setCode('91'); }
    get greenBright():LogFormat { return this._setCode('92'); }
    get yellowBright():LogFormat { return this._setCode('93'); }
    get blueBright():LogFormat { return this._setCode('94'); }
    get magentaBright():LogFormat { return this._setCode('95'); }
    get cyanBright():LogFormat { return this._setCode('96'); }
    get whiteBright():LogFormat { return this._setCode('97'); }
    get orange():LogFormat { return this._setCode('38;5;202'); }


    get bgBlack():LogFormat { return this._setCode('40'); }
    get bgRed():LogFormat { return this._setCode('41'); }
    get bgGreen():LogFormat { return this._setCode('42'); }
    get bgYellow():LogFormat { return this._setCode('43'); }
    get bgBlue():LogFormat { return this._setCode('44'); }
    get bgMagenta():LogFormat { return this._setCode('45'); }
    get bgCyan():LogFormat { return this._setCode('46'); }
    get bgWhite():LogFormat { return this._setCode('47'); }
    get bgBlackBright():LogFormat { return this._setCode('100'); }
    get bgRedBright():LogFormat { return this._setCode('101'); }
    get bgGreenBright():LogFormat { return this._setCode('102'); }
    get bgYellowBright():LogFormat { return this._setCode('103'); }
    get bgBlueBright():LogFormat { return this._setCode('104'); }
    get bgMagentaBright():LogFormat { return this._setCode('105'); }
    get bgCyanBright():LogFormat { return this._setCode('106'); }
    get bgWhiteBright():LogFormat { return this._setCode('107'); }
}


export default LogFormat;
