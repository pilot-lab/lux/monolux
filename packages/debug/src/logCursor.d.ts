import LogConsole from './logConsole';
export declare class LogCursor {
    constructor(console: LogConsole);
    private _console;
    setLine(value: number): LogCursor;
    setColumn(value: number): LogCursor;
    setPosition(x: number, y: number): LogCursor;
    move(x: number, y: number): LogCursor;
    up(value?: number): LogCursor;
    down(value?: number): LogCursor;
    left(value?: number): LogCursor;
    right(value?: number): LogCursor;
    hide(): this;
    show(): this;
    private _encode(value);
}
export default LogCursor;
