import Logger from './logger';
import LogMessage from './logMessage';
export declare class Debug {
    static isProduction: boolean;
    static logger: Logger;
    static setCategoryMode(category: string, isOn: boolean): void;
    static assert(isConditionMet: boolean, message?: string, tag?: string, category?: string): boolean;
    static data(data: any, tag?: string, category?: string): LogMessage;
    static error(message: any, tag?: string, category?: string): LogMessage;
    static info(message: any, tag?: string, category?: string): LogMessage;
    static warn(message: any, tag?: string, category?: string): LogMessage;
    static log(message: any, tag?: string, category?: string): LogMessage;
    static test(message: any, tag?: string, category?: string): LogMessage;
    static save(data: any, fileName?: string): void;
    static copy(data: any, category?: string): void;
    static getClassName(instance: any): string;
}
export default Debug;
