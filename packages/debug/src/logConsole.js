"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const logCursor_1 = require("./logCursor");
const logEnums_1 = require("./logEnums");
class LogConsole {
    constructor(stream = process.stdout) {
        this._stream = stream;
        this._cursor = new logCursor_1.default(this);
    }
    get stream() { return this._stream; }
    get width() {
        let out = process.stdout;
        if (out.getWindowSize)
            return out.getWindowSize()[0];
        else if (out.columns && out.rows)
            out.columns;
        else
            return 0;
    }
    get height() {
        let out = process.stdout;
        if (out.getWindowSize)
            return out.getWindowSize()[1];
        else if (out.columns && out.rows)
            return out.rows;
        else
            return 0;
    }
    get cursor() { return this._cursor; }
    write(message) { this._stream.write(message); return this; }
    erase(region) {
        switch (region) {
            case logEnums_1.LogRegion.SCREEN:
            default:
                this._stream.write(process.platform === 'win32' ? '\x1Bc' : '\x1B[2J\x1B[3J\x1B[H');
                break;
        }
        return this;
    }
}
exports.LogConsole = LogConsole;
exports.default = LogConsole;
//# sourceMappingURL=logConsole.js.map