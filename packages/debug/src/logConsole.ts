import WritableStream = NodeJS.WritableStream;
import LogCursor from './logCursor';
import { LogRegion } from './logEnums'


export class LogConsole {
    constructor(stream:WritableStream = process.stdout) {
        this._stream = stream;
        this._cursor = new LogCursor(this);
    }


    get stream():WritableStream { return this._stream; }
    private _stream:WritableStream;


    get width():number {
        let out:any = process.stdout;
        if (out.getWindowSize) return  out.getWindowSize()[0];
        else if (out.columns && out.rows) out.columns;
        else return 0;
    }


    get height():number {
        let out:any = process.stdout;
        if (out.getWindowSize) return out.getWindowSize()[1];
        else if (out.columns && out.rows) return out.rows;
        else return 0;
    }


    get cursor():LogCursor { return this._cursor; }
    private _cursor:LogCursor;


    write(message:string):LogConsole { this._stream.write(message); return this; }


    erase(region:LogRegion) {
       switch(region) {
            case LogRegion.SCREEN:
            default:
                this._stream.write(process.platform === 'win32' ? '\x1Bc' : '\x1B[2J\x1B[3J\x1B[H');
                break;
        }

        return this;
    }
} // End class


export default LogConsole;
