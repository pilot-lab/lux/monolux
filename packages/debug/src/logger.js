"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const detector_1 = require("@pilotlab/detector");
const is_1 = require("@pilotlab/is");
const strings_1 = require("@pilotlab/strings");
const collections_1 = require("@pilotlab/collections");
const logFormat_1 = require("./logFormat");
const debug_1 = require("./debug");
const logEnums_1 = require("./logEnums");
const logMessage_1 = require("./logMessage");
const logConsole_1 = require("./logConsole");
class Logger {
    constructor(lineLengthMaxNode = 120, lineLengthMaxBrowser = 30, tagLengthMax = detector_1.default.isNodeJs ? 60 : 12, timeStampLength = 8, spaceText = '  ', labelAreaLengthMax = detector_1.default.isNodeJs ? 20 : 12, fillSpacesMin = 3, spaceFillerStartText = '', spaceFillerText = '─', indentStartText = '') {
        this.isTrace = false;
        this.p_categories = new collections_1.MapList();
        this.p_console = new logConsole_1.default();
        this.p_LINE_LENGTH_MAX_NODE = lineLengthMaxNode;
        this.p_LINE_LENGTH_MAX_BROWSER = lineLengthMaxBrowser;
        this.p_TAG_LENGTH_MAX = tagLengthMax;
        this.p_TIME_STAMP_LENGTH = timeStampLength;
        this.p_SPACE = spaceText;
        this.p_LABEL_AREA_LENGTH_MAX = labelAreaLengthMax;
        this.p_FILL_SPACES_MIN = fillSpacesMin;
        this.p_SPACE_FILLER_START = spaceFillerStartText;
        this.p_SPACE_FILLER = spaceFillerText;
        this.p_INDENT_START = indentStartText;
        this.p_HEADER_LENGTH = this.p_TIME_STAMP_LENGTH + this.p_SPACE.length + this.p_LABEL_AREA_LENGTH_MAX + (this.p_SPACE.length * 2);
    }
    get tagLengthMax() { return this.p_TAG_LENGTH_MAX; }
    get lineLengthMax() { return detector_1.default.isNodeJs ? this.p_LINE_LENGTH_MAX_NODE : this.p_LINE_LENGTH_MAX_BROWSER; }
    static get format() {
        if (is_1.default.empty(this._format))
            this._format = new logFormat_1.default();
        return this._format;
    }
    static get time() {
        const today = new Date();
        const hour = today.getHours();
        const minute = today.getMinutes();
        const second = today.getSeconds();
        return `${strings_1.Strings.padDigits(hour, 2)}:${strings_1.Strings.padDigits(minute, 2)}:${strings_1.Strings.padDigits(second, 2)}`;
    }
    static setColor(text, color) {
        switch (color) {
            case logEnums_1.LogColor.BLACK:
                text = Logger.format.black.value(text);
                break;
            case logEnums_1.LogColor.BLACK_BRIGHT:
                text = Logger.format.blackBright.value(text);
                break;
            case logEnums_1.LogColor.BLUE:
                text = Logger.format.blue.value(text);
                break;
            case logEnums_1.LogColor.BLUE_BRIGHT:
                text = Logger.format.blueBright.value(text);
                break;
            case logEnums_1.LogColor.CYAN:
                text = Logger.format.cyan.value(text);
                break;
            case logEnums_1.LogColor.CYAN_BRIGHT:
                text = Logger.format.cyanBright.value(text);
                break;
            case logEnums_1.LogColor.GRAY:
                text = Logger.format.gray.value(text);
                break;
            case logEnums_1.LogColor.GREEN:
                text = Logger.format.green.value(text);
                break;
            case logEnums_1.LogColor.GREEN_BRIGHT:
                text = Logger.format.greenBright.value(text);
                break;
            case logEnums_1.LogColor.MAGENTA:
                text = Logger.format.magenta.value(text);
                break;
            case logEnums_1.LogColor.MAGENTA_BRIGHT:
                text = Logger.format.magentaBright.value(text);
                break;
            case logEnums_1.LogColor.ORANGE:
                text = Logger.format.orange.value(text);
                break;
            case logEnums_1.LogColor.RED:
                text = Logger.format.red.value(text);
                break;
            case logEnums_1.LogColor.RED_BRIGHT:
                text = Logger.format.redBright.value(text);
                break;
            case logEnums_1.LogColor.YELLOW:
                text = Logger.format.yellow.value(text);
                break;
            case logEnums_1.LogColor.YELLOW_BRIGHT:
                text = Logger.format.yellowBright.value(text);
                break;
        }
        return text;
    }
    get console() { return this.p_console; }
    setCategoryMode(category, isOn) {
        this.p_categories.set(category, isOn);
    }
    isCategoryOn(category) {
        if (is_1.default.empty(category, true))
            return true;
        return this.p_categories.get(category);
    }
    assert(isConditionMet, message, tag, category) {
        if (!isConditionMet)
            this.logType(logEnums_1.LogType.ASSERT, message, tag, category);
        return isConditionMet;
    }
    data(message, tag, category) { return this.logType(logEnums_1.LogType.DATA, message, category); }
    error(message, tag, category) { return this.logType(logEnums_1.LogType.ERROR, message, tag, category); }
    info(message, tag, category) { return this.logType(logEnums_1.LogType.INFO, message, tag, category); }
    warn(message, tag, category) { return this.logType(logEnums_1.LogType.WARNING, message, tag, category); }
    log(message, tag, category) { return this.logType(logEnums_1.LogType.LOG, message, tag, category); }
    test(message, tag, category) { return this.logType(logEnums_1.LogType.TEST, message, tag, category); }
    logType(type, message, tag, category) {
        switch (type) {
            case logEnums_1.LogType.ASSERT:
                return this.custom(message, logEnums_1.LogType[logEnums_1.LogType.ASSERT], logEnums_1.LogColor.ORANGE, tag, category);
            case logEnums_1.LogType.DATA:
                return this.custom(JSON.stringify(message), logEnums_1.LogType[logEnums_1.LogType.DATA], logEnums_1.LogColor.BLUE_BRIGHT, debug_1.default.getClassName(message), category);
            case logEnums_1.LogType.ERROR:
                return this.custom(message, logEnums_1.LogType[logEnums_1.LogType.ERROR], logEnums_1.LogColor.RED_BRIGHT, tag, category);
            case logEnums_1.LogType.INFO:
                return this.custom(message, logEnums_1.LogType[logEnums_1.LogType.INFO], logEnums_1.LogColor.CYAN_BRIGHT, tag, category);
            case logEnums_1.LogType.WARNING:
                return this.custom(message, logEnums_1.LogType[logEnums_1.LogType.WARNING], logEnums_1.LogColor.YELLOW_BRIGHT, tag, category);
            case logEnums_1.LogType.LOG:
                return this.custom(message, logEnums_1.LogType[logEnums_1.LogType.LOG], logEnums_1.LogColor.GREEN_BRIGHT, tag, category);
            case logEnums_1.LogType.TEST:
                return this.custom(message, logEnums_1.LogType[logEnums_1.LogType.TEST], logEnums_1.LogColor.MAGENTA_BRIGHT, tag, category);
        }
    }
    custom(message, label = '', labelColor = logEnums_1.LogColor.GRAY, tag, category, spinnerCharacter = ' ') {
        if (is_1.default.notEmpty(category, true) && !this.isCategoryOn(category))
            return new logMessage_1.default(this, label, labelColor, message, 0, tag, category);
        if (!message)
            message = '';
        if (!spinnerCharacter || spinnerCharacter === '')
            spinnerCharacter = ' ';
        let error = message instanceof Error ? message : new Error(message.toString());
        let messageString = error ? error.message : '';
        let timeStamp = Logger.time;
        let logString = Logger.format.gray.value(timeStamp);
        let lineCount = 1;
        let labelFormatted = '';
        let labelLengthMax = this.p_LABEL_AREA_LENGTH_MAX - this.p_FILL_SPACES_MIN - this.p_SPACE.length;
        if (label && label !== '' && typeof label === 'string') {
            labelFormatted = label;
            if (labelFormatted.length > labelLengthMax) {
                labelFormatted = labelFormatted.slice(0, labelLengthMax - 3);
                while (labelFormatted.length < labelLengthMax)
                    labelFormatted += '.';
            }
        }
        let spaceFiller = this.p_SPACE + this.p_SPACE_FILLER_START;
        let labelFormattedLength = labelFormatted.length;
        while (spaceFiller.length < this.p_LABEL_AREA_LENGTH_MAX - labelFormattedLength)
            spaceFiller += this.p_SPACE_FILLER;
        if (labelFormatted && labelFormatted !== '')
            spaceFiller += this.p_SPACE;
        else {
            for (let i = 0; i < this.p_SPACE.length; i++) {
                spaceFiller += this.p_SPACE_FILLER;
            }
        }
        logString += Logger.format.gray.value(spaceFiller);
        switch (labelColor) {
            case logEnums_1.LogColor.BLACK:
                logString += Logger.format.bold.black.value(labelFormatted);
                break;
            case logEnums_1.LogColor.BLACK_BRIGHT:
                logString += Logger.format.bold.blackBright.value(labelFormatted);
                break;
            case logEnums_1.LogColor.BLUE:
                logString += Logger.format.bold.blue.value(labelFormatted);
                break;
            case logEnums_1.LogColor.BLUE_BRIGHT:
                logString += Logger.format.bold.blueBright.value(labelFormatted);
                break;
            case logEnums_1.LogColor.CYAN:
                logString += Logger.format.bold.cyan.value(labelFormatted);
                break;
            case logEnums_1.LogColor.CYAN_BRIGHT:
                logString += Logger.format.bold.cyanBright.value(labelFormatted);
                break;
            case logEnums_1.LogColor.GRAY:
                logString += Logger.format.bold.gray.value(labelFormatted);
                break;
            case logEnums_1.LogColor.GREEN:
                logString += Logger.format.bold.green.value(labelFormatted);
                break;
            case logEnums_1.LogColor.GREEN_BRIGHT:
                logString += Logger.format.bold.greenBright.value(labelFormatted);
                break;
            case logEnums_1.LogColor.MAGENTA:
                logString += Logger.format.bold.magenta.value(labelFormatted);
                break;
            case logEnums_1.LogColor.MAGENTA_BRIGHT:
                logString += Logger.format.bold.magentaBright.value(labelFormatted);
                break;
            case logEnums_1.LogColor.ORANGE:
                logString += Logger.format.bold.orange.value(labelFormatted);
                break;
            case logEnums_1.LogColor.RED:
                logString += Logger.format.bold.red.value(labelFormatted);
                break;
            case logEnums_1.LogColor.RED_BRIGHT:
                logString += Logger.format.bold.redBright.value(labelFormatted);
                break;
            case logEnums_1.LogColor.YELLOW:
                logString += Logger.format.bold.yellow.value(labelFormatted);
                break;
            case logEnums_1.LogColor.YELLOW_BRIGHT:
                logString += Logger.format.bold.yellowBright.value(labelFormatted);
                break;
        }
        let tagFormatted = '';
        if (tag && typeof tag === 'string') {
            tagFormatted = tag;
            if (tagFormatted.length > this.p_TAG_LENGTH_MAX) {
                tagFormatted = tagFormatted.slice(0, this.p_TAG_LENGTH_MAX - 3);
                while (tagFormatted.length < this.p_TAG_LENGTH_MAX)
                    tagFormatted += '.';
            }
            tagFormatted = this.p_SPACE + this.p_SPACE + Logger.format.gray.value(`${tagFormatted}`);
        }
        logString += ' ' + spinnerCharacter;
        let lineLengthMax = detector_1.default.isNodeJs ? this.p_LINE_LENGTH_MAX_NODE : this.p_LINE_LENGTH_MAX_BROWSER;
        if (messageString.length > lineLengthMax) {
            let messageLines = [];
            let tabSpace = '';
            while (tabSpace.length < (timeStamp.length + this.p_SPACE.length))
                tabSpace += ' ';
            tabSpace += this.p_INDENT_START;
            while (tabSpace.length < this.p_HEADER_LENGTH)
                tabSpace += ' ';
            messageLines = strings_1.Strings.getLines(messageString, lineLengthMax);
            lineCount = messageLines.length;
            logString += this.p_SPACE + messageLines[0];
            logString += tagFormatted + '\n';
            for (let i = 1; i < messageLines.length; i++) {
                logString += Logger.format.gray.value(`${tabSpace}`) + messageLines[i] + '\n';
            }
        }
        else {
            logString += this.p_SPACE + (messageString && messageString !== '' ? Logger.format.whiteBright.value(messageString) : Logger.format.gray.value('null, undefined, or empty message'));
            logString += tagFormatted;
        }
        console.log(logString);
        if (this.isTrace && error.stack) {
            let traceString = error.stack.toString();
            let traceStringFormatted = '';
            if (traceString.length > lineLengthMax) {
                let tabSpace = '';
                while (tabSpace.length < this.p_HEADER_LENGTH)
                    tabSpace += ' ';
                let traceLines = strings_1.Strings.getLines(traceString, lineLengthMax);
                for (let i = 0; i < traceLines.length; i++) {
                    traceStringFormatted += tabSpace + Logger.format.bgBlackBright.value(Logger.format.white.value(traceLines[i])) + '\n';
                }
            }
            else {
                traceStringFormatted +=
                    this.p_SPACE + this.p_SPACE + (traceString && traceString !== '' ?
                        Logger.format.whiteBright.value(traceString) :
                        Logger.format.gray.value('null, undefined, or empty message'));
            }
            console.log('');
            console.log(traceStringFormatted);
            console.log('');
        }
        let logMessage = new logMessage_1.default(this, label, labelColor, message, lineCount, tag, category, this.p_HEADER_LENGTH);
        return logMessage;
    }
}
exports.Logger = Logger;
exports.default = Logger;
//# sourceMappingURL=logger.js.map