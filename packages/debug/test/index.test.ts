import {
    AsyncTest,
    Setup,
    SetupFixture,
    Teardown,
    TeardownFixture,
    Test,
    TestCase,
    TestFixture,
    Timeout,
    Expect
} from 'alsatian';
import { Debug, LogColor, LogMessage, LogRegion } from '../index';
import Detector from '@pilotlab/detector';


Debug.setCategoryMode('categoryTest', true);


@TestFixture('Debug')
export class Tests {
    @TestCase(null)
    @TestCase(undefined)
    @TestCase('')
    @TestCase(NaN)
    @TestCase(0)
    @TestCase(1)
    @TestCase(-1)
    @TestCase('hello')
    @TestCase('Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?')
    @TestCase(false)
    @Test('logging')
    test01(testValue:any) {
        Debug.logger.isTrace = false;

        Debug.log(testValue);
        Debug.log(testValue, 'this is a tag');

        Debug.warn(testValue, 'Data.p_processAttributeNumber');
        Debug.error(testValue, 'this a tag that is particularly long and is bound to exceed the maximum tag length constraint');
        Debug.assert(testValue === 'hello', `${testValue} is not equal to 'hello'`, 'testingTag');
        Debug.info(testValue, 'just for your information');
        Debug.test(testValue, 'this is a tag');

        Debug.logger.custom(testValue);
        Debug.logger.custom(testValue, 'Hey there!', LogColor.BLUE_BRIGHT, 'how about this tag?')
    }


    @TestCase('This is a traced warning.')
    @Test('log tracing')
    test02(testValue:any) {
        Debug.logger.isTrace = true;
        Debug.warn(testValue, 'test tag');
    }


    @TestCase({property01: "value01", property02: "value02", property03: "value03", property04: "value04", property05: "value05", property06: "value06", property07: "value07", property08: "value08" })
    @Test('objects and data')
    test03(testValue:any) {
        Debug.logger.isTrace = false;
        Debug.data(testValue);
    }


    @TestCase('Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?')
    @Test('LogMessage return value')
    test04(testValue:any) {
        let logMessage:LogMessage = Debug.log(testValue, 'this is a tag');

        Debug.info(`message line count: ${logMessage.lineCount}`);
        Debug.info(`message label: ${logMessage.label}`);
        Debug.info(`message type: ${logMessage.type.toString()}`);
        Debug.info(`message tag: ${logMessage.tag}`);
        Debug.info(`message category: ${logMessage.category}`);
    }


    @TestCase('Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?')
    @Test('cursor manipulation')
    test05(testValue:any) {
        let logMessage:LogMessage = Debug.log(testValue, 'this is a tag', 'categoryTest');

        if (Debug.logger.isCategoryOn('categoryTest')) {
            Debug.logger.console.cursor.up(logMessage.lineCount + 1);
            Debug.logger.console.erase(LogRegion.DOWN);
            Debug.test(logMessage.message, logMessage.tag, logMessage.category);
        }
    }


    @TestCase('Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?')
    @AsyncTest("spinner")
    @Timeout(10000) // Alsatian will now wait 5 seconds before failing
    public async asyncTest(testValue:string) {
        let logMessage:LogMessage = Debug.log(testValue, 'this is a tag', 'categoryTest');

        const promise:Promise<any> = new Promise((resolve, reject) => {
            setTimeout(() => {
                logMessage.startSpinner();
                setTimeout(() => {
                    logMessage.stopSpinner();
                    resolve();
                }, 8000);
            }, 1000);

        });

        const result:any = await promise;
    }
}
