# debug

Debugging utilities.

## Install

sudo npm install --save @pilotlab/debug

## Usage

```
import Debug from '@pilotlab/debug';

Debug.assert(
     is.notEmpty(myVariable),
     'The value of myVariable is %MY%. The value of otherVariable is %OTHER%.',
     'AwesomeClass.doSomethingCool(...)', {"MY": myVariable, "OTHER": otherVariable});
```
