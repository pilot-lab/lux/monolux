import Debug from './src/debug';
import Logger from './src/logger';
import LogCursor from './src/logCursor';
import LogMessage from './src/logMessage';
import LogSpinner from './src/logSpinner';
import LogConsole from './src/logConsole';
import { LogColor, LogRegion, LogType } from "./src/logEnums";


export {
    Debug,
    Logger,
    LogColor,
    LogCursor,
    LogMessage,
    LogRegion,
    LogSpinner,
    LogConsole,
    LogType
};


export default Debug;



