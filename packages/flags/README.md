# flags

An enum-based flag system, with methods for dynamically adding and removing named flags.

## Install

sudo npm install --save @pilotlab/flags

## Usage

```
import {EnumFlags, FlagEnumGenerator} from '@pilotlab/flags';
```
