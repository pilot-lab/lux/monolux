import IFlagEnumGenerator from './iFlagEnumGenerator';
export default interface IEnumFlags {
    baseEnum: IFlagEnumGenerator;
    value: number;
    toArray(): IEnumFlags[];
    has(value: (number | string)): boolean;
    set(value: (number | string)): IEnumFlags;
    add(value: (number | string)): IEnumFlags;
    delete(value: (number | string)): IEnumFlags;
    intersect(value: (number | string)): IEnumFlags;
    equals(value: (number | string)): boolean;
}
