import IEnum from './interfaces/iEnum';
import IEnumFlags from './interfaces/iEnumFlags';
import IFlagEnumGenerator from './interfaces/iFlagEnumGenerator';
export declare class FlagEnumGenerator<TEnum> implements IFlagEnumGenerator {
    constructor(baseEnum?: IEnum);
    [key: string]: any;
    private _baseEnum;
    readonly flagCount: number;
    private _flagCount;
    readonly maxValue: number;
    addFlag(flagName: string): IFlagEnumGenerator;
    flags(value?: (number | string)): IEnumFlags;
}
export default FlagEnumGenerator;
