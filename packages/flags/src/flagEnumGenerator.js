"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const enumFlags_1 = require("./enumFlags");
class FlagEnumGenerator {
    constructor(baseEnum) {
        this._flagCount = 1;
        this._baseEnum = is_1.default.notEmpty(baseEnum) ? baseEnum : {};
        this[this['NONE'] = 1] = 'none';
        this._baseEnum['NONE'] = this['NONE'];
        if (is_1.default.notEmpty(baseEnum)) {
            for (let key in baseEnum) {
                if (key !== 'NONE' && key !== 'none') {
                    if (baseEnum.hasOwnProperty(key) && typeof baseEnum[key] === 'number') {
                        this.addFlag(key);
                    }
                }
            }
        }
    }
    get flagCount() { return this._flagCount; }
    get maxValue() { return 1 << this._flagCount; }
    addFlag(flagName) {
        this[this[flagName] = 1 << this._flagCount] = flagName.toLowerCase();
        this._baseEnum[flagName] = this[flagName];
        this._flagCount++;
        return this;
    }
    flags(value = this._baseEnum.NONE) { return new enumFlags_1.default(this, value); }
}
exports.FlagEnumGenerator = FlagEnumGenerator;
exports.default = FlagEnumGenerator;
//# sourceMappingURL=flagEnumGenerator.js.map