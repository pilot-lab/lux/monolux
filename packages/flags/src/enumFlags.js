"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const flagEnumGenerator_1 = require("./flagEnumGenerator");
class EnumFlags {
    constructor(baseEnum, value) {
        this.baseEnum = baseEnum instanceof flagEnumGenerator_1.default ? baseEnum : new flagEnumGenerator_1.default(baseEnum);
        this.value = typeof value === 'string' ? baseEnum[value] : value;
    }
    get value() { return this._value; }
    set value(value) { this._value = value; }
    ;
    toString() {
        let list = [];
        for (let i = 1; i < this.baseEnum.maxValue; i = i << 1) {
            if ((this.value & i) !== 0) {
                list.push(this.baseEnum[i]);
            }
        }
        return list.toString();
    }
    toArray() {
        let list = [];
        for (let i = 1; i < this.baseEnum.maxValue; i = i << 1) {
            if ((this.value & i) !== 0) {
                list.push(new EnumFlags(this.baseEnum, i));
            }
        }
        return list;
    }
    has(value) {
        let valueNum = typeof value === 'string' ? this.baseEnum[value] : value;
        return (this.value & valueNum) === valueNum;
    }
    set(value) {
        if (this.equals(value))
            return;
        let valueNum = typeof value === 'string' ? this.baseEnum[value] : value;
        let flags = new EnumFlags(this.baseEnum, valueNum);
        if (value !== this.baseEnum.NONE)
            flags.delete(this.baseEnum.NONE);
        this._value = flags.value;
        return flags;
    }
    add(value) {
        if (this.has(value))
            return;
        let valueNum = typeof value === 'string' ? this.baseEnum[value] : value;
        let flags = new EnumFlags(this.baseEnum, this.value | valueNum);
        if (value !== this.baseEnum.NONE)
            flags.delete(this.baseEnum.NONE);
        this._value = flags.value;
        return flags;
    }
    delete(value) {
        if (!this.has(value))
            return;
        let valueNum = typeof value === 'string' ? this.baseEnum[value] : value;
        let flags = new EnumFlags(this.baseEnum, (this.value ^ valueNum) & this.value);
        if (value !== this.baseEnum.NONE && flags.value === 0)
            flags.add(this.baseEnum.NONE);
        this._value = flags.value;
        return flags;
    }
    intersect(value) {
        let valueNum = typeof value === 'string' ? this.baseEnum[value] : value;
        let final = 0;
        for (let i = 1; i < this.baseEnum.maxValue; i = (i << 1)) {
            if ((this.value & i) !== 0 && (valueNum & i) !== 0)
                final += i;
        }
        let flags = new EnumFlags(this.baseEnum, final);
        this._value = flags.value;
        return flags;
    }
    equals(value) {
        let valueNum = typeof value === 'string' ? this.baseEnum[value] : value;
        return this.value === valueNum;
    }
}
exports.EnumFlags = EnumFlags;
exports.default = EnumFlags;
//# sourceMappingURL=enumFlags.js.map