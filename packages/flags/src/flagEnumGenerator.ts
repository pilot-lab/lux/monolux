import is from '@pilotlab/is';
import IEnum from './interfaces/iEnum';
import IEnumFlags from './interfaces/iEnumFlags';
import IFlagEnumGenerator from './interfaces/iFlagEnumGenerator';
import EnumFlags from './enumFlags';


/**
 * Dependencies: IEnum
 */
export class FlagEnumGenerator<TEnum> implements IFlagEnumGenerator {
    /**
     * Takes a regular enum definition and creates a flagged enum class instance.
     * Note: The original enum will be modified to contain numeric values appropriate for usage as flags.
     * A 'NONE' flag will also be added, if it doesn't already exist.
     * @param baseEnum {enum} The enum definition being extended.
     */
    constructor(baseEnum?:IEnum) {
        this._baseEnum = is.notEmpty(baseEnum) ? baseEnum : {};

        /// Add NONE as the first element.
        this[this['NONE'] = 1] = 'none';
        this._baseEnum['NONE'] = this['NONE'];

        if (is.notEmpty(baseEnum)) {
            for (let key in baseEnum) {
                /// We already added 'NONE', so don't add it again.
                if (key !== 'NONE' && key !== 'none') {
                    if (baseEnum.hasOwnProperty(key) && typeof baseEnum[key] === 'number') {
                        this.addFlag(key);
                    }
                }
            }
        }
    }


    [key:string]:any; // To implement IEnum.


    private _baseEnum:any;


    get flagCount():number { return this._flagCount; }
    private _flagCount:number = 1;


    /**
     * This will be set automatically to the highest numeric flag value,
     * so it can be used in flag calculations.
     */
    get maxValue():number { return 1 << this._flagCount; }


    addFlag(flagName:string):IFlagEnumGenerator {
        this[this[flagName] = 1 << this._flagCount] = flagName.toLowerCase();
        this._baseEnum[flagName] = this[flagName];
        this._flagCount++;
        return this;
    }


    /**
     * Returns a new EnumFlags instance, with the given value, that uses the base enum for flag calculations.
     */
    flags(value:(number | string) = this._baseEnum.NONE):IEnumFlags { return new EnumFlags<TEnum>(this, value); }
}


export default FlagEnumGenerator;
