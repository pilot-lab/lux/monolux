import IEnum from './interfaces/iEnum';
import IEnumFlags from './interfaces/iEnumFlags';
import IFlagEnumGenerator from './interfaces/iFlagEnumGenerator'
import FlagEnumGenerator from './flagEnumGenerator';


export class EnumFlags<TEnum> implements IEnumFlags {
    constructor(baseEnum:IEnum, value:(number | string)) {
        this.baseEnum = baseEnum instanceof FlagEnumGenerator? <FlagEnumGenerator<TEnum>>baseEnum : new FlagEnumGenerator<TEnum>(baseEnum);
        this.value = typeof value === 'string' ? baseEnum[value] : <number>value;
    }


    baseEnum:IFlagEnumGenerator;


    get value():number { return this._value; }
    set value(value:number) { this._value = value; };
    private _value:number;


    toString():string {
        let list:string[] = [];
        for (let i:number = 1; i < this.baseEnum.maxValue; i = i << 1) {
            if ((this.value & i) !== 0) {
                list.push(this.baseEnum[i]);
            }
        }
        return list.toString();
    }


    toArray():IEnumFlags[] {
        let list:IEnumFlags[] = [];
        for (let i:number = 1; i < this.baseEnum.maxValue; i = i << 1) {
            if ((this.value & i) !== 0) {
                list.push(new EnumFlags(this.baseEnum, i));
            }
        }
        return list;
    }


    has(value:(number | string)):boolean {
        let valueNum:number = typeof value === 'string' ? this.baseEnum[value] : <number>value;
        return (this.value & valueNum) === valueNum;
    }


    set(value:(number | string)):IEnumFlags {
        if (this.equals(value)) return;
        let valueNum:number = typeof value === 'string' ? this.baseEnum[value] : <number>value;

        let flags:IEnumFlags = new EnumFlags(this.baseEnum, valueNum);

        if (value !== this.baseEnum.NONE) flags.delete(this.baseEnum.NONE);

        this._value = flags.value;
        return flags;
    }


    add(value:(number | string)):IEnumFlags {
        if (this.has(value)) return;
        let valueNum:number = typeof value === 'string' ? this.baseEnum[value] : <number>value;

        let flags:IEnumFlags = new EnumFlags(this.baseEnum, this.value | valueNum);

        if (value !== this.baseEnum.NONE) flags.delete(this.baseEnum.NONE);

        this._value = flags.value;
        return flags;
    }


    delete(value:(number | string)):IEnumFlags {
        if (!this.has(value)) return;
        let valueNum:number = typeof value === 'string' ? this.baseEnum[value] : <number>value;
        let flags:IEnumFlags = new EnumFlags(this.baseEnum, (this.value ^ valueNum) & this.value);

        if (value !== this.baseEnum.NONE && flags.value === 0) flags.add(this.baseEnum.NONE);

        this._value = flags.value;
        return flags;
    }


    intersect(value:(number | string)):IEnumFlags {
        let valueNum:number = typeof value === 'string' ? this.baseEnum[value] : <number>value;
        let final:number = 0;
        for (let i:number = 1; i < this.baseEnum.maxValue; i = (i << 1)) { if ((this.value & i) !== 0 && (valueNum & i) !== 0) final += i; }
        let flags:IEnumFlags = new EnumFlags(this.baseEnum, final);
        this._value = flags.value;
        return flags;
    }


    equals(value:(number | string)):boolean {
        let valueNum:number = typeof value === 'string' ? this.baseEnum[value] : <number>value;
        return this.value === valueNum;
    }
} // End class


export default EnumFlags;
