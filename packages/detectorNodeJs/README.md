# detector-node-js

Use to determine context and capabilities of run-time environment for Node.js apps. This library will generate console errors if run in a browser outside of a Node.js environment.

## Install

`sudo npm install --save @pilotlab/detector`

## Usage

```
import DetectorNodeJs from @pilotlab/detector-node-js

let isModuleInstalled:boolean = DetectorNodeJs.isNodeModuleInstalled('@pilotlab/strings');
```
