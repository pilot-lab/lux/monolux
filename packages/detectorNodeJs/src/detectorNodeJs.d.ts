export declare class DetectorNodeJS {
    static isNodeModuleInstalled(moduleName: string): boolean;
    static readonly isNwjsApp: boolean;
    static readonly isElectronApp: boolean;
}
export default DetectorNodeJS;
