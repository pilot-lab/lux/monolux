"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
class DetectorNodeJS {
    static isNodeModuleInstalled(moduleName) {
        try {
            const path = require.resolve(moduleName);
            return is_1.default.notEmpty(path);
        }
        catch (e) {
            return false;
        }
    }
    static get isNwjsApp() {
        try {
            return this.isNodeModuleInstalled('nw.gui');
        }
        catch (e) {
            return false;
        }
    }
    static get isElectronApp() {
        try {
            return this.isNodeModuleInstalled('electron');
        }
        catch (e) {
            return false;
        }
    }
}
exports.DetectorNodeJS = DetectorNodeJS;
exports.default = DetectorNodeJS;
//# sourceMappingURL=detectorNodeJs.js.map