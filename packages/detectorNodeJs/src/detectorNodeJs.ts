import is from '@pilotlab/is';


interface NodeRequireFunction { (id: string): any; resolve:(packageName:string) => {}} // End interface
declare const require:NodeRequireFunction;


export class DetectorNodeJS {
    static isNodeModuleInstalled(moduleName:string):boolean {
        try {
            const path:any = require.resolve(moduleName);
            return is.notEmpty(path);
        } catch (e) {
            // process.exit(e.code);
            return false;
        }
    }


    /**
     * Is the app running in an instance of NW.js?
     * See: http://nwjs.io/
     */
    static get isNwjsApp():boolean {
        try { return this.isNodeModuleInstalled('nw.gui'); } catch (e) { return false; }
    }


    /**
     * Are we running in an Electron app?
     * See: http://electron.atom.io/
     */
    static get isElectronApp():boolean {
        try { return this.isNodeModuleInstalled('electron'); } catch (e) { return false; }
    }
} // End class


export default DetectorNodeJS;

