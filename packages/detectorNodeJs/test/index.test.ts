import {
    Setup,
    SetupFixture,
    Teardown,
    TeardownFixture,
    Test,
    TestCase,
    TestFixture,
    Expect
} from 'alsatian';
import { DetectorNodeJs } from '../index';


@TestFixture('Detector Tests')
export class Tests {
    @Setup
    public setup() {}

    @SetupFixture
    public setupFixture() {}

    @Teardown
    public teardown() {}

    @TeardownFixture
    public teardownFixture() {}

    @TestCase('')
    @TestCase(0)
    @TestCase('@pilotlab/strings')
    @TestCase('someothermodule')
    @Test('Detect whether a NodeJS module is installed')
    test01(testValue:any) {
        const isInstalled:boolean = DetectorNodeJs.isNodeModuleInstalled(testValue.toString());
        isInstalled ? console.log(`The module ${testValue} is installed.`) : console.log(`The module ${testValue} is not installed.`);
    }
}
