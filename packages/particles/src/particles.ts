import IParticle from './interfaces/iParticle';
import IParticles from './interfaces/iParticles';
import { NodesBase, INodeFactory } from '@pilotlab/nodes';


export class Particles extends NodesBase<IParticle> implements IParticles {
    constructor(
        factory:INodeFactory,
        parent?:IParticle,
        pathSegment:string = '/'
    ) {
        super(factory, parent, pathSegment);
        this.p_factory = factory;
    }
} // End class


export default Particles;
