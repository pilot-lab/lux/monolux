"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const nodes_1 = require("@pilotlab/nodes");
const particles_1 = require("./particles");
class ParticlesFactory extends nodes_1.NodesFactoryBase {
    instance(parent, ...args) { return new particles_1.default(this.node, parent); }
}
exports.ParticlesFactory = ParticlesFactory;
exports.default = ParticlesFactory;
//# sourceMappingURL=particlesFactory.js.map