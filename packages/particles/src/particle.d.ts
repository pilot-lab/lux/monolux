import { NodeBase } from '@pilotlab/nodes';
import { Attributes, IAttributes } from '@pilotlab/data';
import { Bounds, Color } from '@pilotlab/metrics';
import { IPromise } from '@pilotlab/result';
import IParticle from './interfaces/iParticle';
import IParticles from './interfaces/iParticles';
import IParticleSystem from './interfaces/iParticleSystem';
import ParticleData from './particleData';
import ParticleFactory from './particleFactory';
declare class Particle extends NodeBase<IParticle, IParticles, IParticleSystem> implements IParticle {
    constructor(data?: (IAttributes | Object | string), isInitialize?: boolean);
    readonly data: ParticleData;
    protected p_data: ParticleData;
    readonly bounds: Bounds;
    readonly colorBackground: Color;
    initialize(data?: (IAttributes | Object | string)): IPromise<any>;
    protected p_isRoot: boolean;
    static readonly create: ParticleFactory;
    private static _factory;
    readonly attributes: Attributes;
}
export default Particle;
