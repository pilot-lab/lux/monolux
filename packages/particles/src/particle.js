"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const nodes_1 = require("@pilotlab/nodes");
const result_1 = require("@pilotlab/result");
const particleData_1 = require("./particleData");
const particleFactory_1 = require("./particleFactory");
class Particle extends nodes_1.NodeBase {
    constructor(data, isInitialize = true) {
        super(Particle.create, null, null, false);
        this.p_isRoot = false;
        if (isInitialize)
            this.initialize(data);
    }
    get data() { return this.p_data; }
    get bounds() { return this.p_data.bounds; }
    get colorBackground() { return this.p_data.colorBackground; }
    initialize(data) {
        const result = new result_1.Result();
        this.p_data = new particleData_1.default();
        super.initialize(this.p_isRoot ? nodes_1.NodeType.ROOT : nodes_1.NodeType.BASIC).then(() => {
            this.p_data.initialize(data).then(() => { result.resolve(); });
        });
        return result;
    }
    static get create() {
        if (is_1.default.empty(this._factory))
            this._factory = new particleFactory_1.default();
        return this._factory;
    }
    get attributes() { return this.p_data.attributes; }
}
exports.default = Particle;
//# sourceMappingURL=particle.js.map