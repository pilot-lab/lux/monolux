import { NodesFactoryBase } from '@pilotlab/nodes';
import IParticle from './interfaces/iParticle';
import IParticles from './interfaces/iParticles';
export declare class ParticlesFactory extends NodesFactoryBase<IParticles> {
    instance(parent?: IParticle, ...args: any[]): IParticles;
}
export default ParticlesFactory;
