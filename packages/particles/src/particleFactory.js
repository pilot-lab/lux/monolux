"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const nodes_1 = require("@pilotlab/nodes");
const particlesFactory_1 = require("./particlesFactory");
const particle_1 = require("./particle");
class ParticleFactory extends nodes_1.NodeFactoryBase {
    constructor() {
        super();
        this.p_collection = new particlesFactory_1.default(this);
    }
    instance(createOptions, key, ...args) {
        return new particle_1.default(args[0]);
    }
    get collection() { return this.p_collection; }
    fromObject(obj, ...args) { return null; }
}
exports.ParticleFactory = ParticleFactory;
exports.default = ParticleFactory;
//# sourceMappingURL=particleFactory.js.map