import { List } from '@pilotlab/collections';
import { IAttributeFactory } from '@pilotlab/data';
import { INodes } from '@pilotlab/nodes';
import IParticle from './iParticle';


export interface IParticles extends INodes {
    /*====================================================================*
     START: Factory
     *====================================================================*/
    // create:IAttributeFactory;


    /*====================================================================*
     START: Properties
     *====================================================================*/
    //----- sortedByZ
    // sortedByZ:List<IParticle>;


    /*====================================================================*
     START: Methods
     *====================================================================*/
    // updateScaleXAbsAll():void;
    // updateScaleYAbsAll():void;
    // updateScaleZAbsAll():void;
    // updateOpacityAll():void;
    // renderAll():void;
    // sortByZ():IParticleSystem;
} // End class


export default IParticles;
