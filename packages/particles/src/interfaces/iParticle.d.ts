import { INode } from '@pilotlab/nodes';
import IParticles from './iParticles';
export interface IParticle extends INode {
    children: IParticles;
}
export default IParticle;
