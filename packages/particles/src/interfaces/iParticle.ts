import { Signal } from '@pilotlab/signals';
import { MapList } from '@pilotlab/collections'
import { Animation, ISpeed } from '@pilotlab/animation';
import { AttributeChangeOptions, IAttribute } from '@pilotlab/data';
import { INode } from '@pilotlab/nodes';
// import ISurface from './iSurface';
// import { ParticleAnimationType } from '../particleEnums';
// import ParticleEventArgs from '../particleEventArgs';
// import Particles from '@pilotlab/result';
// import ParticleRotation from '../particleRotation';
// import ParticlePosition from '../particlePosition';
// import ParticleSize from '../particleSize';
// import ParticleScale from '../particleScale';
import IParticles from './iParticles';


export interface IParticle extends INode {
    /*====================================================================*
     START: Properties
     *====================================================================*/
    children:IParticles;
    // surface:ISurface<any>;
    // requestSurface():void;
    // isVisible:boolean;
    // isClipChildren:boolean;
    // opacity:number;
    // opacityAbs:number;
    // internalOpacityAbs:number;


    //----- opacityMin
    /**
     * When the opacity is set below this level, the element won't be rendered and it won't receive mouse events.
     */
    // opacityMin:number;


    // position:ParticlePosition;
    // rotation:ParticleRotation
    // scale:ParticleScale;
    // size:ParticleSize;


    // moveToTop(changeOptions?:AttributeChangeOptions, isRememberCurrentIndex?:boolean):boolean;


    //----- pivot
    /**
     * This is the point around which the Element rotates and from which it
     * grows or shrinks when scaled. It can be modified at runtime.
     * Each dimension of the point should be specified as a decimal, where (0.0, 0.0, 0,0) is equal
     * to the the upper-left corner of the element and (1.0, 1.0, 0.0) is equal to the bottom-right.
     */
    // pivot:Point;
    // internalPivotCurrent:Point;
    // internalOnPivotChanged(isChangePositionValues?:boolean):void;


    /**
     * Applies scaling to a location along the X axis.
     * @param {number} [x] A location on the X axis, relative to the left of the Element.
     * @return {number} The transformed location on the X axis, relative to the left of the Element.
     */
    // getXTransformed(x:number):number;


    /**
     * Applies scaling to a location along the Y axis.
     * @param {number} [y] A location on the Y axis, relative to the left of the Element.
     * @return {number} The transformed location on the Y axis, relative to the top of the Element.
     */
    // getYTransformed(y:number):number;


    /**
     * Applies scaling to a location along the Z axis.
     * @param {number} [z] A location on the Z axis, relative to the left of the Element.
     * @return {number} The transformed location on the Z axis, relative to the Z of the Element.
     */
    // getZTransformed(z:number):number;


    // isAspectRatioMaintained:boolean;
    // isScaleChildren:boolean;


    /*====================================================================*
     START: Signals
     *====================================================================*/
    //----- loaded
    // isLoaded:boolean;
    // loaded:Signal<ParticleEventArgs>;


    //----- Allow the element surface to alert us when it has loaded the native content.
    // doLoaded():void;
    // doUnloaded():void;


    /**
     * Transforms a point from the parent element's coordinate
     * system into the local coordinate system of this element.
     * @param {Point} [point] A point relative to the upper-left of the parent Element.
     * @return {Point} The transformed point relative to the upper-left corner of this Element,
     * with scaling, rotation and taken into account.
     */
    // parentToLocal(point:Point):Point;


    /**
     * Transforms a point from the local coordinate system of this element
     * into the parent element's coordinate system.
     * @param {Point} [point] A point relative to the upper-left of this Element.
     * @return {Point} The transformed point relative to the upper-left corner
     * of the parent Element, with scaling and rotation taken into account.
     */
    // localToParent(point:Point):Point;


    /**
     * Transforms a point from the coordinate system of the root element
     * into the coordinate system of this element.
     * @param {Point} [point] A point relative to the upper-left of the root Element.
     * @return {Point} The transformed point relative to the upper-left corner
     * of this element, with scaling and rotation taken into account.
     */
    // rootToLocal(point:Point):Point;


    /**
     * Transforms a point from the local coordinate system of this element
     * into the coordinate system of the Root.
     * @param {Point} [point] A point relative to the upper-left of this Element.
     * @return {Point} The transformed point relative to the upper-left corner
     * of the parent Root, with scaling and rotation taken into account.
     */
    // localToRoot(point:Point):Point;


    /*====================================================================*
     START: Methods
     *====================================================================*/
    /*--------------------------------------------------------------------*
     START: Methods for updating a Element chain that has been added to the ElementSystem
     *--------------------------------------------------------------------*/
    // internalAttached(changeOptions:AttributeChangeOptions):void;
    // internalDetached(changeOptions?:AttributeChangeOptions):void;
    /*--------------------------------------------------------------------*
     END: Methods for updating a Element chain that has been added to the ElementSystem
     *--------------------------------------------------------------------*/

    // isRender:boolean;
    // render():void;


    // hitTest(pt:Point, isLocal?:boolean):boolean;


    /*====================================================================*
     START: Animation
     *====================================================================*/
    // changeOptions:AttributeChangeOptions;
    // speed:ISpeed
    // internalAnimators:MapList<string, Animation>;
    // internalAddAnimators(type:ElementAnimationType, animators:Animation):void;
    // isAnimating:boolean;
    // animationsCompleted:Signal<IParticle>;
} // End of class


export default IParticle;
