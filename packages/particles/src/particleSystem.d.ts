import { IAttributes } from '@pilotlab/data';
import Particle from './particle';
declare class ParticleSystem extends Particle {
    constructor(data?: (IAttributes | Object | string));
}
export default ParticleSystem;
