"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const particle_1 = require("./particle");
class ParticleSystem extends particle_1.default {
    constructor(data) {
        super(data, false);
        this.p_isRoot = true;
        this.initialize(data);
    }
}
exports.default = ParticleSystem;
//# sourceMappingURL=particleSystem.js.map