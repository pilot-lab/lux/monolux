import is from '@pilotlab/is';
import { INode, INodes, NodeBase, NodeType } from '@pilotlab/nodes';
import {
    AttributeFactory,
    Attributes,
    IAttributeFactory,
    IAttributes,
} from '@pilotlab/data';
import {
    Bounds,
    Color
} from '@pilotlab/metrics';
import { IPromise, Result } from '@pilotlab/result';
import IParticle from './interfaces/iParticle';
import IParticles from './interfaces/iParticles';
import IParticleSystem from './interfaces/iParticleSystem';
import ParticleData from './particleData';
import ParticleFactory from './particleFactory';


class Particle
extends NodeBase<IParticle, IParticles, IParticleSystem>
implements IParticle {
    constructor(
        data?:(IAttributes | Object | string),
        isInitialize:boolean = true
    ) {
        super(Particle.create, null, null, false);
        this.p_isRoot = false;
        if (isInitialize) this.initialize(data);
    }


    get data():ParticleData { return this.p_data; }
    protected p_data:ParticleData;


    get bounds():Bounds { return this.p_data.bounds; }
    get colorBackground():Color { return this.p_data.colorBackground; }


    initialize(data?:(IAttributes | Object | string)):IPromise<any> {
        const result:Result<any> = new Result<any>();

        this.p_data = new ParticleData();

        super.initialize(this.p_isRoot ? NodeType.ROOT : NodeType.BASIC).then(() => {
            this.p_data.initialize(data).then(() => { result.resolve(); });
        });

        return result;
    }


    protected p_isRoot:boolean;


    static get create():ParticleFactory {
        if (is.empty(this._factory)) this._factory = new ParticleFactory();
        return this._factory;
    }
    private static _factory:ParticleFactory;


    get attributes():Attributes { return <Attributes>this.p_data.attributes; }
} // End class


export default Particle;
