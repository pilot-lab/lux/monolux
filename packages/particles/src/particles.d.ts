import IParticle from './interfaces/iParticle';
import IParticles from './interfaces/iParticles';
import { NodesBase, INodeFactory } from '@pilotlab/nodes';
export declare class Particles extends NodesBase<IParticle> implements IParticles {
    constructor(factory: INodeFactory, parent?: IParticle, pathSegment?: string);
}
export default Particles;
