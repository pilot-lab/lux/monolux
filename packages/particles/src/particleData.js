"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const data_1 = require("@pilotlab/data");
const result_1 = require("@pilotlab/result");
class ParticleData extends data_1.AttributeBase {
    constructor() {
        super(data_1.Attribute.create, null, null, null, null, null, false);
    }
    initialize(data) {
        const result = new result_1.Result();
        const dataInitial = {
            bounds: {
                value: {
                    position: { value: { x: 0, y: 0, z: 0 }, dataType: data_1.DataType.POINT_3D, label: 'position' },
                    pivot: { value: { x: 0.5, y: 0.5, z: 0.5 }, dataType: data_1.DataType.POINT_3D, label: 'pivot' },
                    size: { value: { width: 20, height: 20, depth: 20 }, dataType: data_1.DataType.SIZE_3D, label: 'size' },
                    scale: { value: { x: 1, y: 1, z: 1 }, dataType: data_1.DataType.POINT_3D, label: 'scale' }
                },
                type: data_1.DataType.CUBE
            },
            colorBackground: {
                value: { r: 100, g: 100, b: 100 },
                dataType: data_1.DataType.COLOR,
                label: 'background color'
            }
        };
        super.initialize(dataInitial, data_1.DataType.COLLECTION, true).then(() => {
            if (is_1.default.notEmpty(data)) {
                this.attributes.update(data, data_1.AttributeChangeOptions.zero).then(() => { result.resolve(); });
            }
            else
                result.resolve();
        });
        return result;
    }
    static get create() {
        if (is_1.default.empty(this._factory))
            this._factory = new data_1.AttributeFactory();
        return this._factory;
    }
    get attributes() { return this.p_value; }
}
exports.default = ParticleData;
//# sourceMappingURL=particleData.js.map