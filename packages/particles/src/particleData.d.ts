import { Attributes, AttributeBase, IAttributeFactory, IAttribute, IAttributes } from '@pilotlab/data';
import { Bounds, Color } from '@pilotlab/metrics';
import { IPromise } from '@pilotlab/result';
declare class ParticleData extends AttributeBase<Attributes, IAttribute, IAttributes, ParticleData> {
    constructor();
    bounds: Bounds;
    colorBackground: Color;
    initialize(data?: (IAttributes | Object | string)): IPromise<any>;
    static readonly create: IAttributeFactory;
    private static _factory;
    readonly attributes: Attributes;
}
export default ParticleData;
