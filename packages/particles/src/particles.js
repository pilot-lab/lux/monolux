"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const nodes_1 = require("@pilotlab/nodes");
class Particles extends nodes_1.NodesBase {
    constructor(factory, parent, pathSegment = '/') {
        super(factory, parent, pathSegment);
        this.p_factory = factory;
    }
}
exports.Particles = Particles;
exports.default = Particles;
//# sourceMappingURL=particles.js.map