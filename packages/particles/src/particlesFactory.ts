import { NodesFactoryBase } from '@pilotlab/nodes';
import IParticle from './interfaces/iParticle'
import IParticles from './interfaces/iParticles';
import Particles from './particles';



export class ParticlesFactory extends NodesFactoryBase<IParticles> {
    instance(parent?:IParticle, ...args:any[]):IParticles { return new Particles(this.node, parent); }
} // End class


export default ParticlesFactory;
