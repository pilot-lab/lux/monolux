import is from '@pilotlab/is';
import {
    Attribute,
    AttributeChangeOptions,
    AttributeFactory,
    Attributes,
    AttributeBase,
    DataType,
    IAttributeFactory,
    IAttribute,
    IAttributes,
} from '@pilotlab/data';
import {
    Bounds,
    Color
} from '@pilotlab/metrics';
import { IPromise, Result } from '@pilotlab/result';


class ParticleData
    extends AttributeBase<Attributes, IAttribute, IAttributes, ParticleData> {
    constructor() {
        super(Attribute.create, null, null, null, null, null, false);
    }


    bounds:Bounds;
    colorBackground:Color;


    initialize(data?:(IAttributes | Object | string)):IPromise<any> {
        const result:Result<any> = new Result<any>();
        const dataInitial:any = {
            bounds: {
                value: {
                    position: { value: { x: 0, y: 0, z: 0 }, dataType: DataType.POINT_3D, label: 'position' },
                    pivot: { value: { x: 0.5, y: 0.5, z: 0.5 }, dataType: DataType.POINT_3D, label: 'pivot' },
                    size: { value: { width: 20, height: 20, depth: 20 }, dataType: DataType.SIZE_3D, label: 'size' },
                    scale: { value: { x: 1, y: 1, z: 1 }, dataType: DataType.POINT_3D, label: 'scale' }
                },
                type: DataType.CUBE
            },
            colorBackground: {
                value: { r: 100, g: 100, b: 100 },
                dataType: DataType.COLOR,
                label: 'background color'
            }
        };

        super.initialize(dataInitial, DataType.COLLECTION, true).then(() => {
            if (is.notEmpty(data)) {
                this.attributes.update(data, AttributeChangeOptions.zero).then(() => { result.resolve(); });
            } else result.resolve();
        });

        return result;
    }


    static get create():IAttributeFactory {
        if (is.empty(this._factory)) this._factory = new AttributeFactory();
        return this._factory;
    }
    private static _factory:IAttributeFactory;


    get attributes():Attributes { return <Attributes>this.p_value; }
} // End class


export default ParticleData;
