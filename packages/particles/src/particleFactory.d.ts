import { NodeFactoryBase } from '@pilotlab/nodes';
import IParticle from './interfaces/iParticle';
import INodeCreateOptions from "../../nodes/src/interfaces/iNodeCreateOptions";
import ParticlesFactory from './particlesFactory';
export declare class ParticleFactory extends NodeFactoryBase<IParticle> {
    constructor();
    instance(createOptions?: INodeCreateOptions, key?: string, ...args: any[]): IParticle;
    readonly collection: ParticlesFactory;
    protected p_collection: ParticlesFactory;
    fromObject(obj: Object, ...args: any[]): IParticle;
}
export default ParticleFactory;
