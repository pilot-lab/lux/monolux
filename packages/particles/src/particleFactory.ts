import { NodeFactoryBase } from '@pilotlab/nodes';
import IParticle from './interfaces/iParticle'
import INodeCreateOptions from "../../nodes/src/interfaces/iNodeCreateOptions";
import ParticlesFactory from './particlesFactory';
import Particle from './particle';


export class ParticleFactory extends NodeFactoryBase<IParticle> {
    constructor() {
        super();
        this.p_collection = new ParticlesFactory(this);
    }


    instance(createOptions?:INodeCreateOptions, key?:string, ...args:any[]):IParticle {
        return new Particle(args[0]);
    }


    get collection():ParticlesFactory { return this.p_collection; }
    protected p_collection:ParticlesFactory;


    fromObject(obj:Object, ...args:any[]):IParticle { return null; }
} // End class


export default ParticleFactory;
