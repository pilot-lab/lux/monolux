import is from '@pilotlab/is';
import { INode, INodes } from '@pilotlab/nodes';
import {
    Attribute,
    AttributeRoot,
    Attributes,
    AttributeBase,
    Color,
    DataType,
    IAttributes,
    Point,
    AttributeFactory,
    IAttributeFactory
} from '@pilotlab/data';
import { IPromise, Result } from '@pilotlab/result';
import IParticle from './interfaces/iParticle';
import IParticles from './interfaces/iParticles';
import Particle from './particle';
import ParticleData from "./particleData";


class ParticleSystem extends Particle {
    constructor(
        data?:(IAttributes | Object | string),
    ) {
        super(data, false);
        this.p_isRoot = true;
        this.initialize(data);
    }
} // End class


export default ParticleSystem;
