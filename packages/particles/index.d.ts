import IParticle from './src/interfaces/iParticle';
import IParticles from './src/interfaces/iParticles';
import IParticleSystem from './src/interfaces/iParticleSystem';
import Particle from './src/particle';
import ParticleSystem from './src/particleSystem';
export { IParticle, IParticles, IParticleSystem, Particle, ParticleSystem };
export default Particle;
