"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const particle_1 = require("./src/particle");
exports.Particle = particle_1.default;
const particleSystem_1 = require("./src/particleSystem");
exports.ParticleSystem = particleSystem_1.default;
exports.default = particle_1.default;
//# sourceMappingURL=index.js.map