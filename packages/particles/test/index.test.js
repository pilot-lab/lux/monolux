"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var alsatian_1 = require("alsatian");
var index_1 = require("../index");
var Tests = (function () {
    function Tests() {
    }
    Tests.prototype.test01 = function (testValue) {
        var result = new index_1.default();
        result.then(function (value) {
            alsatian_1.Expect(value).toBe(testValue);
        });
        result.resolve(testValue);
    };
    Tests.prototype.test02 = function (testValues) {
        var results = [];
        for (var i = 0; i < testValues.length; i++) {
            var result = new index_1.default();
            results.push(result);
        }
        index_1.default.all(results).then(function (values) {
            alsatian_1.Expect(values.length).toBe(testValues.length);
        });
        for (var i = 0; i < results.length; i++) {
            results[i].resolve(testValues[i]);
        }
    };
    __decorate([
        alsatian_1.TestCase(1),
        alsatian_1.TestCase(2),
        alsatian_1.TestCase(3),
        alsatian_1.Test('Complete with value')
    ], Tests.prototype, "test01", null);
    __decorate([
        alsatian_1.TestCase([1, 2, 3]),
        alsatian_1.TestCase(['a', 'b', 'c']),
        alsatian_1.TestCase([1, 'a', 1, 'b']),
        alsatian_1.Test('Complete all')
    ], Tests.prototype, "test02", null);
    Tests = __decorate([
        alsatian_1.TestFixture('Result Tests')
    ], Tests);
    return Tests;
}());
exports.Tests = Tests;
//# sourceMappingURL=index.test.js.map