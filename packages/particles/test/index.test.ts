import {
    AsyncTest,
    Setup,
    SetupFixture,
    Teardown,
    TeardownFixture,
    Test,
    TestCase,
    TestFixture,
    Timeout,
    Expect
} from 'alsatian';
import {
    Particle,
    ParticleSystem
} from '../index';
import { AttributeChangeOptions, AttributeEventArgs, DataType, IAttribute, IAttributeSetReturn, Point } from '@pilotlab/data';


@TestFixture('Particle Tests')
export class Tests {
    @TestCase(null)
    @AsyncTest("Particle instantiation")
    @Timeout(5000) // Alsatian will now wait 5 seconds before failing
    async test02(testValue:any) {
        const data:any = {
            bounds: {
                value: {
                    position: { value: { x: 120, y: 0, z: 0 }, dataType: DataType.POINT_3D, label: 'position' },
                    pivot: { value: { x: 0.5, y: 0.5, z: 0.5 }, dataType: DataType.POINT_3D, label: 'pivot' },
                    size: { value: { width: 20, height: 20, depth: 20 }, dataType: DataType.SIZE_3D, label: 'size' },
                    scale: { value: { x: 1, y: 1, z: 1 }, dataType: DataType.POINT_3D, label: 'scale' }
                },
                dataType: DataType.CUBE
            },
            colorBackground: {
                value: { r: 200, g: 100, b: 100 },
                dataType: DataType.COLOR,
                label: 'background color'
            }
        };

        const p:Particle = new Particle(data, false);
        p.initialize(data);
        // console.log(p.data.toObject());

        p.colorBackground.initialized.listenOnce(() => {
            console.log(`key: ${p.bounds.position.key}, x: ${p.bounds.position.x}`);

            p.data.attributes.update(data).then(() => {
                console.log(`key: ${p.bounds.position.key}, x updated: ${p.bounds.position.x}`);
            });
        });
        p.bounds.position.x = 340;
        console.log(`key: ${p.bounds.position.key}, x: ${p.bounds.position.x}`);
        console.log(`color.r: ${p.colorBackground.r}`);
        // console.log(p.colorBackground.toObject());

        // const p:Particle = new ParticleSystem({
        //     bounds: {
        //         position: { x: 67, y: 42, z: 51 },
        //         pivot: { x: 1.0, y: 0.0 },
        //         size: { width: 40, height: 40, depth: 40 },
        //         scale: { x: 2, y: 2, z: 2 }
        //     },
        //     colorBackground: { r: 120, g: 130, b: 140 },
        // });

        // p.bounds.initialized.listenOnce(() => {
        //     p.bounds.position.initialized.listenOnce(() => {
        // });
        //         console.log(`p.bounds.position.x: ${p.bounds.position.x}`);
        //     });

        // console.log(`p.isRoot: ${p.isRoot}`);
        //
        // console.log(p.attributes.get('bounds').toObject());
        // console.log(`p.bounds.attributes.get('position').attributes.get('x').value: ${p.bounds.attributes.get('position').attributes.get('x').value}`);
        // console.log(`p.bounds.attributes.get('position') instanceof Point: ${p.bounds.attributes.get('position') instanceof Point}`);
        // console.log(`p.bounds.attributes.get('position').x: ${p.bounds.attributes.get('position').x}`);
        //
        // // console.log(o.toObject());
        // console.log(`p.colorBackground.g: ${p.colorBackground.g}`);
        // p.bounds.position.attributes.set('x', 520, AttributeChangeOptions.signal).then((value:IAttributeSetReturn) => {
        //     console.log(`isChanged: ${value.isChanged}, p.bounds.position.x: ${value.attribute.value}`);
        // });
        //
        // // console.log(`o.attributes.get('point').x: ${(<Point>o.attributes.get('point')).attributes.get('x').value}`);
        //
        // p.bounds.position.changed.listen((args:AttributeEventArgs) => {
        //     console.log(`point changed, attribute: ${args.node.key}, value: ${args.node.value}`);
        // });
        // p.bounds.position.x = 1280;
    }
}
