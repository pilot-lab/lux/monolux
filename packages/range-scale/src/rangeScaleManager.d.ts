import { IAnimationEaseFunction } from '@pilotlab/animation';
import { MapList } from '@pilotlab/collections';
import { Signal } from '@pilotlab/signals';
import RangeScale from './rangeScale';
import RangeScaleBase from './rangeScaleBase';
export declare class RangeScaleManager extends RangeScaleBase {
    constructor(inputMin?: number, inputMax?: number, ease?: IAnimationEaseFunction);
    readonly outputRanges: MapList<string, RangeScale>;
    private _outputRanges;
    updated: Signal<number>;
    addOutputRange(name: string, outputMin: number, outputMax: number): RangeScale;
    update(inputValue: number, isValueNormalized?: boolean): void;
    output(name: string, isConstrain?: boolean, isNormalizeReturnValue?: boolean, isInvertReturnValue?: boolean): number;
}
export default RangeScaleManager;
