import { IAnimationEaseFunction } from '@pilotlab/animation';
import { Signal } from '@pilotlab/signals';
export declare class RangeScaleBase {
    constructor(inputMin?: number, inputMax?: number, ease?: IAnimationEaseFunction);
    inputMin: number;
    protected p_inputMin: number;
    inputMax: number;
    protected p_inputMax: number;
    ease: IAnimationEaseFunction;
    protected p_ease: IAnimationEaseFunction;
    readonly progress: number;
    readonly p: number;
    protected p_lastInputRaw: number;
    protected p_lastInputNormalized: number;
    enteredRangeFromBelow: Signal<number>;
    enteredRangeFromAbove: Signal<number>;
    normalizeInput(inputUnnormalized: number, isConstrain?: boolean, isInvert?: boolean): number;
    unnormalizeInput(inputNormalized: number, isConstrain?: boolean, isInvert?: boolean): number;
    protected p_checkInputPreviousRaw(inputValueRaw: number): void;
}
export default RangeScaleBase;
