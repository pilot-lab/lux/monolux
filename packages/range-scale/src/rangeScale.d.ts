import { IAnimationEaseFunction } from '@pilotlab/animation';
import RangeScaleBase from './rangeScaleBase';
export declare class RangeScale extends RangeScaleBase {
    constructor(inputMin?: number, inputMax?: number, outputMin?: number, outputMax?: number, ease?: IAnimationEaseFunction);
    static normalize(value: number, min: number, max: number, isConstrain?: boolean, isInvert?: boolean): number;
    static unnormalize(valueNormalized: number, min: number, max: number, isConstrain?: boolean, isInvert?: boolean): number;
    static outputEase(value: number, min: number, max: number, ease: IAnimationEaseFunction): number;
    static easeNormalized(value: number, ease: IAnimationEaseFunction, isInvert?: boolean): number;
    outputMin: number;
    private _outputMin;
    outputMax: number;
    private _outputMax;
    isInvert: boolean;
    private _isInvert;
    readonly value: number;
    private _value;
    output(inputValue: number, isValueNormalized?: boolean, isConstrain?: boolean, isNormalizeReturnValue?: boolean, isInvertReturnValue?: boolean): number;
    normalizeOutput(outputUnnormalized: number, isConstrain?: boolean, isInvert?: boolean): number;
    unnormalizeOutput(outputNormalized: number, isConstrain?: boolean, isInvert?: boolean): number;
    private _outputFromNormalized(inputNormalized);
}
export default RangeScale;
