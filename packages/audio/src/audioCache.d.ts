import { Cache } from '@pilotlab/cache';
import { Result, IPromise } from '@pilotlab/result';
export declare class AudioCache extends Cache<ArrayBuffer> {
    constructor();
    static audio: AudioCache;
    protected p_createNew(key: string, result: Result<ArrayBuffer>, isCache: boolean): IPromise<ArrayBuffer>;
}
export default AudioCache;
