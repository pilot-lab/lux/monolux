import { Signal } from '@pilotlab/signals';
import { IAnimationEaseFunction } from '@pilotlab/animation';
import AudioSource from './audioSource';
export declare class AudioManager {
    constructor();
    private _cache;
    private _currentlyPlaying;
    audioContext: AudioContext;
    gainNode: GainNode;
    readonly isPaused: boolean;
    private _isPaused;
    volume: number;
    private _volume;
    audioEnded: Signal<AudioSource>;
    tick(elapsedMilliseconds: number): void;
    playBallOut(): AudioManager;
    playPing(frequency?: number): AudioManager;
    preload(url: string): AudioSource;
    release(url: string): AudioManager;
    releaseAll(): AudioManager;
    play(url?: string, isStopCurrentlyPlaying?: boolean, isLoop?: boolean, isCache?: boolean, durationIn?: number, easeIn?: IAnimationEaseFunction, durationOut?: number, easeOut?: IAnimationEaseFunction): AudioSource;
    private _playAudioSource(audioSource, isStopCurrentlyPlaying?, durationIn?, easeIn?, durationOut?, easeOut?);
    pause(duration?: number, ease?: IAnimationEaseFunction): boolean;
    stop(duration?: number, ease?: IAnimationEaseFunction): AudioManager;
}
export default AudioManager;
