import is from '@pilotlab/is';
import Debug from '@pilotlab/debug';
import { List } from '@pilotlab/collections';
import { Signal } from '@pilotlab/signals';
import { IAnimationEaseFunction } from '@pilotlab/animation';
import AudioCache from './audioCache';
import AudioSource from './audioSource';


/**
 * This is used to avoid :
 * error TS7017: Element implicitly has an 'any' type because type 'Window' has no index signature.
 *
 * See: https://stackoverflow.com/questions/42193262/element-implicitly-has-an-any-type-because-type-window-has-no-index-signatur
 */
interface WindowIndexed { [key: string]: any }


export class AudioManager {
    constructor() {
        try {
            let win:WindowIndexed = window;
            if (win.hasOwnProperty('webkitAudioContext')
                && !win.hasOwnProperty('AudioContext')) {
                win['AudioContext'] = win['webkitAudioContext']
            }

            if (AudioContext) this.audioContext = new AudioContext();
            else Debug.error('Web Audio API is not supported by this browser')
        } catch (e) {
            if (e.message.indexOf('hardware sample rate') < 0) Debug.error(e, 'AudioManager.constructor');
        }
    }


    /*====================================================================*
     START: Properties
     *====================================================================*/
    private _cache:AudioCache = new AudioCache();
    private _currentlyPlaying:List<AudioSource> = new List<AudioSource>();


    audioContext:AudioContext;
    gainNode:GainNode;


    get isPaused():boolean { return this._isPaused; }
    private _isPaused:boolean = false;


    get volume():number { return this._volume; }
    set volume(value:number) {
        if (value < 0) value = 0;
        if (value > 1) value = 1;

        let ratio:number = value / this._volume;

        this._volume = value;

        this._currentlyPlaying.forEach((audioSource:AudioSource):boolean => {
            audioSource.volume = audioSource.volume * ratio;

            return true;
        });
    }
    private _volume:number = 1;


    /*====================================================================*
     START: Signals
     *====================================================================*/
    audioEnded:Signal<AudioSource> = new Signal<AudioSource>();


    /*====================================================================*
     START: Public Methods
     *====================================================================*/
    tick(elapsedMilliseconds:number):void {
        this._currentlyPlaying.forEach((audioSource:AudioSource):boolean => {
            audioSource.tick(elapsedMilliseconds);
            return true;
        });
    }


    playBallOut():AudioManager {
        if (is.empty(this.audioContext)) return;

        let oscillator:OscillatorNode = this.audioContext.createOscillator();
        let gainNode:GainNode = this.audioContext.createGain();

        oscillator.connect(gainNode);
        gainNode.connect(this.audioContext.destination);
        gainNode.gain.value = this.volume;
        oscillator.start(this.audioContext.currentTime);

        oscillator.frequency.setValueAtTime(440, this.audioContext.currentTime);
        oscillator.frequency.linearRampToValueAtTime(440, this.audioContext.currentTime + 0.10);
        oscillator.frequency.linearRampToValueAtTime(220, this.audioContext.currentTime + 0.35);

        this.gainNode.gain.linearRampToValueAtTime(0.004, this.audioContext.currentTime + 0.10);
        this.gainNode.gain.linearRampToValueAtTime(0, this.audioContext.currentTime + 0.35);

        return this;
    }


    playPing(frequency?:number):AudioManager {
        if (is.empty(this.audioContext)) return;

        let oscillator:OscillatorNode = this.audioContext.createOscillator();
        let gainNode:GainNode = this.audioContext.createGain();

        oscillator.connect(gainNode);
        gainNode.connect(this.audioContext.destination);
        gainNode.gain.value = this.volume;
        oscillator.start(this.audioContext.currentTime);

        if (is.empty(frequency)) oscillator.frequency.setValueAtTime(220 + Math.random() * 660, this.audioContext.currentTime);
        else oscillator.frequency.setValueAtTime(frequency, this.audioContext.currentTime);
        this.gainNode.gain.linearRampToValueAtTime(0.004, this.audioContext.currentTime + 0.20);
        this.gainNode.gain.linearRampToValueAtTime(0, this.audioContext.currentTime + 0.40);

        return this;
    }


    preload(url:string):AudioSource {
        if (is.empty(url)) return null;

        //----- A new AudioSource will be created. If the audio ArrayBuffer has been cached,
        //----- the new AudioSource will look for the cached content when it loads.
        let audioSource:AudioSource = new AudioSource(this, url, this._cache, true);

        audioSource.playbackStarted.listen((audioSource:AudioSource) => {
            this._currentlyPlaying.add(audioSource);
        });
        audioSource.ended.listen((audioSource:AudioSource) => {
            if (this._currentlyPlaying.has(audioSource)) this._currentlyPlaying.delete(audioSource);
        });

        return audioSource;
    }


    release(url:string):AudioManager {
        this._cache.release(url);
        return this;
    }


    releaseAll():AudioManager {
        this._cache.releaseAll();
        return this;
    }


    play(
        url?:string,
        isStopCurrentlyPlaying:boolean = true,
        isLoop:boolean = false,
        isCache:boolean = false,
        durationIn:number = 0,
        easeIn?:IAnimationEaseFunction,
        durationOut?:number,
        easeOut?:IAnimationEaseFunction
    ):AudioSource {
        this._isPaused = false;

        let audioSource:AudioSource;

        if (is.notEmpty(url)) {
            //----- A new AudioSource will be created. If the audio ArrayBuffer has been cached,
            //----- the new AudioSource will look for the cached content when it loads.
            audioSource = new AudioSource(this, url, this._cache, isCache);

            audioSource.playbackStarted.listen((audioSource:AudioSource) => {
                if (!this._currentlyPlaying.has(audioSource)) this._currentlyPlaying.add(audioSource);
            });

            audioSource.ended.listen((audioSource:AudioSource) => {
                if (this._currentlyPlaying.has(audioSource)) this._currentlyPlaying.delete(audioSource);
            });

            audioSource.loaded.listen(() => {
                if (!audioSource.isPaused) audioSource.play(true, durationIn, easeIn, isLoop);
            });

            this._playAudioSource(audioSource, isStopCurrentlyPlaying, durationIn, easeIn, durationOut, easeOut);
        }
        else {
            this._currentlyPlaying.forEach((audioSource:AudioSource):boolean => {
                audioSource.play(); // Resume playback
                return true;
            });
        }

        return audioSource;
    }


    private _playAudioSource(
        audioSource:AudioSource, isStopCurrentlyPlaying:boolean = true,
        durationIn:number = 0, easeIn?:IAnimationEaseFunction,
        durationOut?:number, easeOut?:IAnimationEaseFunction
    ):AudioSource {
        this._isPaused = false;

        if (isStopCurrentlyPlaying) {
            if (is.empty(durationOut)) durationOut = durationIn;
            if (is.empty(easeOut)) easeOut = easeIn;
            this.stop(durationOut, easeOut);
            if (durationOut === 0) {
                this._currentlyPlaying.clear();
                this._currentlyPlaying.add(audioSource);
            }
        } else if (!this._currentlyPlaying.has(audioSource)) this._currentlyPlaying.add(audioSource);

        return audioSource;
    }


    pause(duration:number = 0, ease?:IAnimationEaseFunction):boolean {
        if (is.empty(this._currentlyPlaying) || this._isPaused) return false;

        let isPaused:boolean = false;
        this._currentlyPlaying.forEach((audioSource:AudioSource):boolean => {
            if (audioSource.isLoaded && !audioSource.isPaused) {
                if (audioSource.pause(duration, true, ease))
                    isPaused = true;
            }
            return true;
        });

        if (isPaused) {
            this._isPaused = true;
            return true;
        } else return false;
    }


    stop(duration:number = 0, ease?:IAnimationEaseFunction):AudioManager {
        if (is.empty(this._currentlyPlaying)) return this;
        this._currentlyPlaying.forEach((audioSource:AudioSource):boolean => {
            audioSource.stop(duration, true, ease);
            return true;
        });
        this._currentlyPlaying.clear();

        this._isPaused = false;

        return this;
    }

} // End of class


export default AudioManager;
