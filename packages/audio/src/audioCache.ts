import { Cache } from '@pilotlab/cache';
import { Result, IPromise } from '@pilotlab/result';
import Http from '@pilotlab/http';


export class AudioCache extends Cache<ArrayBuffer> {
    constructor() { super(); }


    static audio:AudioCache = new AudioCache();


    protected p_createNew(key:string, result:Result<ArrayBuffer>, isCache:boolean):IPromise<ArrayBuffer> {
        Http.getArrayBuffer(key).then((audio:ArrayBuffer) => { result.resolve(audio); });
        return result;
    }
} // End of class


export default AudioCache;
