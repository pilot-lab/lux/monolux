import is from '@pilotlab/is';
import Debug from '@pilotlab/debug';
import Result from '@pilotlab/result';
import { Signal } from '@pilotlab/signals';
import { IAnimationEaseFunction, AnimationValue, AnimationEventArgs } from '@pilotlab/animation';
import AudioCache from './audioCache';
import AudioManager from './audioManager';
import Animation from "@pilotlab/animation/src/animation";


export class AudioSource {
    constructor(audioManager:AudioManager, url:string, cache?:AudioCache, isCache:boolean = false) {
        this.p_audioManager = audioManager;
        this.load(url, cache, isCache);
    }


    /*====================================================================*
     START: Properties
     *====================================================================*/
    protected p_audioManager:AudioManager;
    protected p_buffer:AudioBuffer;
    protected p_url:string = '';
    protected p_source:AudioBufferSourceNode;
    protected p_isStarted:boolean = false;
    protected p_isStopped:boolean = false;
    protected p_isDisconnected:boolean = false;
    protected p_isLoading:boolean = false;
    protected p_gainNode:GainNode; // For cross-fade volume control.
    protected p_endFadeDuration:number = 0;
    protected p_endFadeEase:IAnimationEaseFunction;
    protected p_startedAtMilliseconds:number;
    protected p_pausedAtMilliseconds:number;


    get isLoaded():boolean { return this.p_isLoaded; }
    protected p_isLoaded:boolean = false;


    get isPaused():boolean { return is.notEmpty(this.p_pausedAtMilliseconds); }


    get volume():number { return this.p_gainNode.gain.value; }
    set volume(value:number) {
        if (value < 0) value = 0;
        if (value > 1) value = 1;

        this.p_gainNode.gain.value = value;
    }


    get duration():number {
        if (is.empty(this.p_buffer)) return 0;
        else return this.p_buffer.duration;
    }


    /*====================================================================*
     START: Signals
     *====================================================================*/
    loaded:Signal<AudioSource> = new Signal<AudioSource>();
    ended:Signal<AudioSource> = new Signal<AudioSource>();
    endFadeStarted:Signal<AudioSource> = new Signal<AudioSource>();
    playbackStarted:Signal<AudioSource> = new Signal<AudioSource>();
    paused:Signal<AudioSource> = new Signal<AudioSource>();


    /*====================================================================*
     START: Public Methods
     *====================================================================*/
    load(url:string, cache?:AudioCache, isCache:boolean = false):Result<AudioSource> {
        let result:Result<AudioSource> = new Result<AudioSource>();
        this.p_url = url;

        this.p_isDisconnected = false;
        this.p_isLoading = true;
        this.p_isLoaded = false;

        if (is.empty(cache)) cache = AudioCache.audio;


        cache.get(url, isCache).then((audio:ArrayBuffer) => {
            this.p_audioManager.audioContext.decodeAudioData(audio, (buffer) => {
                this.p_buffer = <AudioBuffer>buffer;
                this.p_isLoading = false;
                this.p_isLoaded = true;
                this.loaded.dispatch(this);
                result.resolve(this);
            });
        });

        return result;
    }


    get elapsedMilliseconds():number {
        let elapsed:number = 0;

        if (is.empty(this.p_startedAtMilliseconds)) return elapsed;

        if (this.isPaused) elapsed = this.p_pausedAtMilliseconds - this.p_startedAtMilliseconds;
        else elapsed = Date.now() - this.p_startedAtMilliseconds;

        return elapsed;
    }


    tick(elapsedMilliseconds:number):void {
        if (is.notEmpty(this.p_animGain)) this.p_animGain.internalTick(elapsedMilliseconds);

        if (!this.isPaused && is.notEmpty(this.p_buffer) && this.p_endFadeDuration > 0) {
            if (this.elapsedMilliseconds >= (this.duration - this.p_endFadeDuration)) {
                this.endFadeStarted.dispatch(this);
                this.fade(this.p_endFadeDuration, this.volume, 0, this.p_endFadeEase);
            }
        }
    }


    protected p_animGain:Animation;


    play(bFromBeginning:boolean = false, duration:number = 0, ease?:IAnimationEaseFunction, isLoop:boolean = false):AudioSource {
        this._setupBuffer();

        try {
            //----- handle volume fade-in
            if (duration > 0) {
                this.fade(duration, 0, this.p_audioManager.volume, ease);
            } else this.p_gainNode.gain.value = this.p_audioManager.volume;

            //----- start playback
            if (this.p_isLoaded && is.notEmpty(this.p_source)) {
                if (!bFromBeginning && is.notEmpty(this.p_pausedAtMilliseconds)) {
                    this.p_startedAtMilliseconds = Date.now() - this.p_pausedAtMilliseconds;
                    this.p_source.loop = isLoop;
                    this.p_source.start(0, this.p_pausedAtMilliseconds / 1000);
                } else {
                    this.p_startedAtMilliseconds = Date.now();
                    this.p_source.loop = isLoop;
                    this.p_source.start(0);
                }

                this.p_isStarted = true;
                this.p_isStopped = false;
                this.playbackStarted.dispatch(this);
            } else if (this.p_isLoading) {
                this.loaded.listenOnce(() => {
                    this._setupBuffer();
                    if (!this.p_isDisconnected) {
                        this.p_startedAtMilliseconds = Date.now();
                        this.p_source.loop = isLoop;
                        this.p_source.start(0);
                        this.p_isStarted = true;
                        this.p_isStopped = false;
                        this.playbackStarted.dispatch(this);
                    }
                });
            } else if (this.p_url != '') {
                this.load(this.p_url).then(() => {
                    this._setupBuffer();
                    this.p_startedAtMilliseconds = Date.now();
                    this.p_source.loop = isLoop;
                    this.p_source.start(0);
                    this.p_isStarted = true;
                    this.p_isStopped = false;
                    this.playbackStarted.dispatch(this);
                });
            }

            this.p_pausedAtMilliseconds = null;
        } catch (e) { Debug.error(e, 'AudioSource.play()'); }

        return this;
    }


    public pause(duration:number = 0, isUseManagerVolume:boolean = true, ease?:IAnimationEaseFunction):boolean {
        if (!this.p_isLoaded
            || !this.p_isStarted
            || is.empty(this.p_source)
            || is.notEmpty(this.p_pausedAtMilliseconds)
            || is.empty(this.p_source.buffer))
            return false;

        //----- handle volume fade-out
        if (duration > 0) {
            let volume = isUseManagerVolume ? this.p_audioManager.volume : this.volume;
            this.fade(duration, volume, 0, ease).then(() => {
                this.pause();
            });
            return true;
        }

        this.p_pausedAtMilliseconds = Date.now() - this.p_startedAtMilliseconds;
        if (is.notEmpty(this.p_source)) this.p_source.stop(0);
        this.p_isStopped = true;
        this.p_isStarted = false;
        this.paused.dispatch(this);

        return true;
    }


    public stop(duration:number = 0, isUseManagerVolume:boolean = true, ease?:IAnimationEaseFunction):AudioSource {
        //----- handle volume fade-out
        if (duration > 0) {
            let volume = isUseManagerVolume ? this.p_audioManager.volume : this.volume;
            this.fade(duration, volume, 0, ease).then(() => {
                this.stop();
                this.ended.dispatch(this);
            });
            return;
        }

        this.p_isDisconnected = true;
        this.p_pausedAtMilliseconds = null;

        this.p_isStopped = true;
        this.p_isStarted = false;

        this.p_isLoaded = false;

        if (this.p_isLoading) {
            this.loaded.listenOnce(() => {
                this.stop();
                this.ended.dispatch(this);
            });
            return this;
        }
        else if (is.notEmpty(this.p_source)) {
            this.p_source.stop(0);
            this.p_source.disconnect();
        }

        this.p_source = null;
        this.ended.dispatch(this);

        return this;
    }


    public fade(duration:number, from?:number, to?:number, ease?:IAnimationEaseFunction):Result<AudioSource> {
        let result:Result<AudioSource> = new Result<AudioSource>();

        if (is.empty(from)) {
            from = this.p_gainNode.gain.value;
        }

        if (is.empty(to)) {
            if (this.p_gainNode.gain.value >= this.p_audioManager.volume) to = 0;
            else to = this.p_audioManager.volume;
        }

        if (is.notEmpty(this.p_animGain)) {
            this.p_animGain.interrupt();
            this.p_animGain = null;
        }

        this.p_gainNode.gain.value = 0;
        this.p_animGain = new Animation([new AnimationValue(from, to)], duration, ease, 0, 'audio_gain');
        this.p_animGain.completed.listenOnce(() => {
            this.p_animGain = null;
            result.resolve(this);
        });

        this.p_animGain.ticked.listen((args:AnimationEventArgs) => {
            if (args.progress < 1) this.p_gainNode.gain.value = args.values[0].current;
            else this.p_gainNode.gain.value = to;
        }, this);

        return result;
    }


    fadeAtEnd(duration:number = 3, ease?:IAnimationEaseFunction):void {
        this.p_endFadeDuration = duration;
        this.p_endFadeEase = ease;
    }


    /*====================================================================*
     START: Private Methods
     *====================================================================*/
    private _setupBuffer() {
        try {
            this.p_source = this.p_audioManager.audioContext.createBufferSource();   // create a sound source
            this.p_source.buffer = this.p_buffer;                                    // tell the source which sound to play
            this.p_gainNode = this.p_audioManager.audioContext.createGain();         // create a gain node for volume control
            this.p_source.connect(this.p_gainNode);                                  // connect source to gain
            this.p_gainNode.connect(this.p_audioManager.audioContext.destination);   // connect the gain to the context's destination (the speakers)
            this.p_gainNode.gain.value = 0;                                          // initialize gain to 0 (we'll set it appropriately when the sound plays)

            this.p_source.onended = () => {
                if (is.notEmpty(this.p_pausedAtMilliseconds)) return;
                this.ended.dispatch(this);
                this.p_audioManager.audioEnded.dispatch(this);
            };
        } catch (e) { Debug.error(e, 'AudioSource._setupBuffer()'); }
    }
} // End of class


export default AudioSource;
