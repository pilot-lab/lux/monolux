import { IPromise, Result } from '@pilotlab/result';
import GitTools from './gitTools';


export class Monolux {
    static get rootPackageName():string { return this._rootPackageName; }
    private static _rootPackageName:string = 'rootPackage';


    static findPackagesToUpdate(packageInfoAll:any, lastTag:string):any[] {
        const packageNames:string[] = Object.keys(packageInfoAll);
        const dirty:string[] = [];

        for (let i:number = 0; i < packageNames.length; i++) {
            const packageName:string = packageNames[i];
            if (packageName === this.rootPackageName) continue;
            const { packagePath, info } = packageInfoAll[packageName];

            GitTools.diff(packagePath, lastTag).then((diff:string) => {
                if (diff !== '') {
                    const numChanges = diff.split('\n').length;

                    // mainStory.info(
                    //     `- Package ${pkgName} (currently ${chalk.cyan.bold(
                    //         packageInfo.version
                    //     )}) has changed (#files: ${numChanges})`
                    // );

                    dirty.push(packageName);
                }
            });

        }

        return dirty;
    };
}


export default Monolux;
