import { IPromise } from '@pilotlab/result';
export declare class GitTools {
    static diff(path: string, sinceTag: string): IPromise<string>;
}
export default GitTools;
