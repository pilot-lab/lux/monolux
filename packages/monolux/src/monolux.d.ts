export declare class Monolux {
    static readonly rootPackageName: string;
    private static _rootPackageName;
    static findPackagesToUpdate(packageInfoAll: any, lastTag: string): any[];
}
export default Monolux;
