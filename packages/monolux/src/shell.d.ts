import { IPromise } from '@pilotlab/result';
export interface IShellExecuteReturn {
    code: number;
    stdout: string;
    stderr: any;
}
export declare class Shell {
    static execute(cmd: string, cwd?: string, isIgnoreErrors?: boolean): IPromise<IShellExecuteReturn>;
    private static _execError(cmd, cwd, code, stdout, stderr);
}
export default Shell;
