const execa = require('execa');
const split = require('split');
import { Debug } from '@pilotlab/debug';
import { IPromise, Result } from '@pilotlab/result';


export interface IShellExecuteReturn {
    code:number;
    stdout:string;
    stderr:any;
}


export class Shell {
    static execute(cmd:string, cwd?:string, isIgnoreErrors:boolean = false):IPromise<IShellExecuteReturn> {
        const result:Result<IShellExecuteReturn> = new Result<IShellExecuteReturn>();

        try {
            const src = cmd.split(' ')[0].slice(0, 10);

            const child = execa.shell(cmd, {
                cwd: cwd || '.',
                // Workaround for Node.js bug: https://github.com/nodejs/node/issues/10836
                // See also: https://github.com/yarnpkg/yarn/issues/2462
                stdio:
                    process.platform === 'win32' ? ['ignore', 'pipe', 'pipe'] : undefined,
            });

            child.stdout.pipe(split()).on('data', (line:any) => {
                Debug.info(src, `${line}`);
            });

            child.stderr.pipe(split()).on('data', (line:any) => {
                if (line) Debug.error(src, `${line}`);
            });

            child.then((value:IShellExecuteReturn) => {
                Debug.data(value.code);
                if (value.code !== 0 && !isIgnoreErrors) {
                    throw this._execError(cmd, cwd, value.code, value.stdout, value.stderr);
                }

                result.resolve(value);
            });
        } catch (error) {
            if (error.code && isIgnoreErrors) {
                const { code, stdout, stderr } = error;
                result.resolve({ code, stdout, stderr });
                return result;
            }

            const errorNew:any = this._execError(cmd, cwd, error.code, error.stdout, error.stderr);

            Debug.error(errorNew.message);
            throw errorNew;
        }

        return result;
    }


    private static _execError(cmd:any, cwd:any, code:number, stdout:string, stderr:any):any {
        const errorMsg = `Command '${cmd}' failed ${code != null ? `[${code}]` : ''} at ${cwd || "'.'"}`;
        const error:any = new Error(errorMsg);
        error.code = code;
        error.stdout = stdout;
        error.stderr = stderr;
        return error;
    };
}


export default Shell;
