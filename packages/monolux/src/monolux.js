"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const gitTools_1 = require("./gitTools");
class Monolux {
    static get rootPackageName() { return this._rootPackageName; }
    static findPackagesToUpdate(packageInfoAll, lastTag) {
        const packageNames = Object.keys(packageInfoAll);
        const dirty = [];
        for (let i = 0; i < packageNames.length; i++) {
            const packageName = packageNames[i];
            if (packageName === this.rootPackageName)
                continue;
            const { packagePath, info } = packageInfoAll[packageName];
            gitTools_1.default.diff(packagePath, lastTag).then((diff) => {
                if (diff !== '') {
                    const numChanges = diff.split('\n').length;
                    dirty.push(packageName);
                }
            });
        }
        return dirty;
    }
    ;
}
Monolux._rootPackageName = 'rootPackage';
exports.Monolux = Monolux;
exports.default = Monolux;
//# sourceMappingURL=monolux.js.map