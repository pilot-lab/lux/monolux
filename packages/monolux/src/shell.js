"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const execa = require('execa');
const split = require('split');
const debug_1 = require("@pilotlab/debug");
const result_1 = require("@pilotlab/result");
class Shell {
    static execute(cmd, cwd, isIgnoreErrors = false) {
        const result = new result_1.Result();
        try {
            const src = cmd.split(' ')[0].slice(0, 10);
            const child = execa.shell(cmd, {
                cwd: cwd || '.',
                stdio: process.platform === 'win32' ? ['ignore', 'pipe', 'pipe'] : undefined,
            });
            child.stdout.pipe(split()).on('data', (line) => {
                debug_1.Debug.info(src, `${line}`);
            });
            child.stderr.pipe(split()).on('data', (line) => {
                if (line)
                    debug_1.Debug.error(src, `${line}`);
            });
            child.then((value) => {
                debug_1.Debug.data(value.code);
                if (value.code !== 0 && !isIgnoreErrors) {
                    throw this._execError(cmd, cwd, value.code, value.stdout, value.stderr);
                }
                result.resolve(value);
            });
        }
        catch (error) {
            if (error.code && isIgnoreErrors) {
                const { code, stdout, stderr } = error;
                result.resolve({ code, stdout, stderr });
                return result;
            }
            const errorNew = this._execError(cmd, cwd, error.code, error.stdout, error.stderr);
            debug_1.Debug.error(errorNew.message);
            throw errorNew;
        }
        return result;
    }
    static _execError(cmd, cwd, code, stdout, stderr) {
        const errorMsg = `Command '${cmd}' failed ${code != null ? `[${code}]` : ''} at ${cwd || "'.'"}`;
        const error = new Error(errorMsg);
        error.code = code;
        error.stdout = stdout;
        error.stderr = stderr;
        return error;
    }
    ;
}
exports.Shell = Shell;
exports.default = Shell;
//# sourceMappingURL=shell.js.map