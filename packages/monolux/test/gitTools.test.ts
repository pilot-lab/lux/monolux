import {
    AsyncTest,
    Setup,
    SetupFixture,
    Teardown,
    TeardownFixture,
    Test,
    TestCase,
    TestFixture,
    Timeout,
    Expect
} from 'alsatian';

import { Debug } from '@pilotlab/debug';

import {
    GitTools,
    Monolux
} from '../index';



@TestFixture('GitTools')
export class Tests {
    @TestCase('v0.1.54')
    @Timeout(10000) // Alsatian will now wait 5 seconds before failing
    public async asyncTest(testValue:string) {
        const promise:Promise<any> = new Promise((resolve, reject) => {
            GitTools.diff('../', testValue).then((diff:string) => {
                Debug.log(diff, 'git diff');
            });
        });
    }
}
