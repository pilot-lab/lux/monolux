import {
    AsyncTest,
    Setup,
    SetupFixture,
    Teardown,
    TeardownFixture,
    Test,
    TestCase,
    TestFixture,
    Timeout,
    Expect
} from 'alsatian';


import is from '@pilotlab/is';
import { Debug } from '@pilotlab/debug';


import {
    GitTools,
    Monolux,
    Shell
} from '../index';



@TestFixture('ShellTools')
export class Tests {
    @TestCase('v0.1.54')
    @Timeout(10000) // Alsatian will now wait 5 seconds before failing
    public async asyncTest(testValue:string) {
        const promise:Promise<any> = new Promise((resolve, reject) => {
            Shell.execute('ls', '.', false).then((value:any) => {
                if (is.notEmpty(value)) Debug.data(value, 'ShellTools.exec return value');
            });
        });
    }
}
