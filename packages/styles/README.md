# attributes

A base class wrapper for JavaScript objects that provides events listeners for value changes, serialization methods, and property animation.

## Install

`sudo npm install --save @pilotlab/attributes`

## Attribute: Public fields

```
/*====================================================================*
 START: Factory
 *====================================================================*/
create:AttributeFactory;


/*====================================================================*
 START: Properties
 *====================================================================*/
/**
 * A UI-friendly title for the attribute that can be presented to users.
 * If no label is set, the name of the attribute will be returned.
 */
label:string;


/**
 * The value of the associated data.
 */
value:any;
valueTarget:any;
valuePrevious:any;


dataType:DataType;


/**
 * Attributes will be considered empty if no name or value has been set.
 */
isEmpty:boolean;


/**
 * If this function returns true, the attribute will be omitted from data transfer objects
 * when saving data to the data store.
 */
omit:(value:any) => boolean;


validate:(value:any) => any;


onSet:(value:any) => void;


copy:Attribute;


/**
 * An automatically generated unique ID that is used when animating attribute values.
 */
animationID:string;


/*====================================================================*
 START: Public Methods
 *====================================================================*/
/**
 * Save the value to the attribute.
 * @returns {boolean} Returns true if a change was made to the attribute value.
 */
set(
    value:any,
    changeOptions?:ChangeOptions,
    omit?:(value:any) => boolean,
    isValidate?:boolean
):AttributeSetReturn<AttributeBase<any, any, any, any>>;


/**
 * Interrupt any animated value transitions that are in effect.
 */
interrupt():void;


/**
 * Manually trigger a dispatch on the 'changed' and/or saveTriggered signals.
 */
doChanged(isSignalChange?:boolean, isSave?:boolean):void;


toObject(isSaveDataTypes?:boolean, appendToObject?:any, isForceInclude?:boolean):any;
toJson(isSaveDataTypes?:boolean):string;
```

## AttributeCollection: Public fields

```
/*====================================================================*
 START: Factory
 *====================================================================*/
create:AttributeFactory;


/*====================================================================*
 START: Overrides
 *====================================================================*/
//----- Overrides save(...) to require the value parameter.
set(
    name:string,
    value:any,
    dataType?:DataType,
    changeOptions?:ChangeOptions,
    index?:number,
    omit?:(value:any) => boolean
):AttributeSetReturn<AttributeBase<any, any, any, any>>;
```

## AttributeFactory: Public fields

```
newNode<any>(createOptions?:AttributeCreateOptions):Attribute;
newCollection(parent?:Attribute<AttributeCollection>):AttributeCollection;
```

## Usage

```
import {AttributeBase, AttributeCollectionBase} from '@pilotlab/attributes';
```
