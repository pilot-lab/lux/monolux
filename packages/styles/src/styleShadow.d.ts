import { IAttribute, Attributes, Point } from '@pilotlab/attributes';
import StyleElement from './styleElement';
export declare class StyleShadow extends StyleElement {
    constructor(data?: (StyleShadow | Attributes | Object | string));
    opacity: number;
    protected p_opacity: IAttribute;
    blur: number;
    protected p_blur: IAttribute;
    offset: Point;
    protected p_offset: Point;
}
export default StyleShadow;
