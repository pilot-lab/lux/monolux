import {
    DataType,
    IAttribute,
    Attributes,
    AttributeChangeOptions,
    Point
} from '@pilotlab/attributes';
import StyleElement from './styleElement';


export class StyleShadow extends StyleElement {
     constructor(data?:(StyleShadow | Attributes | Object | string)) {
         super(data);

         this.p_offset = <Point>this.children.get('offset', new Point(0, 0), DataType.POINT, null, AttributeChangeOptions.zero);
         this.p_opacity = this.children.get('opacity', 0, DataType.NUMBER_DOUBLE, null, AttributeChangeOptions.zero);
         this.p_blur = this.children.get('blur', 0, DataType.NUMBER_DOUBLE, null, AttributeChangeOptions.zero);
    }


    get opacity():number { return this.p_opacity.value; }
    set opacity(value:number) { this.p_opacity.set(value, AttributeChangeOptions.save.durationZero); }
    protected p_opacity:IAttribute;


    get blur():number { return this.p_blur.value; }
    set blur(value:number) { this.p_blur.set(value, AttributeChangeOptions.save.durationZero); }
    protected p_blur:IAttribute;


    get offset():Point { return this.p_offset; }
    set offset(value:Point) { this.p_offset.set(value, AttributeChangeOptions.save.durationZero); }
    protected p_offset:Point;
} // End Class


export default StyleShadow;
