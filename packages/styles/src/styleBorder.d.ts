import { IAttribute, Attributes } from '@pilotlab/attributes';
import StyleLine from './styleLine';
export declare class StyleBorder extends StyleLine {
    constructor(data?: (StyleBorder | Attributes | Object | string));
    insetFactor: number;
    protected p_insetFactor: IAttribute;
}
export default StyleBorder;
