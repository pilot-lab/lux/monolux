import {
    DataType,
    IAttribute,
    Attributes,
    AttributeChangeOptions
} from '@pilotlab/attributes';
import StyleLine from './styleLine';


export class StyleBorder extends StyleLine {
    constructor(data?:(StyleBorder | Attributes | Object | string)) {
        super(data);
        this.p_insetFactor = this.children.get('insetFactor', 0, DataType.NUMBER_DOUBLE, null, AttributeChangeOptions.zero);
    }


    get insetFactor():number { return this.p_insetFactor.value; }
    set insetFactor(value:number) { this.p_insetFactor.set(value, AttributeChangeOptions.save.durationZero); }
    protected p_insetFactor:IAttribute;
} // End Class


export default StyleBorder;
