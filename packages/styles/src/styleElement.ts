import is from '@pilotlab/is';
import {
    DataType,
    Attribute,
    AttributeBase,
    AttributeRoot,
    Attributes,
    AttributeChangeOptions,
    Color,
    IAttribute,
    IAttributeFactory,
    IAttributes,
    IColor,
    NodeType
} from '@pilotlab/data';
import IStyleElement from "./interfaces/iStyleElement";


export interface IStyleElementAttributes extends IAttributes {
    readonly color:Color;
}


export class StyleElement
    extends AttributeBase<any, IAttribute, Attributes, AttributeRoot>
    implements IStyleElement {
    constructor(color?:IColor, label?:string, key?:string) {
        super(Attribute.create, null, DataType.COLLECTION, label, key, NodeType.COLLECTION, true);

        this.attributes.set('color', color, DataType.COLOR, null, null, AttributeChangeOptions.zero);

        const defaultValue:IColor = Color.empty;
        const formatter = (value:number) => is.empty(value) ? defaultValue : value;
        this.attributes.color.formatters.add(formatter);
    }


    /*====================================================================*
     START: Static
     *====================================================================*/
    static get empty():IStyleElement { return new StyleElement();  }
    static get create():IAttributeFactory { return Attribute.create }


    /*====================================================================*
     START: Property placeholders
     *====================================================================*/
    readonly color:IColor;
    readonly attributes:IStyleElementAttributes;


    get isEmpty():boolean {  return is.empty(this.color); }

} // End of class


export default StyleElement;
