import {
    DataType,
    AttributeDouble,
    AttributeChangeOptions,
    IColor
} from '@pilotlab/data';
import { StyleElement, IStyleElementAttributes } from './styleElement';


export interface IStyleLineAttributes extends IStyleElementAttributes {
    readonly thickness:AttributeDouble;
}


export class StyleLine extends StyleElement {
    constructor(color?:IColor, thickness:number = 1) {
        super(color);
        this.attributes.set('thickness', thickness, DataType.NUMBER_DOUBLE, null, null, AttributeChangeOptions.zero);
    }


    thickness:number;
    readonly attributes:IStyleLineAttributes;
} // End Class


export default StyleLine;
