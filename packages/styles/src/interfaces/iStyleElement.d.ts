import { IColor } from '@pilotlab/data';
export interface IStyleElement {
    color: IColor;
}
export default IStyleElement;
