import { IColor } from '@pilotlab/data';


export interface IStyleElement {
    color:IColor;
} // End interface


export default IStyleElement;
