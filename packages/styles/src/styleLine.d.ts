import { AttributeDouble, IColor } from '@pilotlab/data';
import { StyleElement, IStyleElementAttributes } from './styleElement';
export interface IStyleLineAttributes extends IStyleElementAttributes {
    readonly thickness: AttributeDouble;
}
export declare class StyleLine extends StyleElement {
    constructor(color?: IColor, thickness?: number);
    thickness: number;
    readonly attributes: IStyleLineAttributes;
}
export default StyleLine;
