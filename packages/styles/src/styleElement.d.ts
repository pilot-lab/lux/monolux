import { AttributeBase, AttributeRoot, Attributes, Color, IAttribute, IAttributeFactory, IAttributes, IColor } from '@pilotlab/data';
import IStyleElement from "./interfaces/iStyleElement";
export interface IStyleElementAttributes extends IAttributes {
    readonly color: Color;
}
export declare class StyleElement extends AttributeBase<any, IAttribute, Attributes, AttributeRoot> implements IStyleElement {
    constructor(color?: IColor, label?: string, key?: string);
    static readonly empty: IStyleElement;
    static readonly create: IAttributeFactory;
    readonly color: IColor;
    readonly attributes: IStyleElementAttributes;
    readonly isEmpty: boolean;
}
export default StyleElement;
