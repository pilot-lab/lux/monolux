import FileBase from './fileBase';


export class FileWeb extends FileBase {
    constructor(file:File) {
        super();
        this.p_file = file;
    }
} // End class


export default FileWeb;
