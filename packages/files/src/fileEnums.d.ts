export declare enum FileType {
    FILE_ENTRY = 0,
    FILE_ENTRY_DIRECTORY = 1,
    FILE_HTML = 2,
    FILE_IMAGE = 3,
    FILE_IMAGE_JPEG = 4,
    FILE_IMAGE_PNG = 5,
    FILE_JAVASCRIPT = 6,
    FILE_JSON = 7,
    FILE_SVG = 8,
    FILE_TEXT = 9,
    FILE_VIDEO = 10,
    FILE_XML = 11,
    FILE_ZIP = 12,
}
export declare enum FileCreateMode {
    ALLOW_DUPLICATES = 0,
    REPLACE_EXISTING = 1,
    RETURN_EXISTING = 2,
}
export declare enum FileErrorCode {
    NOT_SUPPORTED_ERR = 0,
    NOT_FOUND_ERR = 1,
    SECURITY_ERR = 2,
    NOT_READABLE_ERR = 4,
    ENCODING_ERR = 5,
    NO_MODIFICATION_ALLOWED_ERR = 6,
    INVALID_STATE_ERR = 7,
    INVALID_MODIFICATION_ERR = 9,
    QUOTA_EXCEEDED_ERR = 10,
    TYPE_MISMATCH_ERR = 11,
    PATH_EXISTS_ERR = 12,
}
