import { IPromise } from '@pilotlab/result';
import { DataType } from '@pilotlab/data';
import IFile from './interfaces/iFile';
import IFileDirectory from './interfaces/iFileDirectory';
export declare abstract class FileBase implements IFile {
    protected p_file: File;
    readonly name: string;
    readonly url: string;
    readonly isDirectory: boolean;
    readonly isFile: boolean;
    getParent(): IPromise<IFileDirectory>;
    delete(): IPromise<boolean>;
    getMimeType(): IPromise<string>;
    getDataType(): IPromise<DataType | string>;
    getSizeInBytes(): IPromise<number>;
    getBlob(): IPromise<Blob>;
    getFile(): IPromise<File>;
    getDataUrl(): IPromise<string>;
    readFile(): IPromise<string>;
    writeToFile(data: (string | Blob), dataType?: (DataType | string), isAppend?: boolean): IPromise<boolean>;
}
export default FileBase;
