import { DataType, Attribute } from '@pilotlab/data';
import { Http } from '@pilotlab/http';
import is from '@pilotlab/is';
import { IPromise, Result } from '@pilotlab/result';
import { FileType } from './fileEnums';


export class FileTools {
    static getMimeType(dataType:(string | DataType | FileType)): string {
        let mimeType: string = 'text/plain';

        if (typeof dataType === 'string') dataType = (<string>dataType).toLowerCase();
        else if (dataType in DataType) dataType = DataType[dataType].toLowerCase();  // Get the string representation of the enum value.
        else if (dataType in FileType) dataType = FileType[dataType].toLowerCase();  // Get the string representation of the enum value.

        switch (dataType) {
            case 'html':
            case 'string_html':
                mimeType = 'text/html';
                break;
            case 'file_image':
            case 'file_image_jpeg':
                mimeType = 'image/jpeg';
                break;
            case 'file_image_png':
                mimeType = 'image/png';
                break;
            case 'javascript':
            case 'string_javascript':
                mimeType = 'text/javascript';
                break;
            case 'json':
            case 'string_json':
                mimeType = 'application/json';
                break;
            case 'svg':
            case 'string_svg':
                mimeType = 'image/svg+xml';
                break;
            case 'video':
            case 'file_video':
                mimeType = 'video/mp4';
                break;
            case 'xml':
            case 'string_xml':
                mimeType = 'application/xml';
                break;
            case 'zip':
            case 'file_zip':
                mimeType = 'application/zip';
                break;
            case 'string':
            case 'file_text':
            case 'string_hex_value':
            default:
                mimeType = 'text/plain';
        }

        return mimeType;
    }


    static getFileType(mimeType:string):FileType {
        let dataType: FileType = FileType.FILE_TEXT;

        if (mimeType.indexOf('/') === 0) mimeType = FileTools.getMimeType(mimeType);

        switch (mimeType) {
            case 'image/jpeg':
                dataType = FileType.FILE_IMAGE_JPEG;
                break;
            case 'image/png':
                dataType = FileType.FILE_IMAGE_PNG;
                break;
            case 'text/javascript':
                dataType = FileType.FILE_JAVASCRIPT;
                break;
            case 'application/json':
                dataType = FileType.FILE_JSON;
                break;
            case 'image/svg+xml':
                dataType = FileType.FILE_SVG;
                break;
            case 'video/mp4':
            case 'video/webm':
            case 'video/ogg':
                dataType = FileType.FILE_VIDEO;
                break;
            case 'application/xml':
                dataType = FileType.FILE_XML;
                break;
            case 'application/zip':
                dataType = FileType.FILE_ZIP;
                break;
            case 'text/plain':
            default:
        }

        return dataType;
    }


    /// Blobs
    /**
     * Don't forget to free memory up when you're done, by calling AppManager.revokeObjectUrl(objectUrl).
     */
    static getDataUrl(file:(Blob | File), isTemporary:boolean = false):IPromise<string> {
        if (is.empty(file)) return Result.resolve(null);

        let result:Result<string> = new Result<string>();

        if (isTemporary) result.resolve(URL.createObjectURL(file));
        else {
            let reader = new FileReader();
            reader.addEventListener('load', () => { result.resolve(reader.result); }, false);

            if (is.notEmpty(file)) reader.readAsDataURL(file);
            else result.reject('No file provided.');
        }

        return result;
    }
    static revokeDataUrl(dataUrl:string) { URL.revokeObjectURL(dataUrl); }


    /**
     * https://github.com/blueimp/JavaScript-Canvas-to-Blob/blob/master/js/canvas-to-blob.js
     */
    static dataUrlToBlob(dataUrl:string, mimeType?:string):IPromise<Blob> {
        let result:Result<Blob> = new Result<Blob>();
        let matches = dataUrl.match(/^blob:/);

        if (is.notEmpty(matches)) {
            Http.getBlob(dataUrl).then((blob:Blob) => {
                result.resolve(blob);
            });

            return result;
        }

        /// Parse the dataUrl components as per RFC 2397
        let byteString:string;
        let dataUrlPattern = /^data:((.*?)(;charset=.*?)?)(;base64)?,/;
        matches = dataUrl.match(dataUrlPattern);

        if (is.empty(matches)) throw new Error('Invalid data URL passed to DataTools.dataUrlToBlob().');

        /// Default to text/plain;charset=US-ASCII
        if (is.empty(mimeType)) mimeType = matches[2] ? matches[1] : 'text/plain' + (matches[3] || ';charset=US-ASCII');

        let isBase64:boolean = !!matches[4];
        let dataString:string = dataUrl.slice(matches[0].length);

        if (isBase64) {
            /// Convert base64 to raw binary data held in a string:
            byteString = atob(dataString)
        } else {
            /// Convert base64/URLEncoded data component to raw binary:
            byteString = decodeURIComponent(dataString)
        }

        return result.resolve(this.byteStringToBlob(byteString, mimeType));
    }


    /**
     * http://stackoverflow.com/questions/16245767/creating-a-blob-from-a-base64-string-in-javascript
     * Performance can be improved a little by processing the byteCharacters in smaller slices,
     * rather than all at once. Apparently, 512 bytes is a good slice size.
     */
    static base64toBlob(base64Data:string, mimeType:string = '', sliceSize:number = 512):Blob {
        return this.byteStringToBlob(atob(base64Data), mimeType, sliceSize);
    }


    static byteStringToBlob(byteString:string, mimeType:string = '', sliceSize:number = 512):Blob {
        let byteArrays = [];

        for (let offset = 0; offset < byteString.length; offset += sliceSize) {
            let slice = byteString.slice(offset, offset + sliceSize);
            let bytes:number[] = new Array(slice.length);
            for (let i = 0; i < slice.length; i++) { bytes[i] = slice.charCodeAt(i); }
            byteArrays.push(new Uint8Array(bytes));
        }

        return new Blob(byteArrays, {type:mimeType});
    }


    static stringToBlob(value:string, type?:(DataType | string)):Blob {
        let mimeTypeString:string = 'text/plain';
        if (is.notEmpty(type)) mimeTypeString = FileTools.getMimeType(type);
        let blob:Blob = new Blob([value], { type:mimeTypeString });
        return blob;
    }


    static arrayBufferToBlob(value:ArrayBuffer, type?:(DataType | string)):Blob {
        let mimeTypeString:string = 'text/plain';
        if (is.notEmpty(type)) mimeTypeString = FileTools.getMimeType(type);
        let dataView:DataView = new DataView(value);
        let blob = new Blob([dataView], { type: mimeTypeString });
        return blob;
    }


    static blobToArrayBuffer(value:Blob, type?:(DataType | string)):IPromise<ArrayBuffer> {
        let result:Result<ArrayBuffer> = new Result<ArrayBuffer>();
        let arrayBuffer;
        let fileReader = new FileReader();
        fileReader.onload = () => {
            arrayBuffer = fileReader.result;
            result.resolve(arrayBuffer);
        };
        fileReader.readAsArrayBuffer(value);
        return result;
    }
} // End Class


export default FileTools;
