import { DataType } from '@pilotlab/data';
import { IPromise } from '@pilotlab/result';
import { FileType } from './fileEnums';
export declare class FileTools {
    static getMimeType(dataType: (string | DataType | FileType)): string;
    static getFileType(mimeType: string): FileType;
    static getDataUrl(file: (Blob | File), isTemporary?: boolean): IPromise<string>;
    static revokeDataUrl(dataUrl: string): void;
    static dataUrlToBlob(dataUrl: string, mimeType?: string): IPromise<Blob>;
    static base64toBlob(base64Data: string, mimeType?: string, sliceSize?: number): Blob;
    static byteStringToBlob(byteString: string, mimeType?: string, sliceSize?: number): Blob;
    static stringToBlob(value: string, type?: (DataType | string)): Blob;
    static arrayBufferToBlob(value: ArrayBuffer, type?: (DataType | string)): Blob;
    static blobToArrayBuffer(value: Blob, type?: (DataType | string)): IPromise<ArrayBuffer>;
}
export default FileTools;
