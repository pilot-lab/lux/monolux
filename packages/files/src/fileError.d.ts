import { FileErrorCode } from './fileEnums';
export declare class FileError extends Error {
    constructor(code?: FileErrorCode);
    readonly code: FileErrorCode;
    protected p_code: FileErrorCode;
}
export default FileError;
