import { FileErrorCode } from './fileEnums';


export class FileError extends Error {
    constructor(code:FileErrorCode = FileErrorCode.NOT_SUPPORTED_ERR) {
        super();
        this.p_code = code;
    }

    get code():FileErrorCode { return this.p_code; }
    protected p_code:FileErrorCode;
} // End class


export default FileError;
