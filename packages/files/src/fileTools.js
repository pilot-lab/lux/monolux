"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const data_1 = require("@pilotlab/data");
const http_1 = require("@pilotlab/http");
const is_1 = require("@pilotlab/is");
const result_1 = require("@pilotlab/result");
const fileEnums_1 = require("./fileEnums");
class FileTools {
    static getMimeType(dataType) {
        let mimeType = 'text/plain';
        if (typeof dataType === 'string')
            dataType = dataType.toLowerCase();
        else if (dataType in data_1.DataType)
            dataType = data_1.DataType[dataType].toLowerCase();
        else if (dataType in fileEnums_1.FileType)
            dataType = fileEnums_1.FileType[dataType].toLowerCase();
        switch (dataType) {
            case 'html':
            case 'string_html':
                mimeType = 'text/html';
                break;
            case 'file_image':
            case 'file_image_jpeg':
                mimeType = 'image/jpeg';
                break;
            case 'file_image_png':
                mimeType = 'image/png';
                break;
            case 'javascript':
            case 'string_javascript':
                mimeType = 'text/javascript';
                break;
            case 'json':
            case 'string_json':
                mimeType = 'application/json';
                break;
            case 'svg':
            case 'string_svg':
                mimeType = 'image/svg+xml';
                break;
            case 'video':
            case 'file_video':
                mimeType = 'video/mp4';
                break;
            case 'xml':
            case 'string_xml':
                mimeType = 'application/xml';
                break;
            case 'zip':
            case 'file_zip':
                mimeType = 'application/zip';
                break;
            case 'string':
            case 'file_text':
            case 'string_hex_value':
            default:
                mimeType = 'text/plain';
        }
        return mimeType;
    }
    static getFileType(mimeType) {
        let dataType = fileEnums_1.FileType.FILE_TEXT;
        if (mimeType.indexOf('/') === 0)
            mimeType = FileTools.getMimeType(mimeType);
        switch (mimeType) {
            case 'image/jpeg':
                dataType = fileEnums_1.FileType.FILE_IMAGE_JPEG;
                break;
            case 'image/png':
                dataType = fileEnums_1.FileType.FILE_IMAGE_PNG;
                break;
            case 'text/javascript':
                dataType = fileEnums_1.FileType.FILE_JAVASCRIPT;
                break;
            case 'application/json':
                dataType = fileEnums_1.FileType.FILE_JSON;
                break;
            case 'image/svg+xml':
                dataType = fileEnums_1.FileType.FILE_SVG;
                break;
            case 'video/mp4':
            case 'video/webm':
            case 'video/ogg':
                dataType = fileEnums_1.FileType.FILE_VIDEO;
                break;
            case 'application/xml':
                dataType = fileEnums_1.FileType.FILE_XML;
                break;
            case 'application/zip':
                dataType = fileEnums_1.FileType.FILE_ZIP;
                break;
            case 'text/plain':
            default:
        }
        return dataType;
    }
    static getDataUrl(file, isTemporary = false) {
        if (is_1.default.empty(file))
            return result_1.Result.resolve(null);
        let result = new result_1.Result();
        if (isTemporary)
            result.resolve(URL.createObjectURL(file));
        else {
            let reader = new FileReader();
            reader.addEventListener('load', () => { result.resolve(reader.result); }, false);
            if (is_1.default.notEmpty(file))
                reader.readAsDataURL(file);
            else
                result.reject('No file provided.');
        }
        return result;
    }
    static revokeDataUrl(dataUrl) { URL.revokeObjectURL(dataUrl); }
    static dataUrlToBlob(dataUrl, mimeType) {
        let result = new result_1.Result();
        let matches = dataUrl.match(/^blob:/);
        if (is_1.default.notEmpty(matches)) {
            http_1.Http.getBlob(dataUrl).then((blob) => {
                result.resolve(blob);
            });
            return result;
        }
        let byteString;
        let dataUrlPattern = /^data:((.*?)(;charset=.*?)?)(;base64)?,/;
        matches = dataUrl.match(dataUrlPattern);
        if (is_1.default.empty(matches))
            throw new Error('Invalid data URL passed to DataTools.dataUrlToBlob().');
        if (is_1.default.empty(mimeType))
            mimeType = matches[2] ? matches[1] : 'text/plain' + (matches[3] || ';charset=US-ASCII');
        let isBase64 = !!matches[4];
        let dataString = dataUrl.slice(matches[0].length);
        if (isBase64) {
            byteString = atob(dataString);
        }
        else {
            byteString = decodeURIComponent(dataString);
        }
        return result.resolve(this.byteStringToBlob(byteString, mimeType));
    }
    static base64toBlob(base64Data, mimeType = '', sliceSize = 512) {
        return this.byteStringToBlob(atob(base64Data), mimeType, sliceSize);
    }
    static byteStringToBlob(byteString, mimeType = '', sliceSize = 512) {
        let byteArrays = [];
        for (let offset = 0; offset < byteString.length; offset += sliceSize) {
            let slice = byteString.slice(offset, offset + sliceSize);
            let bytes = new Array(slice.length);
            for (let i = 0; i < slice.length; i++) {
                bytes[i] = slice.charCodeAt(i);
            }
            byteArrays.push(new Uint8Array(bytes));
        }
        return new Blob(byteArrays, { type: mimeType });
    }
    static stringToBlob(value, type) {
        let mimeTypeString = 'text/plain';
        if (is_1.default.notEmpty(type))
            mimeTypeString = FileTools.getMimeType(type);
        let blob = new Blob([value], { type: mimeTypeString });
        return blob;
    }
    static arrayBufferToBlob(value, type) {
        let mimeTypeString = 'text/plain';
        if (is_1.default.notEmpty(type))
            mimeTypeString = FileTools.getMimeType(type);
        let dataView = new DataView(value);
        let blob = new Blob([dataView], { type: mimeTypeString });
        return blob;
    }
    static blobToArrayBuffer(value, type) {
        let result = new result_1.Result();
        let arrayBuffer;
        let fileReader = new FileReader();
        fileReader.onload = () => {
            arrayBuffer = fileReader.result;
            result.resolve(arrayBuffer);
        };
        fileReader.readAsArrayBuffer(value);
        return result;
    }
}
exports.FileTools = FileTools;
exports.default = FileTools;
//# sourceMappingURL=fileTools.js.map