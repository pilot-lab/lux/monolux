"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const debug_1 = require("@pilotlab/debug");
const result_1 = require("@pilotlab/result");
const data_1 = require("@pilotlab/data");
const fileTools_1 = require("./fileTools");
class FileBase {
    get name() { return is_1.default.notEmpty(this.p_file) ? this.p_file.name : ' '; }
    get url() { return ''; }
    get isDirectory() { return false; }
    get isFile() { return true; }
    getParent() {
        let result = new result_1.Result();
        result.resolve(null);
        return result;
    }
    delete() {
        let result = new result_1.Result();
        result.resolve(false);
        return result;
    }
    getMimeType() {
        let result = new result_1.Result();
        result.resolve(is_1.default.notEmpty(this.p_file) ? this.p_file.type : '');
        return result;
    }
    getDataType() {
        let result = new result_1.Result();
        this.getMimeType().then((mimeType) => result.resolve(data_1.DataTools.getDataType(mimeType)));
        return result;
    }
    getSizeInBytes() {
        let result = new result_1.Result();
        this.getBlob().then((blob) => {
            result.resolve(blob.size);
        });
        return result;
    }
    getBlob() {
        let result = new result_1.Result();
        this.getFile().then((file) => {
            result.resolve(file);
        });
        return result;
    }
    getFile() {
        debug_1.default.warn('IFile.getFile() is not implemented in FileBase.');
        return result_1.Result.resolve(this.p_file);
    }
    getDataUrl() {
        let result = new result_1.Result();
        this.getBlob().then((blob) => {
            let url = URL.createObjectURL(blob);
            result.resolve(url);
        });
        return result;
    }
    readFile() {
        let result = new result_1.Result();
        result.catch((e) => { debug_1.default.error(e, 'FileBase.readFile(...)'); });
        this.getFile().then((file) => {
            let reader = new FileReader();
            reader.onloadend = () => { result.resolve(reader.result); };
            reader.readAsText(file);
        });
        return result;
    }
    writeToFile(data, dataType = data_1.DataType.STRING, isAppend = false) {
        let blob;
        let parts = [];
        let result = new result_1.Result();
        if (typeof data === 'string')
            blob = fileTools_1.default.stringToBlob(data, dataType);
        else
            blob = data;
        this.getFile().then((file) => {
            if (isAppend)
                parts.push(file);
            parts.push(blob);
            this.p_file = new Blob(parts, { type: file.type });
            result.resolve(true);
        });
        return result;
    }
}
exports.FileBase = FileBase;
exports.default = FileBase;
//# sourceMappingURL=fileBase.js.map