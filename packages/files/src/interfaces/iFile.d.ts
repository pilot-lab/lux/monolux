import { IPromise } from '@pilotlab/result';
import { DataType } from '@pilotlab/data';
import IEntry from './iEntry';
export interface IFile extends IEntry {
    getMimeType(): IPromise<string>;
    getDataType(): IPromise<DataType | string>;
    getSizeInBytes(): IPromise<number>;
    getBlob(): IPromise<Blob>;
    getDataUrl(): IPromise<string>;
    readFile(): IPromise<string>;
    writeToFile(data: (string | Blob), dataType?: (DataType | string), isAppend?: boolean): IPromise<boolean>;
}
export default IFile;
