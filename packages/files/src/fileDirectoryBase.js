"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const debug_1 = require("@pilotlab/debug");
const result_1 = require("@pilotlab/result");
class FileDirectoryBase {
    get isFile() { return false; }
    get isDirectory() { return true; }
    get name() { return ''; }
    get url() { return ''; }
    getParent() {
        debug_1.default.warn('IFileDirectory.getParent(...) is not implemented.');
        return result_1.Result.resolve(null);
    }
    delete() {
        debug_1.default.warn('IFileDirectory.delete(...) is not implemented.');
        return result_1.Result.resolve(false);
    }
    getDirectory(path) {
        debug_1.default.warn('IFileDirectory.getDirectory(...) is not implemented.');
        return result_1.Result.resolve(null);
    }
    createDirectory(path, mode) {
        debug_1.default.warn('IFileDirectory.createDirectory(...) is not implemented.');
        return result_1.Result.resolve(null);
    }
    getFile(path) {
        debug_1.default.warn('IFileDirectory.getFile(...) is not implemented.');
        return result_1.Result.resolve(null);
    }
    createFile(path, data, mode, ...options) {
        debug_1.default.warn('IFileDirectory.createFile(...) is not implemented.');
        return result_1.Result.resolve(null);
    }
    getDirectoryList() {
        debug_1.default.warn('IFileDirectory.getDirectoryList(...) is not implemented.');
        return result_1.Result.resolve(null);
    }
    clear() {
        debug_1.default.warn('IFileDirectory.clear(...) is not implemented.');
        return result_1.Result.resolve(null);
    }
}
exports.FileDirectoryBase = FileDirectoryBase;
exports.default = FileDirectoryBase;
//# sourceMappingURL=fileDirectoryBase.js.map