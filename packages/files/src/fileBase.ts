import is from '@pilotlab/is';
import Debug from '@pilotlab/debug';
import { IPromise, Result } from '@pilotlab/result';
import { DataType, DataTools } from '@pilotlab/data';
import FileTools from './fileTools';
import IFile from './interfaces/iFile';
import IFileDirectory from './interfaces/iFileDirectory';


export abstract class FileBase implements IFile {
    /*====================================================================*
     START: Properties
     *====================================================================*/
    protected p_file:File;


    get name():string { return is.notEmpty(this.p_file) ? this.p_file.name : ' '; }
    get url():string { return ''; }
    get isDirectory():boolean { return false; }
    get isFile():boolean { return true; }


    /*====================================================================*
     START: Methods
     *====================================================================*/
    getParent():IPromise<IFileDirectory> {
        let result:Result<IFileDirectory> = new Result<IFileDirectory>();
        result.resolve(null);
        return result;
    }


    delete():IPromise<boolean> {
        let result:Result<boolean> = new Result<boolean>();
        result.resolve(false);
        return result;
    }


    getMimeType():IPromise<string> {
        let result:Result<string> = new Result<string>();
        result.resolve(is.notEmpty(this.p_file) ? this.p_file.type : '');
        return result;
    }


    getDataType():IPromise<DataType | string> {
        let result:Result<DataType | string> = new Result<DataType | string>();
        this.getMimeType().then((mimeType:string) => result.resolve(DataTools.getDataType(mimeType)));
        return result;
    }


    getSizeInBytes():IPromise<number> {
        let result:Result<number> = new Result<number>();
        this.getBlob().then((blob:Blob) => {
            result.resolve(<number>blob.size)
        });
        return result;
    }


    getBlob():IPromise<Blob> {
        let result:Result<Blob> = new Result<Blob>();
        this.getFile().then((file:File) => {
            result.resolve(<Blob>file)
        });
        return result;
    }


    getFile():IPromise<File> {
        Debug.warn('IFile.getFile() is not implemented in FileBase.');
        return Result.resolve<File>(this.p_file);
    }


    getDataUrl():IPromise<string> {
        let result:Result<string> = new Result<string>();

        this.getBlob().then((blob)=> {
            let url = URL.createObjectURL(blob);
            result.resolve(url);
        });

        return result;
    }


    readFile():IPromise<string> {
        let result:Result<string> = new Result<string>();
        result.catch((e) => { Debug.error(e, 'FileBase.readFile(...)') });

        this.getFile().then((file:File) => {
            let reader:FileReader = new FileReader();
            reader.onloadend = () => { result.resolve(reader.result); };
            reader.readAsText(file);
        });

        return result;
    }


    writeToFile(data:(string | Blob), dataType:(DataType | string) = DataType.STRING, isAppend:boolean = false):IPromise<boolean> {
        let blob:Blob;
        let parts:any[] = [];
        let result:Result<boolean> = new Result<boolean>();

        if (typeof data === 'string') blob = FileTools.stringToBlob(data, dataType);
        else blob = data;

        this.getFile().then((file:File) => {
            if (isAppend) parts.push(file);

            parts.push(blob);
            this.p_file = <File>new Blob(parts, { type: file.type});
            result.resolve(true);
        });

        return result;
    }
} // End class


export default FileBase;
