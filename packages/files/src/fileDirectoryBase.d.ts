import { IPromise } from '@pilotlab/result';
import { List } from '@pilotlab/collections';
import IEntry from './interfaces/iEntry';
import IFile from './interfaces/iFile';
import IFileDirectory from './interfaces/iFileDirectory';
import { FileCreateMode } from './fileEnums';
export declare abstract class FileDirectoryBase implements IFileDirectory {
    readonly isFile: boolean;
    readonly isDirectory: boolean;
    readonly name: string;
    readonly url: string;
    getParent(): IPromise<IFileDirectory>;
    delete(): IPromise<boolean>;
    getDirectory(path: string): IPromise<IFileDirectory>;
    createDirectory(path: string, mode?: FileCreateMode): IPromise<IFileDirectory>;
    getFile(path: string): IPromise<IFile>;
    createFile(path: string, data: any, mode?: FileCreateMode, ...options: any[]): IPromise<IFile>;
    getDirectoryList(): IPromise<List<IEntry>>;
    clear(): IPromise<IFileDirectory>;
}
export default FileDirectoryBase;
