import Debug from '@pilotlab/debug';
import { IPromise, Result } from '@pilotlab/result';
import { List } from '@pilotlab/collections';
import IEntry from './interfaces/iEntry';
import IFile from './interfaces/iFile';
import IFileDirectory from './interfaces/iFileDirectory';
import { FileCreateMode } from './fileEnums';


export abstract class FileDirectoryBase implements IFileDirectory {
    get isFile():boolean { return false; }
    get isDirectory():boolean { return true; }
    get name():string { return ''; }
    get url():string { return ''; }


    getParent():IPromise<IFileDirectory> {
        Debug.warn('IFileDirectory.getParent(...) is not implemented.');
        return Result.resolve(null);
    }


    delete():IPromise<boolean> {
        Debug.warn('IFileDirectory.delete(...) is not implemented.');
        return Result.resolve(false);
    }


    getDirectory(path:string):IPromise<IFileDirectory> {
        Debug.warn('IFileDirectory.getDirectory(...) is not implemented.');
        return Result.resolve(null);
    }


    createDirectory(path:string, mode?:FileCreateMode):IPromise<IFileDirectory> {
        Debug.warn('IFileDirectory.createDirectory(...) is not implemented.');
        return Result.resolve(null);
    }


    getFile(path:string):IPromise<IFile> {
        Debug.warn('IFileDirectory.getFile(...) is not implemented.');
        return Result.resolve(null);
    }


    createFile(path:string, data:any, mode?:FileCreateMode, ...options:any[]):IPromise<IFile> {
        Debug.warn('IFileDirectory.createFile(...) is not implemented.');
        return Result.resolve(null);
    }


    getDirectoryList():IPromise<List<IEntry>> {
        Debug.warn('IFileDirectory.getDirectoryList(...) is not implemented.');
        return Result.resolve(null);
    }


    clear():IPromise<IFileDirectory> {
        Debug.warn('IFileDirectory.clear(...) is not implemented.');
        return Result.resolve(null);
    }
} // End class


export default FileDirectoryBase;
