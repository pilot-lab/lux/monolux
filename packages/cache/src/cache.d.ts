import { IPromise, Result } from '@pilotlab/result';
import ICache from './iCache';
export declare abstract class Cache<T> implements ICache {
    private _cache;
    get(key: string, isCache?: boolean): IPromise<T>;
    preLoad(key: string): IPromise<T>;
    release(key: string): void;
    releaseAll(): void;
    protected p_createNew(key: string, result: Result<T>, isCache: boolean): IPromise<T>;
}
export default Cache;
