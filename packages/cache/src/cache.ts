import  { IPromise, Result } from '@pilotlab/result';
import { MapList } from '@pilotlab/collections';
import ICache from './iCache';


export abstract class Cache<T> implements ICache {
    /*====================================================================*
     START: Properties
     *====================================================================*/
    private _cache:MapList<string, T> = new MapList<string, T>();


    /*====================================================================*
     START: Public Methods
     *====================================================================*/
    get(key:string, isCache:boolean = false):IPromise<T> {
        let result:Result<T> = new Result<T>();
        if (this._cache.has(key)) result.resolve(this._cache.get(key));
        else this.p_createNew(key, result, isCache).then((obj:T) => { if (isCache) this._cache.set(key, obj); });
        return result;
    }


    preLoad(key:string):IPromise<T> { return this.get(key, true); }
    release(key:string):void { this._cache.delete(key); }
    releaseAll():void { this._cache.clear(); }


    protected p_createNew(key:string, result:Result<T>, isCache:boolean):IPromise<T> { return result.resolve(null); }
} // End class


export default Cache;
