import { IPromise } from '@pilotlab/result';
export interface ICache {
    get(key: string, isCache?: boolean): IPromise<any>;
    preLoad(key: string): IPromise<any>;
    release(key: string): void;
    releaseAll(): void;
}
export default ICache;
