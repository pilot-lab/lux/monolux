# cache

An abstract base class for custom cache classes.

Simply extend the Cache class and overwrite the p_createNew method as appropriate for the type of content you wish to cache. See the ImageCache example below.

## Install

sudo npm install --save @pilotlab/cache

## Public fields

```
get(key:string, isCache?:boolean):Result<any>;
preLoad(key:string):Result<any>;
release(key:string):void;
releaseAll():void;
```

## Usage

```
import {ICache, Cache} from '@pilotlab/cache';

export class ImageCache extends Cache<HTMLImageElement> {
    static images:ImageCache = new ImageCache();

    protected p_createNew(key:string, result:Result<HTMLImageElement>):Result<HTMLImageElement> {
        if (is.empty(key)) return Result.complete<HTMLImageElement>(null);

        let image:HTMLImageElement = document.createElement('img');

        //----- IMPORTANT: These lines prevent the image element from automatically
        //----- sizing to fit the parent element when it's added to the DOM.
        image.style.maxWidth = 'none';
        image.style.maxHeight = 'none';

        image.onerror = (e:Event) => { Debug.error('Error loading image: ' + key + ', ' + e, 'ImageCache.p_createNew(...)'); };
        image.onload = (e:Event) => { result.resolve(image); };

        image.src = key;

        return result;
    }
} // End of class
```
