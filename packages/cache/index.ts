import ICache from './src/iCache';
import Cache from './src/cache';


export {
    ICache,
    Cache
};
