export interface IHiResTimerCounter {
    frequency: number;
    count: number;
    newCounter(): IHiResTimerCounter;
} // End interface

export default IHiResTimerCounter;
