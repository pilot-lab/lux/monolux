"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
class HiResTimerCounterBrowser {
    constructor() {
        this._frequency = 1000;
    }
    get frequency() { return this._frequency; }
    get count() {
        if (this._isSupportedPerformance()) {
            const win = window;
            return win['performance'].now();
        }
        else
            return Date.now();
    }
    _isSupportedPerformance() {
        try {
            if (is_1.default.empty(window))
                return false;
            let win = window;
            return ('performance' in win);
        }
        catch (e) {
            return false;
        }
    }
    newCounter() { return new HiResTimerCounterBrowser(); }
}
exports.HiResTimerCounterBrowser = HiResTimerCounterBrowser;
exports.default = HiResTimerCounterBrowser;
//# sourceMappingURL=hiResTimerCounterBrowser.js.map