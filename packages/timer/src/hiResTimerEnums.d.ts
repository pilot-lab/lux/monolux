export declare enum TimerEventInterval {
    NONE = 0,
    MILLISECONDS = 1,
    CENTISECONDS = 2,
    DECISECONDS = 3,
    SECONDS = 4,
    MINUTES = 5,
    HOURS = 6,
}
