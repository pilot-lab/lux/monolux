"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const hiResTimerCounterBrowser_1 = require("./hiResTimerCounterBrowser");
const hiResTimerEnums_1 = require("./hiResTimerEnums");
const timespan_1 = require("./timespan");
const signals_1 = require("@pilotlab/signals");
class HiResTimer {
    constructor(counter, isStartTimer = false, eventInterval = hiResTimerEnums_1.TimerEventInterval.NONE) {
        this._eventInterval = hiResTimerEnums_1.TimerEventInterval.NONE;
        this._profileElapsedPrevious = 0;
        this._counter = counter;
        this._eventInterval = eventInterval;
        this._start = 0;
        this._adjustment = 0;
        this._elapsedAtStop = 0;
        this.intervalPassed = new signals_1.Signal(false);
        this._isPaused = false;
        this._isRunning = false;
        this._initialize(isStartTimer);
    }
    get counter() { return this._counter; }
    get eventInterval() { return this._eventInterval; }
    set eventInterval(value) { this._eventInterval = value; }
    _initialize(isStartTimer) {
        this._adjustment = 0;
        this.start();
        this._stop(false);
        this._adjustment = this.elapsedTicks;
    }
    start() {
        if (this._isPaused)
            this._start = this._counter.count - this._elapsedAtStop;
        else
            this._start = this._counter.count;
        if (this._eventInterval !== hiResTimerEnums_1.TimerEventInterval.NONE) {
            let interval = 1000;
            switch (this._eventInterval) {
                case hiResTimerEnums_1.TimerEventInterval.MILLISECONDS:
                    interval = timespan_1.default.unitsInMilliseconds.millisecond;
                    break;
                case hiResTimerEnums_1.TimerEventInterval.CENTISECONDS:
                    interval = timespan_1.default.unitsInMilliseconds.centisecond;
                    break;
                case hiResTimerEnums_1.TimerEventInterval.DECISECONDS:
                    interval = timespan_1.default.unitsInMilliseconds.decisecond;
                    break;
                case hiResTimerEnums_1.TimerEventInterval.SECONDS:
                    interval = timespan_1.default.unitsInMilliseconds.second;
                    break;
                case hiResTimerEnums_1.TimerEventInterval.MINUTES:
                case hiResTimerEnums_1.TimerEventInterval.HOURS:
                    interval = timespan_1.default.unitsInMilliseconds.minute;
            }
            this._intervalReference = setInterval(() => { this.intervalPassed.dispatch(new timespan_1.default(this.elapsedMilliseconds)); }, interval);
        }
        this._isRunning = true;
        this._isPaused = false;
    }
    _stop(isPause) {
        if (this._start <= 0)
            return;
        this._elapsedAtStop = this.elapsedTicks;
        clearInterval(this._intervalReference);
        this._intervalReference = null;
        this._isPaused = isPause;
        this._isRunning = false;
    }
    pause() { this._stop(true); }
    stop() { this._stop(false); }
    get isRunning() { return this._isRunning; }
    get elapsedTicks() {
        let elapsedTicks = this._counter.count - this._start;
        if (elapsedTicks > this._adjustment)
            elapsedTicks -= this._adjustment;
        else
            elapsedTicks = 0;
        return elapsedTicks;
    }
    get elapsedMicroseconds() { return (this.elapsedTicks * 1000000) / this._counter.frequency; }
    get elapsedMilliseconds() { return (this.elapsedTicks * 1000) / this._counter.frequency; }
    get elapsedSeconds() { return this.elapsedTicks / this._counter.frequency; }
    get elapsedMinutes() { return Math.floor(timespan_1.default.unitsInMilliseconds.minute / this.elapsedMilliseconds); }
    get elapsedHours() { return Math.floor(timespan_1.default.unitsInMilliseconds.hour / this.elapsedMilliseconds); }
    get profileElapsed() {
        let profileElapsedCurrent = Math.round(this.elapsedMicroseconds);
        let difference = profileElapsedCurrent - this._profileElapsedPrevious;
        this._profileElapsedPrevious = profileElapsedCurrent;
        return difference;
    }
}
HiResTimer.timerDefault = new HiResTimer(new hiResTimerCounterBrowser_1.default().newCounter(), true);
exports.HiResTimer = HiResTimer;
exports.default = HiResTimer;
//# sourceMappingURL=hiResTimer.js.map