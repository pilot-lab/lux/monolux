export declare class TimeSpan {
    constructor(milliseconds?: number, seconds?: number, minutes?: number, hours?: number, days?: number);
    private _millis;
    private _rounder(value);
    static unitsInMilliseconds: any;
    add(timespan: TimeSpan): TimeSpan;
    subtract(timespan: TimeSpan): TimeSpan;
    negate(): void;
    compare(timespan: TimeSpan): number;
    readonly duration: TimeSpan;
    readonly days: number;
    readonly hours: number;
    readonly minutes: number;
    readonly seconds: number;
    readonly milliseconds: number;
    readonly daysTotal: number;
    readonly hoursTotal: number;
    readonly minutesTotal: number;
    readonly secondsTotal: number;
    readonly millisecondsTotal: number;
    toString(): string;
}
export default TimeSpan;
