"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const hiResTimer_1 = require("./hiResTimer");
const hiResTimerCounterBrowser_1 = require("./hiResTimerCounterBrowser");
const hiResTimerEnums_1 = require("./hiResTimerEnums");
class Timer extends hiResTimer_1.default {
    constructor(isStartTimer = false, eventInterval = hiResTimerEnums_1.TimerEventInterval.NONE) {
        super(new hiResTimerCounterBrowser_1.default(), isStartTimer, eventInterval);
    }
}
exports.Timer = Timer;
exports.default = Timer;
//# sourceMappingURL=timer.js.map