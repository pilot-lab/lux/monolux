import IHiResTimerCounter from './iHiResTimerCounter';
export declare class HiResTimerCounterBrowser implements IHiResTimerCounter {
    readonly frequency: number;
    private _frequency;
    readonly count: number;
    private _isSupportedPerformance();
    newCounter(): IHiResTimerCounter;
}
export default HiResTimerCounterBrowser;
