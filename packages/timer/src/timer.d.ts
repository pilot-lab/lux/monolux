import HiResTimer from './hiResTimer';
import { TimerEventInterval } from "./hiResTimerEnums";
export declare class Timer extends HiResTimer {
    constructor(isStartTimer?: boolean, eventInterval?: TimerEventInterval);
}
export default Timer;
