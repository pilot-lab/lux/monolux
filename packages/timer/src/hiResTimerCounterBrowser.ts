import is from '@pilotlab/is';
import IHiResTimerCounter from './iHiResTimerCounter';


export class HiResTimerCounterBrowser implements IHiResTimerCounter {
    /**
     * Stores the frequency (ticks per second) of the high-resolution performance counter.
     * This value cannot change while the system is running.
     */
    get frequency():number { return this._frequency; }
    private _frequency:number = 1000;


    get count():number {
        if (this._isSupportedPerformance()) {
            const win:any = window;
            return win['performance'].now();
        } else return Date.now();
    }


    private _isSupportedPerformance():boolean {
        try {
            if (is.empty(window)) return false;
            let win:any = window;
            return ('performance' in win);
        } catch(e) { return false; }
    }


    newCounter():IHiResTimerCounter { return new HiResTimerCounterBrowser(); }
} // End class


export default HiResTimerCounterBrowser;
