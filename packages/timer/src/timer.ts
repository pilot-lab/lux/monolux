import HiResTimer from './hiResTimer';
import HiResTimerCounterBrowser from './hiResTimerCounterBrowser';
import { TimerEventInterval } from "./hiResTimerEnums";


export class Timer extends HiResTimer {
    constructor(isStartTimer:boolean = false, eventInterval:TimerEventInterval = TimerEventInterval.NONE) {
        super(new HiResTimerCounterBrowser(), isStartTimer, eventInterval);
    }
} // End class


export default Timer;
