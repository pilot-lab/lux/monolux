"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const strings_1 = require("@pilotlab/strings");
class TimeSpan {
    constructor(milliseconds = 0, seconds = 0, minutes = 0, hours = 0, days = 0) {
        this._millis = 0;
        this._millis = (days * 86400 + hours * 3600 + minutes * 60 + seconds) * 1000 + milliseconds;
    }
    _rounder(value) {
        if (this._millis < 0)
            return Math.ceil(value);
        return Math.floor(value);
    }
    add(timespan) { return new TimeSpan(timespan._millis + this._millis); }
    subtract(timespan) { return new TimeSpan(this._millis - timespan._millis); }
    negate() { this._millis *= -1; }
    compare(timespan) {
        if (this._millis > timespan._millis)
            return 1;
        if (this._millis == timespan._millis)
            return 0;
        if (this._millis < timespan._millis)
            return -1;
        return 0;
    }
    get duration() { return new TimeSpan(Math.abs(this._millis)); }
    get days() { return this._rounder(this._millis / (24 * 3600 * 1000)); }
    get hours() { return this._rounder((this._millis % (24 * 3600 * 1000)) / (3600 * 1000)); }
    get minutes() { return this._rounder((this._millis % (3600 * 1000)) / (60 * 1000)); }
    get seconds() { return this._rounder((this._millis % 60000) / 1000); }
    get milliseconds() { return this._rounder(this._millis % 1000); }
    get daysTotal() { return this._millis / (24 * 3600 * 1000); }
    get hoursTotal() { return this._millis / (3600 * 1000); }
    get minutesTotal() { return this._millis / (60 * 1000); }
    get secondsTotal() { return this._millis / 1000; }
    get millisecondsTotal() { return this._millis; }
    toString() {
        let formatted = (this._millis < 0 ? '-' : '') + (Math.abs(this.days) ? strings_1.default.padDigits(Math.abs(this.days), 2) + '.' : '') +
            strings_1.default.padDigits(Math.abs(this.hours), 2) + ':' +
            strings_1.default.padDigits(Math.abs(this.minutes), 2) + ':' +
            strings_1.default.padDigits(Math.abs(this.seconds), 2) + '.' + strings_1.default.padDigits(Math.abs(this.milliseconds), 3);
        return formatted;
    }
}
TimeSpan.unitsInMilliseconds = {
    millisecond: 1,
    centisecond: 10,
    decisecond: 100,
    second: 1000,
    minute: 60000,
    hour: 3600000,
    day: 86400000
};
exports.TimeSpan = TimeSpan;
exports.default = TimeSpan;
//# sourceMappingURL=timespan.js.map