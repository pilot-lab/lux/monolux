import {
    AsyncTest,
    Setup,
    SetupFixture,
    Teardown,
    TeardownFixture,
    Test,
    TestCase,
    TestFixture,
    Timeout,
    Expect
} from 'alsatian';
import { Timer, TimerEventInterval, TimeSpan, HiResTimerCounterBrowser, HiResTimer } from '../index';
import { SignalBinding, ISignalBinding } from '@pilotlab/signals';
import Debug from '@pilotlab/debug';


@TestFixture('Timer')
export class Tests {
    @Setup
    public setup() {}

    @SetupFixture
    public setupFixture() {}

    @Teardown
    public teardown() {}

    @TeardownFixture
    public teardownFixture() {}

    @TestCase(TimerEventInterval.MILLISECONDS)
    @TestCase(TimerEventInterval.CENTISECONDS)
    @TestCase(TimerEventInterval.DECISECONDS)
    @TestCase(TimerEventInterval.SECONDS)
    @AsyncTest("timer intervalPassed event")
    @Timeout(5000) // Alsatian will now wait 5 seconds before failing
    public async asyncTest(testValue:TimerEventInterval) {
        const timer = new Timer(false, testValue);

        const promise:Promise<any> = new Promise((resolve, reject) => {
            const signalBinding:ISignalBinding<TimeSpan> = timer.intervalPassed.listen((timeSpan:TimeSpan) => {
                Debug.isProduction = false;
                Debug.log(timeSpan.toString(), TimerEventInterval[testValue]);
                if (timeSpan.seconds >= 3) {
                    timer.stop();
                    resolve();
                    signalBinding.detach();
                }
            });

            timer.start();
        });

        const result:any = await promise;
    }
}
