"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const alsatian_1 = require("alsatian");
const index_1 = require("../index");
const debug_1 = require("@pilotlab/debug");
let Tests = class Tests {
    setup() { }
    setupFixture() { }
    teardown() { }
    teardownFixture() { }
    asyncTest(testValue) {
        return __awaiter(this, void 0, void 0, function* () {
            const timer = new index_1.Timer(false, testValue);
            const promise = new Promise((resolve, reject) => {
                const signalBinding = timer.intervalPassed.listen((timeSpan) => {
                    debug_1.default.isProduction = false;
                    debug_1.default.log(timeSpan.toString(), index_1.TimerEventInterval[testValue]);
                    if (timeSpan.seconds >= 3) {
                        timer.stop();
                        resolve();
                        signalBinding.detach();
                    }
                });
                timer.start();
            });
            const result = yield promise;
        });
    }
};
__decorate([
    alsatian_1.Setup
], Tests.prototype, "setup", null);
__decorate([
    alsatian_1.SetupFixture
], Tests.prototype, "setupFixture", null);
__decorate([
    alsatian_1.Teardown
], Tests.prototype, "teardown", null);
__decorate([
    alsatian_1.TeardownFixture
], Tests.prototype, "teardownFixture", null);
__decorate([
    alsatian_1.TestCase(index_1.TimerEventInterval.MILLISECONDS),
    alsatian_1.TestCase(index_1.TimerEventInterval.CENTISECONDS),
    alsatian_1.TestCase(index_1.TimerEventInterval.DECISECONDS),
    alsatian_1.TestCase(index_1.TimerEventInterval.SECONDS),
    alsatian_1.AsyncTest("timer intervalPassed event"),
    alsatian_1.Timeout(5000)
], Tests.prototype, "asyncTest", null);
Tests = __decorate([
    alsatian_1.TestFixture('Timer')
], Tests);
exports.Tests = Tests;
//# sourceMappingURL=index.test.js.map