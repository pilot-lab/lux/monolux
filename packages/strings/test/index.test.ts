/**
 * IMPORTANT:
 * debug uses the Strings class, so don't use @pilotlab/debug
 * in here. Otherwise, we'll create a cross-dependency that will break both packages.
 */


import { Expect, Test, TestCase, TestFixture } from 'alsatian';
import { Debug } from '@pilotlab/debug';
import Strings from '../index';
import Lexer from '../index';

@TestFixture('Strings')
export class Tests {
    @TestCase('Sed ut   perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.')
    @TestCase(JSON.stringify({property01: "value01", property02: "value02", property03: "value03", property04: "value04", property05: "value05", property06: "value06", property07: "value07", property08: "value08" }))
    @Test('tokens, words, and whitespace')
    test01(testValue:any) {
        let tokens:string[] = Strings.tokenize(testValue);
        let words:string[] = Strings.getWords(testValue);
        let lines:string[] = Strings.getLines(testValue, 100);
        let tokenString:string = '';
        let wordString:string = '';

        Debug.log(Strings.removeExtraSpaces(testValue) + '\n');

        tokens.forEach((token) => { tokenString += token; });
        Debug.log(tokenString + '\n');

        words.forEach((word) => { wordString += word + '---'; });
        Debug.log(wordString + '\n');

        lines.forEach((line) => {
            Debug.log(line);
        });
    }


    @TestCase('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAQMAAAAlPW0iAAAABlBMVEUAAAD///+l2Z/dAAAAM0lEQVR4nGP4/5/h/1+G/58ZDrAz3D/McH8yw83NDDeNGe4Ug9C9zwz3gVLMDA/A6P9/AFGGFyjOXZtQAAAAAElFTkSuQmCC')
    @TestCase('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAIBAMAAAA2IaO4AAAAFVBMVEXk5OTn5+ft7e319fX29vb5+fn///++GUmVAAAALUlEQVQIHWNICnYLZnALTgpmMGYIFWYIZTA2ZFAzTTFlSDFVMwVyQhmAwsYMAKDaBy0axX/iAAAAAElFTkSuQmCC')
    @TestCase('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAIBAMAAAA2IaO4AAAAFVBMVEXk5OTn5+ft7e319fX29vb5+fn///++GUmVAAAALUlEQVQIHWNICnYLZnALTgpmMGYIFWYIZTA2ZFAzTTFlSDFVMwVyQhmAwsYMAKDaBy0axX/iAAAAAElFTkSuQmCC')
    @TestCase('data:,Hello%2C%20World!')
    @TestCase('data:,Hello World!')
    @TestCase('data:text/plain;base64,SGVsbG8sIFdvcmxkIQ%3D%3D')
    @TestCase('data:text/html,%3Ch1%3EHello%2C%20World!%3C%2Fh1%3E')
    @TestCase('data:,A%20brief%20note')
    @TestCase('data:text/html;charset=US-ASCII,%3Ch1%3EHello!%3C%2Fh1%3E')
    @Test('data Urls, positive')
    test02(value:any) {
        Expect(Strings.isDataUrl(value)).toBe(true);
    }


    @TestCase('dataxbase64')
    @TestCase('data:HelloWorld')
    @TestCase('data:text/html;charset=,%3Ch1%3EHello!%3C%2Fh1%3E')
    @TestCase('data:text/html;charset,%3Ch1%3EHello!%3C%2Fh1%3E", "data:base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAQMAAAAlPW0iAAAABlBMVEUAAAD///+l2Z/dAAAAM0lEQVR4nGP4/5/h/1+G/58ZDrAz3D/McH8yw83NDDeNGe4Ug9C9zwz3gVLMDA/A6P9/AFGGFyjOXZtQAAAAAElFTkSuQmCC')
    @TestCase('')
    @TestCase('http://wikipedia.org')
    @TestCase('base64')
    @TestCase('iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAQMAAAAlPW0iAAAABlBMVEUAAAD///+l2Z/dAAAAM0lEQVR4nGP4/5/h/1+G/58ZDrAz3D/McH8yw83NDDeNGe4Ug9C9zwz3gVLMDA/A6P9/AFGGFyjOXZtQAAAAAElFTkSuQmCC')
    @Test('data Urls, negative')
    test03(value:any) {
        Expect(Strings.isDataUrl(value)).toBe(false);
    }


    @TestCase('a campaign celebrating a new Wave of women')
    @TestCase('The Viewmaster')
    @TestCase('Ut egestas arcu lorem, a imperdiet nisl aliquet eu. Sed lectus augue, scelerisque ac condimentum a, bibendum sed dui. raesent pellentesque pharetra justo, eget dictum turpis finibus non.')
    @Test('sentence case')
    test04(value:any) {
        Debug.log(Strings.sentenceCase(value));
    }


    @TestCase('aCampaignCelebratingANewWaveOfWomen')
    @TestCase('TheViewmaster')
    @Test('camel case to kebab case')
    test05(value:any) {
        Debug.log(Strings.camelToKebab(value));
    }


    @TestCase('a campaign celebrating a new Wave of women')
    @TestCase('The Viewmaster')
    @TestCase('Ut egestas arcu lorem, a imperdiet nisl aliquet eu. Sed lectus augue, scelerisque ac condimentum a, bibendum sed dui. raesent pellentesque pharetra justo, eget dictum turpis finibus non.')
    @Test('remove white space')
    test06(value:any) {
        Debug.log(Strings.removeWhitespace(value));
    }


    @TestCase('Jennifer')
    @TestCase('YW55IGNhcm5hbCBwbGVhc3VyZS4=')
    @Test('base64')
    testBase64(value:any) {
        Debug.log(`Strings.isValidBase64(${value}): ${Strings.isValidBase64(value)}`);
        Debug.log(`Strings.isBase64(${value}): ${Strings.isBase64(value)}`);
    }


    @TestCase('this_text_is_in_snake_case')
    @TestCase('this.is.dot.separated')
    @TestCase('this-one-is-kebab-case')
    @Test('to camel case')
    testToCamel(value:any) {
        Debug.log(`${Strings.toCamel(value)}`);
    }
}
