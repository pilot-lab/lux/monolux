# strings

String utilities.

## Install

`sudo npm install --save @pilotlab/strings`

## Usage

```
import Strings from @pilotlab/strings

Strings.trimWhitespace(inputString);
```
