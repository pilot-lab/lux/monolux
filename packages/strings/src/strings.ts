import is from '@pilotlab/is';
import { Tokens } from './stringEnums';


export class Strings {
    static isEmptyString(value:string):boolean {
        /// Remove white space.
        value = this.removeWhitespace(value);
        return value.length == 0;
    }


    static removeWhitespace(value:string):string {
        return value.replace(/\s*/g, '');
    }


    static removeExtraSpaces(value:string):string {
        return value.replace(/ +(?= )/g, '');
    }


    static isValidHexString(value:string):boolean {
        return (/^(#[a-fA-F0-9]{3,6})$/i.test(value));
    }


    static isValidUri(value:string):boolean {
        const pattern:RegExp = new RegExp(/^(https?:\/\/)?((([a-z\d]([a-z\d-]*[a-z\d])*)\.)+[a-z]{2,}|((\d{1,3}\.){3}\d{1,3}))(\:\d+)?(\/[-a-z\d%_.~+]*)*(\?[;&a-z\d%_.~+=-]*)?(\#[-a-z\d_]*)?$/i);
        return pattern.test(value);
    }


    static isDataUrl(value:string):boolean {
        const pattern:RegExp = new RegExp(/^\s*data:([a-z]+\/[a-z]+(;[a-z\-]+\=[a-z\-]+)?)?(;base64)?,[a-z0-9\!\$\&\'\,\(\)\*\+\,\;\=\-\.\_\~\:\@\/\?\%\s]*\s*$/i);
        return pattern.test(value);
    }


    static isValidBase64(value:string):boolean {
        /**
         * ^                      # Start of input
         * ([0-9a-zA-Z+/]{4})*    # Groups of 4 valid characters decode
         * *                      # to 24 bits of data for each group
         * (                      # Either ending with:
         * ([0-9a-zA-Z+/]{2}==)   # two valid characters followed by ==
         * |                      # , or
         * ([0-9a-zA-Z+/]{3}=)    # three valid characters followed by =
         * )?                     # , or nothing
         * $                      # End of input
         */
        const pattern:RegExp = new RegExp(/^([0-9a-zA-Z+/]{4})*(([0-9a-zA-Z+/]{2}==)|([0-9a-zA-Z+/]{3}=))?$/);
        return pattern.test(value);
    }


    static isBase64(value:string):boolean {
        try {
            if (is.empty(window)) return false;
            const win:any = window;
            return win.btoa(win.atob(value)) === value;
        } catch (err) {
            return false;
        }
    }


    static normalizeLineBreaks(value:string, lineBreak:string = '\n'): string {
        if (is.empty(value)) return '';
        return value.replace(/\r\r/g, lineBreak).replace(/\r\n/g, lineBreak).replace(/\r/g, lineBreak).replace(/\n/g, lineBreak);
    }


    static replaceMultiple(value:string, replaceAll:string[], replaceWith:string): string {
        let re: RegExp;
        let length: number = replaceAll.length;

        for (let i: number = 0; i < length - 1; i++) {
            re = new RegExp(replaceAll[i], 'g');
            value = value.replace(re, replaceAll[i + 1]);
        }

        return value.replace(replaceAll[length - 1], replaceWith);
    }


    static splitMultiple(value:string, delimiters:string[] = [' '], isPreserveDelimiters:boolean = false): string[] {
        let regExp: RegExp;
        let length: number = delimiters.length;

        if (isPreserveDelimiters) {
            /// Build out a regular expression that will split on all provided delimiters.
            let regExpString: string = '(' + Tokens.break; // Split on ' <br/> ' or ' '

            if (length > 0) {
                regExpString += '|'; // Or

                for (let i: number = 0; i < length - 1; i++) {
                    regExpString += delimiters[i] + '|';
                }

                regExpString += delimiters[length - 1];
            }

            regExpString += ')'; // Close expression

            regExp = new RegExp(regExpString, '');
            return value.split(regExp);
        } else {
            for (let i: number = 0; i < length - 1; i++) {
                regExp = new RegExp(delimiters[i], 'g');
                value = value.replace(regExp, delimiters[i + 1]);
            }

            return value.split(delimiters[length - 1]);
        }
    }


    /**
     * Convert large number to a string with commas in the right places.
     */
    static numberWithCommas(value:number):string {
        return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }


    /**
     * Add 0's to the start of the integer to ensure that it has the specified number of digits.
     */
    static padDigits(value:(string | number), totalDigits:number = 2):string {
        let valueString: string = value.toString();
        while (valueString.length < totalDigits) valueString = '0' + valueString;
        return valueString;
    }


    static sentenceCase(value:string):string {
        let valueFinal:string = '';
        let sentenceStart:boolean = true;

        for (let i = 0; i < value.length; i++) {
            let character:string = value.charAt(i);

            if (sentenceStart && character.match(/^\S$/)) {
                character = character.toUpperCase ();
                sentenceStart = false;
            } else {
                character = character.toLowerCase();
            }

            if (character.match(/^[.!?]$/)) sentenceStart = true;

            valueFinal += character;
        }

        return valueFinal;
    }


   static titleCase(string:string):string { return string.split(' ').map(word => word[0].toUpperCase()).join(' '); }
   static initials(string:string):string { return string.split(' ').map(word => word[0]).join('').slice(0, 2); }


    /**
     * Example:
     *
     * let replacements:any = { "NAME": "Mike", "AGE": "26", "EVENT": "20" };
     * let s:string = 'My key is %NAME% and my age is %AGE%. The following %TOKEN% is invalid.';
     * s = Strings.replacePlaceholders(s, replacements);
     * s === 'My key is Mike and my age is 26. The following %TOKEN% is invalid.'
     */
    static replacePlaceholders(value:string, replacements:any): string {
        value = value.replace(/%\w+%/g, (placeholder: string) => {
            let replacementName: string = placeholder.slice(1, -1);
            let value: any = replacements[replacementName];
            return typeof value === 'string' ? value : (value != null && value != undefined && value != '' ? value.toString() : '');
        });
        return value;
    }


    static tokenize(value:string, delimiters:string[] = [' ', ',']): string[] {
        if (is.empty(delimiters) || delimiters.length < 1) return [];

        value = this.normalizeLineBreaks(value);
        value = value.replace(/\n/g, Tokens.break);
        delimiters.push(Tokens.break);

        let splitText: string[] = this.splitMultiple(value, delimiters, true);

        /// Filter out all the delimiter tokens and return only the words.
        let tokens: string[] = [];
        splitText.forEach((token) => {
            let isToken = true;

            delimiters.forEach((delimiter) => {
                if (token === delimiter) {
                    if (delimiter === ' ') tokens.push(Tokens.space);
                    else if (delimiter === ',') tokens.push(Tokens.comma);
                    else if (delimiter === Tokens.break) tokens.push(Tokens.break);
                    isToken = false;
                }
            });

            if (isToken) tokens.push(token);
        });

        return tokens;
    }


    static getWords(value:string, delimiters:string[] = [' ']):string[] {
        if (is.empty(delimiters) || delimiters.length < 1) return [];

        value = this.removeExtraSpaces(value);
        let tokens: string[] = this.tokenize(value, delimiters);

        /// Filter out all the delimiter tokens and return only the words.
        let words: string[] = [];
        tokens.forEach((token) => {
            if (token !== Tokens.break && token !== Tokens.space) {
                if (token === Tokens.comma) token = ',';
                words.push(token);
            }
        });

        return words;
    }


    static getLines(value:string, characterMaxPerLine:number = 100): string[] {
        let tokens: string[] = this.tokenize(value);
        let lineCurrent: string = '';
        let lines: string[] = [];

        tokens.every((token: string, tokenIndex: number) => {
            /// Create a new line if there is a break or the text needs to wrap due to length
            if (token === Tokens.break) {
                lines.push(lineCurrent);
                lineCurrent = '';
            } else if (token === Tokens.space) {
                token = ' ';
                lineCurrent += token;
            } else if (token === Tokens.comma) {
                token = ',';
                lineCurrent += token;
            } else if (lineCurrent.length + token.length > characterMaxPerLine) {
                lines.push(lineCurrent);
                lineCurrent = token;
            } else {
                lineCurrent += token;
            }

            /// If this is the last token, add the current line to the
            /// array to finish up.
            if (tokenIndex === tokens.length - 1) lines.push(lineCurrent);

            return true;
        });

        return lines;
    }


    /**
     * Returns the function of the given name, within the provided scope.
     * @param functionName {string} The name of the function we're looking for.
     * @param scope {Object} The scope to search within.
     */
    static getFunctionFromString(functionName:string, scope?:any):Function {
        if (is.empty(scope)) scope = window;
        let scopeSplit:string[] = functionName.split('.');
        for (let i = 0; i < scopeSplit.length - 1; i++) {
            scope = scope[scopeSplit[i]];

            if (is.empty(scope)) return null;
        }

        return scope[scopeSplit[scopeSplit.length - 1]];
    }


    /**
     * Tries to get the documentElement from Xml formatted input and returns null if none is found,
     * or the input is not properly formatted XML.
     */
    static getXmlDocumentElement(elem:any):Document {
        /// documentElement is verified for cases where it doesn't yet exist
        /// (such as loading iframes in IE - #4833)
        return (is.notEmpty(elem) ? is.notEmpty(elem.ownerDocument) || elem : 0).documentElement;
    }


    static isHtml(elem:any):boolean {
        try {
            let documentElement:Document = this.getXmlDocumentElement(elem);
            return is.notEmpty(documentElement) ? documentElement.nodeName.toLowerCase() !== 'html' : false;
        } catch(e) { return false; }
    }


    static isXml(elem:any):boolean {
        try {
            let documentElement:Document = this.getXmlDocumentElement(elem);
            return is.notEmpty(documentElement) ? documentElement.nodeName.toLowerCase() !== 'xml' : false;
        } catch(e) { return false; }
    }


    static isSvg(elem:any):boolean {
        try {
            let documentElement:Document = this.getXmlDocumentElement(elem);
            return is.notEmpty(documentElement) ? documentElement.nodeName.toLowerCase() === 'svg' : false;
        } catch(e) { return false; }
    }


    static stringToXmlDocument(s:string):Document {
        try {
            if (!DOMParser) return null;
            let parser:DOMParser = new DOMParser();
            return parser.parseFromString(s, 'application/xml');
        } catch(e) { return null; }
    }


    static xmlToString(xml:Node):string {
        return (new XMLSerializer()).serializeToString(xml);
    }


    static stringToHtmlDocument(s:string):Document {
        try {
            if (!DOMParser) return null;
            let parser:DOMParser = new DOMParser();
            return parser.parseFromString(s, 'text/html');
        } catch(e) { return null; }
    }


    static stringToSvgDocument(s:string):Document {
        try {
            if (!DOMParser) return null;
            let parser:DOMParser = new DOMParser();
            return parser.parseFromString(s, 'image/svg+xml');
        } catch(e) { return null; }
    }


    static camelToKebab(value:string):string {
        let valueFinal = value.substring(0, 1);
        valueFinal += value.substring(1).replace(/[A-Z]|^ms/g, '-$&');
        return valueFinal.toLowerCase();
    }


    static toCamel(value:string):string {
        const SPECIAL_CHARS_REGEXP = /([\:\-\.\_]+(.))/g;
        return value.replace(SPECIAL_CHARS_REGEXP, (_, separator, letter, offset) => {
            return offset ? letter.toUpperCase() : letter;
        });
    }
} // End class


export default Strings;
