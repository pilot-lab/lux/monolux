export class RegularExpressions {
    static escape(value:string):string {
        return value.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&')
    }


    static getGroupCount(value:string):number {
        const regExp:RegExp = new RegExp('|' + value);
        return regExp.exec('').length - 1;
    }


    static union(value:string[]):string {
        /// ?: denotes a non-capturing group.
        const source =  value.map((regExpString:string) => { return "(?:" + regExpString + ")" }).join('|');
        return "(?:" + source + ")"
    }
}

export default RegularExpressions;
