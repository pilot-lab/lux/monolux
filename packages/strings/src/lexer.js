"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
class LexerRule {
    constructor(tokenType, match) {
        this.tokenType = tokenType;
        this.match = match;
    }
}
exports.LexerRule = LexerRule;
class LexerCompiledRules {
    constructor(regExp) {
        this.regExp = regExp;
    }
}
exports.LexerCompiledRules = LexerCompiledRules;
class Lexer {
    constructor() {
        this._index = 0;
        this._line = 0;
        this._column = 0;
        this._rules = [];
    }
    _getRule(match) {
        if (is_1.default.empty(match))
            return -1;
        for (let i = 0; i < this._rules.length; i++) {
            if (is_1.default.notEmpty(match[i + 1]))
                return i;
        }
    }
    compile(rules) {
        this._rules = rules;
        return null;
    }
    runRule(rule) {
        let regExp = this._regExp;
        let buffer = this._buffer;
        let index = regExp.lastIndex = this._index;
        if (index === buffer.length) {
            return undefined;
        }
        let match = regExp.exec(this._buffer);
        let text;
        if (is_1.default.empty(rule)) {
            text = buffer.slice(index);
        }
        else {
            text = match[0];
        }
        let token = {
            type: rule.tokenType,
            value: text,
            offset: index,
        };
        this._index += text.length;
        return token;
    }
}
exports.Lexer = Lexer;
exports.default = Lexer;
//# sourceMappingURL=lexer.js.map