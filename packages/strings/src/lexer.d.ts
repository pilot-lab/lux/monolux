export declare class LexerRule {
    constructor(tokenType: string, match: RegExp[]);
    tokenType: string;
    match: RegExp[];
}
export declare class LexerCompiledRules {
    constructor(regExp: RegExp);
    regExp: RegExp;
}
export declare class Lexer {
    private _regExp;
    private _buffer;
    private _index;
    private _line;
    private _column;
    private _rules;
    private _getRule(match);
    compile(rules: LexerRule[]): LexerCompiledRules;
    runRule(rule: LexerRule): {
        type: string;
        value: string;
        offset: number;
    };
}
export default Lexer;
