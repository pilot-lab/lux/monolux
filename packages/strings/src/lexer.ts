import is from '@pilotlab/is';
import RegularExpressions from './regularExpressions';


export class LexerRule {
    constructor(tokenType:string, match:RegExp[]) {
        this.tokenType = tokenType;
        this.match = match;
    }

    tokenType:string;
    match:RegExp[];
}


export class LexerCompiledRules {
    constructor(regExp:RegExp) {
        this.regExp = regExp;
    }

    regExp:RegExp;
}


export class Lexer {
    private _regExp:RegExp;
    private _buffer:string;
    private _index:number = 0;
    private _line:number = 0;
    private _column:number = 0;
    private _rules:LexerRule[] = [];


    private _getRule(match:string[]) {
        if (is.empty(match)) return -1;

        for (let i = 0; i < this._rules.length; i++) {
            if (is.notEmpty(match[i + 1])) return i;
        }
    }


    compile(rules:LexerRule[]):LexerCompiledRules {
        this._rules = rules;

        /**
         * See: https://ponyfoo.com/articles/regular-expressions-post-es6
         * The sticky matching y flag introduced in ES6 is similar to the global
         * g flag. Like global regular expressions, sticky ones are typically
         * used to match several times until the input string is exhausted.
         * Sticky regular expressions move lastIndex to the position after the
         * last match, just like global regular expressions. The only difference
         * is that a sticky regular expression must start matching where the previous
         * match left off, unlike global regular expressions that move onto the
         * rest of the input string when the regular expression goes unmatched at
         * any given position.
         *
         * The value of sticky is a Boolean and true if the "y" flag was used;
         * otherwise, false.
         */
        // const isSticky:boolean = typeof new RegExp('').sticky === 'boolean';
        //
        // let parts = [];
        //
        // for (let i = 0; i < rules.length; i++) {
        //     let rule:LexerRule = rules[i];
        //     let pattern:string = RegularExpressions.union(rule.match.map((value:RegExp) => value.source));
        //     parts.push(`(${pattern})`);
        // }
        //
        // let suffix:string = isSticky ? '' : '|(?:)';
        // let flags:string = isSticky ? 'ym' : 'gm';
        // let regExpCombined:RegExp = new RegExp(RegularExpressions.union(parts) + suffix, flags);

        // return new LexerCompiledRules(regExpCombined);
        return null;
    }


    runRule(rule:LexerRule) {
        let regExp:RegExp = this._regExp;
        let buffer:string = this._buffer;

        let index:number = regExp.lastIndex = this._index;
        if (index === buffer.length) { return undefined; } // EOF

        let match:string[] = regExp.exec(this._buffer);

        let text:string;

        if (is.empty(rule)) {
            /// consume rest of buffer
            text = buffer.slice(index);
        } else {
            text = match[0];
        }

        let token = {
            type: rule.tokenType,
            value: text,
            offset: index,
        };

        this._index += text.length;

        return token;
    }
} // End class


export default Lexer;
