"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const stringEnums_1 = require("./stringEnums");
class Strings {
    static isEmptyString(value) {
        value = this.removeWhitespace(value);
        return value.length == 0;
    }
    static removeWhitespace(value) {
        return value.replace(/\s*/g, '');
    }
    static removeExtraSpaces(value) {
        return value.replace(/ +(?= )/g, '');
    }
    static isValidHexString(value) {
        return (/^(#[a-fA-F0-9]{3,6})$/i.test(value));
    }
    static isValidUri(value) {
        const pattern = new RegExp(/^(https?:\/\/)?((([a-z\d]([a-z\d-]*[a-z\d])*)\.)+[a-z]{2,}|((\d{1,3}\.){3}\d{1,3}))(\:\d+)?(\/[-a-z\d%_.~+]*)*(\?[;&a-z\d%_.~+=-]*)?(\#[-a-z\d_]*)?$/i);
        return pattern.test(value);
    }
    static isDataUrl(value) {
        const pattern = new RegExp(/^\s*data:([a-z]+\/[a-z]+(;[a-z\-]+\=[a-z\-]+)?)?(;base64)?,[a-z0-9\!\$\&\'\,\(\)\*\+\,\;\=\-\.\_\~\:\@\/\?\%\s]*\s*$/i);
        return pattern.test(value);
    }
    static isValidBase64(value) {
        const pattern = new RegExp(/^([0-9a-zA-Z+/]{4})*(([0-9a-zA-Z+/]{2}==)|([0-9a-zA-Z+/]{3}=))?$/);
        return pattern.test(value);
    }
    static isBase64(value) {
        try {
            if (is_1.default.empty(window))
                return false;
            const win = window;
            return win.btoa(win.atob(value)) === value;
        }
        catch (err) {
            return false;
        }
    }
    static normalizeLineBreaks(value, lineBreak = '\n') {
        if (is_1.default.empty(value))
            return '';
        return value.replace(/\r\r/g, lineBreak).replace(/\r\n/g, lineBreak).replace(/\r/g, lineBreak).replace(/\n/g, lineBreak);
    }
    static replaceMultiple(value, replaceAll, replaceWith) {
        let re;
        let length = replaceAll.length;
        for (let i = 0; i < length - 1; i++) {
            re = new RegExp(replaceAll[i], 'g');
            value = value.replace(re, replaceAll[i + 1]);
        }
        return value.replace(replaceAll[length - 1], replaceWith);
    }
    static splitMultiple(value, delimiters = [' '], isPreserveDelimiters = false) {
        let regExp;
        let length = delimiters.length;
        if (isPreserveDelimiters) {
            let regExpString = '(' + stringEnums_1.Tokens.break;
            if (length > 0) {
                regExpString += '|';
                for (let i = 0; i < length - 1; i++) {
                    regExpString += delimiters[i] + '|';
                }
                regExpString += delimiters[length - 1];
            }
            regExpString += ')';
            regExp = new RegExp(regExpString, '');
            return value.split(regExp);
        }
        else {
            for (let i = 0; i < length - 1; i++) {
                regExp = new RegExp(delimiters[i], 'g');
                value = value.replace(regExp, delimiters[i + 1]);
            }
            return value.split(delimiters[length - 1]);
        }
    }
    static numberWithCommas(value) {
        return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    static padDigits(value, totalDigits = 2) {
        let valueString = value.toString();
        while (valueString.length < totalDigits)
            valueString = '0' + valueString;
        return valueString;
    }
    static sentenceCase(value) {
        let valueFinal = '';
        let sentenceStart = true;
        for (let i = 0; i < value.length; i++) {
            let character = value.charAt(i);
            if (sentenceStart && character.match(/^\S$/)) {
                character = character.toUpperCase();
                sentenceStart = false;
            }
            else {
                character = character.toLowerCase();
            }
            if (character.match(/^[.!?]$/))
                sentenceStart = true;
            valueFinal += character;
        }
        return valueFinal;
    }
    static titleCase(string) { return string.split(' ').map(word => word[0].toUpperCase()).join(' '); }
    static initials(string) { return string.split(' ').map(word => word[0]).join('').slice(0, 2); }
    static replacePlaceholders(value, replacements) {
        value = value.replace(/%\w+%/g, (placeholder) => {
            let replacementName = placeholder.slice(1, -1);
            let value = replacements[replacementName];
            return typeof value === 'string' ? value : (value != null && value != undefined && value != '' ? value.toString() : '');
        });
        return value;
    }
    static tokenize(value, delimiters = [' ', ',']) {
        if (is_1.default.empty(delimiters) || delimiters.length < 1)
            return [];
        value = this.normalizeLineBreaks(value);
        value = value.replace(/\n/g, stringEnums_1.Tokens.break);
        delimiters.push(stringEnums_1.Tokens.break);
        let splitText = this.splitMultiple(value, delimiters, true);
        let tokens = [];
        splitText.forEach((token) => {
            let isToken = true;
            delimiters.forEach((delimiter) => {
                if (token === delimiter) {
                    if (delimiter === ' ')
                        tokens.push(stringEnums_1.Tokens.space);
                    else if (delimiter === ',')
                        tokens.push(stringEnums_1.Tokens.comma);
                    else if (delimiter === stringEnums_1.Tokens.break)
                        tokens.push(stringEnums_1.Tokens.break);
                    isToken = false;
                }
            });
            if (isToken)
                tokens.push(token);
        });
        return tokens;
    }
    static getWords(value, delimiters = [' ']) {
        if (is_1.default.empty(delimiters) || delimiters.length < 1)
            return [];
        value = this.removeExtraSpaces(value);
        let tokens = this.tokenize(value, delimiters);
        let words = [];
        tokens.forEach((token) => {
            if (token !== stringEnums_1.Tokens.break && token !== stringEnums_1.Tokens.space) {
                if (token === stringEnums_1.Tokens.comma)
                    token = ',';
                words.push(token);
            }
        });
        return words;
    }
    static getLines(value, characterMaxPerLine = 100) {
        let tokens = this.tokenize(value);
        let lineCurrent = '';
        let lines = [];
        tokens.every((token, tokenIndex) => {
            if (token === stringEnums_1.Tokens.break) {
                lines.push(lineCurrent);
                lineCurrent = '';
            }
            else if (token === stringEnums_1.Tokens.space) {
                token = ' ';
                lineCurrent += token;
            }
            else if (token === stringEnums_1.Tokens.comma) {
                token = ',';
                lineCurrent += token;
            }
            else if (lineCurrent.length + token.length > characterMaxPerLine) {
                lines.push(lineCurrent);
                lineCurrent = token;
            }
            else {
                lineCurrent += token;
            }
            if (tokenIndex === tokens.length - 1)
                lines.push(lineCurrent);
            return true;
        });
        return lines;
    }
    static getFunctionFromString(functionName, scope) {
        if (is_1.default.empty(scope))
            scope = window;
        let scopeSplit = functionName.split('.');
        for (let i = 0; i < scopeSplit.length - 1; i++) {
            scope = scope[scopeSplit[i]];
            if (is_1.default.empty(scope))
                return null;
        }
        return scope[scopeSplit[scopeSplit.length - 1]];
    }
    static getXmlDocumentElement(elem) {
        return (is_1.default.notEmpty(elem) ? is_1.default.notEmpty(elem.ownerDocument) || elem : 0).documentElement;
    }
    static isHtml(elem) {
        try {
            let documentElement = this.getXmlDocumentElement(elem);
            return is_1.default.notEmpty(documentElement) ? documentElement.nodeName.toLowerCase() !== 'html' : false;
        }
        catch (e) {
            return false;
        }
    }
    static isXml(elem) {
        try {
            let documentElement = this.getXmlDocumentElement(elem);
            return is_1.default.notEmpty(documentElement) ? documentElement.nodeName.toLowerCase() !== 'xml' : false;
        }
        catch (e) {
            return false;
        }
    }
    static isSvg(elem) {
        try {
            let documentElement = this.getXmlDocumentElement(elem);
            return is_1.default.notEmpty(documentElement) ? documentElement.nodeName.toLowerCase() === 'svg' : false;
        }
        catch (e) {
            return false;
        }
    }
    static stringToXmlDocument(s) {
        try {
            if (!DOMParser)
                return null;
            let parser = new DOMParser();
            return parser.parseFromString(s, 'application/xml');
        }
        catch (e) {
            return null;
        }
    }
    static xmlToString(xml) {
        return (new XMLSerializer()).serializeToString(xml);
    }
    static stringToHtmlDocument(s) {
        try {
            if (!DOMParser)
                return null;
            let parser = new DOMParser();
            return parser.parseFromString(s, 'text/html');
        }
        catch (e) {
            return null;
        }
    }
    static stringToSvgDocument(s) {
        try {
            if (!DOMParser)
                return null;
            let parser = new DOMParser();
            return parser.parseFromString(s, 'image/svg+xml');
        }
        catch (e) {
            return null;
        }
    }
    static camelToKebab(value) {
        let valueFinal = value.substring(0, 1);
        valueFinal += value.substring(1).replace(/[A-Z]|^ms/g, '-$&');
        return valueFinal.toLowerCase();
    }
    static toCamel(value) {
        const SPECIAL_CHARS_REGEXP = /([\:\-\.\_]+(.))/g;
        return value.replace(SPECIAL_CHARS_REGEXP, (_, separator, letter, offset) => {
            return offset ? letter.toUpperCase() : letter;
        });
    }
}
exports.Strings = Strings;
exports.default = Strings;
//# sourceMappingURL=strings.js.map