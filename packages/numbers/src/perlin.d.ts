export declare class Perlin {
    constructor();
    _PERLIN_YWRAPB: number;
    _PERLIN_YWRAP: number;
    _PERLIN_ZWRAPB: number;
    _PERLIN_ZWRAP: number;
    _PERLIN_SIZE: number;
    _perlin: number[];
    _perlin_octaves: number;
    _perlin_amp_falloff: number;
    noise(x: number, y: number, z: number): number;
    detail(octaves: number, falloff: number): void;
    seed(value: number): void;
    private _scaled_cosine(i);
}
export default Perlin;
