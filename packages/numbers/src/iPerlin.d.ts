export interface IPerlin {
    noise(x: number, y: number, z: number): number;
    detail(octaves: number, falloff: number): void;
    seed(value: number): void;
}
export default IPerlin;
