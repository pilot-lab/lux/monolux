export declare class IntersectionPoint {
    x: number;
    y: number;
    isOnLine1: boolean;
    isOnLine2: boolean;
}
export declare class Numbers {
    static r(min?: number, max?: number, isInt?: boolean): number;
    static random(min?: number, max?: number, isInt?: boolean): number;
    static randomByte(): number;
    static angleToRadians(angleInDegrees: number): number;
    static angleToDegrees(angleInRadians: number): number;
    static angleRadians(x1: number, y1: number, x2: number, y2: number): number;
    static angle(x1: number, y1: number, x2: number, y2: number): number;
    static screenToCartesian(point: number[], screenWidth: number, screenHeight: number): number[];
    static cartesianToScreen(point: number[], screenWidth: number, screenHeight: number): number[];
    static constrainToInterval(value: number, interval: number): number;
    static round(value: number, decimalPlaces: number): number;
    static floor(value: number, decimalPlaces: number): number;
    static ceil(value: number, decimalPlaces: number): number;
    private static _decimalAdjust(type, value, exp);
    static intersection(line1X1: number, line1Y1: number, line1X2: number, line1Y2: number, line2X1: number, line2Y1: number, line2X2: number, line2Y2: number): IntersectionPoint;
    static distanceBetweenPoints(pointA: number[], pointB: number[]): number;
    static midpoint(x1: number, y1: number, x2: number, y2: number): number[];
    static nearestPointOnLine(point: number[], lineStart: number[], lineEnd: number[]): number[];
    static pointOnLine(p: number, lineStart: number[], lineEnd: number[]): number[];
    static pointOnLineScreen(p: number, lineStart: number[], lineEnd: number[], screenWidth: number, screenHeight: number): number[];
    static distanceToLine(point: number[], lineStart: number[], lineEnd: number[]): number;
    static randomPointWithinCircle(x: number, y: number, radius: number): number[];
    static isPointInEllipse(x: number, y: number, ellipseCenterX: number, ellipseCenterY: number, radiusX: number, radiusY: number, rotation: number): boolean;
    static pointOnEllipse(angleAlongCircumference: number, ellipseRadiusX: number, ellipseRadiusY: number, ellipseCenterX?: number, ellipseCenterY?: number): number[];
    static rotate(x: number, y: number, angle: number, originX?: number, originY?: number): number[];
    static rotateRadians(x: number, y: number, angle: number, originX?: number, originY?: number): number[];
    static rotateX(x: number, y: number, z: number, angle: number, originX?: number, originY?: number, originZ?: number): number[];
    static rotateXRadians(x: number, y: number, z: number, angle: number, originX?: number, originY?: number, originZ?: number): number[];
    static rotateY(x: number, y: number, z: number, angle: number, originX?: number, originY?: number, originZ?: number): number[];
    static rotateYRadians(x: number, y: number, z: number, angle: number, originX?: number, originY?: number, originZ?: number): number[];
    static rotateZ(x: number, y: number, z: number, angle: number, originX?: number, originY?: number, originZ?: number): number[];
    static rotateZRadians(x: number, y: number, z: number, angle: number, originX?: number, originY?: number, originZ?: number): number[];
    static getIndices(indexStart: number, indexEnd: number, isShuffle: boolean): number[];
    static normalize(value: number, min: number, max: number, isConstrain?: boolean, isInvert?: boolean): number;
    static unnormalize(valueNormalized: number, min: number, max: number, isConstrain?: boolean, isInvert?: boolean): number;
    static clamp(value: number, min: number, max: number): number;
}
export default Numbers;
