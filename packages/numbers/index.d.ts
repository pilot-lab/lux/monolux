import Perlin from './src/perlin';
import Numbers from './src/numbers';
import IPerlin from './src/iPerlin';
export { Perlin, Numbers, IPerlin };
export default Numbers;
