import is from '@pilotlab/is';
import Result from '@pilotlab/result';
import { Size } from '@pilotlab/data';


export class WebTools {
    static loadJavascript(url:string):Result<boolean> {
        let result:Result<boolean> = new Result<boolean>();

        let script:HTMLScriptElement = document.createElement('script');
        script.setAttribute('type', 'text/javascript');
        script.setAttribute('src', url);

        if (is.notEmpty(script)) {
            script.onload = () => { result.resolve(true); };
            let firstScript:HTMLScriptElement = <HTMLScriptElement>document.body.getElementsByTagName('script')[0];
            if (is.empty(firstScript)) document.body.appendChild(script);
            else document.body.insertBefore(script, firstScript);
        } else result.resolve(false);

        return result;
    }


    /**
     * Method to calculate the size of the content
     * and return back the object which contains the width and the height of the content
     * @param {Object} node - html string or dom element
     * @return {object} width/height of content
     */
    static calculateContentSize(node:any) {
        let realWidth:number, realHeight:number;
        let tmpDiv:HTMLElement;
        let previousDisplay:string;

        tmpDiv = document.createElement('div');
        tmpDiv.style.visibility = 'hidden';

        /// dom object
        if (typeof node == 'object' && node != null) {
            let parent = node.parent;
            previousDisplay = node.style.display;
            /// must set node to inline
            node.style.display = 'inline';
            tmpDiv.appendChild(node);
            /// must set node container to inline too
            tmpDiv.style.display = 'inline';
            document.body.appendChild(tmpDiv);
            realWidth = tmpDiv.offsetWidth;
            realHeight = tmpDiv.offsetHeight;
            node.style.display = previousDisplay;
            document.body.removeChild(tmpDiv);
            if (typeof parent == 'object' && parent != null) {
                parent.appendChild(node);
            }
        }
        /// html string
        else if (typeof node == 'string' && node != '') {
            previousDisplay = tmpDiv.style.display;
            tmpDiv.innerHTML = node;
            tmpDiv.style.display = 'inline';
            document.body.appendChild(tmpDiv);
            realWidth = tmpDiv.offsetWidth;
            realHeight = tmpDiv.offsetHeight;
            tmpDiv.style.display = previousDisplay;
            document.body.removeChild(tmpDiv);
        } else {
            realWidth = 0;
            realHeight = 0;
        }

        return {width: realWidth, height: realHeight};
    }


    static calculateHeight(strHtml:string, width?:number):number {
        let realHeight:number;
        let tmpDiv:HTMLElement;

        tmpDiv = document.createElement('div');
        tmpDiv.style.visibility = 'hidden';
        tmpDiv.style.position = 'absolute';
        tmpDiv.style.display = 'block';
        if (width !== undefined) tmpDiv.style.width = width + 'px';

        tmpDiv.innerHTML = strHtml;
        document.body.appendChild(tmpDiv);
        realHeight = tmpDiv.offsetHeight;
        document.body.removeChild(tmpDiv);

        return realHeight;
    }


    static calculateHtmlSize(strHtml:string, width?:number):Size {
        let realWidth:number;
        let realHeight:number;
        let tmpDiv:HTMLElement;

        tmpDiv = document.createElement('div');
        tmpDiv.style.visibility = 'hidden';
        tmpDiv.style.position = 'absolute';
        tmpDiv.style.display = 'block';
        if (width !== undefined) tmpDiv.style.width = width + 'px';

        tmpDiv.innerHTML = strHtml;
        document.body.appendChild(tmpDiv);
        realWidth = tmpDiv.offsetWidth;
        realHeight = tmpDiv.offsetHeight;
        document.body.removeChild(tmpDiv);

        return new Size(realWidth, realHeight);
    }


    static autoEllipseText(text:string, width:number, height:number, className:string):string {
        let tmpDiv:HTMLElement;
        let returnText:string = text;

        tmpDiv = document.createElement('span');
        tmpDiv.style.maxWidth = 'none';
        tmpDiv.style.maxHeight = 'none';
        tmpDiv.style.visibility = 'hidden';
        tmpDiv.style.position = 'absolute';
        tmpDiv.style.display = 'block';
        tmpDiv.style.width = width + 'px';
        tmpDiv.className = className;
        document.body.appendChild(tmpDiv);

        let i = 1;
        tmpDiv.innerHTML = text;

        if (tmpDiv.offsetHeight > height) {
            tmpDiv.innerHTML = '';

            while (tmpDiv.offsetHeight < height && i < text.length) {
                tmpDiv.innerHTML = text.substr(0, i) + '...';
                i++;
            }

            returnText = tmpDiv.innerHTML;
        }

        document.body.removeChild(tmpDiv);

        return returnText;
    }


    static imageToCanvas(image:HTMLImageElement | HTMLCanvasElement | HTMLVideoElement):HTMLCanvasElement {
        if (is.empty(image)) return;

        document.body.appendChild(image);

        let imageWidth:number = image.offsetWidth;
        let imageHeight:number = image.offsetHeight;

        let canvas:HTMLCanvasElement = document.createElement('canvas');
        let context:CanvasRenderingContext2D = canvas.getContext('2d');
        canvas.width = imageWidth;
        canvas.height = imageHeight;
        context.drawImage(<HTMLImageElement>image, 0, 0);

        document.body.removeChild(image);

        return canvas;
    }


    static canvasToImage(canvas:HTMLCanvasElement):HTMLImageElement {
        let image:HTMLImageElement = new Image();
        image.src = canvas.toDataURL('image/png');
        return image;
    }


    static getScrollOffset():number[] {
        try {
            const supportPageOffset = window.pageXOffset !== undefined;
            const isCSS1Compat = ((document.compatMode || '') === 'CSS1Compat');

            let x: number = 0;
            let y: number = 0;

            if (supportPageOffset) x = window.pageXOffset;
            else if (isCSS1Compat) x = document.documentElement.scrollLeft;
            else x = document.body.scrollLeft;

            if (supportPageOffset) y = window.pageYOffset;
            else if (isCSS1Compat) y = document.documentElement.scrollTop;
            else y = document.body.scrollTop;

            return [x, y];
        } catch (e) {
            return [10000, 10000];
        }
    }
} // End of class


export default WebTools;
