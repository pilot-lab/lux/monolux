import Result from '@pilotlab/result';
import { Size } from '@pilotlab/data';
export declare class WebTools {
    static loadJavascript(url: string): Result<boolean>;
    static calculateContentSize(node: any): {
        width: number;
        height: number;
    };
    static calculateHeight(strHtml: string, width?: number): number;
    static calculateHtmlSize(strHtml: string, width?: number): Size;
    static autoEllipseText(text: string, width: number, height: number, className: string): string;
    static imageToCanvas(image: HTMLImageElement | HTMLCanvasElement | HTMLVideoElement): HTMLCanvasElement;
    static canvasToImage(canvas: HTMLCanvasElement): HTMLImageElement;
    static getScrollOffset(): number[];
}
export default WebTools;
