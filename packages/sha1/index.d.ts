import { IPromise } from '@pilotlab/result';
export declare class SHA1 {
    static POW_2_24: number;
    static POW_2_32: number;
    static hex(n: number): string;
    static lrot(n: number, bits: number): number;
    static blobToArrayBuffer(value: Blob): IPromise<ArrayBuffer>;
    static stringToArrayBuffer(s: string): ArrayBuffer;
    static hash(content: (Blob | ArrayBuffer | string)): IPromise<string>;
    static hashFromArrayBuffer(contentArrayBuffer: ArrayBuffer): string;
}
export default SHA1;
