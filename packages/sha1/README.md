# sha1

Generates SHA1 IDs.

## Install

`sudo npm install --save @pilotlab/sha1`

## Usage 

```
import SHA1 from '@pilotlab/sha1';

SHA1.hash(blob).then((hash:string) => {
    /// Do something with hash.
});
```
