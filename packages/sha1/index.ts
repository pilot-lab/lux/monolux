import { Result, IPromise } from '@pilotlab/result';


/**
 * Original code by T. Michael Keesey
 * https://bitbucket.org/keesey/sha1/overview
 *
 * Licensed under MIT open source license http://opensource.org/licenses/MIT
 *
 * Call sha1.hash(x) to get a SHA-1 hash. The variable x can be an ArrayBuffer (optimal usage) or a string. Any other type of object will be converted into a string.
 * NOTE: Be sure to pass the .buffer property of a typed array rather than the array itself.
 *
 * INCORRECT:
 *
 * let data = new Uint8Array(256);
 *
 * /// Will generate SHA-1 hash for the string "[object Uint8Array]"!
 * let hash = sha1.hash(data);
 *
 * CORRECT:
 *
 * let data = new Uint8Array(256);
 *
 * let hash = sha1.hash(data.buffer);
 */
class Uint32ArrayBigEndian {
    constructor(length: number) { this.bytes = new Uint8Array(length << 2); }


    bytes:Uint8Array;


    get(index: number): number {
        index <<= 2;
        return (this.bytes[index] * SHA1.POW_2_24)
            + ((this.bytes[index + 1] << 16)
            | (this.bytes[index + 2] << 8)
            | this.bytes[index + 3]);
    }
    set(index: number, value: number) {
        let high = Math.floor(value / SHA1.POW_2_24),
            rest = value - (high * SHA1.POW_2_24);
        index <<= 2;
        this.bytes[index] = high;
        this.bytes[index + 1] = rest >> 16;
        this.bytes[index + 2] = (rest >> 8) & 0xFF;
        this.bytes[index + 3] = rest & 0xFF;
    }
} // End class


/**
 * Dependencies: none
 */
export class SHA1 {
    static POW_2_24 = Math.pow(2, 24);
    static POW_2_32 = Math.pow(2, 32);


    static hex(n: number): string {
        let s = '',
            v: number;
        for (let i = 7; i >= 0; --i) {
            v = (n >>> (i << 2)) & 0xF;
            s += v.toString(16);
        }
        return s;
    }


    static lrot(n: number, bits: number): number { return ((n << bits) | (n >>> (32 - bits))); }


    static blobToArrayBuffer(value:Blob):IPromise<ArrayBuffer> {
        let result:Result<ArrayBuffer> = new Result<ArrayBuffer>();
        let arrayBuffer;
        let fileReader = new FileReader();
        fileReader.onload = () => {
            arrayBuffer = fileReader.result;
            result.resolve(arrayBuffer);
        };
        fileReader.readAsArrayBuffer(value);
        return result;
    }


    static stringToArrayBuffer(s:string):ArrayBuffer {
        s = s.replace(/[\u0080-\u07ff]/g,
            function (c:string) {
                let code = c.charCodeAt(0);
                return String.fromCharCode(0xC0 | code >> 6, 0x80 | code & 0x3F);
            });

        s = s.replace(/[\u0080-\uffff]/g,
            function (c:string) {
                let code = c.charCodeAt(0);
                return String.fromCharCode(0xE0 | code >> 12, 0x80 | code >> 6 & 0x3F, 0x80 | code & 0x3F);
            });

        let n = s.length,
            array = new Uint8Array(n);

        for (let i = 0; i < n; ++i) { array[i] = s.charCodeAt(i); }

        return array.buffer;
    }


    static hash(content:(Blob | ArrayBuffer | string)):IPromise<string>{
        let result:Result<string> = new Result<string>();
        let resultGetArrayBuffer:Result<ArrayBuffer> = new Result<ArrayBuffer>();

        /// First get an array buffer from the supplied content.
        if (content instanceof ArrayBuffer) resultGetArrayBuffer.resolve(<ArrayBuffer>content);
        else if (content instanceof Blob){
            this.blobToArrayBuffer(<Blob>content).then((arrayBuffer:ArrayBuffer) => {
                resultGetArrayBuffer.resolve(arrayBuffer);
            });
        } else resultGetArrayBuffer.resolve(this.stringToArrayBuffer(String(content)));

        /// Then use the array buffer to generate an SHA-1 hash.
        resultGetArrayBuffer.then((arrayBuffer:ArrayBuffer) => {
            result.resolve(this.hashFromArrayBuffer(arrayBuffer));
        });

        return result;
    }


    static hashFromArrayBuffer(contentArrayBuffer:ArrayBuffer):string {
        let h0 = 0x67452301,
            h1 = 0xEFCDAB89,
            h2 = 0x98BADCFE,
            h3 = 0x10325476,
            h4 = 0xC3D2E1F0,
            i:number,
            sbytes = contentArrayBuffer.byteLength,
            sbits = sbytes << 3,
            minbits = sbits + 65,
            bits = Math.ceil(minbits / 512) << 9,
            bytes = bits >>> 3,
            slen = bytes >>> 2,
            s = new Uint32ArrayBigEndian(slen),
            s8 = s.bytes,
            j:number,
            w = new Uint32Array(80),
            sourceArray = new Uint8Array(contentArrayBuffer);

        for (i = 0; i < sbytes; ++i) { s8[i] = sourceArray[i]; }

        s8[sbytes] = 0x80;
        s.set(slen - 2, Math.floor(sbits / this.POW_2_32));
        s.set(slen - 1, sbits & 0xFFFFFFFF);

        for (i = 0; i < slen; i += 16) {
            for (j = 0; j < 16; ++j) { w[j] = s.get(i + j); }
            for (; j < 80; ++j) { w[j] = this.lrot(w[j - 3] ^ w[j - 8] ^ w[j - 14] ^ w[j - 16], 1); }

            let a = h0,
                b = h1,
                c = h2,
                d = h3,
                e = h4,
                f:number,
                k:number,
                temp:number;

            for (j = 0; j < 80; ++j) {
                if (j < 20) {
                    f = (b & c) | ((~b) & d);
                    k = 0x5A827999;
                } else if (j < 40) {
                    f = b ^ c ^ d;
                    k = 0x6ED9EBA1;
                } else if (j < 60) {
                    f = (b & c) ^ (b & d) ^ (c & d);
                    k = 0x8F1BBCDC;
                } else {
                    f = b ^ c ^ d;
                    k = 0xCA62C1D6;
                }

                temp = (this.lrot(a, 5) + f + e + k + w[j]) & 0xFFFFFFFF;
                e = d;
                d = c;
                c = this.lrot(b, 30);
                b = a;
                a = temp;
            }

            h0 = (h0 + a) & 0xFFFFFFFF;
            h1 = (h1 + b) & 0xFFFFFFFF;
            h2 = (h2 + c) & 0xFFFFFFFF;
            h3 = (h3 + d) & 0xFFFFFFFF;
            h4 = (h4 + e) & 0xFFFFFFFF;
        }

        return this.hex(h0) + this.hex(h1) + this.hex(h2) + this.hex(h3) + this.hex(h4);
    }
} // End class


export default SHA1;
