import is from '@pilotlab/is';


export class Factory<TBaseClass> {
    constructor(scope:any, className:string, description:any = {}) {
        this.scope = scope;
        this.className = className;
        this.description = description;
    }


    scope:any;
    className:string;
    description:any;


    /// TODO: passing args in the constructor breaks things in @pilotab/data
    instance(args?:any[]):TBaseClass {
        try {
            const ClassProxy:any = this.scope[this.className];
            return new ClassProxy();
        } catch(e) {
            console.log(`Unable to create class in Factory: ${this.className}`);
        }
    }


    /**
     * Reference: https://www.stevefenton.co.uk/Content/Blog/Date/201407/Blog/Creating-TypeScript-Classes-Dynamically/
     * @param className {string} The key of the class that we want to create an instance of.
     * @param scope {Object} the context where the class is defined (ie. window).
     * @param args {...} Any arguments we want to pass to the constructor.
     */
    static getClassInstance<TClass>(className:string, scope?:any, ...args:any[]):TClass {
        if (is.empty(scope)) scope = window;

        const ClassProxy:any = scope[className];
        return new ClassProxy();
    }
} // End class


export default Factory;
