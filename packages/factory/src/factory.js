"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
class Factory {
    constructor(scope, className, description = {}) {
        this.scope = scope;
        this.className = className;
        this.description = description;
    }
    instance(args) {
        try {
            const ClassProxy = this.scope[this.className];
            return new ClassProxy();
        }
        catch (e) {
            console.log(`Unable to create class in Factory: ${this.className}`);
        }
    }
    static getClassInstance(className, scope, ...args) {
        if (is_1.default.empty(scope))
            scope = window;
        const ClassProxy = scope[className];
        return new ClassProxy();
    }
}
exports.Factory = Factory;
exports.default = Factory;
//# sourceMappingURL=factory.js.map