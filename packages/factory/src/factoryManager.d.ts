import { MapList } from '@pilotlab/collections';
import Factory from './factory';
export declare class FactoryManager<TBaseClass> {
    readonly factories: MapList<string, Factory<TBaseClass>>;
    protected p_factories: MapList<string, Factory<TBaseClass>>;
    register<TClass extends TBaseClass>(className: string, scope?: Object, aliases?: string[], description?: any): void;
    instance(className: string, ...args: any[]): TBaseClass;
    getDescription(className: string): any;
}
export default FactoryManager;
