export declare class Factory<TBaseClass> {
    constructor(scope: any, className: string, description?: any);
    scope: any;
    className: string;
    description: any;
    instance(args?: any[]): TBaseClass;
    static getClassInstance<TClass>(className: string, scope?: any, ...args: any[]): TClass;
}
export default Factory;
