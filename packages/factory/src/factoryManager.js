"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const debug_1 = require("@pilotlab/debug");
const collections_1 = require("@pilotlab/collections");
const factory_1 = require("./factory");
class FactoryManager {
    constructor() {
        this.p_factories = new collections_1.MapList();
    }
    get factories() { return this.p_factories; }
    register(className, scope = window, aliases = [], description) {
        if (is_1.default.empty(scope))
            scope = window;
        if (is_1.default.empty(aliases))
            aliases = [];
        let classFactory = new factory_1.default(scope, className, description);
        this.p_factories.set(className.toLowerCase(), classFactory);
        if (is_1.default.notEmpty(aliases)) {
            for (let i = 0; i < aliases.length; i++) {
                this.p_factories.set(aliases[i].toLowerCase(), classFactory);
            }
        }
    }
    instance(className, ...args) {
        try {
            let classFactory = this.p_factories.get(className.toLowerCase());
            if (is_1.default.empty(classFactory)) {
                debug_1.default.log('FactoryManager was unable to find a registered factory named: ' + className);
                return null;
            }
            return classFactory.instance(args);
        }
        catch (e) {
            debug_1.default.error(`${e}, className: ${className}`, 'FactoryManager.instance(...)');
        }
    }
    getDescription(className) {
        const classFactory = this.p_factories.get(className.toLowerCase());
        return is_1.default.notEmpty(classFactory) ? classFactory.description : {};
    }
}
exports.FactoryManager = FactoryManager;
exports.default = FactoryManager;
//# sourceMappingURL=factoryManager.js.map