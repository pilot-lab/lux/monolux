import is from '@pilotlab/is';
import Debug from '@pilotlab/debug';
import {MapList} from '@pilotlab/collections';
import Factory from './factory';


export class FactoryManager<TBaseClass> {
    get factories():MapList<string, Factory<TBaseClass>> { return this.p_factories; }
    protected p_factories:MapList<string, Factory<TBaseClass>> = new MapList<string, Factory<TBaseClass>>();


    /// NOTE: In a browser-based application, the scope will be window.
    register<TClass extends TBaseClass>(className:string, scope:Object = window, aliases:string[] = [], description?:any):void {
        if (is.empty(scope)) scope = window;
        if (is.empty(aliases)) aliases = [];
        let classFactory:Factory<TClass> = new Factory<TClass>(scope, className, description);
        this.p_factories.set(className.toLowerCase(), classFactory);
        if (is.notEmpty(aliases)) {
            for (let i:number = 0; i < aliases.length; i++) {
                this.p_factories.set(aliases[i].toLowerCase(), classFactory);
            }
        }
    }


    instance(className:string, ...args:any[]):TBaseClass {
        try {
            let classFactory:Factory<TBaseClass> = this.p_factories.get(className.toLowerCase());
            if (is.empty(classFactory)) {
                Debug.log('FactoryManager was unable to find a registered factory named: ' + className);
                return null;
            }

            return classFactory.instance(args);
        } catch(e) { Debug.error(`${e}, className: ${className}`, 'FactoryManager.instance(...)'); }
    }


    getDescription(className:string):any {
        const classFactory:Factory<any> =this.p_factories.get(className.toLowerCase());
        return is.notEmpty(classFactory) ? classFactory.description : {};
    }
} // End class


export default FactoryManager;
