import {
    AsyncTest,
    Setup,
    SetupFixture,
    Teardown,
    TeardownFixture,
    Test,
    TestCase,
    TestFixture,
    Timeout,
    Expect
} from 'alsatian';
import { Debug } from '@pilotlab/debug';


import {
    IProgressRunFunction,
    ProgressCommand,
    ProgressReport,
    ProgressRunArgs,
    ProgressTracker
} from '../index';


@TestFixture('Attribute Tests')
export class Tests {
    @AsyncTest('timer intervalPassed event')
    @Timeout(5000) // Alsatian will now wait 5 seconds before failing
    public async asyncTest() {
        const promise:Promise<any> = new Promise((resolve, reject) => {
            /// Instantiate our progress tracker.
            let progress:ProgressTracker = new ProgressTracker();

            /// Queue up our first command.
            progress.queue(
                (args:ProgressRunArgs, lastProgressCommandReturnValue:any, ...runArgs:any[]) => {
                    /// This command just logs a message to the console.
                    Debug.info('Progress Command 01');

                    /// Queue up a sub-command.
                    args.queue(
                        (args:ProgressRunArgs, lastProgressCommandReturnValue:any, ...runArgs:any[]) => {
                            Debug.info('Progress Command 01 Sub-command 01');
                            args.result.resolve();
                        },
                        this,
                        'Command 01 sub-command 01 running'
                    );

                    /// This sub-command has a relative weight of 3, so it's expected to take 3
                    /// times as long to complete as our first sub-command.
                    args.queue(
                        (args:ProgressRunArgs, lastProgressCommandReturnValue:any, ...runArgs:any[]) => {
                            Debug.info('Progress Command 01 Sub-command 02');
                            args.result.resolve();
                        },
                        this,
                        'Command 01 sub-command 02 running',
                        3 // relative weight of task (default is 1)
                    );

                    /// Run our queued sub-commands before continuing with our
                    /// top-level commands.
                    args.run().then(() => { args.result.resolve(); });
                },
                this,
                'Command 01 running'
            );

            /// Queue up another top-level command.
            progress.queue(
                (args:ProgressRunArgs, lastProgressCommandReturnValue:any, ...runArgs:any[]) => {
                    Debug.info('Progress Command 02');

                    /// Only 1 sub-command here.
                    args.queue(
                        (args:ProgressRunArgs, lastProgressCommandReturnValue:any, ...runArgs:any[]) => {
                            Debug.info('Progress Command 02 Sub-command 01');
                            args.result.resolve();
                        },
                        this,
                        'Command 01 sub-command 01 running'
                    );

                    args.run().then(() => { args.result.resolve(); });
                },
                this,
                'Command 02 running',
                0.2 // relative weight of task (default is 1)
            );

            /// Log a message every time a queued command is run and the progress tracker proceeds by 1 step.
            progress.progressed.listen((report:ProgressReport) => {
                Debug.log(`step: ${report.stepCurrent}/${report.stepsTotal}, progress: ${report.percent}%`, report.message)
            });

            /// Let us know when all the commands have been run.
            progress.completed.listen(() => {
                Debug.log('Progress completed');
                resolve();
            });

            progress.run();
        });
    }
} // End Tests
