import is from '@pilotlab/is';
import { HiResTimer } from '@pilotlab/timer';
import { IPromise, Result } from '@pilotlab/result';
import { Command } from '@pilotlab/commands';
import ProgressRunArgs from './progressRunArgs';
import ProgressTracker from './progressTracker';
import IProgressRunFunction from './iProgressRunFunction';


export class ProgressCommand extends Command {
    constructor(
        progressTracker:ProgressTracker,
        runFunction?:IProgressRunFunction,
        scope?:any,
        message?:string,
        weight:number = 1
    ) {
        super(runFunction, null, scope);
        this.parentTracker = progressTracker;
        this.message = message;
        this.weight = weight;

        let timer:HiResTimer = is.notEmpty(progressTracker) ? progressTracker.timer : null;
        this.subTracker = new ProgressTracker(timer, this);
    }


    parentTracker:ProgressTracker;
    weight:number;
    subTracker:ProgressTracker;


    run(...args:any[]):IPromise<any> {
        if (is.empty(this.runFunction)) return Result.resolve<any>(null);
        if (is.notEmpty(args) && args.length > 0) this.runArgs = args;

        let progressRunArgs:ProgressRunArgs = new ProgressRunArgs(this.subTracker);
        let argsFinal:any[] = [progressRunArgs, this.parentTracker.lastReturnValue];
        if (is.notEmpty(this.runArgs)) argsFinal = argsFinal.concat(this.runArgs);

        let returnValue:any = this.runFunction.apply(this.scope, argsFinal);

        if (returnValue instanceof Result) return returnValue;
        else return Result.resolve<any>(returnValue);
    }
} // End class


export default ProgressCommand;
