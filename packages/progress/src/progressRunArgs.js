"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const result_1 = require("@pilotlab/result");
class ProgressRunArgs {
    constructor(subTracker) {
        this._subTracker = subTracker;
        this.result = new result_1.Result();
    }
    get subTracker() { return this._subTracker; }
    get completed() { return this._subTracker.completed; }
    queue(runFunction, scope, message, weight = 1) {
        this._subTracker.queue(runFunction, scope, message, weight);
        return this._subTracker;
    }
    run() {
        return this._subTracker.run();
    }
}
exports.ProgressRunArgs = ProgressRunArgs;
exports.default = ProgressRunArgs;
//# sourceMappingURL=progressRunArgs.js.map