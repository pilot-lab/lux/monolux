"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const result_1 = require("@pilotlab/result");
const commands_1 = require("@pilotlab/commands");
const progressRunArgs_1 = require("./progressRunArgs");
const progressTracker_1 = require("./progressTracker");
class ProgressCommand extends commands_1.Command {
    constructor(progressTracker, runFunction, scope, message, weight = 1) {
        super(runFunction, null, scope);
        this.parentTracker = progressTracker;
        this.message = message;
        this.weight = weight;
        let timer = is_1.default.notEmpty(progressTracker) ? progressTracker.timer : null;
        this.subTracker = new progressTracker_1.default(timer, this);
    }
    run(...args) {
        if (is_1.default.empty(this.runFunction))
            return result_1.Result.resolve(null);
        if (is_1.default.notEmpty(args) && args.length > 0)
            this.runArgs = args;
        let progressRunArgs = new progressRunArgs_1.default(this.subTracker);
        let argsFinal = [progressRunArgs, this.parentTracker.lastReturnValue];
        if (is_1.default.notEmpty(this.runArgs))
            argsFinal = argsFinal.concat(this.runArgs);
        let returnValue = this.runFunction.apply(this.scope, argsFinal);
        if (returnValue instanceof result_1.Result)
            return returnValue;
        else
            return result_1.Result.resolve(returnValue);
    }
}
exports.ProgressCommand = ProgressCommand;
exports.default = ProgressCommand;
//# sourceMappingURL=progressCommand.js.map