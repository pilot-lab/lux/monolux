"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const signals_1 = require("@pilotlab/signals");
const timer_1 = require("@pilotlab/timer");
const commands_1 = require("@pilotlab/commands");
const progressCommand_1 = require("./progressCommand");
const progressReport_1 = require("./progressReport");
class ProgressTracker {
    constructor(timer, parentCommand, message) {
        this._stepsTotal = 0;
        this._stepCurrent = 0;
        this._weightTotal = 0;
        this._weightCompleted = 0;
        this.message = '';
        this._commands = new commands_1.CommandManager();
        this._isCompleted = false;
        this.progressed = new signals_1.Signal(false);
        this.completed = new signals_1.Signal(true);
        this._timer = is_1.default.notEmpty(timer) ? timer : timer_1.HiResTimer.timerDefault;
        if (is_1.default.notEmpty(parentCommand))
            this._parentCommand = parentCommand;
        this._commands.commandRunStart.listen(this._reportProgressCommandStart, this);
        this._commands.commandRunComplete.listen(this._reportProgressCommandComplete, this);
        if (is_1.default.notEmpty(message))
            this.message = message;
    }
    get timer() { return this._timer; }
    get parentCommand() { return this._parentCommand; }
    get stepsTotal() { return this._stepsTotal; }
    get stepCurrent() { return this._stepCurrent; }
    get weightTotal() { return this._weightTotal; }
    get weightCompleted() { return this._weightCompleted; }
    get commands() { return this._commands; }
    get lastReturnValue() { return this._commands.lastReturnValue; }
    get isCompleted() { return this._isCompleted; }
    queue(runFunction, scope, message = '', weight = 1) {
        let command = this._createCommand(runFunction, scope, message, weight);
        this.queueCommand(command);
        return this;
    }
    queueCommand(command, runArgs) {
        if (is_1.default.empty(command))
            return this;
        this._isCompleted = false;
        if (is_1.default.empty(command.parentTracker) && command.parentTracker != this)
            command.parentTracker = this;
        this._commands.queue(command, runArgs);
        this._stepsTotal++;
        this._weightTotal += command.weight;
        command.subTracker.progressed.listen(this._reportSubTrackerProgress, this);
        return this;
    }
    run() {
        this._startTime = is_1.default.notEmpty(this._timer) ? Math.round(this._timer.elapsedMilliseconds) : 0;
        this._timeLastStepCompleted = this._startTime;
        let result = this._commands.run();
        result.then(() => {
            this._commands.clearAllQueues();
            this._isCompleted = true;
            this._reportCompleted();
        });
        return result;
    }
    _createCommand(runFunction, scope, message, weight) {
        return new progressCommand_1.default(this, runFunction, scope, message, weight);
    }
    _reportProgressCommandStart(args) { this._reportProgress(args, false); }
    _reportProgressCommandComplete(args) { this._reportProgress(args, true); }
    _reportProgress(args, isStepCompleted) {
        let command = args.command;
        let weightCompleted = this._weightCompleted;
        if (isStepCompleted)
            this._weightCompleted += command.weight;
        this._stepCurrent = args.totalRunning - args.pending;
        let progress = this._weightCompleted / this._weightTotal;
        let progressUponStepCompletion = (weightCompleted + command.weight) / this._weightTotal;
        let percent = Math.floor(progress * 100);
        let elapsed = is_1.default.notEmpty(this._timer) ? Math.round(this._timer.elapsedMilliseconds) : 0;
        let timeToCompleteStep = 0;
        if (isStepCompleted) {
            timeToCompleteStep = elapsed - this._timeLastStepCompleted;
            this._timeLastStepCompleted = elapsed;
        }
        let timeElapsedSinceStart = elapsed - this._startTime;
        let report = new progressReport_1.default(command, this._stepsTotal, this._stepCurrent, this._weightTotal, this._weightCompleted, command.weight, isStepCompleted, timeToCompleteStep, timeElapsedSinceStart, progress, progressUponStepCompletion, percent, command.message);
        this.progressed.dispatch(report);
    }
    _reportSubTrackerProgress(args) {
        let subCommand = args.commandCurrent;
        let subTracker = subCommand.parentTracker;
        let command = subTracker.parentCommand;
        let subProgress = args.progress;
        let weightCurrent = command.weight * subProgress;
        let weightCompleted = this._weightCompleted + weightCurrent;
        let progress = weightCompleted / this._weightTotal;
        let percent = Math.floor(progress * 100);
        let elapsed = is_1.default.notEmpty(this._timer) ? Math.round(this._timer.elapsedMilliseconds) : 0;
        let timeToCompleteStep = 0;
        if (args.isStepCompleted) {
            timeToCompleteStep = elapsed - this._timeLastStepCompleted;
        }
        let timeElapsedSinceStart = elapsed - this._startTime;
        let report = new progressReport_1.default(command, this._stepsTotal, this._stepCurrent, this._weightTotal, weightCompleted, weightCurrent, args.isStepCompleted, timeToCompleteStep, timeElapsedSinceStart, progress, args.progressUponStepCompletion, percent, command.message + ' : ' + args.message);
        this.progressed.dispatch(report);
    }
    _reportCompleted() {
        this._weightCompleted = this._weightTotal;
        this._stepCurrent = this._stepsTotal;
        let timeElapsedSinceStart = is_1.default.notEmpty(this._timer) ? Math.round(this._timer.elapsedMilliseconds) - this._startTime : 0;
        let report = new progressReport_1.default(null, this._stepsTotal, this._stepCurrent, this._weightTotal, this._weightCompleted, 0, true, 0, timeElapsedSinceStart, 1, 1, 100, this.message);
        this.completed.dispatch(report);
    }
}
exports.ProgressTracker = ProgressTracker;
exports.default = ProgressTracker;
//# sourceMappingURL=progressTracker.js.map