import is from '@pilotlab/is';
import { IPromise } from '@pilotlab/result';
import { Signal } from '@pilotlab/signals';
import { HiResTimer } from '@pilotlab/timer';
import { CommandManager, CommandEventArgs } from '@pilotlab/commands';
import IProgressRunFunction from './iProgressRunFunction';
import ProgressCommand from './progressCommand';
import ProgressReport from './progressReport';


export class ProgressTracker {
    constructor(timer?:HiResTimer, parentCommand?:ProgressCommand, message?:string) {
        this._timer = is.notEmpty(timer) ? timer : HiResTimer.timerDefault;

        if (is.notEmpty(parentCommand)) this._parentCommand = parentCommand;
        this._commands.commandRunStart.listen(this._reportProgressCommandStart, this);
        this._commands.commandRunComplete.listen(this._reportProgressCommandComplete, this);
        if (is.notEmpty(message)) this.message = message;
    }


    /*====================================================================*
     START: Properties
     *====================================================================*/
    private _startTime:number;
    private _timeLastStepCompleted:number;


    get timer():HiResTimer { return this._timer; }
    private _timer:HiResTimer;


    get parentCommand():ProgressCommand { return this._parentCommand; }
    private _parentCommand:ProgressCommand;


    get stepsTotal():number { return this._stepsTotal; }
    private _stepsTotal:number = 0;


    get stepCurrent():number { return this._stepCurrent; }
    private _stepCurrent:number = 0;


    get weightTotal():number { return this._weightTotal; }
    private _weightTotal:number = 0;


    get weightCompleted():number { return this._weightCompleted; }
    private _weightCompleted:number = 0;


    message:string = '';


    get commands():CommandManager { return this._commands; }
    private _commands:CommandManager = new CommandManager();


    get lastReturnValue():any { return this._commands.lastReturnValue; }


    get isCompleted():boolean { return this._isCompleted; }
    private _isCompleted:boolean = false;



    progressed:Signal<ProgressReport> = new Signal<ProgressReport>(false);
    completed:Signal<ProgressReport> = new Signal<ProgressReport>(true);


    /*====================================================================*
     START: Public Methods
     *====================================================================*/
    /**
     * When run, a ProgressCommand IProgressRunFunction will receive a ProgressRunArgs object as the first argument.
     *
     * The ProgressRunArgs instance can be used to queue ProgressCommands recursively.
     *
     * The ProgressRunArgs object also holds a Result object that can be completed by the receiving IProgressRunFunction.
     * If this result is returned by the IProgressRunFunction, the tracker will wait until it has been completed
     * before progressing to the next command in the queue. Otherwise it will progress immediately.
     *
     * If a value was returned from the previously run progress command, it will be passed to the current
     * IProgressRunFunction as the second argument. If an array of values was returned from the previous command,
     * the values will be split into separate arguments following the ProgressRunArgs argument.
     */
    queue(
        runFunction:IProgressRunFunction,
        scope:any,
        message:string = '',
        weight:number = 1
    ):ProgressTracker {
        let command:ProgressCommand = this._createCommand(runFunction, scope, message, weight);
        this.queueCommand(command);
        return this;
    }


    queueCommand(command:ProgressCommand, runArgs?:any[]):ProgressTracker {
        if (is.empty(command)) return this;

        this._isCompleted = false;

        if (is.empty(command.parentTracker) && command.parentTracker != this) command.parentTracker = this;

        this._commands.queue(command, runArgs);

        this._stepsTotal++;
        this._weightTotal += command.weight;

        command.subTracker.progressed.listen(this._reportSubTrackerProgress, this);

        return this;
    }


    /**
     * Runs all commands in the queue, in sequence. Each subsequent command is run only after the previous
     * command has completed. The result returned by the run() method completes with the return value of the
     * last command in the queue.
     */
    run():IPromise<any> {
        this._startTime = is.notEmpty(this._timer) ? Math.round(this._timer.elapsedMilliseconds) : 0;
        this._timeLastStepCompleted = this._startTime;

        let result:IPromise<any> = this._commands.run();

        result.then(() => {
            this._commands.clearAllQueues();
            this._isCompleted = true;
            this._reportCompleted();
        });

        return result;
    }


    /*====================================================================*
     START: Private Methods
     *====================================================================*/
    private _createCommand(
        runFunction:IProgressRunFunction,
        scope:any,
        message:string,
        weight:number
    ):ProgressCommand {
        return new ProgressCommand(this, runFunction, scope, message, weight);
    }


    private _reportProgressCommandStart(args:CommandEventArgs):void { this._reportProgress(args, false); }
    private _reportProgressCommandComplete(args:CommandEventArgs):void { this._reportProgress(args, true); }
    private _reportProgress(args:CommandEventArgs, isStepCompleted:boolean):void {
        let command:ProgressCommand = (<ProgressCommand>args.command);
        let weightCompleted:number = this._weightCompleted;
        if (isStepCompleted) this._weightCompleted += command.weight;

        this._stepCurrent = args.totalRunning - args.pending;
        let progress:number = this._weightCompleted / this._weightTotal;
        let progressUponStepCompletion:number = (weightCompleted + command.weight) / this._weightTotal;
        let percent:number = Math.floor(progress * 100);

        let elapsed:number = is.notEmpty(this._timer) ? Math.round(this._timer.elapsedMilliseconds) : 0;
        let timeToCompleteStep:number = 0;

        if (isStepCompleted) {
            timeToCompleteStep = elapsed - this._timeLastStepCompleted;
            this._timeLastStepCompleted = elapsed;
        }

        let timeElapsedSinceStart:number = elapsed - this._startTime;

        let report:ProgressReport = new ProgressReport(
            command,
            this._stepsTotal,
            this._stepCurrent,
            this._weightTotal,
            this._weightCompleted,
            command.weight,
            isStepCompleted,
            timeToCompleteStep,
            timeElapsedSinceStart,
            progress,
            progressUponStepCompletion,
            percent,
            command.message
        );

        this.progressed.dispatch(report);
    }


    private _reportSubTrackerProgress(args:ProgressReport):void {
        let subCommand:ProgressCommand = (<ProgressCommand>args.commandCurrent);
        let subTracker:ProgressTracker = subCommand.parentTracker;
        let command:ProgressCommand = subTracker.parentCommand;

        let subProgress:number = args.progress;
        let weightCurrent:number = command.weight * subProgress;
        let weightCompleted:number = this._weightCompleted + weightCurrent;

        let progress:number = weightCompleted / this._weightTotal;
        let percent:number = Math.floor(progress * 100);

        let elapsed:number = is.notEmpty(this._timer) ? Math.round(this._timer.elapsedMilliseconds) : 0;
        let timeToCompleteStep:number = 0;

        if (args.isStepCompleted) {
            timeToCompleteStep = elapsed - this._timeLastStepCompleted;
        }

        let timeElapsedSinceStart:number = elapsed - this._startTime;

        let report:ProgressReport = new ProgressReport(
            command,
            this._stepsTotal,
            this._stepCurrent,
            this._weightTotal,
            weightCompleted,
            weightCurrent,
            args.isStepCompleted,
            timeToCompleteStep,
            timeElapsedSinceStart,
            progress,
            args.progressUponStepCompletion,
            percent,
            command.message + ' : ' + args.message
        );

        this.progressed.dispatch(report);
    }


    private _reportCompleted():void {
        this._weightCompleted = this._weightTotal;
        this._stepCurrent = this._stepsTotal;

        let timeElapsedSinceStart:number = is.notEmpty(this._timer) ? Math.round(this._timer.elapsedMilliseconds) - this._startTime : 0;

        let report:ProgressReport = new ProgressReport(
            null,
            this._stepsTotal,
            this._stepCurrent,
            this._weightTotal,
            this._weightCompleted,
            0,
            true,
            0,
            timeElapsedSinceStart,
            1,
            1,
            100,
            this.message
        );

        this.completed.dispatch(report);
    }
} // End Class


export default ProgressTracker;
