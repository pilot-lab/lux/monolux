"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const debug_1 = require("@pilotlab/debug");
class ProgressReport {
    constructor(commandCurrent, stepsTotal, stepCurrent, weightTotal, weightCompleted, weightCurrent, isStepCompleted, timeToCompleteStep, timeElapsedSinceStart, progress, progressUponStepCompletion, percent, message) {
        this.commandCurrent = commandCurrent;
        this.stepsTotal = stepsTotal;
        this.stepCurrent = stepCurrent;
        this.weightTotal = weightTotal;
        this.weightCompleted = weightCompleted;
        this.weightCurrent = weightCurrent;
        this.isStepCompleted = isStepCompleted;
        this.timeToCompleteStep = timeToCompleteStep;
        this.timeElapsedSinceStart = timeElapsedSinceStart;
        this.progress = progress;
        this.progressUponStepCompletion = progressUponStepCompletion;
        this.percent = percent;
        this.message = message;
    }
    log() {
        debug_1.default.log(`progress: ${this.progress}\n` +
            `progress upon step completion: ${this.progressUponStepCompletion}\n` +
            `percent complete: ${this.percent}\n` +
            `total steps: ${this.stepsTotal}\n` +
            `current step: ${this.stepCurrent}\n` +
            `step status: ${this.isStepCompleted ? 'completed' : 'starting'}\n` +
            `time to complete: ${this.timeToCompleteStep}`);
    }
}
exports.ProgressReport = ProgressReport;
exports.default = ProgressReport;
//# sourceMappingURL=progressReport.js.map