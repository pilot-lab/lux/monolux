import { IPromise, Result } from '@pilotlab/result';
import { Signal } from '@pilotlab/signals';
import ProgressTracker from './progressTracker';
import ProgressReport from './progressReport';
import IProgressRunFunction from './iProgressRunFunction';


export class ProgressRunArgs {
    constructor(
        subTracker:ProgressTracker
    ) {
        this._subTracker = subTracker;
        this.result = new Result<any>();
    }


    get subTracker():ProgressTracker { return this._subTracker; }
    private _subTracker:ProgressTracker;


    /**
     * This result can be ignored, or returned by the receiving IProgressRunFunction.
     * If it is returned, the ProgressTracker will wait until the result has been completed
     * before progressing to the next queued command. Otherwise the ProgressTracker will
     * progress immediately upon return of the IProgressRunFunction.
     */
    result:Result<any>;


    get completed():Signal<ProgressReport> { return this._subTracker.completed; }


    queue(
        runFunction?:IProgressRunFunction,
        scope?:any,
        message?:string,
        weight:number = 1
    ):ProgressTracker {
        this._subTracker.queue(runFunction, scope, message, weight);
        return this._subTracker;
    }


    run():IPromise<any> {
        return this._subTracker.run();
    }
} // End class

export default ProgressRunArgs;
