import Debug from '@pilotlab/debug';
import ProgressCommand from './progressCommand';


export class ProgressReport {
    constructor(
        commandCurrent:ProgressCommand,
        stepsTotal:number,
        stepCurrent:number,
        weightTotal:number,
        weightCompleted:number,
        weightCurrent:number,
        isStepCompleted:boolean,
        timeToCompleteStep:number,
        timeElapsedSinceStart:number,
        progress:number,
        progressUponStepCompletion:number,
        percent:number,
        message:string
    ) {
        this.commandCurrent = commandCurrent;
        this.stepsTotal = stepsTotal;
        this.stepCurrent = stepCurrent;
        this.weightTotal = weightTotal;
        this.weightCompleted = weightCompleted;
        this.weightCurrent = weightCurrent;
        this.isStepCompleted = isStepCompleted;
        this.timeToCompleteStep = timeToCompleteStep;
        this.timeElapsedSinceStart = timeElapsedSinceStart;
        this.progress = progress;
        this.progressUponStepCompletion = progressUponStepCompletion;
        this.percent = percent;
        this.message = message;
    }


    commandCurrent:ProgressCommand;
    stepsTotal:number;
    stepCurrent:number;
    isStepCompleted:boolean;
    timeToCompleteStep:number;
    timeElapsedSinceStart:number;
    weightTotal:number;
    weightCompleted:number;
    weightCurrent:number;
    progress:number;
    progressUponStepCompletion:number;
    percent:number;
    message:string;


    log():void {
        Debug.log(
            `progress: ${this.progress}\n` +
            `progress upon step completion: ${this.progressUponStepCompletion}\n` +
            `percent complete: ${this.percent}\n` +
            `total steps: ${this.stepsTotal}\n` +
            `current step: ${this.stepCurrent}\n` +
            `step status: ${this.isStepCompleted ? 'completed' : 'starting'}\n` +
            `time to complete: ${this.timeToCompleteStep}`
        );
    }
} // End class


export default ProgressReport;
