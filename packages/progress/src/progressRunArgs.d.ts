import { IPromise, Result } from '@pilotlab/result';
import { Signal } from '@pilotlab/signals';
import ProgressTracker from './progressTracker';
import ProgressReport from './progressReport';
import IProgressRunFunction from './iProgressRunFunction';
export declare class ProgressRunArgs {
    constructor(subTracker: ProgressTracker);
    readonly subTracker: ProgressTracker;
    private _subTracker;
    result: Result<any>;
    readonly completed: Signal<ProgressReport>;
    queue(runFunction?: IProgressRunFunction, scope?: any, message?: string, weight?: number): ProgressTracker;
    run(): IPromise<any>;
}
export default ProgressRunArgs;
