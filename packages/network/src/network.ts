export class Network {
    /**
     * USAGE (with NodeJS):
     * import * as os from 'os';
     * import Network from '@pilotlab/network';
     * const ips:string[] = Network.getIPs(os.networkInterfaces);
     */
    static getIPs(networkInterfaces:any):string[] {
        const ips:string[] = [];

        Object.keys(networkInterfaces).forEach(function (ifname) {
            let alias = 0;

            networkInterfaces[ifname].forEach(function (iface:any) {
                if ('IPv4' !== iface.family || iface.internal !== false) {
                    /// skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
                    return;
                }

                if (alias >= 1) {
                    /// this single interface has multiple ipv4 addresses
                    ips.push(iface.address);
                } else {
                    /// this interface has only one ipv4 adress
                    ips.push(iface.address);
                }

                ++alias;
            });
        });

        return ips;
    }
} // End class


export default Network;
