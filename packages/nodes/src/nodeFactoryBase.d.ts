import { FactoryManager } from '@pilotlab/factory';
import INode from './interfaces/iNode';
import INodeFactory from './interfaces/iNodeFactory';
import INodeCreateOptions from './interfaces/iNodeCreateOptions';
import INodesFactory from './interfaces/iNodesFactory';
export declare abstract class NodeFactoryBase<TNode extends INode> implements INodeFactory {
    constructor();
    protected p_factoryManager: FactoryManager<TNode>;
    register(scope: Object, className: string, aliases?: string[], description?: any): void;
    instance(options?: INodeCreateOptions): TNode;
    readonly collection: INodesFactory;
    protected p_collection: INodesFactory;
    fromAny(value: (TNode | Object | string), ...args: any[]): TNode;
    fromString(value: string, parseString?: (value: string) => TNode, ...args: any[]): TNode;
    fromJson(value: string, ...args: any[]): TNode;
    fromObject(value: Object, ...args: any[]): TNode;
    fromArray(values: any[], ...args: any[]): TNode;
}
export default NodeFactoryBase;
