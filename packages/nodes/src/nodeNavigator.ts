import is from '@pilotlab/is';
import { IPromise } from '@pilotlab/result';
import Debug from '@pilotlab/debug';
import { Signal } from '@pilotlab/signals';
import { Animation } from '@pilotlab/animation';
import INode from './interfaces/iNode';
import INodeNavigator from './interfaces/iNodeNavigator';
import NodeNavigationTracker from './nodeNavigationTracker';


Debug.setCategoryMode('navigation', false);


export class NodeNavigator<TNode extends INode> implements INodeNavigator {
    constructor(rootNode:TNode, historyMax:number = 10) {
        if (is.empty(rootNode)) Debug.error('An empty root node was passed to constructor.', 'NodeNavigator.constructor(...)');
        this._rootNode = rootNode;
        this._historyMax = historyMax;
        this.arrivedAtNode = new Signal<NodeNavigationTracker<TNode>>();
    }


    private _rootNode:TNode;
    private _trackerRunning:NodeNavigationTracker<TNode>;
    private _trackerWaiting:NodeNavigationTracker<TNode>;
    private _historyIndex:number = 0;


    get history():string[] { return this._history; }
    private _history:string[] = [];


    get historyMax():number { return this._historyMax; }
    private _historyMax:number = 10;


    internalNodeStart:INode;


    arrivedAtNode:Signal<NodeNavigationTracker<TNode>> = new Signal<NodeNavigationTracker<TNode>>();


    to(
        pathDestination:string,
        pathSegmentDelimiter?:string,
        isInterruptCurrent:boolean = true,
        forEachParentSegment?:(nodeCurrent:TNode, nodeParent:TNode) => IPromise<any>,
        forEachChildSegment?:(nodeCurrent:TNode, nodeChild:TNode) => IPromise<any>,
        isUpdateHistory:boolean = true
    ):NodeNavigationTracker<TNode> {
        let nodeStart:INode = this.internalNodeStart;
        if (is.empty(nodeStart)) nodeStart = this._rootNode;
        if (is.empty(pathDestination)) pathDestination = <string>this._rootNode.key;

        if (is.empty(pathSegmentDelimiter)) {
            if (is.notEmpty(this._rootNode)) pathSegmentDelimiter = this._rootNode.children.pathSegmentDelimiter;
            else if (is.notEmpty(nodeStart)) pathSegmentDelimiter = nodeStart.children.pathSegmentDelimiter;
            else pathSegmentDelimiter = '/';
        }

        let trackerNew:NodeNavigationTracker<TNode> = new NodeNavigationTracker<any>(
            this, nodeStart, pathDestination, pathSegmentDelimiter, forEachParentSegment, forEachChildSegment, isUpdateHistory
        );

        /// No need to continue if we're already at the destination.
         if (pathDestination === (nodeStart.path + pathSegmentDelimiter + nodeStart.key)) {
            trackerNew.update(nodeStart, true, false, false);
            return trackerNew;
        }

        /// Store a reference to any currently running navigation process, then clear _trackerRunning.
        let trackerPrevious:NodeNavigationTracker<TNode> = this._trackerRunning;
        this._trackerRunning = null;

        /// Just in case another navigation process was waiting and hasn't been started yet, clear it.
        this._trackerWaiting = null;

        if (is.empty(trackerPrevious)) {
            /// No navigation process is currently running, so let's run our new one.
            this._runTracker(trackerNew);
        } else {
            /// A tracker is already running, so we need to wait for it to complete.

            /// If another tracker is already waiting, just bump it.
            /// The most recent navigation request is the only one we're interested in.
            this._trackerWaiting = trackerNew;

            /// If we need to interrupt the current navigation process, to it now.
            if (isInterruptCurrent && is.notEmpty(trackerPrevious)) trackerPrevious.interrupt();
        }

        return trackerNew;
    }


    private _runTracker(tracker:NodeNavigationTracker<TNode>):void {
        Debug.log('Running navigation tracker: ' + tracker.key, null, 'navigation');

        /// When _trackerRunning has completed, start _trackerWaiting.
        tracker.then((trackerCompleted:NodeNavigationTracker<TNode>) => {
            Debug.log(
                'Navigation tracker completed: ' + tracker.key
                + ', isInterrupted: ' + tracker.isInterrupted
                + ', isCompleted: ' + tracker.isCompleted
                + ', isStarted: ' + tracker.isStarted
                + ', isError: ' + tracker.isError,
                null, 'navigation'
            );

            Debug.log('Is navigation tracker waiting: ' + is.notEmpty(this._trackerWaiting), null, 'navigation');

            /// Clear _trackerRunning, so any new navigation request can be started immediately.
            this._trackerRunning = null;

            if (is.notEmpty(this._trackerWaiting)) {
                Debug.log('Waiting navigation tracker key: ' + this._trackerWaiting.key, null, 'animation');
                let trackerWaiting:NodeNavigationTracker<TNode> = this._trackerWaiting;
                this._trackerWaiting = null;
                trackerWaiting.update(trackerCompleted.nodeCurrent);

                /// Pause briefly before running the waiting navigation process, to avoid jerky animations.
                Animation.pause(0.01).then(() => { this._runTracker(trackerWaiting); });
            }
        });

        /// Set _trackerRunning the the tracker we're about to start.
        this._trackerRunning = tracker;

        /// Let's just wait until any other animations have completed, to avoid any potential collisions.
        if (Animation.animate.isAllAnimationCompleted) {
            NodeNavigator.navigatePath(tracker.nodeCurrent, tracker);
        } else {
            Animation.animate.allAnimationsCompleted.listenOnce(() => {
                NodeNavigator.navigatePath(tracker.nodeCurrent, tracker);
            });
        }
    }


    updateHistory(pathCurrent:string):void {
        this.history.push(pathCurrent);
        if (this.history.length > this.historyMax) this.history.shift();
        this._historyIndex = this.history.length - 1;
    }


    up(
        isInterruptCurrent:boolean = false,
        forEachParentSegment?:(nodeCurrent:TNode, nodeParent:TNode) => IPromise<any>,
        forEachChildSegment?:(nodeCurrent:TNode, nodeChild:TNode) => IPromise<any>
    ):NodeNavigationTracker<TNode> {
        let nodeStart:INode = this.internalNodeStart;
        let path:string = (nodeStart.isRoot || is.empty(nodeStart.parent)) ? '' : nodeStart.path + this._rootNode.children.pathSegmentDelimiter + nodeStart.key;
        return this.to(path, null, isInterruptCurrent, forEachParentSegment, forEachChildSegment, true);
    }


    back(
        isInterruptCurrent:boolean = false,
        forEachParentSegment?:(nodeCurrent:TNode, nodeParent:TNode) => IPromise<any>,
        forEachChildSegment?:(nodeCurrent:TNode, nodeChild:TNode) => IPromise<any>
    ):NodeNavigationTracker<TNode> {
        this._historyIndex--;
        if (this._historyIndex < 0) this._historyIndex = 0;
        let path:string = this._history[this._historyIndex];
        return this.to(path, null, isInterruptCurrent, forEachParentSegment, forEachChildSegment, false);
    }


    forward(
        isInterruptCurrent:boolean = false,
        forEachParentSegment?:(nodeCurrent:TNode, nodeParent:TNode) => IPromise<any>,
        forEachChildSegment?:(nodeCurrent:TNode, nodeChild:TNode) => IPromise<any>
    ):NodeNavigationTracker<TNode> {
        this._historyIndex++;
        if (this._historyIndex > this._history.length - 1) this._historyIndex = this._history.length - 1;
        let path:string = this._history[this._historyIndex];
        return this.to(path, null, isInterruptCurrent, forEachParentSegment, forEachChildSegment, false);
    }


    /*====================================================================*
     START: Path Navigation
     *====================================================================*/
    static navigatePath(
        nodeStart:INode,
        tracker:NodeNavigationTracker<any>
    ):NodeNavigationTracker<any> {
        Debug.log('NodeNavigation.navigatePath(...), tracker: ' + tracker.key, null, 'animation');

        if (is.empty(nodeStart)) { return this._arriveAtNode(tracker, null, true, false, true); }

        let pathCurrent:string = nodeStart.path + tracker.pathSegmentDelimiter + nodeStart.key;
        let route:string[] = this.getRouteFromPath(tracker.pathDestination, pathCurrent, tracker.pathSegmentDelimiter);

        if (is.empty(route) || route.length < 1) {
            return this._arriveAtNode(tracker, nodeStart, true, false, true);
        }

        this._navigateToNextSegmentInRoute(nodeStart, route, tracker);

        return tracker;
    }


    static getRouteFromPath(pathDestination:string, pathCurrent:string, pathSegmentDelimiter:string):string[] {
        let lastCommonAncestor:string = this.findCommonAncestor(pathDestination, pathCurrent, pathSegmentDelimiter);

        let routeToLastCommonAncestor:string[] = pathCurrent.substring(pathCurrent.lastIndexOf(lastCommonAncestor)).split(pathSegmentDelimiter);
        routeToLastCommonAncestor = routeToLastCommonAncestor.reverse();
        routeToLastCommonAncestor.shift();

        let routeToDestinationFromLastCommonAncestor:string[] = pathDestination.substring(
            pathDestination.lastIndexOf(lastCommonAncestor) + lastCommonAncestor.length + 1
        ).split(pathSegmentDelimiter);

        return routeToLastCommonAncestor.concat(routeToDestinationFromLastCommonAncestor);
    }


    static findCommonAncestor(path1:string, path2:string, pathSegmentDelimiter:string):string {
        let pathSegments1:string[] = path1.split(pathSegmentDelimiter);
        let pathSegments2:string[] = path2.split(pathSegmentDelimiter);
        let isSameAncestor:boolean = true;
        let lastCommonAncestor:string = pathSegments1[0];
        let index:number = 0;

        /// Compare the segments of both paths from the root and stop when they diverge.
        while (isSameAncestor && index < pathSegments1.length && index < pathSegments2.length) {
            if (pathSegments1[index].localeCompare(pathSegments2[index]) === 0) {
                /// The segments are the same.
                lastCommonAncestor = pathSegments1[index];
                index++;
            } else {
                /// The segments are different.
                isSameAncestor = false;
            }
        }

        return lastCommonAncestor;
    }


    private static _navigateToNextSegmentInRoute(
        nodeCurrent:INode,
        route:string[],
        tracker:NodeNavigationTracker<any>
    ):NodeNavigationTracker<any> {
        if (!tracker.isInterrupted && !tracker.isStarted) tracker.start();
        else {
            if (is.empty(route) || route.length < 1 || is.empty(route[0])) {
                /// We've reached the end of the route.
                return this._arriveAtNode(tracker, nodeCurrent, true, false, false);
            } else if (tracker.isInterrupted) {
                /// Navigation was interrupted, even though we haven't reached the end of the path.
                return this._arriveAtNode(tracker, nodeCurrent, true, true, false);
            } else {
                this._arriveAtNode(tracker, nodeCurrent, false, false, false);
            }
        }

        let segment:string = route.shift();

        if (nodeCurrent.key === segment) {
            /// we're already in the right spot, so stay put.
            this._navigateToNextSegmentInRoute(nodeCurrent, route, tracker);
        } else if (is.notEmpty(nodeCurrent.parent) && nodeCurrent.parent.key === segment) {
            /// Navigate up to the parent.
            if (is.notEmpty(tracker.forEachParentSegment)) {
                /// First apply the action on the parent that was specified as a parameter.
                tracker.forEachParentSegment(nodeCurrent, nodeCurrent.parent).then(() => {
                    this._navigateToNextSegmentInRoute(nodeCurrent.parent, route, tracker);
                });
            } else this._navigateToNextSegmentInRoute(nodeCurrent.parent, route, tracker);
        } else {
            /// Looking for child.
            let child:INode = nodeCurrent.children.getByKey(segment);

            if (is.notEmpty(child)) {
                /// Found a child matching the key.
                /// Navigate to the child.
                if (is.notEmpty(tracker.forEachChildSegment)) {
                    /// First apply the action on the child that was specified as a parameter.
                    tracker.forEachChildSegment(nodeCurrent, child).then(() => {
                        this._navigateToNextSegmentInRoute(child, route, tracker);
                    });
                } else this._navigateToNextSegmentInRoute(child, route, tracker);
            }
        }

        return tracker;
    }


    private static _arriveAtNode(
        tracker:NodeNavigationTracker<any>,
        nodeCurrent:INode,
        isCompleted:boolean,
        isInterrupted:boolean,
        isError:boolean = false
    ):NodeNavigationTracker<any> {
        tracker.update(nodeCurrent, isCompleted, isInterrupted, isError);
        tracker.navigator.arrivedAtNode.dispatch(tracker);
        return tracker;
    }
} // End of class


export default NodeNavigator;
