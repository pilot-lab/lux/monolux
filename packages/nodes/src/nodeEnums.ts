export enum NodeType {
    ROOT,
    BASIC,
    COLLECTION
} // End enum


export enum NodeChangeType {
    ADDED,
    ATTACHED,
    UPDATED,
    DETACHED,
    DELETED
} // End enum


export enum NodeChangeActions {
    NONE = 0,
    SIGNAL_CHANGE = 1 << 0
} // End enum


export enum NodeChangeInitiationType {
    NONE,
    ANIMATION,
    AUTOMATIC,
    PROPERTY_CHANGE,
    INTERRUPT,
    MANUAL_TARGET_CHANGE,
    USER_INTERACTION
} // End enum

