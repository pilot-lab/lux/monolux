"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const initializable_1 = require("@pilotlab/initializable");
const signals_1 = require("@pilotlab/signals");
const collections_1 = require("@pilotlab/collections");
const nodeChangeOptions_1 = require("./nodeChangeOptions");
const nodeCreateOptions_1 = require("./nodeCreateOptions");
const nodeEnums_1 = require("./nodeEnums");
const nodeEventArgs_1 = require("./nodeEventArgs");
class NodesBase extends initializable_1.Initializable {
    constructor(factory, parent, pathSegmentDelimiter = '/', isInitialize = true) {
        super();
        this.p_mapKeys = new collections_1.MapList();
        this.p_pathSegmentDelimiter = '/';
        this.p_pathSegmentParentSymbol = '..';
        this.changed = new signals_1.Signal(false);
        this.childAdded = new signals_1.Signal();
        this.childAttached = new signals_1.Signal();
        this.childDetached = new signals_1.Signal();
        this.childDeleted = new signals_1.Signal();
        this.childIndexChanged = new signals_1.Signal();
        this.compareKeys = (child1, child2) => {
            if (is_1.default.emptyAny(child1, child2, child1.key, child2.key))
                return 0;
            return child1.key.toLowerCase().localeCompare(child2.key.toLowerCase());
        };
        this.compareIndices = (child1, child2) => {
            let index1 = (is_1.default.empty(child1) || !is_1.default.number(child1.index) || child1.index < 0 || this.p_list.size < 2) ? (this.p_list.size - 1) : child1.index;
            let index2 = (is_1.default.empty(child2) || !is_1.default.number(child2.index) || child2.index < 0 || this.p_list.size < 2) ? (this.p_list.size - 1) : child2.index;
            return index1 - index2;
        };
        this.compareIndicesDescending = (child1, child2) => {
            let index1 = (is_1.default.empty(child1) || !is_1.default.number(child1.index) || child1.index < 0 || this.p_list.size < 2) ? (this.p_list.size - 1) : child1.index;
            let index2 = (is_1.default.empty(child2) || !is_1.default.number(child2.index) || child2.index < 0 || this.p_list.size < 2) ? (this.p_list.size - 1) : child2.index;
            return index2 - index1;
        };
        this.p_factory = factory;
        this.p_list = new collections_1.List();
        if (is_1.default.notEmpty(parent))
            this.internalParent = parent;
        if (is_1.default.notEmpty(pathSegmentDelimiter))
            this.p_pathSegmentDelimiter = pathSegmentDelimiter;
        if (isInitialize)
            this.initialize();
    }
    get create() { return this.p_factory; }
    get list() { return this.p_list; }
    get array() { return this.p_list.array; }
    get parent() { return this.internalParent; }
    get path() {
        let node = this.parent;
        let path = this.pathSegmentDelimiter;
        while (is_1.default.notEmpty(node) && is_1.default.notEmpty(node.key) && !node.isRoot && is_1.default.notEmpty(node.parentCollection)) {
            path = node.parentCollection.pathSegmentDelimiter + node.key + path;
            node = node.parentCollection.parent;
        }
        if (is_1.default.notEmpty(this.parent) && is_1.default.notEmpty(this.parent.root) && is_1.default.notEmpty(this.parent.root.key))
            path = this.parent.root.key + path;
        return path;
    }
    get pathSegmentDelimiter() { return this.p_pathSegmentDelimiter; }
    set pathSegmentDelimiter(value) { this.p_pathSegmentDelimiter = value; }
    get pathSegmentParentSymbol() { return this.p_pathSegmentParentSymbol; }
    set pathSegmentParentSymbol(value) { this.p_pathSegmentParentSymbol = value; }
    get copy() {
        return this.create.collection.instance();
    }
    set isSignalChange(value) {
        for (let i = 0, length = this.p_list.size; i < length; i++) {
            this.p_list.item(i).isSignalChange = value;
        }
    }
    _childAdded(args) {
        this.p_onChildAdded(args);
        if (args.changeOptions && args.changeOptions.isSignalChange) {
            this.childAdded.dispatch(args);
        }
    }
    p_onChildAdded(args) { }
    internalChildAttached(args) {
        this.p_onChildAttached(args);
        if (args.changeOptions && args.changeOptions.isSignalChange) {
            this.childAttached.dispatch(args);
        }
    }
    p_onChildAttached(args) { }
    internalChildDetached(args) {
        this.p_onChildDetached(args);
        if (args.changeOptions && args.changeOptions.isSignalChange) {
            this.childDetached.dispatch(args);
        }
    }
    p_onChildDetached(args) { }
    _childDeleted(args) {
        this.p_onChildDeleted(args);
        if (args.changeOptions && args.changeOptions.isSignalChange) {
            this.childDeleted.dispatch(args);
        }
    }
    p_onChildDeleted(args) { }
    _childIndexChanged(args) {
        this.p_onChildIndexChanged(args);
        if (args.changeOptions && args.changeOptions.isSignalChange) {
            this.childIndexChanged.dispatch(args);
        }
    }
    p_onChildIndexChanged(args) { }
    internalChanged(args) {
        this.changed.dispatch(args);
        if (is_1.default.notEmpty(this.parent) && is_1.default.notEmpty(this.parent.internalChanged))
            this.parent.internalChanged(args);
    }
    internalAttachedAll(changeOptions) {
        for (let i = 0; i < this.p_list.size; i++) {
            let child = this.p_list.item(i);
            child.internalParentCollection = this;
            child.internalAttached(changeOptions);
        }
    }
    internalDetachedAll(changeOptions) {
        for (let i = 0; i < this.p_list.size; i++) {
            this.p_list.item(i).internalDetached(changeOptions);
        }
    }
    add(child, index, changeOptions = nodeChangeOptions_1.default.signal.durationZero) {
        if (is_1.default.empty(child))
            return child;
        this.p_add(child, index, changeOptions);
        if (is_1.default.notEmpty(changeOptions) && (changeOptions.isSignalChange)) {
            this._childAdded(new nodeEventArgs_1.NodeEventArgs(child, nodeEnums_1.NodeChangeType.ADDED, changeOptions));
        }
        return child;
    }
    p_add(child, index, changeOptions = nodeChangeOptions_1.default.signal.durationZero) {
        if (is_1.default.notEmpty(child.index) && child.index <= this.p_list.size && child.index >= 0)
            index = child.index;
        if (is_1.default.notEmpty(index) && index <= this.p_list.size && index >= 0) {
            this.p_list.add(child, index);
            let indexActual = this.p_list.indexOf(child);
            this.updateIndices(indexActual, this.p_list.size - 1, changeOptions);
        }
        else {
            this.p_list.add(child);
            child.internalSetIndex(this.p_list.size - 1, changeOptions);
        }
        if (is_1.default.notEmpty(child.key))
            this.p_mapKeys.set(child.key, child);
        child.internalParentCollection = this;
        child.internalAdded(changeOptions);
        if (is_1.default.notEmpty(this.internalParent) && is_1.default.notEmpty(this.internalParent.root)) {
            child.internalAttached(changeOptions);
        }
        child.indexChanged.listen(this._childIndexChanged, this);
    }
    addByKey(child, key, index, changeOptions = nodeChangeOptions_1.default.signal.durationZero) {
        if (is_1.default.empty(child) || is_1.default.empty(key))
            return null;
        child.key = key;
        return this.add(child, index, changeOptions);
    }
    addByPath(child, path, index, changeOptions = nodeChangeOptions_1.default.default) { return this.getOrCreate(path, new nodeCreateOptions_1.default(null, null, null, changeOptions)); }
    delete(child, changeOptions = nodeChangeOptions_1.default.default) {
        if (is_1.default.empty(child))
            return child;
        this.p_delete(child, changeOptions);
        this._childDeleted(new nodeEventArgs_1.NodeEventArgs(child, nodeEnums_1.NodeChangeType.DELETED, changeOptions));
        return child;
    }
    p_delete(child, changeOptions = nodeChangeOptions_1.default.default) {
        let indexActual = this.p_list.indexOf(child);
        let isNodeInList = this.p_list.delete(child);
        child.internalSetIndex(null, nodeChangeOptions_1.default.zero);
        this.updateIndices(is_1.default.notEmpty(indexActual) ? indexActual : 0, this.p_list.size - 1, changeOptions);
        if (!isNodeInList)
            return null;
        if (is_1.default.notEmpty(child.key))
            this.p_mapKeys.delete(child.key);
        child.internalDeleted(changeOptions);
        if (is_1.default.notEmpty(this.internalParent) && is_1.default.notEmpty(this.internalParent.root)) {
            child.internalDetached(changeOptions);
        }
        child.internalParentCollection = null;
        child.indexChanged.delete(this._childIndexChanged, this);
    }
    deleteAt(index, changeOptions = nodeChangeOptions_1.default.default) {
        if (index > this.p_list.size - 1 || index < 0)
            return null;
        else {
            let child = this.p_list.item(index);
            return this.delete(child, changeOptions);
        }
    }
    deleteByKey(key, changeOptions = nodeChangeOptions_1.default.default) {
        let child = this.p_mapKeys.get(key);
        return this.delete(child, changeOptions);
    }
    clear(changeOptions = nodeChangeOptions_1.default.default) {
        this.p_list.forEach((child) => {
            this.delete(child, changeOptions);
            return true;
        });
        return this;
    }
    setIndex(child, index, changeOptions = nodeChangeOptions_1.default.default) {
        if (is_1.default.empty(child))
            return;
        let indexPrevious = this.p_list.indexOf(child);
        this.p_list.delete(child);
        this.p_list.add(child, index);
        let indexUpdateStart = index < indexPrevious ? index : indexPrevious;
        this.updateIndices(indexUpdateStart, this.p_list.size - 1, changeOptions);
    }
    updateIndices(indexStart = 0, indexEnd, changeOptions = nodeChangeOptions_1.default.default) {
        if (indexStart < 0)
            indexStart = 0;
        if (is_1.default.empty(indexEnd) || indexEnd >= this.p_list.size)
            indexEnd = this.p_list.size - 1;
        for (let i = indexStart; i <= indexEnd; i++) {
            let child = this.p_list.item(i);
            if (is_1.default.notEmpty(child))
                child.internalSetIndex(i, changeOptions.durationZero);
        }
    }
    getByKey(key) { return this.p_mapKeys.get(key); }
    getByPath(path, createOptions) {
        if (is_1.default.notEmpty(path) && typeof path === 'string' && path.indexOf(this.pathSegmentDelimiter) > -1)
            return this.getOrCreate(path, createOptions);
        return this.p_mapKeys.get(path);
    }
    getOrCreate(path, createOptions, segmentCreateOptions) {
        let child;
        if (is_1.default.empty(path))
            return this.create.instance();
        else if (is_1.default.empty(path) || path.indexOf(this.pathSegmentDelimiter) < 0) {
            let key = path;
            if (is_1.default.notEmpty(key))
                child = this.p_mapKeys.get(key);
            if (is_1.default.notEmpty(child))
                return child;
            else if (is_1.default.notEmpty(createOptions)) {
                child = this.create.instance(createOptions);
                if (is_1.default.notEmpty(child)) {
                    if (is_1.default.notEmpty(key)) {
                        this.addByKey(child, key, createOptions.index, createOptions.changeOptions);
                    }
                    else {
                        this.add(child, createOptions.index, createOptions.changeOptions);
                    }
                }
                return child;
            }
        }
        else {
            let pathSegments = path.split(this.pathSegmentDelimiter);
            let segment;
            let collection = this;
            while (pathSegments.length > 0) {
                segment = pathSegments.shift();
                if (segment === this.p_pathSegmentParentSymbol)
                    child = collection.parent;
                else if (is_1.default.empty(createOptions))
                    child = collection.getByPath(segment);
                else if (pathSegments.length > 0 && is_1.default.notEmpty(segmentCreateOptions)) {
                    child = collection.getByPath(segment, segmentCreateOptions);
                }
                else {
                    const changeOptions = is_1.default.notEmpty(createOptions) ? createOptions.changeOptions : nodeChangeOptions_1.default.default;
                    segmentCreateOptions = new nodeCreateOptions_1.default(null, null, null, changeOptions);
                    child = collection.getByPath(segment, createOptions);
                }
                if (is_1.default.empty(child))
                    break;
                collection = child.children;
                if (is_1.default.empty(collection))
                    break;
            }
            return child;
        }
    }
    item(index) { return this.p_list.item(index); }
    indexOf(node) { return this.p_list.indexOf(node); }
    forEach(handler) { this.p_list.forEach(handler); }
    has(node) { return this.p_list.has(node); }
    hasKey(key) { return this.p_mapKeys.has(key); }
    sortByKey(changeOptions = nodeChangeOptions_1.default.default) { return this.sort(this.compareKeys, changeOptions); }
    sortByIndex(changeOptions = nodeChangeOptions_1.default.default) { return this.sort(this.compareIndices, changeOptions); }
    sortByIndexDescending(changeOptions = nodeChangeOptions_1.default.default) { return this.sort(this.compareIndicesDescending, changeOptions); }
    sort(comparer, changeOptions = nodeChangeOptions_1.default.default) {
        this.p_list.sort(comparer);
        for (let i = 0; i < this.p_list.size; i++) {
            let child = this.p_list.item(i);
            if (is_1.default.notEmpty(child))
                child.internalSetIndex(i, nodeChangeOptions_1.default.zero);
        }
        return this;
    }
    toMap(isIncludeDataTypes = false) {
        let map = new collections_1.MapList();
        this.p_list.forEach(function (child) {
            if (is_1.default.empty(child))
                return true;
            map.set(child.key, child.toObject(isIncludeDataTypes));
            return true;
        });
        return map;
    }
    toObject(isIncludeDataTypes = true, isIncludeLabels = true, isForceIncludeAll = false) {
        let object = {};
        this.p_list.forEach((node) => {
            if (is_1.default.empty(node))
                return true;
            node.toObject(isIncludeDataTypes, isIncludeLabels, object, isForceIncludeAll);
            return true;
        });
        return object;
    }
    toArray() {
        let array = [];
        for (let i = 0; i < this.p_list.size; i++) {
            if (is_1.default.notEmpty(this.p_list.item(i)))
                array[i] = this.p_list.item(i).toObject(false);
        }
        return array;
    }
    toJson(isIncludeDataTypes = false) {
        return JSON.stringify(this.toObject(isIncludeDataTypes));
    }
}
exports.NodesBase = NodesBase;
exports.default = NodesBase;
//# sourceMappingURL=nodesBase.js.map