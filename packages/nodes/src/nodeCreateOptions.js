"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const nodeChangeOptions_1 = require("./nodeChangeOptions");
class NodeCreateOptions {
    constructor(label, key, index, changeOptions = nodeChangeOptions_1.default.default) {
        this.key = key;
        this.label = label;
        this.index = index;
        this.changeOptions = changeOptions;
    }
}
exports.NodeCreateOptions = NodeCreateOptions;
exports.default = NodeCreateOptions;
//# sourceMappingURL=nodeCreateOptions.js.map