import is from '@pilotlab/is';
import Identifier from '@pilotlab/ids';
import Debug from '@pilotlab/debug';
import { Initializable } from '@pilotlab/initializable';
import { IPromise, Result } from '@pilotlab/result';
import { Signal } from '@pilotlab/signals';
import NodeChangeOptions from './nodeChangeOptions';
import INode from './interfaces/iNode';
import INodeChangeOptions from './interfaces/iNodeChangeOptions';
import INodes from './interfaces/iNodes';
import INodeFactory from './interfaces/iNodeFactory';
import INodeNavigator from './interfaces/iNodeNavigator';
import { NodeType, NodeChangeType } from './nodeEnums';
import { NodeEventArgs } from './nodeEventArgs';
import { NodeNavigator } from './nodeNavigator';


/**
 * A base class used to describe any member of a hierarchically structured system.
 *
 * ----- Node lifecycle:
 * ---------------------------------------------------------------------
 *
 * Added:       A node has been added to the parent node's child collection, but is not necessarily
 *              connected to the entire node system yet, and may not hold a reference back to the root node.
 *
 * Attached:    The node has not only been added to its parent's child collection, but has also
 *              been attached to the broader node system and holds a reference to the root of the node system.
 *
 * Deleted:     A node has been deleted from the parent node's child collection, but is not disconnected
 *              from the entire node system yet and still holds a reference back to the root node.
 *
 * Detached:    The node has not only been deleted from its parent's child collection, but has also been detached
 *              from the broader node system and no longer holds a reference to the root of the node system.
 */
export abstract class NodeBase<
    TNode extends INode,
    TNodes extends INodes,
    TRoot extends INode
> extends Initializable implements INode {
    protected constructor(
        factory:INodeFactory,
        nodeType:NodeType = NodeType.BASIC,
        key?:string,
        isInitialize:boolean = true
    ) {
        super();

        this.internalIsAttached = false;
        this.p_factory = factory;
        this._changed = new Signal<NodeEventArgs<any>>(false);
        this.p_isSignalChange = true;
        this.p_index = null;
        this.p_indexPrevious = null;
        this.p_nodeType = NodeType.BASIC;

        if (is.notEmpty(key, true)) this.p_key = key;

        this.added = new Signal<NodeEventArgs<TNode>>(true);
        this.attached = new Signal<NodeEventArgs<TNode>>(true);
        this.deleted = new Signal<NodeEventArgs<TNode>>(false);
        this.detached = new Signal<NodeEventArgs<TNode>>(false);
        this.indexChanged = new Signal<NodeEventArgs<any>>(false);

        if (isInitialize) this.initialize(nodeType);
    }


    protected p_onInitializeStarted(result:Result<any>, args:any[]):IPromise<any> {
        const nodeType:NodeType = args[0];

        this.p_onSetChildCollection(nodeType);
        this.p_setNodeType(nodeType, NodeChangeOptions.zero);

        return result.resolve();
    }


    /*====================================================================*
     START: Factory
     *====================================================================*/
    get create():INodeFactory { return this.p_factory; }
    protected p_factory:INodeFactory;



    /*====================================================================*
     START: Properties
     *====================================================================*/
    get isAttached():boolean { return this.internalIsAttached; }
    internalIsAttached:boolean;


    /**
     * A globally unique identifier. This may be generated automatically.
     */
    get key():string {
        if (is.empty(this.p_key, true)) {
            this.p_key = Identifier.generate();
        }

        return this.p_key;
    }
    set key(value:string) { this.p_key = value; }
    protected p_key:string;


    get parent():TNode {
        if (is.notEmpty(this.parentCollection)) return <TNode>this.parentCollection.parent;
        else return null;
    }


    get parentCollection():TNodes { return <TNodes>this.internalParentCollection; }
    internalParentCollection:TNodes;


    get root():TRoot {
        if (this.isAttached) return this.p_root;
        else return null;
    }
    protected p_root:TRoot;


    get isRoot():boolean {
        if (is.empty(this.root)) return false;
        else return this.p_root === <TRoot><INode>this;
    }


    get isCollection():boolean {
        return this.nodeType === NodeType.ROOT || this.nodeType === NodeType.COLLECTION;
    }


    get children():TNodes { return this.p_children; }
    protected p_onSetChildCollection(nodeType:NodeType):void {
        if (nodeType === NodeType.BASIC) return;
        if (is.notEmpty(this.create) && is.notEmpty(this.create.collection)) {
            this.p_children = <TNodes>this.create.collection.instance(this);
        }
    }
    protected p_children:TNodes;


    get isSignalChange():boolean { return this.p_isSignalChange; }
    set isSignalChange(value:boolean) {
        if (this.isCollection) this.children.isSignalChange = value;
        this.p_isSignalChange = value;
    }
    protected p_isSignalChange:boolean;


    get nodeType():NodeType { return this.p_nodeType; }
    set nodeType(value:NodeType) { this.p_setNodeType(value, NodeChangeOptions.signal.durationZero); }
    protected p_setNodeType(value:NodeType, changeOptions:INodeChangeOptions):NodeBase<any, any, any> {
        if (is.empty(value) || this.p_nodeType === value) return;

        if (this.p_nodeType === NodeType.BASIC && value !== NodeType.BASIC) this.p_onSetChildCollection(value);

        if (value === NodeType.ROOT) {
            this.p_root = <TRoot><INode>this;
            this.internalAttached(changeOptions);
        }

        this.p_nodeType = value;

        this.p_onNodeTypeChanged(value, changeOptions);

        return this;
    }
    protected p_nodeType:NodeType;
    protected p_onNodeTypeChanged(type:NodeType, changeOptions:INodeChangeOptions) {}


    /**
     * Based on backbone.js.
     * Returns the relative path (URL) where the node's resource would be located on the server.
     * If your models are located somewhere else, override this method with the correct logic.
     * Generates URLs of the form: "[nodes.pathSegmentDelimiter][model.id]" by default, but you may override
     * by specifying an explicit pathRoot if the node's parent collection shouldn't be taken into account.
     *
     * A node with an id of 101, stored in a nodes with a path of
     * "/documents/7/notes", would have this path: "/documents/7/notes/101"
     */
    get path():string {
        let value:string = '';
        let node:INode = this.parent;

        while (is.notEmpty(node) && is.notEmpty(node.key) && !node.isRoot && is.notEmpty(node.parentCollection)) {
            value = node.parentCollection.pathSegmentDelimiter + node.key + value;
            node = node.parentCollection.parent;
        }

        if (!this.isRoot && is.notEmpty(this.root) && is.notEmpty(this.root.key)) value = this.root.key + value;

        return value;
    }


    get index():number { return this.p_index; }
    set index(value:number) {
        this.setIndex(value, NodeChangeOptions.zero, true);
    }
    protected p_index:number;
    get indexPrevious():number { return this.p_indexPrevious; }
    protected p_indexPrevious:number;
    internalSetIndex(value:number, changeOptions:INodeChangeOptions):void {
        if (this.p_index === value) return;
        this.p_indexPrevious = this.p_index;
        this.p_index = value;
        this.p_onIndexSet(value, this.p_indexPrevious, changeOptions);
        this.indexChanged.dispatch(new NodeEventArgs<TNode>(<TNode><INode>this, NodeChangeType.UPDATED, changeOptions));
    }
    protected p_onIndexSet(indexNew:number, indexPrevious:number, changeOptions:INodeChangeOptions):void {}


    /**
     * Used to change the index of a node when additional options are required.
     * @param value
     * @param changeOptions
     * @param isRememberCurrentIndex
     * @param isSoftSwap: If set to true, indices will be changed without actually removing or adding nodes to the collection.
     * @returns {boolean}
     */
    setIndex(value:number, changeOptions:INodeChangeOptions = NodeChangeOptions.default, isRememberCurrentIndex:boolean = true, isSoftSwap:boolean = false):boolean {
        if (is.empty(this.parentCollection) || is.empty(value) || !is.number(value)) return false;
        let parentCollection:INodes = this.parentCollection;
        let indexPrevious:number = this.index;

        if (value > parentCollection.list.size - 1) value = parentCollection.list.size - 1;
        if (value < 0) value = 0;

        let child:INode = this;
        if (isSoftSwap) parentCollection.setIndex(child, value, changeOptions);
        else if (parentCollection.delete(this, <INodeChangeOptions>NodeChangeOptions.signal.durationZero)) parentCollection.add(this, value, changeOptions);
        if (!isRememberCurrentIndex && indexPrevious > -1) this.p_indexPrevious = indexPrevious;

        return true;
    }


    /**
     * Gets the nest level, or depth, indicates how many levels deep the control is nested within other controls.
     * The Root has a NestLevel of 0, while children of the root have a level of 1.
     */
    get nestLevel():number {
        if (is.empty(this.parentCollection) || is.empty(this.parentCollection.parent) || this.isRoot) return 0;
        return this.parentCollection.parent.nestLevel + 1;
    }


    get copy():TNode { return <TNode>this.create.instance(); }


    /*====================================================================*
     START: Signals
     *====================================================================*/
    /**
     * A node has been added to the parent node's child collection, but is not necessarily
     * connect to the entire node system yet, with a reference back to the root node.
     */
    readonly added:Signal<NodeEventArgs<TNode>>;


    /**
     * The node has not only been added to its parent's child collection, but has also been
     * attached to the broader node system and holds a reference to the root of the node system.
     */
    readonly attached:Signal<NodeEventArgs<TNode>>;


    /**
     * The node is about to be deleted from its parent's child collection list.
     * If the node is attached to the broader node system, it will then be detached.
     */
    readonly deleted:Signal<NodeEventArgs<TNode>>;


    /**
     * Then node has been first deleted from its parent node's child collection, then detached
     * from the broader node system. The node's references to the former parent node and root node have been deleted.
     */
    readonly detached:Signal<NodeEventArgs<TNode>>;


    readonly indexChanged:Signal<NodeEventArgs<TNode>>;


    /**
     * The node has undergone one of the changes described by the NodeChangeType enum,
     * which includes the following change types: ADDED, ATTACHED, UPDATED, DETACHED, DELETED.
     * The type of change that has been applied to this node will be specified in the changeType
     * property of the dispatched NodeEventArgs.
     */
    get changed():Signal<NodeEventArgs<TNode>> { return this._changed; };
    private readonly _changed:Signal<NodeEventArgs<TNode>>;
    /// IMPORTANT: This signal should only be dispatched from internalChanged.


    /*====================================================================*
     START: Public Methods
     *====================================================================*/
    delete(changeOptions:INodeChangeOptions = NodeChangeOptions.default):IPromise<boolean> {
        if (is.notEmpty(this.parentCollection)) {
            this.parentCollection.delete(this, changeOptions);
            return Result.resolve(true);
        } else return Result.resolve(false);
    }


    returnToPreviousIndex(changeOptions:INodeChangeOptions = NodeChangeOptions.default):boolean {
        return this.setIndex(this.indexPrevious);
    }


    toObject(isIncludeDataTypes:boolean = false, isIncludeLabels:boolean = true, appendToObject?:any, isForceInclude:boolean = false):any {
        return {};
    }


    toJson(isIncludeDataTypes:boolean = false):string { return JSON.stringify(this.toObject(isIncludeDataTypes)); }


    /*====================================================================*
     START: Internal
     *====================================================================*/
    internalChanged(args:NodeEventArgs<any>):void {
        if ((args.changeOptions.isSignalChange) && this.isSignalChange) this.changed.dispatch(args);

        if (is.notEmpty(this.parentCollection) && (is.notEmpty(this.parentCollection.internalChanged))) {
            this.parentCollection.internalChanged(args);
        }
    }


    internalAdded(changeOptions:INodeChangeOptions):void {
        this.p_onAdded(changeOptions);
        let args:NodeEventArgs<TNode> = new NodeEventArgs<TNode>(<TNode><INode>this, NodeChangeType.ADDED, changeOptions);
        this.added.dispatch(args);
        this.internalChanged(args);
    }
    protected p_onAdded(changeOptions:INodeChangeOptions):void {}


    internalAttached(changeOptions:INodeChangeOptions):void {
        try {
            if (is.notEmpty(this.internalParentCollection) && is.notEmpty(this.internalParentCollection.parent)) {
                if (!this.isRoot) this.p_root = <TRoot>this.internalParentCollection.parent.root;
            }

            if (is.notEmpty(this.children)) this.children.internalAttachedAll(changeOptions);

            this.internalIsAttached = true;
            let args:NodeEventArgs<TNode> = new NodeEventArgs<TNode>(<TNode><INode>this, NodeChangeType.ATTACHED, changeOptions);
            this.p_onAttached(changeOptions);
            this.attached.dispatch(args);
            this.internalChanged(args);

            if (is.notEmpty(this.internalParentCollection)) this.internalParentCollection.internalChildAttached(args);
        } catch (e) { Debug.error(e, 'NodeBase.internalAttached()'); }
    }
    protected p_onAttached(changeOptions:INodeChangeOptions):void {}


    internalDeleted(changeOptions:INodeChangeOptions):void {
        this.p_indexPrevious = this.index;
        let args:NodeEventArgs<TNode> = new NodeEventArgs<TNode>(<TNode><INode>this, NodeChangeType.DELETED, changeOptions);
        this.deleted.dispatch(args);
        this.internalChanged(args);
        this.p_onDeleted(changeOptions);
    }
    protected p_onDeleted(changeOptions:INodeChangeOptions):void {}


    internalDetached(changeOptions:INodeChangeOptions):void {
        if (is.notEmpty(this.children)) this.children.internalDetachedAll(changeOptions);
        this.internalIsAttached = false;
        let args:NodeEventArgs<TNode> = new NodeEventArgs<TNode>(<TNode><INode>this, NodeChangeType.DETACHED, changeOptions);
        this.detached.dispatch(args);
        this.internalChanged(args);
        this.p_onDetached(changeOptions);
        if (is.notEmpty(this.internalParentCollection)) this.internalParentCollection.internalChildDetached(args);
    }
    protected p_onDetached(changeOptions:INodeChangeOptions):void {}


    /*====================================================================*
     START: Path Navigation
     *====================================================================*/
    get navigate():INodeNavigator {
        let navigator:INodeNavigator;

        if (this.isRoot || (this.isAttached && is.empty(this.root))) {
            if (is.empty(this.p_navigator)) this.p_navigator = new NodeNavigator<TNode>(<TNode><INode>this);
            navigator = this.p_navigator;
        } else navigator = this.root.navigate;

        navigator.internalNodeStart = <TNode><INode>this;

        return navigator;
    }
    protected p_navigator:NodeNavigator<TNode>;
} // End of class


export default NodeBase;
