"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const debug_1 = require("@pilotlab/debug");
const signals_1 = require("@pilotlab/signals");
const animation_1 = require("@pilotlab/animation");
const nodeNavigationTracker_1 = require("./nodeNavigationTracker");
debug_1.default.setCategoryMode('navigation', false);
class NodeNavigator {
    constructor(rootNode, historyMax = 10) {
        this._historyIndex = 0;
        this._history = [];
        this._historyMax = 10;
        this.arrivedAtNode = new signals_1.Signal();
        if (is_1.default.empty(rootNode))
            debug_1.default.error('An empty root node was passed to constructor.', 'NodeNavigator.constructor(...)');
        this._rootNode = rootNode;
        this._historyMax = historyMax;
        this.arrivedAtNode = new signals_1.Signal();
    }
    get history() { return this._history; }
    get historyMax() { return this._historyMax; }
    to(pathDestination, pathSegmentDelimiter, isInterruptCurrent = true, forEachParentSegment, forEachChildSegment, isUpdateHistory = true) {
        let nodeStart = this.internalNodeStart;
        if (is_1.default.empty(nodeStart))
            nodeStart = this._rootNode;
        if (is_1.default.empty(pathDestination))
            pathDestination = this._rootNode.key;
        if (is_1.default.empty(pathSegmentDelimiter)) {
            if (is_1.default.notEmpty(this._rootNode))
                pathSegmentDelimiter = this._rootNode.children.pathSegmentDelimiter;
            else if (is_1.default.notEmpty(nodeStart))
                pathSegmentDelimiter = nodeStart.children.pathSegmentDelimiter;
            else
                pathSegmentDelimiter = '/';
        }
        let trackerNew = new nodeNavigationTracker_1.default(this, nodeStart, pathDestination, pathSegmentDelimiter, forEachParentSegment, forEachChildSegment, isUpdateHistory);
        if (pathDestination === (nodeStart.path + pathSegmentDelimiter + nodeStart.key)) {
            trackerNew.update(nodeStart, true, false, false);
            return trackerNew;
        }
        let trackerPrevious = this._trackerRunning;
        this._trackerRunning = null;
        this._trackerWaiting = null;
        if (is_1.default.empty(trackerPrevious)) {
            this._runTracker(trackerNew);
        }
        else {
            this._trackerWaiting = trackerNew;
            if (isInterruptCurrent && is_1.default.notEmpty(trackerPrevious))
                trackerPrevious.interrupt();
        }
        return trackerNew;
    }
    _runTracker(tracker) {
        debug_1.default.log('Running navigation tracker: ' + tracker.key, null, 'navigation');
        tracker.then((trackerCompleted) => {
            debug_1.default.log('Navigation tracker completed: ' + tracker.key
                + ', isInterrupted: ' + tracker.isInterrupted
                + ', isCompleted: ' + tracker.isCompleted
                + ', isStarted: ' + tracker.isStarted
                + ', isError: ' + tracker.isError, null, 'navigation');
            debug_1.default.log('Is navigation tracker waiting: ' + is_1.default.notEmpty(this._trackerWaiting), null, 'navigation');
            this._trackerRunning = null;
            if (is_1.default.notEmpty(this._trackerWaiting)) {
                debug_1.default.log('Waiting navigation tracker key: ' + this._trackerWaiting.key, null, 'animation');
                let trackerWaiting = this._trackerWaiting;
                this._trackerWaiting = null;
                trackerWaiting.update(trackerCompleted.nodeCurrent);
                animation_1.Animation.pause(0.01).then(() => { this._runTracker(trackerWaiting); });
            }
        });
        this._trackerRunning = tracker;
        if (animation_1.Animation.animate.isAllAnimationCompleted) {
            NodeNavigator.navigatePath(tracker.nodeCurrent, tracker);
        }
        else {
            animation_1.Animation.animate.allAnimationsCompleted.listenOnce(() => {
                NodeNavigator.navigatePath(tracker.nodeCurrent, tracker);
            });
        }
    }
    updateHistory(pathCurrent) {
        this.history.push(pathCurrent);
        if (this.history.length > this.historyMax)
            this.history.shift();
        this._historyIndex = this.history.length - 1;
    }
    up(isInterruptCurrent = false, forEachParentSegment, forEachChildSegment) {
        let nodeStart = this.internalNodeStart;
        let path = (nodeStart.isRoot || is_1.default.empty(nodeStart.parent)) ? '' : nodeStart.path + this._rootNode.children.pathSegmentDelimiter + nodeStart.key;
        return this.to(path, null, isInterruptCurrent, forEachParentSegment, forEachChildSegment, true);
    }
    back(isInterruptCurrent = false, forEachParentSegment, forEachChildSegment) {
        this._historyIndex--;
        if (this._historyIndex < 0)
            this._historyIndex = 0;
        let path = this._history[this._historyIndex];
        return this.to(path, null, isInterruptCurrent, forEachParentSegment, forEachChildSegment, false);
    }
    forward(isInterruptCurrent = false, forEachParentSegment, forEachChildSegment) {
        this._historyIndex++;
        if (this._historyIndex > this._history.length - 1)
            this._historyIndex = this._history.length - 1;
        let path = this._history[this._historyIndex];
        return this.to(path, null, isInterruptCurrent, forEachParentSegment, forEachChildSegment, false);
    }
    static navigatePath(nodeStart, tracker) {
        debug_1.default.log('NodeNavigation.navigatePath(...), tracker: ' + tracker.key, null, 'animation');
        if (is_1.default.empty(nodeStart)) {
            return this._arriveAtNode(tracker, null, true, false, true);
        }
        let pathCurrent = nodeStart.path + tracker.pathSegmentDelimiter + nodeStart.key;
        let route = this.getRouteFromPath(tracker.pathDestination, pathCurrent, tracker.pathSegmentDelimiter);
        if (is_1.default.empty(route) || route.length < 1) {
            return this._arriveAtNode(tracker, nodeStart, true, false, true);
        }
        this._navigateToNextSegmentInRoute(nodeStart, route, tracker);
        return tracker;
    }
    static getRouteFromPath(pathDestination, pathCurrent, pathSegmentDelimiter) {
        let lastCommonAncestor = this.findCommonAncestor(pathDestination, pathCurrent, pathSegmentDelimiter);
        let routeToLastCommonAncestor = pathCurrent.substring(pathCurrent.lastIndexOf(lastCommonAncestor)).split(pathSegmentDelimiter);
        routeToLastCommonAncestor = routeToLastCommonAncestor.reverse();
        routeToLastCommonAncestor.shift();
        let routeToDestinationFromLastCommonAncestor = pathDestination.substring(pathDestination.lastIndexOf(lastCommonAncestor) + lastCommonAncestor.length + 1).split(pathSegmentDelimiter);
        return routeToLastCommonAncestor.concat(routeToDestinationFromLastCommonAncestor);
    }
    static findCommonAncestor(path1, path2, pathSegmentDelimiter) {
        let pathSegments1 = path1.split(pathSegmentDelimiter);
        let pathSegments2 = path2.split(pathSegmentDelimiter);
        let isSameAncestor = true;
        let lastCommonAncestor = pathSegments1[0];
        let index = 0;
        while (isSameAncestor && index < pathSegments1.length && index < pathSegments2.length) {
            if (pathSegments1[index].localeCompare(pathSegments2[index]) === 0) {
                lastCommonAncestor = pathSegments1[index];
                index++;
            }
            else {
                isSameAncestor = false;
            }
        }
        return lastCommonAncestor;
    }
    static _navigateToNextSegmentInRoute(nodeCurrent, route, tracker) {
        if (!tracker.isInterrupted && !tracker.isStarted)
            tracker.start();
        else {
            if (is_1.default.empty(route) || route.length < 1 || is_1.default.empty(route[0])) {
                return this._arriveAtNode(tracker, nodeCurrent, true, false, false);
            }
            else if (tracker.isInterrupted) {
                return this._arriveAtNode(tracker, nodeCurrent, true, true, false);
            }
            else {
                this._arriveAtNode(tracker, nodeCurrent, false, false, false);
            }
        }
        let segment = route.shift();
        if (nodeCurrent.key === segment) {
            this._navigateToNextSegmentInRoute(nodeCurrent, route, tracker);
        }
        else if (is_1.default.notEmpty(nodeCurrent.parent) && nodeCurrent.parent.key === segment) {
            if (is_1.default.notEmpty(tracker.forEachParentSegment)) {
                tracker.forEachParentSegment(nodeCurrent, nodeCurrent.parent).then(() => {
                    this._navigateToNextSegmentInRoute(nodeCurrent.parent, route, tracker);
                });
            }
            else
                this._navigateToNextSegmentInRoute(nodeCurrent.parent, route, tracker);
        }
        else {
            let child = nodeCurrent.children.getByKey(segment);
            if (is_1.default.notEmpty(child)) {
                if (is_1.default.notEmpty(tracker.forEachChildSegment)) {
                    tracker.forEachChildSegment(nodeCurrent, child).then(() => {
                        this._navigateToNextSegmentInRoute(child, route, tracker);
                    });
                }
                else
                    this._navigateToNextSegmentInRoute(child, route, tracker);
            }
        }
        return tracker;
    }
    static _arriveAtNode(tracker, nodeCurrent, isCompleted, isInterrupted, isError = false) {
        tracker.update(nodeCurrent, isCompleted, isInterrupted, isError);
        tracker.navigator.arrivedAtNode.dispatch(tracker);
        return tracker;
    }
}
exports.NodeNavigator = NodeNavigator;
exports.default = NodeNavigator;
//# sourceMappingURL=nodeNavigator.js.map