import { IPromise } from '@pilotlab/result';
import { Signal } from '@pilotlab/signals';
import INode from './iNode';
import INodeNavigationTracker from './iNodeNavigationTracker';


export interface INodeNavigator {
    readonly history:string[];
    readonly historyMax:number;

    internalNodeStart:INode;


    arrivedAtNode:Signal<INodeNavigationTracker>;


    to(
        pathDestination:string,
        pathSegmentDelimiter?:string,
        isInterruptCurrent?:boolean,
        forEachParentSegment?:(nodeCurrent:INode, nodeParent:INode) => IPromise<any>,
        forEachChildSegment?:(nodeCurrent:INode, nodeChild:INode) => IPromise<any>,
        isUpdateHistory?:boolean
    ):INodeNavigationTracker;


    updateHistory(pathCurrent:string):void;


    up(
        isInterruptCurrent?:boolean,
        forEachParentSegment?:(nodeCurrent:INode, nodeParent:INode) => IPromise<any>,
        forEachChildSegment?:(nodeCurrent:INode, nodeChild:INode) => IPromise<any>
    ):INodeNavigationTracker;


    back(
        isInterruptCurrent?:boolean,
        forEachParentSegment?:(nodeCurrent:INode, nodeParent:INode) => IPromise<any>,
        forEachChildSegment?:(nodeCurrent:INode, nodeChild:INode) => IPromise<any>
    ):INodeNavigationTracker;


    forward(
        isInterruptCurrent?:boolean,
        forEachParentSegment?:(nodeCurrent:INode, nodeParent:INode) => IPromise<any>,
        forEachChildSegment?:(nodeCurrent:INode, nodeChild:INode) => IPromise<any>
    ):INodeNavigationTracker;
} // End of interface


export default INodeNavigator;
