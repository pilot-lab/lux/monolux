import INodeChangeOptions from './iNodeChangeOptions';


export interface INodeCreateOptions {
    key:string;
    label:string;
    index:number;
    changeOptions:INodeChangeOptions;
}


export default INodeCreateOptions;
