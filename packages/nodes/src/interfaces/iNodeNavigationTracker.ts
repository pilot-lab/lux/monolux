import { IPromise } from '@pilotlab/result';
import INode from './iNode';
import INodeNavigator from './iNodeNavigator';


export interface INodeNavigationTracker {
    update(
        nodeCurrent:INode,
        isCompleted?:boolean,
        isInterrupted?:boolean,
        isError?:boolean
    ):INodeNavigationTracker;


    start():void;
    interrupt():INodeNavigationTracker;
    next:INodeNavigationTracker;
    then(onDone:(value:INodeNavigationTracker) => any, onError?:(error:Error) => void):IPromise<any>;


    readonly key:string;
    readonly navigator:INodeNavigator;
    readonly nodeCurrent:INode;
    readonly pathDestination:string;
    readonly pathSegmentDelimiter:string;
    readonly forEachParentSegment:(nodeCurrent:INode, nodeParent:INode) => IPromise<any>;
    readonly forEachChildSegment:(nodeCurrent:INode, nodeChild:INode) => IPromise<any>;
    readonly isStarted:boolean;
    readonly isCompleted:boolean;
    readonly isInterrupted:boolean;
    readonly isUpdateHistory:boolean;
    readonly isError:boolean;
    readonly result:IPromise<INodeNavigationTracker>;
} // End interface


export default INodeNavigationTracker;
