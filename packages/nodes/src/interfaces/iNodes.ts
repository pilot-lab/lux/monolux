import { Signal } from '@pilotlab/signals';
import { List, MapList } from '@pilotlab/collections';
import INodeChangeOptions from './iNodeChangeOptions';
import INodeCreateOptions from './iNodeCreateOptions';
import INode from './iNode';
import INodeFactory from './iNodeFactory';
import NodesBase from '../nodesBase';
import NodeEventArgs from '../nodeEventArgs';


export interface INodes {
    /*====================================================================*
     START: Properties
     *====================================================================*/
    create:INodeFactory;
    readonly parent:INode;
    readonly list:List<INode>;
    readonly array:Array<INode>;
    readonly path:string;
    readonly pathSegmentDelimiter:string;
    readonly copy:INodes;
    isSignalChange:boolean;


    /*====================================================================*
     START: Signals
     *====================================================================*/
    changed:Signal<NodeEventArgs<any>>;


    childAdded:Signal<NodeEventArgs<INode>>;
    childAttached:Signal<NodeEventArgs<INode>>;
    childDetached:Signal<NodeEventArgs<INode>>;
    childDeleted:Signal<NodeEventArgs<INode>>;


    /*====================================================================*
     START: Public Methods
     *====================================================================*/
    /// Add
    add(child:INode, index?:number, changeOptions?:INodeChangeOptions):INode;
    addByKey(child:INode, key:string, index?:number, changeOptions?:INodeChangeOptions):INode;
    addByPath(child:INode, path:string, index?:number, changeOptions?:INodeChangeOptions):INode;


    /// Remove
    delete(child:INode, changeOptions?:INodeChangeOptions):INode;
    deleteAt(index:number, changeOptions?:INodeChangeOptions):INode;
    deleteByKey(key:string, changeOptions?:INodeChangeOptions):INode;


    clear(changeOptions?:INodeChangeOptions):INodes;


    /// Get
    getByKey(key:string, ...createArgs:any[]):INode;
    getByPath(path:string, ...createArgs:any[]):INode;
    getOrCreate(
        path:string,
        createOptions?:INodeCreateOptions,
        segmentCreateOptions?:INodeCreateOptions
    ):INode;


    item(index:number):INode;
    indexOf(child:INode):number;
    forEach(handler:(child:INode) => boolean):void;
    has(child:INode):boolean;
    hasKey(key:string):boolean;


    setIndex(child:INode, index?:number, changeOptions?:INodeChangeOptions):void;
    updateIndices(indexStart?:number, indexEnd?:number, changeOptions?:INodeChangeOptions):void;


    /// Compare and sort
    compareKeys(child1:INode, child2:INode):number;
    compareIndices(child1:INode, child2:INode):number;
    compareIndicesDescending(child1:INode, child2:INode):number;
    sortByKey():INodes;
    sortByIndex():NodesBase<INode>
    sortByIndexDescending():NodesBase<INode>
    sort(comparer:(child1:INode, child2:INode) => number):INodes;


    toMap(isIncludeDataTypes?:boolean):MapList<string, any>;
    toObject(isIncludeDataTypes?:boolean, isIncludeLabels?:boolean, isForceIncludeAll?:boolean):Object;
    toArray():any[];
    toJson(isIncludeDataTypes?:boolean):string;


    /*====================================================================*
     START: Internal
     *====================================================================*/
    internalParent:INode;
    internalChildAttached(args:NodeEventArgs<INode>):void;
    internalChildDetached(args:NodeEventArgs<INode>):void;
    internalAttachedAll(changeOptions:INodeChangeOptions):void;
    internalChanged(args:NodeEventArgs<INode>):void;
    internalDetachedAll(changeOptions:INodeChangeOptions):void;
} // End interface


export default INodes;
