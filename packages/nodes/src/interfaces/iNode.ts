import { IInitializable } from '@pilotlab/initializable';
import { IPromise } from '@pilotlab/result';
import { Signal } from '@pilotlab/signals';
import INodeChangeOptions from './iNodeChangeOptions';
import INodes from './iNodes';
import INodeFactory from './iNodeFactory';
import NodeEventArgs from '../nodeEventArgs';
import INodeNavigator from './iNodeNavigator';
import { NodeType } from '../nodeEnums';


export interface INode extends IInitializable {
    /*====================================================================*
     START: Properties
     *====================================================================*/
    key:string;


    readonly create:INodeFactory;
    nodeType:NodeType;
    readonly root:INode;
    readonly parent:INode;
    readonly parentCollection:INodes;
    readonly children:INodes;
    index:number;
    readonly indexPrevious:number;
    readonly nestLevel:number;
    readonly copy:INode;
    navigate:INodeNavigator;


    readonly isRoot:boolean;
    readonly isCollection:boolean;
    readonly isAttached:boolean;
    isSignalChange:boolean;


    /**
     * Based on backbone.js. From backbone docs:
     * Returns the relative path (URL) where the node's resource would be located on the server.
     * If your models are located somewhere else, override this method with the correct logic.
     * Generates URLs of the form: "[nodes.pathSegmentDelimiter][model.id]" by default, but you may override
     * by specifying an explicit pathRoot if the node's parent collection shouldn't be taken into account.
     *
     * A node with an id of 101, stored in a nodes with a path of
     * "/documents/7/notes", would have this path: "/documents/7/notes/101"
     */
    readonly path:string;


    /*====================================================================*
     START: Signals
     *====================================================================*/
    /**
     * A node has been added to the parent node's child collection, but is not necessarily
     * connected to the entire node system yet, with a reference back to the root node.
     */
    added:Signal<NodeEventArgs<INode>>;


    /**
     * The node has not only been added to its parent's child collection, but has also been
     * attached to the broader node system and holds a reference to the root of the node system.
     */
    attached:Signal<NodeEventArgs<INode>>;


    /**
     * The node has undergone one of the changes described by the NodeChangeType enum,
     * which includes the following change types: ADDED, ATTACHED, UPDATED, DETACHED, DELETED.
     * The type of change that has been applied to this node will be specified in the changeType
     * property of the dispatched NodeEventArgs.
     */
    readonly changed:Signal<NodeEventArgs<INode>>;


    /**
     * The node is about to be deleted from its parent's child collection list.
     * If the node is attached to the broader node system, it will then be detached.
     */
    deleted:Signal<NodeEventArgs<INode>>;


    /**
     * The node has been first deleted from its parent node's child collection, then detached
     * from the broader node system. The node's references to the former parent node and root node have been deleted.
     */
    detached:Signal<NodeEventArgs<INode>>;


    indexChanged:Signal<NodeEventArgs<INode>>;


    /*====================================================================*
     START: Public Methods
     *====================================================================*/
    delete(changeOptions?:INodeChangeOptions):IPromise<boolean>;
    setIndex(value?:number, changeOptions?:INodeChangeOptions, isRememberCurrentIndex?:boolean, isSoftSwap?:boolean):boolean;
    returnToPreviousIndex(changeOptions?:INodeChangeOptions):boolean;


    toObject(isSaveDataTypes?:boolean, isIncludeLabels?:boolean, appendToObject?:any, isForceInclude?:boolean):any;
    toJson(isSaveDataTypes?:boolean):string;


    /*====================================================================*
     START: Internal
     *====================================================================*/
    internalIsAttached:boolean;
    internalParentCollection:INodes;
    internalSetIndex(value:number, changeOptions:INodeChangeOptions):void;
    internalAdded(changeOptions:INodeChangeOptions):void;
    internalAttached(changeOptions:INodeChangeOptions):void;
    internalChanged(args:NodeEventArgs<any>):void;
    internalDeleted(changeOptions:INodeChangeOptions):void;
    internalDetached(changeOptions:INodeChangeOptions):void;
} // End interface


export default INode;
