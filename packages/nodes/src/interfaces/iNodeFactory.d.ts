import INodeCreateOptions from './iNodeCreateOptions';
import INodesFactory from './iNodesFactory';
import INode from './iNode';
export interface INodeFactory {
    collection: INodesFactory;
    register(scope: Object, className: string, aliases?: string[], description?: any): void;
    instance(createOptions?: INodeCreateOptions): INode;
    fromAny(value: (INode | Object | string), ...args: any[]): INode;
    fromString(value: string, parseString?: (value: string) => INode, ...args: any[]): INode;
    fromJson(value: string, ...args: any[]): INode;
    fromObject(value: Object, ...args: any[]): INode;
    fromArray(value: any[], ...args: any[]): INode;
}
export default INodeFactory;
