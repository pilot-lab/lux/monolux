import { IAnimationOptions, ISpeed } from '@pilotlab/animation';
import { NodeChangeActions, NodeChangeInitiationType } from '../nodeEnums';
export interface INodeChangeOptions extends IAnimationOptions {
    readonly durationZero: INodeChangeOptions;
    readonly durationDefault: INodeChangeOptions;
    setDuration(value: (number | ISpeed)): INodeChangeOptions;
    readonly noSignal: INodeChangeOptions;
    readonly signal: INodeChangeOptions;
    actions: NodeChangeActions;
    isSignalChange: boolean;
    setIsSignalChange(value: boolean): INodeChangeOptions;
    changeInitiationType: NodeChangeInitiationType;
    setChangeInitiationType(value: NodeChangeInitiationType): INodeChangeOptions;
    readonly copy: INodeChangeOptions;
}
export default INodeChangeOptions;
