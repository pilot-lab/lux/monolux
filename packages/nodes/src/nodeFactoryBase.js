"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const factory_1 = require("@pilotlab/factory");
const nodeBase_1 = require("./nodeBase");
class NodeFactoryBase {
    constructor() { this.p_factoryManager = new factory_1.FactoryManager(); }
    register(scope, className, aliases = [], description) {
        this.p_factoryManager.register(className, scope, aliases, description);
    }
    instance(options) { return null; }
    get collection() { return this.p_collection; }
    fromAny(value, ...args) {
        let node;
        if (value instanceof nodeBase_1.default)
            node = value.copy;
        else if (typeof value === 'string') {
            node = this.fromString(value, ...args);
        }
        else if (Array.isArray(value)) {
            node = this.fromArray(value, ...args);
        }
        else
            node = this.fromObject(value, ...args);
        if (is_1.default.empty(node))
            return this.instance();
        return node;
    }
    fromString(value, parseString, ...args) {
        let node;
        if (typeof value === 'string') {
            node = is_1.default.notEmpty(parseString) ? parseString(value) : this.fromJson(value, ...args);
            if (is_1.default.empty(node))
                return null;
        }
        else
            node = this.fromObject(value, ...args);
        return node;
    }
    fromJson(value, ...args) { return this.fromObject(JSON.parse(value), ...args); }
    fromObject(value, ...args) { return null; }
    fromArray(values, ...args) { return null; }
}
exports.NodeFactoryBase = NodeFactoryBase;
exports.default = NodeFactoryBase;
//# sourceMappingURL=nodeFactoryBase.js.map