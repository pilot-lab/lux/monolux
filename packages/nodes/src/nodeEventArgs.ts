import is from '@pilotlab/is';
import { IPromise } from '@pilotlab/result';
import { NodeChangeInitiationType } from './nodeEnums';
import NodeChangeOptions from './nodeChangeOptions';
import INode from  './interfaces/iNode';
import INodeChangeOptions from './interfaces/iNodeChangeOptions';
import { NodeChangeType } from './nodeEnums';


export class NodeEventArgs<TNode extends INode> {
    constructor(
        node:TNode,
        changeType:NodeChangeType,
        changeOptions:INodeChangeOptions = NodeChangeOptions.default,
        result?:IPromise<any>,
        isMultipleNodeChange:boolean = false
    ) {
        this.node = node;
        this.changeType = changeType;
        this.changeOptions = changeOptions;
        this.isMultipleNodeChange = isMultipleNodeChange;
        if (is.notEmpty(result)) this.result = result;
    }


    node:TNode;
    changeType:NodeChangeType;
    result:IPromise<any>;
    resultList:IPromise<any>[] = [];
    changeOptions:INodeChangeOptions;
    isMultipleNodeChange:boolean = false;
} // End class


export default NodeEventArgs;
