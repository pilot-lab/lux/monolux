export declare enum NodeType {
    ROOT = 0,
    BASIC = 1,
    COLLECTION = 2,
}
export declare enum NodeChangeType {
    ADDED = 0,
    ATTACHED = 1,
    UPDATED = 2,
    DETACHED = 3,
    DELETED = 4,
}
export declare enum NodeChangeActions {
    NONE = 0,
    SIGNAL_CHANGE = 1,
}
export declare enum NodeChangeInitiationType {
    NONE = 0,
    ANIMATION = 1,
    AUTOMATIC = 2,
    PROPERTY_CHANGE = 3,
    INTERRUPT = 4,
    MANUAL_TARGET_CHANGE = 5,
    USER_INTERACTION = 6,
}
