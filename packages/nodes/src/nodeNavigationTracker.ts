import is from '@pilotlab/is';
import { IPromise, Result } from '@pilotlab/result';
import Debug from '@pilotlab/debug';
import { Identifier } from '@pilotlab/ids';
import INode from './interfaces/iNode';
import NodeNavigator from './nodeNavigator';
import INodeNavigationTracker from './interfaces/iNodeNavigationTracker';


Debug.setCategoryMode('navigation', false);


export class NodeNavigationTracker<TNode extends INode> implements INodeNavigationTracker{
    constructor(
        navigator:NodeNavigator<TNode>,
        nodeCurrent:TNode,
        pathDestination:string,
        pathSegmentDelimiter:string = '/',
        forEachParentSegment?:(nodeCurrent:INode, nodeParent:INode) => IPromise<any>,
        forEachChildSegment?:(nodeCurrent:INode, nodeChild:INode) => IPromise<any>,
        isUpdateHistory:boolean = true
    ) {
        this._key = 'nodeNavigationTracker_' + Identifier.getSessionUniqueInteger();

        this._navigator = navigator;
        this._nodeCurrent = nodeCurrent;
        this._pathDestination = pathDestination;
        this._pathSegmentDelimiter = pathSegmentDelimiter;
        this._forEachParentSegment = forEachParentSegment;
        this._forEachChildSegment = forEachChildSegment;
        this._isUpdateHistory = isUpdateHistory;

        this.update(nodeCurrent, false, false, false);

        this._result = new Result<NodeNavigationTracker<TNode>>();
        if (is.empty(this._navigator)) this._result.resolve(this);
    }


    update(
        nodeCurrent:INode,
        isCompleted:boolean = false,
        isInterrupted:boolean = false,
        isError:boolean = false
    ):NodeNavigationTracker<any> {
        this._nodeCurrent = <TNode>nodeCurrent;

        if ((isInterrupted || isCompleted) && !this._isCompleted) {
            this._isCompleted = true;
            this._result.resolve(this);
        }

        this._isError = isError;

        return this;
    }


    start():void {
        Debug.log('Navigation tracker started: ' + this._key, null, 'navigation');

        this._isStarted = true;

        /// If we need to update the history, wait until the navigation process has actually started,
        /// in case we get bumped by another navigation request.
        if (this.isUpdateHistory) this._navigator.updateHistory(this._nodeCurrent.path);
    }


    interrupt():NodeNavigationTracker<TNode> {
        this._isInterrupted = true;
        return this;
    }


    get key():string { return this._key; }
    private _key:string;


    get navigator():NodeNavigator<TNode> { return this._navigator; }
    private _navigator:NodeNavigator<TNode>;


    get nodeCurrent():TNode { return this._nodeCurrent; }
    private _nodeCurrent:TNode;


    get pathDestination():string { return this._pathDestination; }
    private _pathDestination:string;


    get pathSegmentDelimiter():string { return this._pathSegmentDelimiter; }
    private _pathSegmentDelimiter:string;


    get forEachParentSegment():(nodeCurrent:INode, nodeParent:INode) => IPromise<any> { return this._forEachParentSegment; }
    private _forEachParentSegment:(nodeCurrent:INode, nodeParent:INode) => IPromise<any>;


    get forEachChildSegment():(nodeCurrent:INode, nodeChild:INode) => IPromise<any> { return this._forEachChildSegment; }
    private _forEachChildSegment:(nodeCurrent:INode, nodeChild:INode) => IPromise<any>;


    get isStarted():boolean { return this._isStarted; }
    private _isStarted:boolean = false;


    get isCompleted():boolean { return this._isCompleted; }
    private _isCompleted:boolean = false;


    get isInterrupted():boolean { return this._isInterrupted; }
    private _isInterrupted:boolean = false;


    get isUpdateHistory():boolean { return this._isUpdateHistory; }
    private _isUpdateHistory:boolean = false;


    get isError():boolean { return this._isError; }
    private _isError:boolean = false;


    get next():NodeNavigationTracker<TNode> { return this._next; }
    set next(value:NodeNavigationTracker<TNode>) { this._next = value; }
    private _next:NodeNavigationTracker<TNode>;


    get result():Result<NodeNavigationTracker<TNode>> { return this._result; }
    private _result:Result<NodeNavigationTracker<TNode>>;


    then(onDone:(value:NodeNavigationTracker<TNode>) => any, onError?:(error:Error) => void):IPromise<any> {
        return this._result.then(onDone, onError);
    }
} // End class


export default NodeNavigationTracker;
