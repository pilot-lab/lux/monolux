import is from '@pilotlab/is';
import INode from './interfaces/iNode';
import INodeFactory from './interfaces/iNodeFactory';
import INodesFactory from './interfaces/iNodesFactory';
import INodes from './interfaces/iNodes';
import NodeBase from './nodeBase';
import NodesBase from './nodesBase';


export abstract class NodesFactoryBase<TNodes extends INodes>
implements INodesFactory {
    constructor(nodeFactory:INodeFactory) {
        this.p_node = nodeFactory;
    }


    get node():INodeFactory { return this.p_node; }
    protected p_node:INodeFactory;


    instance(parent?:INode, ...args:any[]):TNodes { return null; }


    fromAny(value:(INode | TNodes | Object | string | Array<any>), parent?:INode, ...args:any[]):TNodes {
        let collection:TNodes;

        if (value instanceof NodeBase) {
            collection = <TNodes>(<INodes>value.children).copy;
            collection.internalParent = parent;
        } else if (value instanceof NodesBase) {
            collection = <TNodes>(<INodes>value).copy;
            collection.internalParent = parent;
        } else if (typeof value === 'string') {
            collection = this.fromString(value, parent, null, ...args);
        } else if (Array.isArray(value)) {
            collection = this.fromArray(value, parent, ...args);
        } else collection = this.fromObject(value, parent, ...args);

        if (is.empty(collection)) return this.instance(parent, ...args);
        return collection;
    }


    fromString(value:string, parent?:INode, parseString?:(data:string) => TNodes, ...args:any[]):TNodes {
        let collection:TNodes;

        if (typeof value === 'string') {
            collection = is.notEmpty(parseString) ? parseString(value) : <TNodes>this.fromJson(value, parent, ...args);
            if (is.empty(collection)) return;
        } else collection = this.fromObject(value, parent, ...args);

        return collection;
    }


    fromJson(value:string, parent?:INode, ...args:any[]):TNodes { return this.fromObject(JSON.parse(value), parent, ...args);  }
    fromObject(value:Object, parent?:INode, ...args:any[]):TNodes { return null; }
    fromArray(value:any[], parent?:INode, ...args:any[]):TNodes { return null; }
} // End class


export default NodesFactoryBase;
