"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const result_1 = require("@pilotlab/result");
const debug_1 = require("@pilotlab/debug");
const ids_1 = require("@pilotlab/ids");
debug_1.default.setCategoryMode('navigation', false);
class NodeNavigationTracker {
    constructor(navigator, nodeCurrent, pathDestination, pathSegmentDelimiter = '/', forEachParentSegment, forEachChildSegment, isUpdateHistory = true) {
        this._isStarted = false;
        this._isCompleted = false;
        this._isInterrupted = false;
        this._isUpdateHistory = false;
        this._isError = false;
        this._key = 'nodeNavigationTracker_' + ids_1.Identifier.getSessionUniqueInteger();
        this._navigator = navigator;
        this._nodeCurrent = nodeCurrent;
        this._pathDestination = pathDestination;
        this._pathSegmentDelimiter = pathSegmentDelimiter;
        this._forEachParentSegment = forEachParentSegment;
        this._forEachChildSegment = forEachChildSegment;
        this._isUpdateHistory = isUpdateHistory;
        this.update(nodeCurrent, false, false, false);
        this._result = new result_1.Result();
        if (is_1.default.empty(this._navigator))
            this._result.resolve(this);
    }
    update(nodeCurrent, isCompleted = false, isInterrupted = false, isError = false) {
        this._nodeCurrent = nodeCurrent;
        if ((isInterrupted || isCompleted) && !this._isCompleted) {
            this._isCompleted = true;
            this._result.resolve(this);
        }
        this._isError = isError;
        return this;
    }
    start() {
        debug_1.default.log('Navigation tracker started: ' + this._key, null, 'navigation');
        this._isStarted = true;
        if (this.isUpdateHistory)
            this._navigator.updateHistory(this._nodeCurrent.path);
    }
    interrupt() {
        this._isInterrupted = true;
        return this;
    }
    get key() { return this._key; }
    get navigator() { return this._navigator; }
    get nodeCurrent() { return this._nodeCurrent; }
    get pathDestination() { return this._pathDestination; }
    get pathSegmentDelimiter() { return this._pathSegmentDelimiter; }
    get forEachParentSegment() { return this._forEachParentSegment; }
    get forEachChildSegment() { return this._forEachChildSegment; }
    get isStarted() { return this._isStarted; }
    get isCompleted() { return this._isCompleted; }
    get isInterrupted() { return this._isInterrupted; }
    get isUpdateHistory() { return this._isUpdateHistory; }
    get isError() { return this._isError; }
    get next() { return this._next; }
    set next(value) { this._next = value; }
    get result() { return this._result; }
    then(onDone, onError) {
        return this._result.then(onDone, onError);
    }
}
exports.NodeNavigationTracker = NodeNavigationTracker;
exports.default = NodeNavigationTracker;
//# sourceMappingURL=nodeNavigationTracker.js.map