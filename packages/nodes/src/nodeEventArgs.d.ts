import { IPromise } from '@pilotlab/result';
import INode from './interfaces/iNode';
import INodeChangeOptions from './interfaces/iNodeChangeOptions';
import { NodeChangeType } from './nodeEnums';
export declare class NodeEventArgs<TNode extends INode> {
    constructor(node: TNode, changeType: NodeChangeType, changeOptions?: INodeChangeOptions, result?: IPromise<any>, isMultipleNodeChange?: boolean);
    node: TNode;
    changeType: NodeChangeType;
    result: IPromise<any>;
    resultList: IPromise<any>[];
    changeOptions: INodeChangeOptions;
    isMultipleNodeChange: boolean;
}
export default NodeEventArgs;
