import { ISpeed, IAnimationEaseFunction, AnimationOptions } from '@pilotlab/animation';
import { IPromise, Result } from '@pilotlab/result';
import { NodeChangeActions, NodeChangeInitiationType } from './nodeEnums';
import INodeChangeOptions from './interfaces/iNodeChangeOptions';
export declare class NodeChangeOptions extends AnimationOptions implements INodeChangeOptions {
    constructor(isSignalChange?: boolean, durationSpeed?: (number | ISpeed), ease?: IAnimationEaseFunction, changeInitiationType?: NodeChangeInitiationType, configuration?: Object, isInitialize?: boolean);
    protected p_onInitializeStarted(result: Result<any>, args: any[]): IPromise<any>;
    static readonly default: INodeChangeOptions;
    static readonly signal: INodeChangeOptions;
    static readonly zero: INodeChangeOptions;
    actions: NodeChangeActions;
    readonly durationZero: INodeChangeOptions;
    readonly durationDefault: INodeChangeOptions;
    setDuration(value: (number | ISpeed)): INodeChangeOptions;
    readonly noSignal: INodeChangeOptions;
    readonly signal: INodeChangeOptions;
    isSignalChange: boolean;
    setIsSignalChange(value: boolean): INodeChangeOptions;
    changeInitiationType: NodeChangeInitiationType;
    protected p_changeInitiationType: NodeChangeInitiationType;
    setChangeInitiationType(value: NodeChangeInitiationType): INodeChangeOptions;
    readonly copy: INodeChangeOptions;
}
export default NodeChangeOptions;
