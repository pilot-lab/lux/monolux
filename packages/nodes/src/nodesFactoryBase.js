"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const nodeBase_1 = require("./nodeBase");
const nodesBase_1 = require("./nodesBase");
class NodesFactoryBase {
    constructor(nodeFactory) {
        this.p_node = nodeFactory;
    }
    get node() { return this.p_node; }
    instance(parent, ...args) { return null; }
    fromAny(value, parent, ...args) {
        let collection;
        if (value instanceof nodeBase_1.default) {
            collection = value.children.copy;
            collection.internalParent = parent;
        }
        else if (value instanceof nodesBase_1.default) {
            collection = value.copy;
            collection.internalParent = parent;
        }
        else if (typeof value === 'string') {
            collection = this.fromString(value, parent, null, ...args);
        }
        else if (Array.isArray(value)) {
            collection = this.fromArray(value, parent, ...args);
        }
        else
            collection = this.fromObject(value, parent, ...args);
        if (is_1.default.empty(collection))
            return this.instance(parent, ...args);
        return collection;
    }
    fromString(value, parent, parseString, ...args) {
        let collection;
        if (typeof value === 'string') {
            collection = is_1.default.notEmpty(parseString) ? parseString(value) : this.fromJson(value, parent, ...args);
            if (is_1.default.empty(collection))
                return;
        }
        else
            collection = this.fromObject(value, parent, ...args);
        return collection;
    }
    fromJson(value, parent, ...args) { return this.fromObject(JSON.parse(value), parent, ...args); }
    fromObject(value, parent, ...args) { return null; }
    fromArray(value, parent, ...args) { return null; }
}
exports.NodesFactoryBase = NodesFactoryBase;
exports.default = NodesFactoryBase;
//# sourceMappingURL=nodesFactoryBase.js.map