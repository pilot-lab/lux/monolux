"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const nodeChangeOptions_1 = require("./nodeChangeOptions");
class NodeEventArgs {
    constructor(node, changeType, changeOptions = nodeChangeOptions_1.default.default, result, isMultipleNodeChange = false) {
        this.resultList = [];
        this.isMultipleNodeChange = false;
        this.node = node;
        this.changeType = changeType;
        this.changeOptions = changeOptions;
        this.isMultipleNodeChange = isMultipleNodeChange;
        if (is_1.default.notEmpty(result))
            this.result = result;
    }
}
exports.NodeEventArgs = NodeEventArgs;
exports.default = NodeEventArgs;
//# sourceMappingURL=nodeEventArgs.js.map