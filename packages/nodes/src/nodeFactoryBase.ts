import is from '@pilotlab/is';
import { FactoryManager } from '@pilotlab/factory';
import INode from './interfaces/iNode';
import INodeFactory from './interfaces/iNodeFactory';
import INodeCreateOptions from './interfaces/iNodeCreateOptions';
import INodesFactory from './interfaces/iNodesFactory';
import NodeBase from './nodeBase';


export abstract class NodeFactoryBase<TNode extends INode>
implements INodeFactory {
    constructor() { this.p_factoryManager = new FactoryManager(); }


    protected p_factoryManager:FactoryManager<TNode>;


    register(scope:Object, className:string, aliases:string[] = [], description?:any):void {
        this.p_factoryManager.register(className, scope, aliases, description);
    }


    instance(options?:INodeCreateOptions):TNode { return null; }


    get collection():INodesFactory { return this.p_collection; }
    protected p_collection:INodesFactory;


    fromAny(value:(TNode | Object | string), ...args:any[]):TNode {
        let node:TNode;

        if (value instanceof NodeBase) node = <TNode>(<INode>value).copy;
        else if (typeof value === 'string') {
            node = this.fromString(value, ...args);
        } else if (Array.isArray(value)) {
            node = this.fromArray(value, ...args);
        } else node = this.fromObject(value, ...args);

        if (is.empty(node)) return this.instance();
        return node;
    }


    fromString(value:string, parseString?:(value:string) => TNode, ...args:any[]):TNode {
        let node:TNode;

        if (typeof value === 'string') {
            node = is.notEmpty(parseString) ? parseString(value) : this.fromJson(value, ...args);
            if (is.empty(node)) return null;
        } else node = this.fromObject(value, ...args);

        return node;
    }


    fromJson(value:string, ...args:any[]):TNode { return this.fromObject(JSON.parse(value), ...args); }
    fromObject(value:Object, ...args:any[]):TNode { return null; }
    fromArray(values:any[], ...args:any[]):TNode { return null; }
} // End class


export default NodeFactoryBase;
