"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const animation_1 = require("@pilotlab/animation");
const nodeEnums_1 = require("./nodeEnums");
class NodeChangeOptions extends animation_1.AnimationOptions {
    constructor(isSignalChange = true, durationSpeed, ease, changeInitiationType, configuration, isInitialize = true) {
        super(null, null, null, false);
        if (isInitialize)
            this.initialize(isSignalChange, durationSpeed, ease, changeInitiationType, configuration);
    }
    p_onInitializeStarted(result, args) {
        const isSignalChange = args[0];
        const durationSpeed = args[1];
        const ease = args[2];
        const changeInitiationType = args[3];
        const configuration = args[4];
        this.isSignalChange = isSignalChange;
        this.changeInitiationType = is_1.default.notEmpty(changeInitiationType) ? changeInitiationType : nodeEnums_1.NodeChangeInitiationType.NONE;
        if (is_1.default.notEmpty(durationSpeed))
            this.durationSpeed = durationSpeed;
        if (is_1.default.notEmpty(ease))
            this.ease = ease;
        if (is_1.default.notEmpty(configuration))
            this.configuration = configuration;
        return result.resolve();
    }
    static get default() { return new NodeChangeOptions(); }
    static get signal() { return new NodeChangeOptions(true); }
    static get zero() { return new NodeChangeOptions(false).durationZero; }
    get durationZero() { return this.copy.setDuration(0); }
    get durationDefault() { return this.copy.setDuration(null); }
    setDuration(value) {
        this.p_durationSpeed = value;
        return this;
    }
    get noSignal() { return this.copy.setIsSignalChange(false); }
    get signal() { return this.copy.setIsSignalChange(true); }
    get isSignalChange() { return this.actions === nodeEnums_1.NodeChangeActions.SIGNAL_CHANGE; }
    set isSignalChange(value) { value ? this.actions = nodeEnums_1.NodeChangeActions.SIGNAL_CHANGE : this.actions = nodeEnums_1.NodeChangeActions.NONE; }
    setIsSignalChange(value) {
        this.isSignalChange = value;
        return this;
    }
    get changeInitiationType() { return this.p_changeInitiationType; }
    set changeInitiationType(value) { this.p_changeInitiationType = value; }
    setChangeInitiationType(value) {
        this.changeInitiationType = value;
        return this;
    }
    get copy() {
        let changeOptionsCopy = new NodeChangeOptions(this.isSignalChange, this.durationSpeed, this.ease, this.changeInitiationType, this.configuration);
        changeOptionsCopy.repeatCount = this.repeatCount;
        return changeOptionsCopy;
    }
}
exports.NodeChangeOptions = NodeChangeOptions;
exports.default = NodeChangeOptions;
//# sourceMappingURL=nodeChangeOptions.js.map