import is from '@pilotlab/is';
import { Initializable } from '@pilotlab/initializable';
import { Signal } from '@pilotlab/signals';
import { List, MapList } from '@pilotlab/collections';
import NodeChangeOptions from './nodeChangeOptions';
import { ICompareFunction } from '@pilotlab/function-types';
import INode from './interfaces/iNode';
import INodeFactory from './interfaces/iNodeFactory';
import INodeChangeOptions from './interfaces/iNodeChangeOptions';
import INodes from './interfaces/iNodes';
import NodeCreateOptions from './nodeCreateOptions';
import INodeCreateOptions from './interfaces/iNodeCreateOptions';
import { NodeChangeType } from './nodeEnums';
import { NodeEventArgs } from './nodeEventArgs';


export abstract class NodesBase<TNode extends INode>
extends Initializable implements INodes {
    constructor(
        factory:INodeFactory,
        parent:TNode,
        pathSegmentDelimiter:string = '/',
        isInitialize:boolean = true
    ) {
        super();
        this.p_factory = factory;
        this.p_list = new List<TNode>();
        if (is.notEmpty(parent)) this.internalParent = parent;
        if (is.notEmpty(pathSegmentDelimiter)) this.p_pathSegmentDelimiter = pathSegmentDelimiter;
        if (isInitialize) this.initialize();
    }


    /*====================================================================*
     START: Factory
     *====================================================================*/
    get create():INodeFactory { return this.p_factory; }
    protected p_factory:INodeFactory;


    /*====================================================================*
     START: Properties
     *====================================================================*/
    /**
     * The list of child nodes associated with this collection.
     */
    get list():List<TNode> { return this.p_list; }
    get array():Array<TNode> { return this.p_list.array; }
    protected p_list:List<TNode>;
    protected p_mapKeys:MapList<string, TNode> = new MapList<string, TNode>();


    get parent():TNode { return this.internalParent; }
    internalParent:TNode;


    /**
     * The path property represents the collection's relative location within the node hierarchy.
     * It can be used with routers or to retrieve the collection's data from a database.
     * Nodes within the collection will use path to construct paths of their own.
     */
    get path():string {
        let node:INode = this.parent;
        let path:string = this.pathSegmentDelimiter;
        while (is.notEmpty(node) && is.notEmpty(node.key) && !node.isRoot && is.notEmpty(node.parentCollection)) {
            path = node.parentCollection.pathSegmentDelimiter + node.key + path;
            node = node.parentCollection.parent;
        }

        if (is.notEmpty(this.parent) && is.notEmpty(this.parent.root) && is.notEmpty(this.parent.root.key)) path = this.parent.root.key + path;

        return path;
    }


    /**
     * The delimiter string used to separate segments of the node's path.
     * Examples: '/', '.', '_'
     */
    get pathSegmentDelimiter():string { return this.p_pathSegmentDelimiter; }


    /**
     * The pathSegmentDelimiter property can be used to indicate the relative path of the node collection,
     * for use with routers or to indicate the location of associated data in a database.
     * Nodes within the collection will use pathSegmentDelimiter to construct paths of their own.
     */
    set pathSegmentDelimiter(value:string) { this.p_pathSegmentDelimiter = value; }
    protected p_pathSegmentDelimiter:string = '/';


    get pathSegmentParentSymbol():string { return this.p_pathSegmentParentSymbol; }
    set pathSegmentParentSymbol(value:string) { this.p_pathSegmentParentSymbol = value; }
    protected p_pathSegmentParentSymbol:string = '..';


    get copy():INodes {
        return this.create.collection.instance();
    }


    set isSignalChange(value:boolean) {
        for (let i:number = 0, length = this.p_list.size; i < length; i++) {
            this.p_list.item(i).isSignalChange = value;
        }
    }


    /*====================================================================*
     START: Signals
     *====================================================================*/
    /// IMPORTANT: This signal should only be dispatched from internalChanged.
    changed:Signal<NodeEventArgs<any>> = new Signal<NodeEventArgs<any>>(false);


    childAdded:Signal<NodeEventArgs<TNode>> = new Signal<NodeEventArgs<TNode>>();
    private _childAdded(args:NodeEventArgs<TNode>) {
        this.p_onChildAdded(args);
        if (args.changeOptions && args.changeOptions.isSignalChange) {
            this.childAdded.dispatch(args);
        }
    }
    protected p_onChildAdded(args:NodeEventArgs<TNode>):void {}


    childAttached:Signal<NodeEventArgs<TNode>> = new Signal<NodeEventArgs<TNode>>();
    internalChildAttached(args:NodeEventArgs<TNode>):void {
        this.p_onChildAttached(args);
        if (args.changeOptions && args.changeOptions.isSignalChange) {
            this.childAttached.dispatch(args);
        }
    }
    protected p_onChildAttached(args:NodeEventArgs<TNode>):void {}


    childDetached:Signal<NodeEventArgs<TNode>> = new Signal<NodeEventArgs<TNode>>();
    internalChildDetached(args:NodeEventArgs<TNode>):void {
        this.p_onChildDetached(args);
        if (args.changeOptions && args.changeOptions.isSignalChange) {
            this.childDetached.dispatch(args);
        }
    }
    protected p_onChildDetached(args:NodeEventArgs<TNode>):void {}


    childDeleted:Signal<NodeEventArgs<TNode>> = new Signal<NodeEventArgs<TNode>>();
    private _childDeleted(args:NodeEventArgs<TNode>):void {
        this.p_onChildDeleted(args);
        if (args.changeOptions && args.changeOptions.isSignalChange) {
            this.childDeleted.dispatch(args);
        }
    }
    protected p_onChildDeleted(args:NodeEventArgs<TNode>):void {}


    childIndexChanged:Signal<NodeEventArgs<any>> = new Signal<NodeEventArgs<any>>();
    private _childIndexChanged(args:NodeEventArgs<any>):void {
        this.p_onChildIndexChanged(args);
        if (args.changeOptions && args.changeOptions.isSignalChange) {
            this.childIndexChanged.dispatch(args);
        }
    }
    protected p_onChildIndexChanged(args:NodeEventArgs<TNode>):void {}


    /*====================================================================*
     START: Methods
     *====================================================================*/
    /*--------------------------------------------------------------------*
     START: Internal
     *--------------------------------------------------------------------*/
    internalChanged(args:NodeEventArgs<any>):void {
        this.changed.dispatch(args);
        if (is.notEmpty(this.parent) && is.notEmpty(this.parent.internalChanged)) this.parent.internalChanged(args);
    }


    internalAttachedAll(changeOptions:INodeChangeOptions):void {
        for (let i:number = 0; i < this.p_list.size; i++) {
            let child:INode = this.p_list.item(i);
            child.internalParentCollection = this;
            child.internalAttached(changeOptions);
        }
    }


    internalDetachedAll(changeOptions:INodeChangeOptions):void {
        for (let i:number = 0; i < this.p_list.size; i++) {
            this.p_list.item(i).internalDetached(changeOptions);
        }
    }


    /*--------------------------------------------------------------------*
     START: Add
     *--------------------------------------------------------------------*/
    add(
        child:TNode,
        index?:number,
        changeOptions:INodeChangeOptions = <INodeChangeOptions>NodeChangeOptions.signal.durationZero
    ):TNode {
        if (is.empty(child)) return child;

        this.p_add(child, index, changeOptions);

        if (is.notEmpty(changeOptions) && (changeOptions.isSignalChange)) {
            this._childAdded(new NodeEventArgs<TNode>(child, NodeChangeType.ADDED, changeOptions));
        }

        return child;
    }
    protected p_add(
        child:TNode,
        index?:number,
        changeOptions:INodeChangeOptions = <INodeChangeOptions>NodeChangeOptions.signal.durationZero
    ):void {
        if (is.notEmpty(child.index) && child.index <= this.p_list.size && child.index >= 0) index = child.index;

        if (is.notEmpty(index) && index <= this.p_list.size && index >= 0) {
            this.p_list.add(child, index);
            let indexActual = this.p_list.indexOf(child);
            this.updateIndices(indexActual, this.p_list.size - 1, changeOptions);
        } else {
            this.p_list.add(child);
            child.internalSetIndex(this.p_list.size - 1, changeOptions);
        }

        if (is.notEmpty(child.key)) this.p_mapKeys.set(<string>child.key, child);

        child.internalParentCollection = this;
        child.internalAdded(changeOptions);

        if (is.notEmpty(this.internalParent) && is.notEmpty(this.internalParent.root)) {
            child.internalAttached(changeOptions);
        }

        child.indexChanged.listen(this._childIndexChanged, this);
    }


    addByKey(
        child:TNode,
        key:string,
        index?:number,
        changeOptions:INodeChangeOptions = <INodeChangeOptions>NodeChangeOptions.signal.durationZero
    ):TNode {
        if (is.empty(child) || is.empty(key)) return null;
        child.key = key;
        return this.add(child, index, changeOptions);
    }


    addByPath(
        child:TNode,
        path:string,
        index?:number,
        changeOptions:INodeChangeOptions = <INodeChangeOptions>NodeChangeOptions.default
    ):TNode { return this.getOrCreate(path, new NodeCreateOptions(null, null, null, changeOptions)); }


    /*--------------------------------------------------------------------*
     START: Remove
     *--------------------------------------------------------------------*/
    delete(
        child:TNode,
        changeOptions:INodeChangeOptions = <INodeChangeOptions>NodeChangeOptions.default
    ):TNode {
        if (is.empty(child)) return child;
        this.p_delete(child, changeOptions);
        this._childDeleted(new NodeEventArgs<TNode>(child, NodeChangeType.DELETED, changeOptions));
        return child;
    }
    protected p_delete(
        child:TNode,
        changeOptions:INodeChangeOptions = <INodeChangeOptions>NodeChangeOptions.default
    ):void {
        let indexActual:number = this.p_list.indexOf(child);

        let isNodeInList:boolean = this.p_list.delete(child);
        child.internalSetIndex(null, <INodeChangeOptions>NodeChangeOptions.zero);

        this.updateIndices(is.notEmpty(indexActual) ? indexActual : 0, this.p_list.size - 1, changeOptions);

        if (!isNodeInList) return null;
        if (is.notEmpty(child.key)) this.p_mapKeys.delete(<string>child.key);

        child.internalDeleted(changeOptions);

        if (is.notEmpty(this.internalParent) && is.notEmpty(this.internalParent.root)) {
            child.internalDetached(changeOptions);
        }

        (<INode>child).internalParentCollection = null;

        child.indexChanged.delete(this._childIndexChanged, this);
    }


    deleteAt(
        index:number,
        changeOptions:INodeChangeOptions = <INodeChangeOptions>NodeChangeOptions.default
    ):TNode {
        if (index > this.p_list.size - 1 || index < 0) return null;
        else {
            let child:TNode = this.p_list.item(index);
            return this.delete(child, changeOptions);
        }
    }


    deleteByKey(
        key:string,
        changeOptions:INodeChangeOptions = <INodeChangeOptions>NodeChangeOptions.default
    ):TNode {
        let child:TNode = this.p_mapKeys.get(key);
        return this.delete(child, changeOptions);
    }


    clear(changeOptions:INodeChangeOptions = <INodeChangeOptions>NodeChangeOptions.default):NodesBase<any> {
        this.p_list.forEach((child:TNode):boolean => {
            this.delete(child, changeOptions);
            return true;
        });
        return this;
    }


    /*--------------------------------------------------------------------*
     START: Update indices
     *--------------------------------------------------------------------*/
    setIndex(child:TNode, index?:number, changeOptions:INodeChangeOptions = <INodeChangeOptions>NodeChangeOptions.default):void {
        if (is.empty(child)) return;

        let indexPrevious:number = this.p_list.indexOf(child);

        this.p_list.delete(child);
        this.p_list.add(child, index);

        let indexUpdateStart:number = index < indexPrevious ? index : indexPrevious;
        this.updateIndices(indexUpdateStart, this.p_list.size - 1, changeOptions);
    }


    updateIndices(
        indexStart:number = 0,
        indexEnd?:number,
        changeOptions:INodeChangeOptions = <INodeChangeOptions>NodeChangeOptions.default
    ):void {
        if (indexStart < 0) indexStart = 0;
        if (is.empty(indexEnd) || indexEnd >= this.p_list.size) indexEnd = this.p_list.size - 1;

        for (let i = indexStart; i <= indexEnd; i++) {
            let child:TNode = this.p_list.item(i);
            if (is.notEmpty(child)) child.internalSetIndex(i, <INodeChangeOptions>changeOptions.durationZero);
        }
    }


    /*--------------------------------------------------------------------*
     START: Get
     *--------------------------------------------------------------------*/
    /**
     * Returns the node with the given key.
     * If the node doesn't exist, an empty node or null will be returned,
     * depending upon the specific concrete class implementation.
     */
    getByKey(key:string):TNode { return this.p_mapKeys.get(key); }


    getByPath(path:string, createOptions?:INodeCreateOptions):TNode {
        if (is.notEmpty(path) && typeof path === 'string' && path.indexOf(this.pathSegmentDelimiter) > -1) return this.getOrCreate(path, createOptions);
        return this.p_mapKeys.get(path);
    }


    /**
     * Looks for a node at the given key or path. If no node is found and appropriate initializers have been passed
     * as arguments in the createOptions and createSegmentOptions parameters, the method will attempt to build
     * a path, creating a new node for each segment of the path and attaching the subsequent segment node as a child.
     * Finally, a new node will be created using the createOptions instance to initialize the new node.
     * @param path {string} Either a node key, or a multiple segment path ending in a node key.
     * @param createOptions {NodeCreateOptions} If this is passed, a new node will be created using these initializers, if no existing node is found.
     * @param segmentCreateOptions {NodeCreateOptions} Provides initializers for the construction of any segment nodes that need to be added to build the specified path.
     * @param isAllowPath {boolean} Allow path formatted string, with multiple segments separated by a delimiter?
     * @returns TNode
     */
    getOrCreate(
        path:string,
        createOptions?:INodeCreateOptions,
        segmentCreateOptions?:INodeCreateOptions
    ):TNode {
        let child:INode;

        if (is.empty(path)) return <TNode>this.create.instance(); // Empty child, or null.
        else if (is.empty(path) || path.indexOf(this.pathSegmentDelimiter) < 0) {
            /// We've hit the last segment of the path

            let key:string = path;

            if (is.notEmpty(key)) child = this.p_mapKeys.get(key);
            if (is.notEmpty(child)) return <TNode>child;
            else if (is.notEmpty(createOptions)) {
                child = this.create.instance(createOptions);

                if (is.notEmpty(child)) {
                    if (is.notEmpty(key)) {
                        this.addByKey(
                            <TNode>child,
                            key,
                            createOptions.index,
                            createOptions.changeOptions
                        );
                    } else {
                        this.add(
                            <TNode>child,
                            createOptions.index,
                            createOptions.changeOptions
                        );
                    }
                }

                return <TNode>child;
            }
        } else {
            /// We received a multiple segment path as the first argument.
            let pathSegments:string[] = path.split(this.pathSegmentDelimiter);
            let segment:string;
            let collection:INodes = this;

            while (pathSegments.length > 0) {
                segment = pathSegments.shift();

                if (segment === this.p_pathSegmentParentSymbol) child = collection.parent;
                else if (is.empty(createOptions)) child = collection.getByPath(segment);
                else if (pathSegments.length > 0 && is.notEmpty(segmentCreateOptions)) {
                    child = collection.getByPath(segment, segmentCreateOptions);
                } else {
                    const changeOptions = is.notEmpty(createOptions) ? createOptions.changeOptions : NodeChangeOptions.default;
                    segmentCreateOptions = new NodeCreateOptions(null, null, null, changeOptions);
                    child = collection.getByPath(segment, createOptions);
                }

                if (is.empty(child)) break;
                collection = child.children;
                if (is.empty(collection)) break;
            }

            return <TNode>child;
        }
    }


    /*--------------------------------------------------------------------*
     START: Iterable methods
     *--------------------------------------------------------------------*/
    item(index:number):TNode { return this.p_list.item(index); }
    indexOf(node:TNode):number { return this.p_list.indexOf(node); }
    forEach(handler:(node:TNode) => boolean):void { this.p_list.forEach(handler); }
    has(node:TNode):boolean { return this.p_list.has(node); }
    hasKey(key:string):boolean { return this.p_mapKeys.has(key); }


    /*--------------------------------------------------------------------*
     START: Sort
     *--------------------------------------------------------------------*/
    compareKeys:ICompareFunction<TNode> = (child1:TNode, child2:TNode):number => {
        if (is.emptyAny(child1, child2, child1.key, child2.key)) return 0;
        return (<string>child1.key).toLowerCase().localeCompare((<string>child2.key).toLowerCase());
    };
    compareIndices:ICompareFunction<TNode> = (child1:TNode, child2:TNode):number => {
        let index1:number = (is.empty(child1) || !is.number(child1.index) || child1.index < 0 || this.p_list.size < 2) ? (this.p_list.size - 1) : child1.index;
        let index2:number = (is.empty(child2) || !is.number(child2.index) || child2.index < 0 || this.p_list.size < 2) ? (this.p_list.size - 1) : child2.index;
        return index1 - index2;
    };
    compareIndicesDescending:ICompareFunction<TNode> = (child1:TNode, child2:TNode):number => {
        let index1:number = (is.empty(child1) || !is.number(child1.index) || child1.index < 0 || this.p_list.size < 2) ? (this.p_list.size - 1) : child1.index;
        let index2:number = (is.empty(child2) || !is.number(child2.index) || child2.index < 0 || this.p_list.size < 2) ? (this.p_list.size - 1) : child2.index;
        return index2 - index1;
    };


    sortByKey(changeOptions:INodeChangeOptions = NodeChangeOptions.default):NodesBase<TNode> { return this.sort(this.compareKeys, changeOptions); }
    sortByIndex(changeOptions:INodeChangeOptions = NodeChangeOptions.default):NodesBase<TNode> { return this.sort(this.compareIndices, changeOptions); }
    sortByIndexDescending(changeOptions:INodeChangeOptions = NodeChangeOptions.default):NodesBase<TNode> { return this.sort(this.compareIndicesDescending, changeOptions); }
    sort(comparer:ICompareFunction<TNode>, changeOptions:INodeChangeOptions = NodeChangeOptions.default):NodesBase<TNode> {
        this.p_list.sort(comparer);

        for(let i = 0; i < this.p_list.size; i++) {
            let child:INode = this.p_list.item(i);
            if (is.notEmpty(child)) child.internalSetIndex(i, NodeChangeOptions.zero);
        }

        return this;
    }



    toMap(isIncludeDataTypes:boolean = false):MapList<string, any> {
        let map:MapList<string, any> = new MapList<string, any>();

        this.p_list.forEach(function (child:TNode):boolean {
            if (is.empty(child)) return true;
            map.set(<string>child.key, child.toObject(isIncludeDataTypes));
            return true;
        });

        return map;
    }


    toObject(isIncludeDataTypes:boolean = true, isIncludeLabels:boolean = true, isForceIncludeAll:boolean = false):Object {
        let object:Object = {};

        this.p_list.forEach((node:TNode):boolean => {
            if (is.empty(node)) return true;
            node.toObject(isIncludeDataTypes, isIncludeLabels, object, isForceIncludeAll);
            return true;
        });

        return object;
    }


    toArray():any[] {
        let array:any[] = [];

        for (let i:number = 0; i < this.p_list.size; i++) {
            if (is.notEmpty(this.p_list.item(i))) array[i] = this.p_list.item(i).toObject(false);
        }

        return array;
    }


    toJson(isIncludeDataTypes:boolean = false):string {
        return JSON.stringify(this.toObject(isIncludeDataTypes));
    }
} // End of class


export default NodesBase;
