import INodeChangeOptions from './interfaces/iNodeChangeOptions';
import INodeCreateOptions from './interfaces/iNodeCreateOptions';
export declare class NodeCreateOptions implements INodeCreateOptions {
    constructor(label?: string, key?: string, index?: number, changeOptions?: INodeChangeOptions);
    key: string;
    label: string;
    index: number;
    changeOptions: INodeChangeOptions;
}
export default NodeCreateOptions;
