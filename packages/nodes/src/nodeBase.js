"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const ids_1 = require("@pilotlab/ids");
const debug_1 = require("@pilotlab/debug");
const initializable_1 = require("@pilotlab/initializable");
const result_1 = require("@pilotlab/result");
const signals_1 = require("@pilotlab/signals");
const nodeChangeOptions_1 = require("./nodeChangeOptions");
const nodeEnums_1 = require("./nodeEnums");
const nodeEventArgs_1 = require("./nodeEventArgs");
const nodeNavigator_1 = require("./nodeNavigator");
class NodeBase extends initializable_1.Initializable {
    constructor(factory, nodeType = nodeEnums_1.NodeType.BASIC, key, isInitialize = true) {
        super();
        this.internalIsAttached = false;
        this.p_factory = factory;
        this._changed = new signals_1.Signal(false);
        this.p_isSignalChange = true;
        this.p_index = null;
        this.p_indexPrevious = null;
        this.p_nodeType = nodeEnums_1.NodeType.BASIC;
        if (is_1.default.notEmpty(key, true))
            this.p_key = key;
        this.added = new signals_1.Signal(true);
        this.attached = new signals_1.Signal(true);
        this.deleted = new signals_1.Signal(false);
        this.detached = new signals_1.Signal(false);
        this.indexChanged = new signals_1.Signal(false);
        if (isInitialize)
            this.initialize(nodeType);
    }
    p_onInitializeStarted(result, args) {
        const nodeType = args[0];
        this.p_onSetChildCollection(nodeType);
        this.p_setNodeType(nodeType, nodeChangeOptions_1.default.zero);
        return result.resolve();
    }
    get create() { return this.p_factory; }
    get isAttached() { return this.internalIsAttached; }
    get key() {
        if (is_1.default.empty(this.p_key, true)) {
            this.p_key = ids_1.default.generate();
        }
        return this.p_key;
    }
    set key(value) { this.p_key = value; }
    get parent() {
        if (is_1.default.notEmpty(this.parentCollection))
            return this.parentCollection.parent;
        else
            return null;
    }
    get parentCollection() { return this.internalParentCollection; }
    get root() {
        if (this.isAttached)
            return this.p_root;
        else
            return null;
    }
    get isRoot() {
        if (is_1.default.empty(this.root))
            return false;
        else
            return this.p_root === this;
    }
    get isCollection() {
        return this.nodeType === nodeEnums_1.NodeType.ROOT || this.nodeType === nodeEnums_1.NodeType.COLLECTION;
    }
    get children() { return this.p_children; }
    p_onSetChildCollection(nodeType) {
        if (nodeType === nodeEnums_1.NodeType.BASIC)
            return;
        if (is_1.default.notEmpty(this.create) && is_1.default.notEmpty(this.create.collection)) {
            this.p_children = this.create.collection.instance(this);
        }
    }
    get isSignalChange() { return this.p_isSignalChange; }
    set isSignalChange(value) {
        if (this.isCollection)
            this.children.isSignalChange = value;
        this.p_isSignalChange = value;
    }
    get nodeType() { return this.p_nodeType; }
    set nodeType(value) { this.p_setNodeType(value, nodeChangeOptions_1.default.signal.durationZero); }
    p_setNodeType(value, changeOptions) {
        if (is_1.default.empty(value) || this.p_nodeType === value)
            return;
        if (this.p_nodeType === nodeEnums_1.NodeType.BASIC && value !== nodeEnums_1.NodeType.BASIC)
            this.p_onSetChildCollection(value);
        if (value === nodeEnums_1.NodeType.ROOT) {
            this.p_root = this;
            this.internalAttached(changeOptions);
        }
        this.p_nodeType = value;
        this.p_onNodeTypeChanged(value, changeOptions);
        return this;
    }
    p_onNodeTypeChanged(type, changeOptions) { }
    get path() {
        let value = '';
        let node = this.parent;
        while (is_1.default.notEmpty(node) && is_1.default.notEmpty(node.key) && !node.isRoot && is_1.default.notEmpty(node.parentCollection)) {
            value = node.parentCollection.pathSegmentDelimiter + node.key + value;
            node = node.parentCollection.parent;
        }
        if (!this.isRoot && is_1.default.notEmpty(this.root) && is_1.default.notEmpty(this.root.key))
            value = this.root.key + value;
        return value;
    }
    get index() { return this.p_index; }
    set index(value) {
        this.setIndex(value, nodeChangeOptions_1.default.zero, true);
    }
    get indexPrevious() { return this.p_indexPrevious; }
    internalSetIndex(value, changeOptions) {
        if (this.p_index === value)
            return;
        this.p_indexPrevious = this.p_index;
        this.p_index = value;
        this.p_onIndexSet(value, this.p_indexPrevious, changeOptions);
        this.indexChanged.dispatch(new nodeEventArgs_1.NodeEventArgs(this, nodeEnums_1.NodeChangeType.UPDATED, changeOptions));
    }
    p_onIndexSet(indexNew, indexPrevious, changeOptions) { }
    setIndex(value, changeOptions = nodeChangeOptions_1.default.default, isRememberCurrentIndex = true, isSoftSwap = false) {
        if (is_1.default.empty(this.parentCollection) || is_1.default.empty(value) || !is_1.default.number(value))
            return false;
        let parentCollection = this.parentCollection;
        let indexPrevious = this.index;
        if (value > parentCollection.list.size - 1)
            value = parentCollection.list.size - 1;
        if (value < 0)
            value = 0;
        let child = this;
        if (isSoftSwap)
            parentCollection.setIndex(child, value, changeOptions);
        else if (parentCollection.delete(this, nodeChangeOptions_1.default.signal.durationZero))
            parentCollection.add(this, value, changeOptions);
        if (!isRememberCurrentIndex && indexPrevious > -1)
            this.p_indexPrevious = indexPrevious;
        return true;
    }
    get nestLevel() {
        if (is_1.default.empty(this.parentCollection) || is_1.default.empty(this.parentCollection.parent) || this.isRoot)
            return 0;
        return this.parentCollection.parent.nestLevel + 1;
    }
    get copy() { return this.create.instance(); }
    get changed() { return this._changed; }
    ;
    delete(changeOptions = nodeChangeOptions_1.default.default) {
        if (is_1.default.notEmpty(this.parentCollection)) {
            this.parentCollection.delete(this, changeOptions);
            return result_1.Result.resolve(true);
        }
        else
            return result_1.Result.resolve(false);
    }
    returnToPreviousIndex(changeOptions = nodeChangeOptions_1.default.default) {
        return this.setIndex(this.indexPrevious);
    }
    toObject(isIncludeDataTypes = false, isIncludeLabels = true, appendToObject, isForceInclude = false) {
        return {};
    }
    toJson(isIncludeDataTypes = false) { return JSON.stringify(this.toObject(isIncludeDataTypes)); }
    internalChanged(args) {
        if ((args.changeOptions.isSignalChange) && this.isSignalChange)
            this.changed.dispatch(args);
        if (is_1.default.notEmpty(this.parentCollection) && (is_1.default.notEmpty(this.parentCollection.internalChanged))) {
            this.parentCollection.internalChanged(args);
        }
    }
    internalAdded(changeOptions) {
        this.p_onAdded(changeOptions);
        let args = new nodeEventArgs_1.NodeEventArgs(this, nodeEnums_1.NodeChangeType.ADDED, changeOptions);
        this.added.dispatch(args);
        this.internalChanged(args);
    }
    p_onAdded(changeOptions) { }
    internalAttached(changeOptions) {
        try {
            if (is_1.default.notEmpty(this.internalParentCollection) && is_1.default.notEmpty(this.internalParentCollection.parent)) {
                if (!this.isRoot)
                    this.p_root = this.internalParentCollection.parent.root;
            }
            if (is_1.default.notEmpty(this.children))
                this.children.internalAttachedAll(changeOptions);
            this.internalIsAttached = true;
            let args = new nodeEventArgs_1.NodeEventArgs(this, nodeEnums_1.NodeChangeType.ATTACHED, changeOptions);
            this.p_onAttached(changeOptions);
            this.attached.dispatch(args);
            this.internalChanged(args);
            if (is_1.default.notEmpty(this.internalParentCollection))
                this.internalParentCollection.internalChildAttached(args);
        }
        catch (e) {
            debug_1.default.error(e, 'NodeBase.internalAttached()');
        }
    }
    p_onAttached(changeOptions) { }
    internalDeleted(changeOptions) {
        this.p_indexPrevious = this.index;
        let args = new nodeEventArgs_1.NodeEventArgs(this, nodeEnums_1.NodeChangeType.DELETED, changeOptions);
        this.deleted.dispatch(args);
        this.internalChanged(args);
        this.p_onDeleted(changeOptions);
    }
    p_onDeleted(changeOptions) { }
    internalDetached(changeOptions) {
        if (is_1.default.notEmpty(this.children))
            this.children.internalDetachedAll(changeOptions);
        this.internalIsAttached = false;
        let args = new nodeEventArgs_1.NodeEventArgs(this, nodeEnums_1.NodeChangeType.DETACHED, changeOptions);
        this.detached.dispatch(args);
        this.internalChanged(args);
        this.p_onDetached(changeOptions);
        if (is_1.default.notEmpty(this.internalParentCollection))
            this.internalParentCollection.internalChildDetached(args);
    }
    p_onDetached(changeOptions) { }
    get navigate() {
        let navigator;
        if (this.isRoot || (this.isAttached && is_1.default.empty(this.root))) {
            if (is_1.default.empty(this.p_navigator))
                this.p_navigator = new nodeNavigator_1.NodeNavigator(this);
            navigator = this.p_navigator;
        }
        else
            navigator = this.root.navigate;
        navigator.internalNodeStart = this;
        return navigator;
    }
}
exports.NodeBase = NodeBase;
exports.default = NodeBase;
//# sourceMappingURL=nodeBase.js.map