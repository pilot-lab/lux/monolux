import is from '@pilotlab/is';
import { ISpeed, IAnimationEaseFunction, AnimationOptions } from '@pilotlab/animation';
import { IPromise, Result } from '@pilotlab/result';
import { NodeChangeActions, NodeChangeInitiationType } from './nodeEnums';
import INodeChangeOptions from './interfaces/iNodeChangeOptions';


export class NodeChangeOptions extends AnimationOptions implements INodeChangeOptions {
    constructor(
        isSignalChange:boolean = true,
        durationSpeed?:(number | ISpeed),
        ease?:IAnimationEaseFunction,
        changeInitiationType?:NodeChangeInitiationType,
        configuration?:Object,
        isInitialize:boolean = true
    ) {
        super(null, null, null, false);
        if (isInitialize) this.initialize(isSignalChange, durationSpeed, ease, changeInitiationType, configuration);
    }


    protected p_onInitializeStarted(result:Result<any>, args:any[]):IPromise<any> {
        const isSignalChange:boolean = args[0];
        const durationSpeed:(number | ISpeed) = args[1];
        const ease:IAnimationEaseFunction = args[2];
        const changeInitiationType:NodeChangeInitiationType = args[3];
        const configuration:Object = args[4];

        this.isSignalChange = isSignalChange;
        this.changeInitiationType = is.notEmpty(changeInitiationType) ? changeInitiationType : NodeChangeInitiationType.NONE;

        if (is.notEmpty(durationSpeed)) this.durationSpeed = durationSpeed;
        if (is.notEmpty(ease)) this.ease = ease;
        if (is.notEmpty(configuration)) this.configuration = configuration;

        return result.resolve();
    }


    static get default():INodeChangeOptions { return new NodeChangeOptions(); }
    static get signal():INodeChangeOptions { return new NodeChangeOptions(true); }
    static get zero():INodeChangeOptions { return <INodeChangeOptions>new NodeChangeOptions(false).durationZero; }


    actions:NodeChangeActions;


    get durationZero():INodeChangeOptions { return this.copy.setDuration(0); }
    get durationDefault():INodeChangeOptions { return this.copy.setDuration(null); }
    setDuration(value:(number | ISpeed)):INodeChangeOptions {
        this.p_durationSpeed = value;
        return this;
    }


    get noSignal():INodeChangeOptions { return this.copy.setIsSignalChange(false); }
    get signal():INodeChangeOptions { return this.copy.setIsSignalChange(true); }


    get isSignalChange():boolean { return this.actions === NodeChangeActions.SIGNAL_CHANGE; }
    set isSignalChange(value:boolean) { value ? this.actions = NodeChangeActions.SIGNAL_CHANGE : this.actions = NodeChangeActions.NONE; }
    setIsSignalChange(value:boolean):INodeChangeOptions {
        this.isSignalChange = value;
        return this;
    }


    get changeInitiationType():NodeChangeInitiationType { return this.p_changeInitiationType; }
    set changeInitiationType(value:NodeChangeInitiationType) { this.p_changeInitiationType = value; }
    protected p_changeInitiationType:NodeChangeInitiationType;
    setChangeInitiationType(value:NodeChangeInitiationType):INodeChangeOptions {
        this.changeInitiationType = value;
        return this;
    }


    get copy():INodeChangeOptions {
        let changeOptionsCopy:INodeChangeOptions = new NodeChangeOptions(this.isSignalChange, this.durationSpeed, this.ease, this.changeInitiationType, this.configuration);
        changeOptionsCopy.repeatCount = this.repeatCount;
        return changeOptionsCopy;
    }
} // End class


export default NodeChangeOptions;
