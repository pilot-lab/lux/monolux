"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var NodeType;
(function (NodeType) {
    NodeType[NodeType["ROOT"] = 0] = "ROOT";
    NodeType[NodeType["BASIC"] = 1] = "BASIC";
    NodeType[NodeType["COLLECTION"] = 2] = "COLLECTION";
})(NodeType = exports.NodeType || (exports.NodeType = {}));
var NodeChangeType;
(function (NodeChangeType) {
    NodeChangeType[NodeChangeType["ADDED"] = 0] = "ADDED";
    NodeChangeType[NodeChangeType["ATTACHED"] = 1] = "ATTACHED";
    NodeChangeType[NodeChangeType["UPDATED"] = 2] = "UPDATED";
    NodeChangeType[NodeChangeType["DETACHED"] = 3] = "DETACHED";
    NodeChangeType[NodeChangeType["DELETED"] = 4] = "DELETED";
})(NodeChangeType = exports.NodeChangeType || (exports.NodeChangeType = {}));
var NodeChangeActions;
(function (NodeChangeActions) {
    NodeChangeActions[NodeChangeActions["NONE"] = 0] = "NONE";
    NodeChangeActions[NodeChangeActions["SIGNAL_CHANGE"] = 1] = "SIGNAL_CHANGE";
})(NodeChangeActions = exports.NodeChangeActions || (exports.NodeChangeActions = {}));
var NodeChangeInitiationType;
(function (NodeChangeInitiationType) {
    NodeChangeInitiationType[NodeChangeInitiationType["NONE"] = 0] = "NONE";
    NodeChangeInitiationType[NodeChangeInitiationType["ANIMATION"] = 1] = "ANIMATION";
    NodeChangeInitiationType[NodeChangeInitiationType["AUTOMATIC"] = 2] = "AUTOMATIC";
    NodeChangeInitiationType[NodeChangeInitiationType["PROPERTY_CHANGE"] = 3] = "PROPERTY_CHANGE";
    NodeChangeInitiationType[NodeChangeInitiationType["INTERRUPT"] = 4] = "INTERRUPT";
    NodeChangeInitiationType[NodeChangeInitiationType["MANUAL_TARGET_CHANGE"] = 5] = "MANUAL_TARGET_CHANGE";
    NodeChangeInitiationType[NodeChangeInitiationType["USER_INTERACTION"] = 6] = "USER_INTERACTION";
})(NodeChangeInitiationType = exports.NodeChangeInitiationType || (exports.NodeChangeInitiationType = {}));
//# sourceMappingURL=nodeEnums.js.map