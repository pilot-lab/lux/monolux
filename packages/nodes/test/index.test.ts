import is from '@pilotlab/is';
import Debug from '@pilotlab/debug';
import Identifier from '@pilotlab/ids';


import {
    AsyncTest,
    Setup,
    SetupFixture,
    Teardown,
    TeardownFixture,
    Test,
    TestCase,
    TestFixture,
    Timeout,
    Expect
} from 'alsatian';


import {
    INode,
    INodeChangeOptions,
    INodes,
    INodeCreateOptions,
    INodeFactory,
    INodesFactory,
    NodeBase,
    NodeChangeOptions,
    NodesBase,
    NodeCreateOptions,
    NodeType,
    NodeChangeActions,
    NodeChangeInitiationType,
    NodeChangeType,
    NodeEventArgs,
    NodeFactoryBase,
    NodesFactoryBase,
    NodeNavigationTracker,
    NodeNavigator
} from '../index';


class NodesFactory extends NodesFactoryBase<Nodes> {
    instance(parent:Node):Nodes { return new Nodes(new NodeFactory(), parent); }
} // End class
class NodeFactory extends NodeFactoryBase<Node> {
    instance(createOptions?:INodeCreateOptions):Node { return new Node(createOptions.key, createOptions.label); }
    get collection():INodesFactory { return new NodesFactory(this); }
} // End class
class Nodes extends NodesBase<Node> {} // End class
class NodeRoot extends NodeBase<Node, Nodes, NodeRoot> {
    constructor(key?:string, label?:string) {
        super(new NodeFactory(), NodeType.ROOT, key);
        this.label = label;
    }

    label:string;
} // End class
class Node extends NodeBase<Node, Nodes, NodeRoot> {
    constructor(key?:string, label?:string) {
        super(new NodeFactory(), NodeType.BASIC, key);
        this.label = label;
    }

    label:string;
} // End class
class NodeContainer extends NodeBase<Node, Nodes, NodeRoot> {
    constructor(key?:string, label?:string) {
        super(new NodeFactory(), NodeType.COLLECTION, key);
        this.label = label;
    }

    label:string;
} // End class


@TestFixture("Node Tests")
export class Tests {
    @AsyncTest("instantiation")
    @Timeout(5000) // Alsatian will now wait 5 seconds before failing
    public async asyncTest(testValue:any) {
        const promise:Promise<any> = new Promise((resolve, reject) => {
            let nodeRoot:Node = new NodeRoot('nodes');
            let node01:NodeContainer = new NodeContainer(Identifier.generate(), 'Node 01');
            let node02:NodeContainer = new NodeContainer(Identifier.generate(), 'Node 02');
            let node03:Node = new Node(Identifier.generate(), 'Node 03');
            let node04:Node = new Node(Identifier.generate(), 'Node 04');

            node01.children.changed.listen((args:NodeEventArgs<Node>) => {
                Debug.log(`Change in node 01 children: ${NodeChangeType[args.changeType]}, node: ${args.node.label}`);
            });

            node02.changed.listen((args:NodeEventArgs<Node>) => {
                Debug.log(`Change in node 02: ${NodeChangeType[args.changeType]}`);
            });

            node03.added.listenOnce(() => {
                Debug.log('node03 was added!');
            });

            nodeRoot.children.childAdded.listen((args:NodeEventArgs<Node>) => {
                Debug.log(`Child added to root: ${args.node.label}`);
            });

            nodeRoot.children.add(node01);
            node01.children.add(node02);
            node01.children.add(node03);
            node02.children.add(node04);

            Debug.log(`node02 index: ${node02.index}`);

            Debug.log(`node01 ID: ${node01.key}, label: ${node01.label}`);
            Debug.log(`node02 ID: ${node02.key}, label: ${node02.label}`);
            Debug.log(`node03 ID: ${node03.key}, label: ${node03.label}`);
            Debug.log(`node04 ID: ${node04.key}, label: ${node04.label}`);

            Debug.log(`${node04.index}`, 'node04.index');

            node04.attached.listen((args:NodeEventArgs<any>) => {
                Debug.log(`${args.node.parent.key}`, 'node04 parent.key');
                Debug.log(`${node04.parentCollection.parent.key}`, 'node04 parentCollection.parent.key');
                Debug.log(`${node04.path}`, 'node04 path');
            });

            node02.deleted.listen((args:NodeEventArgs<Node>) => {
                Debug.log('Node02 deleted.');
            });

            node01.children.childDeleted.listen((args:NodeEventArgs<Node>) => {
                Debug.log(`Child deleted from Node01: ${args.node.label}`);
            });

            node02.setIndex(1);
            Debug.log(`node02 index: ${node02.index}`);

            node02.delete();

            resolve();
        });

        const result:any = await promise;
    }


    @AsyncTest("deletion")
    @Timeout(5000) // Alsatian will now wait 5 seconds before failing
    public async asyncDeletion(testValue:any) {
        const promise:Promise<any> = new Promise((resolve, reject) => {
            let nodeRoot:Node = new NodeRoot('nodes');
            let node01:NodeContainer = new NodeContainer(Identifier.generate(), 'Node 01');
            let node02:NodeContainer = new NodeContainer(Identifier.generate(), 'Node 02');

            node01.children.changed.listen((args:NodeEventArgs<Node>) => {
                Debug.log(`Change in node 01 children: ${NodeChangeType[args.changeType]}, node: ${args.node.label}`);
            });

            node02.changed.listen((args:NodeEventArgs<Node>) => {
                Debug.log(`Change in node 02: ${NodeChangeType[args.changeType]}`);
            });

            nodeRoot.children.add(node01);
            node01.children.add(node02);

            node01.children.childDeleted.listen((args:NodeEventArgs<Node>) => {
                Debug.log(`Child deleted from Node01: ${args.node.label}, remaining: ${node01.children.list.size}`);
            });

            node01.children.deleteByKey(node02.key);

            resolve();
        });

        const result:any = await promise;
    }
}
