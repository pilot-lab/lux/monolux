"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var lux_debug_1 = require("@pilotlab/debug");
var lux_ids_1 = require("@pilotlab/ids");
var alsatian_1 = require("alsatian");
var index_1 = require("../index");
var NodesFactory = (function (_super) {
    __extends(NodesFactory, _super);
    function NodesFactory() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NodesFactory.prototype.instance = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        return new Nodes(args[0], args[1]);
    };
    return NodesFactory;
}(index_1.NodesFactoryBase));
var NodeFactory = (function (_super) {
    __extends(NodeFactory, _super);
    function NodeFactory() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NodeFactory.prototype.instance = function (createOptions) { return new Node(); };
    Object.defineProperty(NodeFactory.prototype, "collection", {
        get: function () { return new NodesFactory(); },
        enumerable: true,
        configurable: true
    });
    return NodeFactory;
}(index_1.NodeFactoryBase));
var Nodes = (function (_super) {
    __extends(Nodes, _super);
    function Nodes() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return Nodes;
}(index_1.NodesBase));
var Node = (function (_super) {
    __extends(Node, _super);
    function Node(type, ID, key, label) {
        return _super.call(this, new NodeFactory(), type, ID, key, label) || this;
    }
    return Node;
}(index_1.NodeBase));
var Tests = (function () {
    function Tests() {
    }
    Tests.prototype.asyncTest = function (testValue) {
        return __awaiter(this, void 0, void 0, function () {
            var promise, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        promise = new Promise(function (resolve, reject) {
                            var nodeRoot = new Node(index_1.NodeType.ROOT, 'nodes');
                            var node01 = new Node(index_1.NodeType.COLLECTION, lux_ids_1.default.generate());
                            var node02 = new Node(index_1.NodeType.COLLECTION, lux_ids_1.default.generate());
                            var node03 = new Node(index_1.NodeType.BASIC, lux_ids_1.default.generate(), 'node03', 'Node 03');
                            var node04 = new Node(index_1.NodeType.BASIC, lux_ids_1.default.generate(), 'node04', 'Node 04');
                            node03.attached.listenOnce(function () {
                                lux_debug_1.default.log('node03 was attached!');
                            });
                            nodeRoot.children.add(node01);
                            node01.children.add(node02);
                            node02.children.add(node03);
                            node02.children.add(node04);
                            lux_debug_1.default.log("node01 ID: " + node01.ID + ", key: " + node01.key + ", label: " + node01.label);
                            lux_debug_1.default.log("node02 ID: " + node02.ID + ", key: " + node02.key + ", label: " + node02.label);
                            lux_debug_1.default.log("node03 ID: " + node03.ID + ", key: " + node03.key + ", label: " + node03.label);
                            lux_debug_1.default.log("node04 ID: " + node04.ID + ", key: " + node04.key + ", label: " + node04.label);
                            lux_debug_1.default.log("" + node04.index, 'node04.index');
                            lux_debug_1.default.log("" + node04.parent.ID, 'node04 parent.ID');
                            lux_debug_1.default.log("" + node04.parentCollection.parent.ID, 'node04 parentCollection.parent.ID');
                            lux_debug_1.default.log("" + node04.path, 'node04 path');
                            resolve();
                        });
                        return [4, promise];
                    case 1:
                        result = _a.sent();
                        return [2];
                }
            });
        });
    };
    __decorate([
        alsatian_1.AsyncTest("instantiation"),
        alsatian_1.Timeout(5000)
    ], Tests.prototype, "asyncTest", null);
    Tests = __decorate([
        alsatian_1.TestFixture("Node Tests")
    ], Tests);
    return Tests;
}());
exports.Tests = Tests;
//# sourceMappingURL=index.test.js.map
