import ImagerBase from './imagerBase';
export declare class Imager extends ImagerBase {
    constructor(image: (HTMLImageElement | HTMLCanvasElement | HTMLVideoElement | Blob | File | string));
}
export default Imager;
