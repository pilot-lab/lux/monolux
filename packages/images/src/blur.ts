import Debug from '@pilotlab/debug';


export class BlurStack {
    r:number = 0;
    g:number = 0;
    b:number = 0;
    a:number = 0;
    next:BlurStack;
}


export enum BlurQuality {
    LOW,
    HIGH
}


export class BlurDescription {
    constructor(radius:number = 3, quality:BlurQuality = BlurQuality.LOW, isBlurAlphaChannel:boolean = false) {
        this.radius = radius;
        this.quality = quality;
        this.isBlurAlphaChannel = isBlurAlphaChannel;
    }


    radius:number = 3;
    quality:BlurQuality = BlurQuality.LOW;
    isBlurAlphaChannel:boolean = false;
}


export class Blur {
    /*====================================================================*
     START: Public static
     *====================================================================*/
    static blurCanvas(
        canvas:HTMLCanvasElement,
        radius:number, quality:BlurQuality = BlurQuality.LOW, isBlurAlphaChannel:boolean = false
    ):void { this._stackBoxBlurCanvas(canvas, 0, 0, canvas.width, canvas.height, radius, quality, isBlurAlphaChannel); }


    static blurImage(
        image:HTMLImageElement,
        radius:number, quality:BlurQuality = BlurQuality.LOW, isBlurAlphaChannel:boolean = false
    ):void {
        if (isNaN(radius) || radius < 1) return;

        let w:number = image.offsetWidth;
        let h:number = image.offsetHeight;
        if (w <= 0 || h <=0) return;

        let canvas:HTMLCanvasElement = document.createElement('canvas');
        canvas.width = w;
        canvas.height = h;

        let context = canvas.getContext('2d');
        context.drawImage(image, 0, 0);

        this._stackBoxBlurCanvas(canvas, 0, 0, w, h, radius, quality, isBlurAlphaChannel);

        image.src = canvas.toDataURL('image/png');
    }


    /*====================================================================*
     START: Private static
     *====================================================================*/
    private static _mul_table = [ 1,171,205,293,57,373,79,137,241,27,391,357,41,19,283,265,497,469,443,421,25,191,365,349,335,161,155,149,9,278,269,261,505,245,475,231,449,437,213,415,405,395,193,377,369,361,353,345,169,331,325,319,313,307,301,37,145,285,281,69,271,267,263,259,509,501,493,243,479,118,465,459,113,446,55,435,429,423,209,413,51,403,199,393,97,3,379,375,371,367,363,359,355,351,347,43,85,337,333,165,327,323,5,317,157,311,77,305,303,75,297,294,73,289,287,71,141,279,277,275,68,135,67,133,33,262,260,129,511,507,503,499,495,491,61,121,481,477,237,235,467,232,115,457,227,451,7,445,221,439,218,433,215,427,425,211,419,417,207,411,409,203,202,401,399,396,197,49,389,387,385,383,95,189,47,187,93,185,23,183,91,181,45,179,89,177,11,175,87,173,345,343,341,339,337,21,167,83,331,329,327,163,81,323,321,319,159,79,315,313,39,155,309,307,153,305,303,151,75,299,149,37,295,147,73,291,145,289,287,143,285,71,141,281,35,279,139,69,275,137,273,17,271,135,269,267,133,265,33,263,131,261,130,259,129,257,1];
    private static _shg_table = [0,9,10,11,9,12,10,11,12,9,13,13,10,9,13,13,14,14,14,14,10,13,14,14,14,13,13,13,9,14,14,14,15,14,15,14,15,15,14,15,15,15,14,15,15,15,15,15,14,15,15,15,15,15,15,12,14,15,15,13,15,15,15,15,16,16,16,15,16,14,16,16,14,16,13,16,16,16,15,16,13,16,15,16,14,9,16,16,16,16,16,16,16,16,16,13,14,16,16,15,16,16,10,16,15,16,14,16,16,14,16,16,14,16,16,14,15,16,16,16,14,15,14,15,13,16,16,15,17,17,17,17,17,17,14,15,17,17,16,16,17,16,15,17,16,17,11,17,16,17,16,17,16,17,17,16,17,17,16,17,17,16,16,17,17,17,16,14,17,17,17,17,15,16,14,16,15,16,13,16,15,16,14,16,15,16,12,16,15,16,17,17,17,17,17,13,16,15,17,17,17,16,15,17,17,17,16,15,17,17,14,16,17,17,16,17,17,16,15,17,16,14,17,16,15,17,16,17,17,16,17,15,16,17,14,17,16,15,17,16,17,13,17,16,17,17,16,17,14,17,16,17,16,17,16,17,9];


    /**
     * StackBoxBlur - a fast almost Box Blur For Canvas
     * Version: 	0.3
     *
     * Author:	Mario Klingemann
     * Contact: 	mario@quasimondo.com
     * Website:	http://www.quasimondo.com/
     * Twitter:	@quasimondo
     *
     * In case you find this class useful - especially in commercial projects -
     * I am not totally unhappy for a small donation to my PayPal account
     * mario@quasimondo.de
     *
     * Copyright (c) 2010 Mario Klingemann
     *
     * Permission is hereby granted, free of charge, to any person
     * obtaining a copy of this software and associated documentation
     * files (the "Software"), to deal in the Software without
     * restriction, including without limitation the rights to use,
     * copy, modify, merge, publish, distribute, sublicense, and/or sell
     * copies of the Software, and to permit persons to whom the
     * Software is furnished to do so, subject to the following conditions:
     *
     * The above copyright notice and this permission notice shall be
     * included in all copies or substantial portions of the Software.
     *
     * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
     * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
     * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
     * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
     * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
     * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
     * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
     * OTHER DEALINGS IN THE SOFTWARE.
     */
    private static _stackBoxBlurCanvas(
        canvas:HTMLCanvasElement,
        top_x:number, top_y:number,
        width:number, height:number,
        radius:number = 3,
        quality:BlurQuality = BlurQuality.LOW,
        isBlurAlphaChannel:boolean = false
    ):void {
        if (isNaN(radius) || radius < 1) return;
        radius |= 0;

        let iterations:number = quality === BlurQuality.LOW ? 1 : 2;
        if (isNaN(iterations)) iterations = 1;
        iterations |= 0;
        if (iterations > 3) iterations = 3;
        if (iterations < 1) iterations = 1;

        let context = canvas.getContext('2d');
        let imageData;

        try {imageData = context.getImageData(top_x, top_y, width, height); }
        catch(e) { Debug.error(e, 'Cannot access image in BlurCanvas.stackBoxBlurCanvasRGBA(...)'); }

        let pixels = imageData.data;

        let x, y, i, p, yp, yi, yw, r_sum, g_sum, b_sum,
            r_out_sum, g_out_sum, b_out_sum,
            r_in_sum, g_in_sum, b_in_sum,
            pr, pg, pb, rbs;

        let a_sum, a_out_sum, a_in_sum, pa;

        let div:number = radius + radius + 1;
        let w4:number = width << 2;
        let widthMinus1:number  = width - 1;
        let heightMinus1:number = height - 1;
        let radiusPlus1:number  = radius + 1;

        let stackStart:BlurStack = new BlurStack();
        let stack:BlurStack = stackStart;
        let stackEnd:BlurStack;

        for (i = 1; i < div; i++) {
            stack = stack.next = new BlurStack();
            if (i == radiusPlus1) stackEnd = stack;
        }
        stack.next = stackStart;
        let stackIn:BlurStack;

        let mul_sum:number = this._mul_table[radius];
        let shg_sum:number = this._shg_table[radius];

        while (iterations-- > 0) {
            yw = yi = 0;

            for (y = height; --y > -1;) {
                r_sum = radiusPlus1 * (pr = pixels[yi]);
                g_sum = radiusPlus1 * (pg = pixels[yi+1]);
                b_sum = radiusPlus1 * (pb = pixels[yi+2]);

                if (isBlurAlphaChannel) a_sum = radiusPlus1 * (pa = pixels[yi+3]);

                stack = stackStart;

                for(i = radiusPlus1; --i > -1;) {
                    stack.r = pr;
                    stack.g = pg;
                    stack.b = pb;
                    if (isBlurAlphaChannel) stack.a = pa;
                    stack = stack.next;
                }

                for(i = 1; i < radiusPlus1; i++) {
                    p = yi + ((widthMinus1 < i ? widthMinus1 : i) << 2);

                    if (isBlurAlphaChannel) {
                        r_sum += (stack.r = pixels[p]);
                        g_sum += (stack.g = pixels[p + 1]);
                        b_sum += (stack.b = pixels[p + 2]);
                        a_sum += (stack.a = pixels[p + 3]);
                    } else {
                        r_sum += (stack.r = pixels[p++]);
                        g_sum += (stack.g = pixels[p++]);
                        b_sum += (stack.b = pixels[p]);
                    }

                    stack = stack.next;
                }

                stackIn = stackStart;

                for (x = 0; x < width; x++) {
                    pixels[yi++] = (r_sum * mul_sum) >>> shg_sum;
                    pixels[yi++] = (g_sum * mul_sum) >>> shg_sum;
                    pixels[yi++] = (b_sum * mul_sum) >>> shg_sum;
                    if (isBlurAlphaChannel) pixels[yi++] = (a_sum * mul_sum) >>> shg_sum;

                    p =  (yw + ((p = x + radius + 1) < widthMinus1 ? p : widthMinus1)) << 2;

                    if (isBlurAlphaChannel) {
                        r_sum -= stackIn.r - (stackIn.r = pixels[p]);
                        g_sum -= stackIn.g - (stackIn.g = pixels[p + 1]);
                        b_sum -= stackIn.b - (stackIn.b = pixels[p + 2]);
                        a_sum -= stackIn.a - (stackIn.a = pixels[p + 3]);
                    } else {
                        r_sum -= stackIn.r - (stackIn.r = pixels[p++]);
                        g_sum -= stackIn.g - (stackIn.g = pixels[p++]);
                        b_sum -= stackIn.b - (stackIn.b = pixels[p]);
                    }

                    stackIn = stackIn.next;

                }

                yw += width;
            }

            for (x = 0; x < width; x++) {
                yi = x << 2;

                if (isBlurAlphaChannel) {
                    r_sum = radiusPlus1 * (pr = pixels[yi]);
                    g_sum = radiusPlus1 * (pg = pixels[yi + 1]);
                    b_sum = radiusPlus1 * (pb = pixels[yi + 2]);
                    a_sum = radiusPlus1 * (pa = pixels[yi + 3]);
                } else {
                    r_sum = radiusPlus1 * (pr = pixels[yi++]);
                    g_sum = radiusPlus1 * (pg = pixels[yi++]);
                    b_sum = radiusPlus1 * (pb = pixels[yi]);
                }

                stack = stackStart;

                for(i = 0; i < radiusPlus1; i++) {
                    stack.r = pr;
                    stack.g = pg;
                    stack.b = pb;
                    if (isBlurAlphaChannel) stack.a = pa;

                    stack = stack.next;
                }

                yp = width;

                for(i = 1; i <= radius; i++) {
                    yi = (yp + x) << 2;

                    if (isBlurAlphaChannel) {
                        r_sum += (stack.r = pixels[yi]);
                        g_sum += (stack.g = pixels[yi + 1]);
                        b_sum += (stack.b = pixels[yi + 2]);
                        a_sum += (stack.a = pixels[yi + 3]);
                    } else {
                        r_sum += (stack.r = pixels[yi++]);
                        g_sum += (stack.g = pixels[yi++]);
                        b_sum += (stack.b = pixels[yi]);
                    }

                    stack = stack.next;

                    if( i < heightMinus1 ) yp += width;
                }

                yi = x;
                stackIn = stackStart;

                for (y = 0; y < height; y++) {
                    p = yi << 2;

                    if (isBlurAlphaChannel) {
                        pixels[p + 3] = pa = (a_sum * mul_sum) >>> shg_sum;

                        if (pa > 0) {
                            pa = 255 / pa;
                            pixels[p] = ((r_sum * mul_sum) >>> shg_sum) * pa;
                            pixels[p + 1] = ((g_sum * mul_sum) >>> shg_sum) * pa;
                            pixels[p + 2] = ((b_sum * mul_sum) >>> shg_sum) * pa;
                        } else pixels[p] = pixels[p + 1] = pixels[p + 2] = 0;
                    } else {
                        pixels[p]   = (r_sum * mul_sum) >>> shg_sum;
                        pixels[p+1] = (g_sum * mul_sum) >>> shg_sum;
                        pixels[p+2] = (b_sum * mul_sum) >>> shg_sum;
                    }

                    p = (x + (((p = y + radiusPlus1) < heightMinus1 ? p : heightMinus1) * width )) << 2;

                    r_sum -= stackIn.r - (stackIn.r = pixels[p]);
                    g_sum -= stackIn.g - (stackIn.g = pixels[p+1]);
                    b_sum -= stackIn.b - (stackIn.b = pixels[p+2]);
                    if (isBlurAlphaChannel) a_sum -= stackIn.a - (stackIn.a = pixels[p+3]);

                    stackIn = stackIn.next;

                    yi += width;
                } // End for (y = 0; y < height; y++)
            } // End for (x = 0; x < width; x++)
        } // End while (iterations-- > 0)

        context.putImageData(imageData, top_x, top_y);
    }
} // End class


export default Blur;

