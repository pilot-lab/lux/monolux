export declare class BlurStack {
    r: number;
    g: number;
    b: number;
    a: number;
    next: BlurStack;
}
export declare enum BlurQuality {
    LOW = 0,
    HIGH = 1,
}
export declare class BlurDescription {
    constructor(radius?: number, quality?: BlurQuality, isBlurAlphaChannel?: boolean);
    radius: number;
    quality: BlurQuality;
    isBlurAlphaChannel: boolean;
}
export declare class Blur {
    static blurCanvas(canvas: HTMLCanvasElement, radius: number, quality?: BlurQuality, isBlurAlphaChannel?: boolean): void;
    static blurImage(image: HTMLImageElement, radius: number, quality?: BlurQuality, isBlurAlphaChannel?: boolean): void;
    private static _mul_table;
    private static _shg_table;
    private static _stackBoxBlurCanvas(canvas, top_x, top_y, width, height, radius?, quality?, isBlurAlphaChannel?);
}
export default Blur;
