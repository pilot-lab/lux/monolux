import { Cache } from '@pilotlab/cache';
import { IPromise } from '@pilotlab/result';
export declare class ImageCache extends Cache<HTMLImageElement> {
    static images: ImageCache;
    protected p_createNew(key: string, result: IPromise<HTMLImageElement>): IPromise<HTMLImageElement>;
}
export default ImageCache;
