import { AbstractDistanceCalculator } from "./abstractDistanceCalculator";
export declare abstract class AbstractCIE94 extends AbstractDistanceCalculator {
    protected _kA: number;
    protected _Kl: number;
    protected _K1: number;
    protected _K2: number;
    calculateRaw(r1: number, g1: number, b1: number, a1: number, r2: number, g2: number, b2: number, a2: number): number;
}
export declare class CIE94Textiles extends AbstractCIE94 {
    protected _setDefaults(): void;
}
export declare class CIE94GraphicArts extends AbstractCIE94 {
    protected _setDefaults(): void;
}
