import { AbstractDistanceCalculator } from "./abstractDistanceCalculator";
export declare class CIEDE2000 extends AbstractDistanceCalculator {
    private static readonly _kA;
    private static readonly _pow25to7;
    private static readonly _deg360InRad;
    private static readonly _deg180InRad;
    private static readonly _deg30InRad;
    private static readonly _deg6InRad;
    private static readonly _deg63InRad;
    private static readonly _deg275InRad;
    private static readonly _deg25InRad;
    calculateRaw(r1: number, g1: number, b1: number, a1: number, r2: number, g2: number, b2: number, a2: number): number;
    calculateRawInLab(Lab1: {
        L: number;
        a: number;
        b: number;
    }, Lab2: {
        L: number;
        a: number;
        b: number;
    }): number;
    private static _calculatehp(b, ap);
    private static _calculateRT(ahp, aCp);
    private static _calculateT(ahp);
    private static _calculate_ahp(C1pC2p, h_bar, h1p, h2p);
    private static _calculate_dHp(C1pC2p, h_bar, h2p, h1p);
}
