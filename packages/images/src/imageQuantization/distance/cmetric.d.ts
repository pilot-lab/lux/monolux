import { AbstractDistanceCalculator } from "./abstractDistanceCalculator";
export declare class CMETRIC extends AbstractDistanceCalculator {
    calculateRaw(r1: number, g1: number, b1: number, a1: number, r2: number, g2: number, b2: number, a2: number): number;
}
