/**
 * @preserve
 * Copyright 2015-2016 Igor Bezkrovnyi
 * All rights reserved. (MIT Licensed)
 *
 * iq.ts - Image Quantization Library
 */
import * as constants from "./constants/index"
import * as conversion from "./conversion/index"
import * as distance from "./distance/index"
import * as palette from "./palette/index"
import * as image from "./image/index"
import * as quality from "./quality/index"
import * as utils from "./utils/index"

export {
    constants,
    conversion,
    distance,
    palette,
    image,
    quality,
    utils
}
