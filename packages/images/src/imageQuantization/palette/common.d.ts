import { PointContainer } from "../utils/pointContainer";
import { Palette } from "../utils/palette";
export interface IPaletteQuantizer {
    sample(pointBuffer: PointContainer): void;
    quantize(): Palette;
}
