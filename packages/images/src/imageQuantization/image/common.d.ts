import { PointContainer } from "../utils/pointContainer";
import { Palette } from "../utils/palette";
export interface IImageDitherer {
    quantize(pointBuffer: PointContainer, palette: Palette): PointContainer;
}
