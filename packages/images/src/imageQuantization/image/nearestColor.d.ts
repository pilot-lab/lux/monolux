import { IImageDitherer } from "./common";
import { AbstractDistanceCalculator } from "../distance/abstractDistanceCalculator";
import { PointContainer } from "../utils/pointContainer";
import { Palette } from "../utils/palette";
export declare class NearestColor implements IImageDitherer {
    private _distance;
    constructor(colorDistanceCalculator: AbstractDistanceCalculator);
    quantize(pointBuffer: PointContainer, palette: Palette): PointContainer;
}
