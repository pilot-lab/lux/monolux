import { IImageDitherer } from "./common";
import { AbstractDistanceCalculator } from "../distance/abstractDistanceCalculator";
import { PointContainer } from "../utils/pointContainer";
import { Palette } from "../utils/palette";
export declare class ErrorDiffusionRiemersma implements IImageDitherer {
    private _distance;
    private _weights;
    private _errorQueueSize;
    private _errorPropagation;
    private _max;
    constructor(colorDistanceCalculator: AbstractDistanceCalculator, errorQueueSize?: number, errorPropagation?: number);
    quantize(pointBuffer: PointContainer, palette: Palette): PointContainer;
    private _createWeights();
}
