export declare function rgb2hsl(r: number, g: number, b: number): {
    h: number;
    s: number;
    l: number;
};
