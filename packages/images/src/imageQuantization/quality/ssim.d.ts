import { PointContainer } from "../utils/pointContainer";
export declare class SSIM {
    compare(image1: PointContainer, image2: PointContainer): number;
    private _iterate(image1, image2, callback);
    private _calculateLumaValuesForWindow(image, x, y, width, height);
    private _calculateAverageLuma(lumaValues);
}
