import is from '@pilotlab/is';
import { List } from '@pilotlab/collections';
import { Signal } from '@pilotlab/signals';
import ImagerBase from './imagerBase';


export class TileRow { tiles:List<HTMLCanvasElement> = new List<HTMLCanvasElement>(); } // End class


export class ImageTiles extends ImagerBase {
    constructor(
        image:(HTMLImageElement | HTMLCanvasElement | HTMLVideoElement | Blob | File | string),
        tileMaxSize?:number,
        scale?:number
    ) {
        super();

        if (is.notEmpty(tileMaxSize)) this._tileMaxSize = tileMaxSize;
        if (is.notEmpty(scale)) this._scale = scale;

        this.p_initialize(image);
    }


    /*====================================================================*
     START: Members and Properties
     *====================================================================*/
    get tileMaxSize():number { return this._tileMaxSize; }
    private _tileMaxSize:number = 2000;
    private _scale:number = 1;


    get rowCount():number { return this._numRows; }
    get columnCount():number { return this._numCols; }
    private _numRows:number;
    private _numCols:number;


    get tileWidth():number { return this._tileWidth; }
    get tileHeight():number { return this._tileHeight; }
    private _tileWidth:number;
    private _tileHeight:number;
    private _tileMinSize:number = 1;

    rows:List<TileRow> = new List<TileRow>();


    get canvas():HTMLCanvasElement {
        let canvas:HTMLCanvasElement = document.createElement('canvas');
        canvas.width = this.width;
        canvas.height = this.height;
        let context:CanvasRenderingContext2D = canvas.getContext('2d');
        this.drawToCanvas(context, 0, 0, this.width, this.height);
        return canvas;
    }


    /*====================================================================*
     START: Signals
     *====================================================================*/
    loaded:Signal<ImageTiles> = new Signal<ImageTiles>(true);


    /*====================================================================*
     START: Protected Methods
     *====================================================================*/
    protected p_initializeImage(image:HTMLImageElement | HTMLCanvasElement | HTMLVideoElement):void {
        if (is.empty(image)) return;

        document.body.appendChild(image);
        this.p_imageWidth = image.offsetWidth;
        this.p_imageHeight = image.offsetHeight;

        this._numCols = Math.ceil(this.width / this._tileMaxSize);
        this._numRows = Math.ceil(this.height / this._tileMaxSize);

        let tileWidthFloat:number = this.width / this._numCols;
        let tileHeightFloat:number = this.height / this._numRows;
        this._tileWidth = Math.floor(tileWidthFloat);
        this._tileHeight = Math.floor(tileHeightFloat);

        for (let row:number = 0; row < this._numRows; row++) {
            let tileRow:TileRow = new TileRow();

            for (let col:number = 0; col < this._numCols; col++) {
                let canvas:HTMLCanvasElement = document.createElement('canvas');
                let context:CanvasRenderingContext2D = canvas.getContext('2d');
                canvas.width = Math.ceil((this._tileWidth < this._tileMinSize ? this._tileMinSize : this._tileWidth) * this._scale);
                canvas.height = Math.ceil((this._tileHeight < this._tileMinSize ? this._tileMinSize : this._tileHeight) * this._scale);
                context.drawImage(<HTMLImageElement>image, col * tileWidthFloat, row * tileHeightFloat, tileWidthFloat, tileHeightFloat, 0, 0, this._tileWidth * this._scale, this._tileHeight * this._scale);
                tileRow.tiles.add(canvas);
            }

            this.rows.add(tileRow);
        }

        this.p_isLoaded = true;
        this.loaded.dispatch(this);

        document.body.removeChild(image);
    }


    /*====================================================================*
     START: Public Methods
     *====================================================================*/
    drawToCanvas(context:CanvasRenderingContext2D, x:number = 0, y:number = 0, width:number = this.width, height:number = this.height):void {
        let tile:HTMLCanvasElement;
        let scaleWidth:number = width / this.width;
        let scaleHeight:number = height / this.height;
        let tileWidth:number = Math.ceil(this._tileWidth * scaleWidth);
        let tileHeight:number = Math.ceil(this._tileHeight * scaleHeight);

        for (let row:number = 0; row < this._numRows; row++) {
            for (let col:number = 0; col < this._numCols; col++) {
                tile = this.rows.item(row).tiles.item(col);
                context.drawImage(tile, 0, 0, this._tileWidth * this._scale, this._tileHeight * this._scale, x + (col * tileWidth), y + (row * tileHeight), tileWidth + 1, tileHeight + 1);
            }
        }
    }
} // End class


export default ImageTiles
