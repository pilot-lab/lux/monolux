import { List } from '@pilotlab/collections';
import { Signal } from '@pilotlab/signals';
import ImagerBase from './imagerBase';
export declare class TileRow {
    tiles: List<HTMLCanvasElement>;
}
export declare class ImageTiles extends ImagerBase {
    constructor(image: (HTMLImageElement | HTMLCanvasElement | HTMLVideoElement | Blob | File | string), tileMaxSize?: number, scale?: number);
    readonly tileMaxSize: number;
    private _tileMaxSize;
    private _scale;
    readonly rowCount: number;
    readonly columnCount: number;
    private _numRows;
    private _numCols;
    readonly tileWidth: number;
    readonly tileHeight: number;
    private _tileWidth;
    private _tileHeight;
    private _tileMinSize;
    rows: List<TileRow>;
    readonly canvas: HTMLCanvasElement;
    loaded: Signal<ImageTiles>;
    protected p_initializeImage(image: HTMLImageElement | HTMLCanvasElement | HTMLVideoElement): void;
    drawToCanvas(context: CanvasRenderingContext2D, x?: number, y?: number, width?: number, height?: number): void;
}
export default ImageTiles;
