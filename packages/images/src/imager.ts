import ImagerBase from './imagerBase';


//----- Requires imageQuantization (iq.js) and blur.ts
export class Imager extends ImagerBase {
    constructor(image:(HTMLImageElement | HTMLCanvasElement | HTMLVideoElement | Blob | File | string)) {
        super(image);
    }
} // End class


export default Imager;
