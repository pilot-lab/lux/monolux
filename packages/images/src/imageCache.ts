import is from '@pilotlab/is';
import { Cache } from '@pilotlab/cache';
import Debug from '@pilotlab/debug';
import { IPromise, Result } from '@pilotlab/result';


export class ImageCache extends Cache<HTMLImageElement> {
    static images:ImageCache = new ImageCache();


    protected p_createNew(key:string, result:IPromise<HTMLImageElement>):IPromise<HTMLImageElement> {
        if (is.empty(key)) return Result.resolve<HTMLImageElement>(null);

        let image:HTMLImageElement = document.createElement('img');

        //----- IMPORTANT: These lines prevent the image element from automatically
        //----- sizing to fit the parent element when it's added to the DOM.
        image.style.maxWidth = 'none';
        image.style.maxHeight = 'none';

        image.onerror = (e:Event) => { Debug.error('Error loading image: ' + key + ', ' + e, 'ImageCache.p_createNew(...)'); };
        image.onload = (e:Event) => { result.resolve(image); };

        image.src = key;

        return result;
    }
} // End of class


export default ImageCache;
