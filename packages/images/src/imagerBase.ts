import is from '@pilotlab/is';
import { FileTools } from '@pilotlab/files';
import { Color, DataTools } from '@pilotlab/data';
import Debug from '@pilotlab/debug';
import { IPromise } from '@pilotlab/result';
import { Signal } from '@pilotlab/signals';
import Blur from './blur';
import * as IQ from './imageQuantization/iq';
import { BlurQuality } from './blur';


//----- Requires imageQuantization (iq.js) and blur.ts
export abstract class ImagerBase {
    constructor(image?:(HTMLImageElement | HTMLCanvasElement | HTMLVideoElement | Blob | File | string)) {
        if (is.empty(image)) return;
        this.p_initialize(image);
    }


    protected p_initialize(image:(HTMLImageElement | HTMLCanvasElement | HTMLVideoElement | Blob | File | string)):void {
        if (image instanceof Blob || image instanceof File) {
            FileTools.getDataUrl(<Blob>image).then((dataUrl:string) => {
                this.p_loadImage(dataUrl);
                if (is.notEmpty(this.image)) this.p_initializeImage(this.image);
            });
        } else if (typeof image === 'string') this.p_loadImage(image);
        else this.p_initializeImage(<HTMLImageElement>image);
    }


    /*====================================================================*
     START: Members and Properties
     *====================================================================*/
    get isLoaded():boolean { return this.p_isLoaded; }
    protected p_isLoaded:boolean = false;


    get width():number { return this.p_imageWidth; }
    get height():number { return this.p_imageHeight; }
    protected p_imageWidth:number;
    protected p_imageHeight:number;


    get image():HTMLImageElement { return this.p_image; }
    protected p_image:HTMLImageElement;


    get canvas():HTMLCanvasElement {
        let canvas:HTMLCanvasElement = document.createElement('canvas');
        let context:CanvasRenderingContext2D = canvas.getContext('2d');
        canvas.width = this.p_imageWidth;
        canvas.height = this.p_imageHeight;
        context.drawImage(<HTMLImageElement>this.image, 0, 0);
        return canvas;
    }


    get context():CanvasRenderingContext2D { return this.canvas.getContext('2d'); }


    get dataUrl():string { return this.canvas.toDataURL(); }


    get blob():IPromise<Blob> {
        let dataUrl:string = this.dataUrl;
        return FileTools.dataUrlToBlob(dataUrl, 'image/png');
    }


    get blurRadius():number { return this.p_blurRadius; }
    protected p_blurRadius:number = 0;


    /*====================================================================*
     START: Events
     *====================================================================*/
    loaded:Signal<ImagerBase> = new Signal<ImagerBase>(true);
    blurred:Signal<number> = new Signal<number>(true);


    /*====================================================================*
     START: Private Methods
     *====================================================================*/
    protected p_loadImage(src:string):HTMLImageElement {
        this.p_image = document.createElement('img');

        //----- IMPORTANT: These lines prevent the image element from automatically
        //----- sizing to fit the parent element when it's added to the DOM.
        this.image.style.maxWidth = 'none';
        this.image.style.maxHeight = 'none';

        this.image.onerror = (e:Event) => { Debug.error('Error loading image: ' + src + ', ' + e, 'ImageTiles._loadImage(...)'); };
        this.image.onload = (e:Event) => { this.p_initializeImage(this.image); };

        this.image.src = src;

        return this.image;
    }


    protected p_initializeImage(image:HTMLImageElement | HTMLCanvasElement | HTMLVideoElement):void {
        if (is.empty(image)) return;

        document.body.appendChild(image);

        this.p_imageWidth = image.offsetWidth;
        this.p_imageHeight = image.offsetHeight;

        this.p_isLoaded = true;
        this.loaded.dispatch(this);

        document.body.removeChild(image);
    }


    /*====================================================================*
     START: Public Methods
     *====================================================================*/
    drawToCanvas(context:CanvasRenderingContext2D, x:number = 0, y:number = 0, width:number = this.width, height:number = this.height):void {
        context.drawImage(this.image, 0, 0, this.width, this.height,  Math.ceil(x + width), Math.ceil(y + height), Math.ceil(width), Math.ceil(height));
    }


    getPalette(colorCount:number = 6):Color[] {
        //----- create pointContainer and fill it with image
        let pointContainer:IQ.utils.PointContainer = IQ.utils.PointContainer.fromHTMLImageElement(this.image);

        //----- create chosen distance calculator (see classes implementing `IQ.distance.IDistanceCalculator`)
        let distanceCalculator:IQ.distance.Euclidean = new IQ.distance.Euclidean();

        //----- create chosen palette quantizer (see classes implementing `IQ.palette.IPaletteQuantizer`)
        let paletteQuantizer:IQ.palette.RGBQuant = new IQ.palette.RGBQuant(distanceCalculator, colorCount);

        //----- feed out pointContainer filled with image to paletteQuantizer
        paletteQuantizer.sample(pointContainer);

        //----- take generated palette
        let palette:IQ.utils.Palette = paletteQuantizer.quantize();

        let colorPointContainer:IQ.utils.PointContainer = palette.getPointContainer();
        let colorPoints:IQ.utils.Point[] = colorPointContainer.getPointArray();

        let colors:Color[] = [];

        for (let i:number = 0; i < colorPoints.length; i++) {
            let colorPoint:IQ.utils.Point = colorPoints[i];
            colors[i] = new Color([colorPoint.r ,colorPoint.g, colorPoint.b, colorPoint.a]);
        }

        return colors;
    }


    getDominantColor():Color { return this.getPalette(1)[0]; }


    getAverageColor():Color {
        let context:CanvasRenderingContext2D = this.canvas.getContext('2d');
        if (is.empty(context)) return Color.empty;

        let blockSize:number = 5; // only visit every 5 pixels
        let data;
        let i:number = -4;
        let color:Color = new Color();
        let count:number = 0;

        try { data = context.getImageData(0, 0, this.width, this.height); }
        catch(e) { /* Image may be on a different domain */ return Color.empty; }

        while ( (i += blockSize * 4) < data.data.length ) {
            ++count;
            color.r += data.data[i];
            color.g += data.data[i+1];
            color.b += data.data[i+2];
        }

        //----- ~~ used to floor values
        color.r = ~~(color.r/count);
        color.g = ~~(color.g/count);
        color.b = ~~(color.b/count);

        return color;
    }


    blur(radius:number, quality:BlurQuality = BlurQuality.HIGH, isBlurAlphaChannel:boolean = true):void {
        let canvas:HTMLCanvasElement = this.canvas;
        Blur.blurCanvas(canvas, radius, quality, isBlurAlphaChannel);
        this.p_blurRadius = radius;
        this.blurred.dispatch(radius);
        this.image.src = canvas.toDataURL();
    }
} // End class


export default ImagerBase;
