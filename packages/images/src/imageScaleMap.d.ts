import { Signal } from '@pilotlab/signals';
import ImageTiles from './imageTiles';
export declare class ImageScaleMap {
    constructor(image: (HTMLImageElement | HTMLVideoElement | HTMLCanvasElement | string), scales?: number[], tileMaxSize?: number);
    protected p_getNextScale(image: (HTMLImageElement | HTMLCanvasElement | HTMLVideoElement | Blob | File | string), scales: number[], indexCurrent?: number): void;
    readonly isLoaded: boolean;
    private _isLoaded;
    private _tileMaxSize;
    private _mapCacheCanvases;
    loaded: Signal<ImageScaleMap>;
    getImage(scale: number): ImageTiles;
}
export default ImageScaleMap;
