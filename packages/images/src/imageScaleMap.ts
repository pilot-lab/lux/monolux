import is from '@pilotlab/is';
import { MapList } from '@pilotlab/collections';
import { Signal } from '@pilotlab/signals';
import ImageTiles from './imageTiles';


export class ImageScaleMap {
    constructor(
        image:(HTMLImageElement | HTMLVideoElement | HTMLCanvasElement | string),
        scales:number[] = [1.0, 0.5, 0.25, 0.125, 0.0625, 0.03125, 0.015625],
        tileMaxSize?:number
    ) {
        if (is.notEmpty(tileMaxSize)) this._tileMaxSize = tileMaxSize;

        function sortLargeToSmall(a:number, b:number) { return b - a; }
        scales.sort(sortLargeToSmall);

        this.p_getNextScale(image, scales);
    }


    protected p_getNextScale(
        image:(HTMLImageElement | HTMLCanvasElement | HTMLVideoElement | Blob | File | string),
        scales:number[],
        indexCurrent:number = 0
    ):void {
        let scale:number = scales[indexCurrent];
        let scalePrevious:number = 1;
        if (indexCurrent > 0) scalePrevious = scales[indexCurrent - 1];
        let imageTiles:ImageTiles = new ImageTiles(image, this._tileMaxSize, scale / scalePrevious);

        this._mapCacheCanvases.set(scale, imageTiles);

        indexCurrent++;
        imageTiles.loaded.listen(() => {
            if (indexCurrent === scales.length) {
                this._isLoaded = true;
                this.loaded.dispatch(this);
            } else {
                /// Use the new scaled down image to create the next image at a lower scale.
                /// This method of step-down scaling provides better image quality than generating
                /// all the smaller images from the original image.

                /// See: http://stackoverflow.com/questions/17861447/html5-canvas-drawimage-how-to-apply-antialiasing

                /// NOTE: The optimal step-down is half the resolution each step.

                /// NOTE: The following formula can be used to calculate the total number of steps required
                /// to reach a particular target resolution.
                /// steps = Math.ceil(Math.log(sourceWidth / targetWidth) / Math.log(2))

                this.p_getNextScale(imageTiles.canvas, scales, indexCurrent)
            }
        });
    }


    /*====================================================================*
     START: Properties and Fields
     *====================================================================*/
    get isLoaded():boolean { return this._isLoaded; }
    private _isLoaded:boolean = false;
    private _tileMaxSize:number = 2000;
    private _mapCacheCanvases:MapList<number, ImageTiles> = new MapList<number, ImageTiles>();


    /*====================================================================*
     START: Signals
     *====================================================================*/
    loaded:Signal<ImageScaleMap> = new Signal<ImageScaleMap>(true);


    /*====================================================================*
     START: Public Methods
     *====================================================================*/
    getImage(scale:number):ImageTiles {
        let keyTmp:number = -1;

        this._mapCacheCanvases.forEach((value:any, key:number) => {
            if (key === scale) {
                keyTmp = key;
                return false;
            } else if (keyTmp < 0) {
                keyTmp = key;
                return true;
            } else if (Math.abs(scale - key) < Math.abs(scale - keyTmp)) {
                keyTmp = key;
                return true;
            }
        });

        return this._mapCacheCanvases.get(keyTmp);
    }
} // End class


export default ImageScaleMap;
