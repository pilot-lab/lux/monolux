# commands

Command manager with undo/redo capability, as well as debounce and throttle commands.

## Install

`sudo npm install --save @pilotlab/commands`

## Usage

```
import {CommandManager, Command} from '@pilotlab/commands';
```
