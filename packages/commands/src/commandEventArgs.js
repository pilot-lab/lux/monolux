"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class CommandEventArgs {
    constructor(command, progress, returnValue, ...args) {
        this.totalRunning = 1;
        this.pending = 1;
        this.isCommandComplete = false;
        this.command = command;
        this.progress = progress;
        this.returnValue = returnValue;
        this.args = args;
    }
}
exports.CommandEventArgs = CommandEventArgs;
exports.default = CommandEventArgs;
//# sourceMappingURL=commandEventArgs.js.map