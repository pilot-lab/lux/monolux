import { IPromise } from '@pilotlab/result';
export declare class Command {
    constructor(runFunction?: (...args: any[]) => any, undoFunction?: (...args: any[]) => any, scope?: any);
    runFunction: (...args: any[]) => any;
    undoFunction: (...args: any[]) => any;
    runArgs: any[];
    undoArgs: any[];
    scope: any;
    message: string;
    readonly isEmpty: boolean;
    run(...args: any[]): IPromise<any>;
    undo(...args: any[]): IPromise<any>;
}
export default Command;
