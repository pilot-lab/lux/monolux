import is from '@pilotlab/is';
import { IPromise, Result } from '@pilotlab/result';
import Command from './command';


export class ThrottlingCommand extends Command {
    constructor(runFunction?:(...args:any[]) => any, undoFunction?:(...args:any[]) => any, scope?:any, delayInMilliseconds:number = 250) {
        super(runFunction, undoFunction, scope);
        this.delayInMilliseconds = delayInMilliseconds;
    }


    delayInMilliseconds:number;
    private _timeout:any;
    private _last:number = 0;


    /**
     * Run the command only once in the given time.
     * If the command is called multiple times within the given timeframe, only the last call will be run.
     */
    debounce(...args:any[]):IPromise<any> {
        if (is.empty(this.runFunction)) return Result.resolve<any>(null);
        if (is.notEmpty(args) && args.length > 0) this.runArgs = args;

        if (is.notEmpty(this._timeout)) clearTimeout(this._timeout);

        let returnValue:any;
        let resultReturnValue:Result<any> = new Result<any>();

        if (this.delayInMilliseconds <= 0) {
            returnValue = this.runFunction.apply(this.scope, this.runArgs);

            if (returnValue instanceof Result) returnValue.then((value:any) => resultReturnValue.resolve(value));
            else resultReturnValue.resolve(returnValue);
        }
        else this._timeout = setTimeout(() => {
            returnValue = this.runFunction.apply(this.scope, this.runArgs);

            if (returnValue instanceof Result) returnValue.then((value:any) => resultReturnValue.resolve(value));
            else resultReturnValue.resolve(returnValue);
        }, this.delayInMilliseconds);

        return resultReturnValue;
    }


    /**
     * Allow the command to be run multiple times, but not faster than the given interval.
     * Command calls will be dropped if they come in faster than the interval allows.
     * Only the last call made, or calls that come in after the interval has elapsed will
     * be recognized and run.
     */
    throttle(...args:any[]):IPromise<any> {
        if (is.empty(this.runFunction)) return Result.resolve<any>(null);
        if (is.notEmpty(args) && args.length > 0) this.runArgs = args;

        let returnValue:any;
        let resultReturnValue:Result<any> = new Result<any>();
        let now:number = +new Date;

        if (this._last > 0 && now < this._last + this.delayInMilliseconds) {
            if (is.notEmpty(this._timeout)) clearTimeout(this._timeout);
            this._timeout = setTimeout(() => {
                this._last = now;
                returnValue = this.runFunction.apply(this.scope, this.runArgs);

                if (returnValue instanceof Result) returnValue.then((value:any) => resultReturnValue.resolve(value));
                else resultReturnValue.resolve(returnValue);
            }, this.delayInMilliseconds);
        } else {
            this._last = now;
            returnValue = this.runFunction.apply(this.scope, this.runArgs);

            if (returnValue instanceof Result) returnValue.then((value:any) => resultReturnValue.resolve(value));
            else resultReturnValue.resolve(returnValue);
        }

        return resultReturnValue;
    }
} // End class


export default ThrottlingCommand;
