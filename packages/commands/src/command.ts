import is from '@pilotlab/is';
import { IPromise, Result } from '@pilotlab/result';


export class Command {
    constructor(runFunction?:(...args:any[]) => any, undoFunction?:(...args:any[]) => any, scope?:any) {
        this.runFunction = runFunction;

        if (is.notEmpty(undoFunction)) this.undoFunction = undoFunction;
        else this.undoFunction = runFunction;

        if (is.notEmpty(scope)) this.scope = scope;
    }


    /*====================================================================*
     START: Properties
     *====================================================================*/
    runFunction:(...args:any[]) => any;
    undoFunction:(...args:any[]) => any;
    runArgs:any[];
    undoArgs:any[];
    scope:any;
    message:string = '';


    get isEmpty():boolean { return is.empty(this.runFunction); }


    /*====================================================================*
     START: Public Methods
     *====================================================================*/
    run(...args:any[]):IPromise<any> {
        if (is.empty(this.runFunction)) return Result.resolve<any>(null);

        if (is.notEmpty(args) && args.length > 0) this.runArgs = args;

        let returnValue:any = this.runFunction.apply(this.scope, this.runArgs);

        if (returnValue instanceof Result) return returnValue;
        else return Result.resolve<any>(returnValue);
    }


    undo(...args:any[]):IPromise<any> {
        if (is.empty(this.undoFunction)) Result.resolve<any>(null);

        if (is.notEmpty(args) && args.length > 0) this.undoArgs = args;

        let returnValue:any = this.undoFunction.apply(this.scope, this.undoArgs);

        if (returnValue instanceof Result) return returnValue;
        else return Result.resolve<any>(returnValue);
    }
} // End class


export default Command;
