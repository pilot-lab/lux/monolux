import { IPromise } from '@pilotlab/result';
import Command from './command';
export declare class ThrottlingCommand extends Command {
    constructor(runFunction?: (...args: any[]) => any, undoFunction?: (...args: any[]) => any, scope?: any, delayInMilliseconds?: number);
    delayInMilliseconds: number;
    private _timeout;
    private _last;
    debounce(...args: any[]): IPromise<any>;
    throttle(...args: any[]): IPromise<any>;
}
export default ThrottlingCommand;
