"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const result_1 = require("@pilotlab/result");
class Command {
    constructor(runFunction, undoFunction, scope) {
        this.message = '';
        this.runFunction = runFunction;
        if (is_1.default.notEmpty(undoFunction))
            this.undoFunction = undoFunction;
        else
            this.undoFunction = runFunction;
        if (is_1.default.notEmpty(scope))
            this.scope = scope;
    }
    get isEmpty() { return is_1.default.empty(this.runFunction); }
    run(...args) {
        if (is_1.default.empty(this.runFunction))
            return result_1.Result.resolve(null);
        if (is_1.default.notEmpty(args) && args.length > 0)
            this.runArgs = args;
        let returnValue = this.runFunction.apply(this.scope, this.runArgs);
        if (returnValue instanceof result_1.Result)
            return returnValue;
        else
            return result_1.Result.resolve(returnValue);
    }
    undo(...args) {
        if (is_1.default.empty(this.undoFunction))
            result_1.Result.resolve(null);
        if (is_1.default.notEmpty(args) && args.length > 0)
            this.undoArgs = args;
        let returnValue = this.undoFunction.apply(this.scope, this.undoArgs);
        if (returnValue instanceof result_1.Result)
            return returnValue;
        else
            return result_1.Result.resolve(returnValue);
    }
}
exports.Command = Command;
exports.default = Command;
//# sourceMappingURL=command.js.map