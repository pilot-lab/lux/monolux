import is from '@pilotlab/is';
import Debug from '@pilotlab/debug';
import { Identifier } from '@pilotlab/ids';
import Result from '@pilotlab/result';
import { MapList } from '@pilotlab/collections';


export const enum HttpRequestMethod {
    GET,
    POST,
    DELETE
} // End enum


export class HttpRequestConfiguration {
    constructor(url:string, method?:HttpRequestMethod, data?:any, requestHeaders?:MapList<string, string>) {
        this.url = url;
        if (is.notEmpty(method)) this.method = method;
        this.data = data;

        if (is.notEmpty(requestHeaders)) {
            requestHeaders.forEach((key:string, value:string):boolean => {
                this.headers.set(key, value);
                return true;
            });
        }
    }


    url:string;
    method:HttpRequestMethod = HttpRequestMethod.GET;
    data:any;
    headers:MapList<string, string> = new MapList<string, string>();
    transformResponse:(value:any, result:Result<any>) => void;
    responseType:any; // This should hold a string, if it gets set correctly.
} // End class


export class Http {
    /**
     * Returns an Result with the response as a JSON object.
     * USAGE: `Http.getJson('data.json).then((json:any) => { console.log(json); });`
     */
    static getJson(url:string, data?:Object, requestHeaders?:MapList<string, string>):Result<any> {
        // let config:HttpRequestConfiguration = new HttpRequestConfiguration(url, HttpRequestMethod.GET, data, requestHeaders);
        // config.headers.set('Accept', 'application/json');
        // config.headers.set('Content-type', 'application/json; charset=utf-8');
        //
        // config.transformResponse = (response:any, result:Result<any>) => {
        //     result.resolve(JSON.parse(response));
        // };
        //
        // return Http.sendRequest<any>(config);

        let result:Result<any> = new Result<any>();
        let xmlHttp:XMLHttpRequest = new XMLHttpRequest();
        xmlHttp.overrideMimeType('application/json');
        xmlHttp.open('GET', 'position.json', true);
        xmlHttp.onreadystatechange = () => {
            if (xmlHttp.readyState == 4 && xmlHttp.status === 200) {
                /// .open will NOT return a value but simply returns undefined in asynchronous mode
                result.resolve(xmlHttp.responseText);
            }
        };
        xmlHttp.send(null);
        return result;
    }


    /**
     * Use for cross-domain calls.
     * Returns an Result with the response as a JSON object.
     */
    static getJsonP(url:string):Result<Object> {
        let resultReturn:Result<Object> = new Result<Object>();
        let callbackName:string = 'jsonp_callback_' + Identifier.generate();
        let script:HTMLScriptElement = document.createElement('script');
        script.src = url + (url.indexOf('?') >= 0 ? '&' : '?') + 'callback=' + callbackName;
        document.body.appendChild(script);

        let win:any = window;
        win.document[callbackName] = function (data:any):any {
            delete win.document[callbackName];
            document.body.removeChild(script);
            resultReturn.resolve(data);
        };

        return resultReturn;
    }


    /**
     * This is a hacky way to load a local json file when the page is being browsed from
     * the local file system, rather than served from a web server. The json file will be loaded
     * into a script tag. This requires that you add the following line to the top of the json file:
     * window['whatever-passcode-you-want']=
     * where the passcode is just a unique identifier you can use to assign the json to a property and retrieve it.
     * @param url {string} The path to the local json file.
     * @param passCode {string} The passcode contained in the window['whatever-passcode-you-want']= line at the top of the json file.
     * @returns {Result<Object>}
     */
    static getLocalJson(url:string, passCode:string):Result<any> {
        let resultReturn:Result<any> = new Result<any>();
        let win:any = window;

        try {
            let guid:string = Identifier.generate();
            let className:string = 'json_script_classname_' + guid;
            let script:HTMLScriptElement = document.createElement('script');
            script.type = 'text/javascript';
            script.onerror = () => { Debug.error('Http.getLocalJson: Error loading file.'); };
            script.async = false;
            script.className = className;

            let isDoneLoading:boolean = false;
            script.onload = function () {
                if (!isDoneLoading && (!document.readyState ||
                    document.readyState === 'loaded' || document.readyState === 'complete')) {
                    isDoneLoading = true;

                    resultReturn.resolve(win[passCode]);

                    document.head.removeChild(script);
                    delete win[passCode];
                }
            };

            document.head.appendChild(script);
            script.src = url;
        } catch (e) { Debug.error(e, 'Http.getLocalJson(...)')}

        return resultReturn;
    }


    /**
     * Returns an Result with the response as javascript text.
     */
    static getJavascript(url:string, data?:Object, requestHeaders?:MapList<string, string>):Result<any> {
        let config:HttpRequestConfiguration = new HttpRequestConfiguration(url, HttpRequestMethod.GET, data, requestHeaders);
        config.headers.set('Accept', 'text/javascript');
        config.headers.set('Content-type', 'text/javascript; charset=utf-8');
        return Http.sendRequest<any>(config);
    }


    /**
     * Returns an Result with the response as a XML object.
     */
    static getXml(url:string, data?:Object, requestHeaders?:MapList<string, string>):Result<any> {
        let config:HttpRequestConfiguration = new HttpRequestConfiguration(url, HttpRequestMethod.GET, data, requestHeaders);
        config.headers.set('Accept', 'application/xml');
        config.headers.set('Content-type', 'application/xml; charset=utf-8');
        return Http.sendRequest<any>(config);
    }


    /**
     * Use for retrieving binary files, like audio and images, where you need the
     * return value in an array buffer.
     */
    static getArrayBuffer(url:string):Result<any> {
        let config:HttpRequestConfiguration = new HttpRequestConfiguration(url, HttpRequestMethod.GET);
        config.responseType = 'arraybuffer';
        return Http.sendRequest<any>(config);
    }


    /**
     * Use for returning files as Blobs.
     */
    static getBlob(url:string):Result<Blob> {
        let config:HttpRequestConfiguration = new HttpRequestConfiguration(url);
        config.responseType = 'blob'; // Force the HTTP response, response-type header to be blob.
        return Http.sendRequest<Blob>(config);
    }


    /**
     *  Returns an Result with the response as SVG.
     */
    static getSvg(url:string, data?:Object, requestHeaders?:MapList<string, string>):Result<any> {
        let config:HttpRequestConfiguration = new HttpRequestConfiguration(url, HttpRequestMethod.GET, data, requestHeaders);
        config.headers.set('Accept', 'image/svg+xml');
        config.headers.set('Content-type', 'image/svg+xml; charset=utf-8');
        return Http.sendRequest<any>(config);
    }


    /**
     * Returns an Result with the response as a JSON object.
     */
    static sendPost(url:string, data:Object, requestHeaders?:MapList<string, string>):Result<Object> {
        let config:HttpRequestConfiguration = new HttpRequestConfiguration(url, HttpRequestMethod.POST, data, requestHeaders);
        return Http.sendRequest<any>(config);
    }


    /**
     * Base request, to support any type of call with options.
     */
    static sendRequest<T>(config:HttpRequestConfiguration):Result<T> {
        try {
            let result:Result<T> = new Result<T>();

            let xmlHttp:XMLHttpRequest = new XMLHttpRequest();
            let method:string = 'GET';
            switch (config.method) {
                case HttpRequestMethod.POST:
                    method = 'POST';
                    break;
                case HttpRequestMethod.GET:
                    method = 'GET';
                    break;
                case HttpRequestMethod.DELETE:
                    method = 'DELETE';
                    break;
                default :
                    throw new Error(config.method + ' is not implemented')
            }
            xmlHttp.open(method, config.url, true);
            if (is.notEmpty(config.responseType)) xmlHttp.responseType = config.responseType;

            if (is.notEmpty(config.headers)) {
                config.headers.forEach((key:string, value:string):boolean => {
                    xmlHttp.setRequestHeader(key, value);
                    return true;
                });
            }

            xmlHttp.onreadystatechange = () => {
                if (xmlHttp.readyState == 4 && xmlHttp.status === 200) {
                    if (is.notEmpty(config.transformResponse)) config.transformResponse(<T>xmlHttp.response, result);
                    else result.resolve(<T>xmlHttp.response);
                }
            };

            if (is.notEmpty(config.data)) {
                if (config.data instanceof ArrayBuffer || config.data instanceof Blob)
                    xmlHttp.send(config.data);
                else
                    xmlHttp.send(JSON.stringify(config.data));
            }
            else xmlHttp.send();

            return result;
        } catch (e) { Debug.error(e, 'Http._sendRequest()'); }
    }


    static formatParams(params:any, isParent:boolean = true):string {
        let prefix = isParent ? '?' : '';
        let result =
            Object
                .keys(params)
                .map(function (key) {
                    let val = params[key];
                    if (typeof val == 'object')
                        return Http.formatParams(val, false);
                    else if (typeof val == 'function')
                        return key + "=" + encodeURIComponent(val.toString());
                    else if (val == null)
                        return '';
                    else
                        return key + "=" + val;
                })
                .join("&");

        if (isParent) {
            result = result.replace(/(^\&+|\&+$)/mg, '')
        }

        return is.notEmpty(result) ? prefix + result : '';
    }
} // End class


export default Http;
