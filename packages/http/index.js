"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const debug_1 = require("@pilotlab/debug");
const ids_1 = require("@pilotlab/ids");
const result_1 = require("@pilotlab/result");
const collections_1 = require("@pilotlab/collections");
var HttpRequestMethod;
(function (HttpRequestMethod) {
    HttpRequestMethod[HttpRequestMethod["GET"] = 0] = "GET";
    HttpRequestMethod[HttpRequestMethod["POST"] = 1] = "POST";
    HttpRequestMethod[HttpRequestMethod["DELETE"] = 2] = "DELETE";
})(HttpRequestMethod = exports.HttpRequestMethod || (exports.HttpRequestMethod = {}));
class HttpRequestConfiguration {
    constructor(url, method, data, requestHeaders) {
        this.method = 0;
        this.headers = new collections_1.MapList();
        this.url = url;
        if (is_1.default.notEmpty(method))
            this.method = method;
        this.data = data;
        if (is_1.default.notEmpty(requestHeaders)) {
            requestHeaders.forEach((key, value) => {
                this.headers.set(key, value);
                return true;
            });
        }
    }
}
exports.HttpRequestConfiguration = HttpRequestConfiguration;
class Http {
    static getJson(url, data, requestHeaders) {
        let result = new result_1.default();
        let xmlHttp = new XMLHttpRequest();
        xmlHttp.overrideMimeType('application/json');
        xmlHttp.open('GET', 'position.json', true);
        xmlHttp.onreadystatechange = () => {
            if (xmlHttp.readyState == 4 && xmlHttp.status === 200) {
                result.resolve(xmlHttp.responseText);
            }
        };
        xmlHttp.send(null);
        return result;
    }
    static getJsonP(url) {
        let resultReturn = new result_1.default();
        let callbackName = 'jsonp_callback_' + ids_1.Identifier.generate();
        let script = document.createElement('script');
        script.src = url + (url.indexOf('?') >= 0 ? '&' : '?') + 'callback=' + callbackName;
        document.body.appendChild(script);
        let win = window;
        win.document[callbackName] = function (data) {
            delete win.document[callbackName];
            document.body.removeChild(script);
            resultReturn.resolve(data);
        };
        return resultReturn;
    }
    static getLocalJson(url, passCode) {
        let resultReturn = new result_1.default();
        let win = window;
        try {
            let guid = ids_1.Identifier.generate();
            let className = 'json_script_classname_' + guid;
            let script = document.createElement('script');
            script.type = 'text/javascript';
            script.onerror = () => { debug_1.default.error('Http.getLocalJson: Error loading file.'); };
            script.async = false;
            script.className = className;
            let isDoneLoading = false;
            script.onload = function () {
                if (!isDoneLoading && (!document.readyState ||
                    document.readyState === 'loaded' || document.readyState === 'complete')) {
                    isDoneLoading = true;
                    resultReturn.resolve(win[passCode]);
                    document.head.removeChild(script);
                    delete win[passCode];
                }
            };
            document.head.appendChild(script);
            script.src = url;
        }
        catch (e) {
            debug_1.default.error(e, 'Http.getLocalJson(...)');
        }
        return resultReturn;
    }
    static getJavascript(url, data, requestHeaders) {
        let config = new HttpRequestConfiguration(url, 0, data, requestHeaders);
        config.headers.set('Accept', 'text/javascript');
        config.headers.set('Content-type', 'text/javascript; charset=utf-8');
        return Http.sendRequest(config);
    }
    static getXml(url, data, requestHeaders) {
        let config = new HttpRequestConfiguration(url, 0, data, requestHeaders);
        config.headers.set('Accept', 'application/xml');
        config.headers.set('Content-type', 'application/xml; charset=utf-8');
        return Http.sendRequest(config);
    }
    static getArrayBuffer(url) {
        let config = new HttpRequestConfiguration(url, 0);
        config.responseType = 'arraybuffer';
        return Http.sendRequest(config);
    }
    static getBlob(url) {
        let config = new HttpRequestConfiguration(url);
        config.responseType = 'blob';
        return Http.sendRequest(config);
    }
    static getSvg(url, data, requestHeaders) {
        let config = new HttpRequestConfiguration(url, 0, data, requestHeaders);
        config.headers.set('Accept', 'image/svg+xml');
        config.headers.set('Content-type', 'image/svg+xml; charset=utf-8');
        return Http.sendRequest(config);
    }
    static sendPost(url, data, requestHeaders) {
        let config = new HttpRequestConfiguration(url, 1, data, requestHeaders);
        return Http.sendRequest(config);
    }
    static sendRequest(config) {
        try {
            let result = new result_1.default();
            let xmlHttp = new XMLHttpRequest();
            let method = 'GET';
            switch (config.method) {
                case 1:
                    method = 'POST';
                    break;
                case 0:
                    method = 'GET';
                    break;
                case 2:
                    method = 'DELETE';
                    break;
                default:
                    throw new Error(config.method + ' is not implemented');
            }
            xmlHttp.open(method, config.url, true);
            if (is_1.default.notEmpty(config.responseType))
                xmlHttp.responseType = config.responseType;
            if (is_1.default.notEmpty(config.headers)) {
                config.headers.forEach((key, value) => {
                    xmlHttp.setRequestHeader(key, value);
                    return true;
                });
            }
            xmlHttp.onreadystatechange = () => {
                if (xmlHttp.readyState == 4 && xmlHttp.status === 200) {
                    if (is_1.default.notEmpty(config.transformResponse))
                        config.transformResponse(xmlHttp.response, result);
                    else
                        result.resolve(xmlHttp.response);
                }
            };
            if (is_1.default.notEmpty(config.data)) {
                if (config.data instanceof ArrayBuffer || config.data instanceof Blob)
                    xmlHttp.send(config.data);
                else
                    xmlHttp.send(JSON.stringify(config.data));
            }
            else
                xmlHttp.send();
            return result;
        }
        catch (e) {
            debug_1.default.error(e, 'Http._sendRequest()');
        }
    }
    static formatParams(params, isParent = true) {
        let prefix = isParent ? '?' : '';
        let result = Object
            .keys(params)
            .map(function (key) {
            let val = params[key];
            if (typeof val == 'object')
                return Http.formatParams(val, false);
            else if (typeof val == 'function')
                return key + "=" + encodeURIComponent(val.toString());
            else if (val == null)
                return '';
            else
                return key + "=" + val;
        })
            .join("&");
        if (isParent) {
            result = result.replace(/(^\&+|\&+$)/mg, '');
        }
        return is_1.default.notEmpty(result) ? prefix + result : '';
    }
}
exports.Http = Http;
exports.default = Http;
//# sourceMappingURL=index.js.map