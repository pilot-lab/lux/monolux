# http

Ajax tools.

## Install

sudo npm install --save @pilotlab/http

## Usage

```
import {Http} from '@pilotlab/http';

Http.getBlob(blobUrl).then((blob:Blob) => {
    /// Do something with the blob.
});
```
