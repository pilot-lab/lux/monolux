import Result from '@pilotlab/result';
import { MapList } from '@pilotlab/collections';
export declare const enum HttpRequestMethod {
    GET = 0,
    POST = 1,
    DELETE = 2,
}
export declare class HttpRequestConfiguration {
    constructor(url: string, method?: HttpRequestMethod, data?: any, requestHeaders?: MapList<string, string>);
    url: string;
    method: HttpRequestMethod;
    data: any;
    headers: MapList<string, string>;
    transformResponse: (value: any, result: Result<any>) => void;
    responseType: any;
}
export declare class Http {
    static getJson(url: string, data?: Object, requestHeaders?: MapList<string, string>): Result<any>;
    static getJsonP(url: string): Result<Object>;
    static getLocalJson(url: string, passCode: string): Result<any>;
    static getJavascript(url: string, data?: Object, requestHeaders?: MapList<string, string>): Result<any>;
    static getXml(url: string, data?: Object, requestHeaders?: MapList<string, string>): Result<any>;
    static getArrayBuffer(url: string): Result<any>;
    static getBlob(url: string): Result<Blob>;
    static getSvg(url: string, data?: Object, requestHeaders?: MapList<string, string>): Result<any>;
    static sendPost(url: string, data: Object, requestHeaders?: MapList<string, string>): Result<Object>;
    static sendRequest<T>(config: HttpRequestConfiguration): Result<T>;
    static formatParams(params: any, isParent?: boolean): string;
}
export default Http;
