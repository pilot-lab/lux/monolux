export declare class Injector {
    private static _registry;
    static get(key: string): any;
    static set(key: string, value: any): void;
}
export declare function injectProperty(...keys: string[]): (target: any, key: string) => void;
export declare function inject(...keys: string[]): (...args: any[]) => any;
export default Injector;
