/**
 * See:
 *
 * https://github.com/GermainBergeron/dose
 *
 * and article at http://source.coveo.com/2016/02/04/typescript-injection-decorator/
 */
export class Injector {
    private static _registry:{[key:string]:any} = {};
    static get(key:string) { return Injector._registry[key]; }
    static set(key:string, value:any) { Injector._registry[key] = value; }
} // End class


/* Injection functions */
function injectMethod(...keys:string[]) {
    return (target:any, key:string, descriptor:any) => {
        let originalMethod = descriptor.value;

        descriptor.value = function (...args:any[]) {
            let add = keys.map((key:string) => Injector.get(key));
            args = args.concat(add);

            return originalMethod.apply(this, args);
        };

        return descriptor;
    };
} // End function


export function injectProperty(...keys:string[]) {
    return (target:any, key:string) => {
        target[key] = Injector.get(keys[0]);
    };
} // End function


export function inject(...keys:string[]) {
    return (...args:any[]) => {
        let params = [];

        for (let i = 0; i < args.length; i++) {
            args[i] ? params.push(args[i]) : null
        }

        switch (params.length) {
            case 2:
                return injectProperty(keys[0]).apply(this, args);
            case 3:
                return injectMethod(...keys).apply(this, args);
            default:
                throw new Error('Decorators are not valid here!');
        }
    };
} // End function


export default Injector;
