import IPromise from './src/iPromise';
import Result from './src/result';
import ResultHandler from './src/resultHandler';


export {
    IPromise,
    Result,
    ResultHandler
}


export default Result;
