import is from '@pilotlab/is';
import Result from './result';


export class ResultHandler<TValue> {
    constructor(onDone:(value:TValue) => any) {
        this.onDone = onDone;
        this.result = new Result<any>();
    }


    onDone:(value:TValue) => any;
    result:Result<any>;


    handle(value:TValue):void {
        let returnValue:any = this.onDone(value);
        if (returnValue instanceof Result) {
            returnValue.then((value:any) => {
                if (is.notEmpty(this.result)) this.result.resolve(value);
            });
        } else if (is.notEmpty(this.result)) this.result.resolve(returnValue);
    }
} // End class


export default ResultHandler;
