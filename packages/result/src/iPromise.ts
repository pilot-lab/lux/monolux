export interface IPromise<TValue> extends PromiseLike<TValue> {
    /**
     * Attaches callbacks for the resolution and/or rejection of the Promise.
     * @param onResolve The callback to execute when the Promise is resolved.
     * @param onReject The callback to execute when the Promise is rejected.
     * @returns A Promise for the completion of which ever callback is executed.
     */
    then<TResult>(onResolve?:(value:TValue) => TResult | PromiseLike<TResult>, onReject?:(reason: any) => void | TResult | PromiseLike<TResult>):PromiseLike<TResult>;
}


export default IPromise;
