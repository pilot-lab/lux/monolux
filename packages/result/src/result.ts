import is from '@pilotlab/is';
import ResultHandler from './resultHandler';
import { IPromise } from './iPromise';


/**
 * Result class for simplified async communication
 */
export class Result<TValue> implements IPromise<TValue> {
    constructor() {
        if (arguments.length > 0 && is.notEmpty(arguments[0])) this.debugInfo = arguments[0];

        //* Initialize properties
        this.isHandled = false;
        this._handlers = [];
        this._errorHandlers = [];
        this._error = null;
        this._isCompleted = false;
    }


    /*====================================================================*
     START: Properties
     *====================================================================*/
    isHandled:boolean;


    get isCompleted():boolean { return this._isCompleted; }


    get value():TValue { return this._value; }
    set value(value:TValue) { this._value = value; }
    _value:TValue;


    _handlers:ResultHandler<TValue>[];
    _errorHandlers:{ (error:any): void; }[];
    _error:any;
    _isCompleted:boolean;


    debugInfo:string;


    /*====================================================================*
     START: Public Methods
     *====================================================================*/
    resolve(value?:TValue):IPromise<TValue> {
        if (this._isCompleted) return this;
        this._isCompleted = true;

        this._value = value;
        this._handleComplete();

        return this;
    }


    reject(error?:any):IPromise<TValue> {
        if (this._isCompleted) return;
        this._isCompleted = true;

        let errorFinal:Error = typeof error === 'string' ? new Error(error) : error;

        this._error = (errorFinal || new Error('Unspecified error'));
        this._handleError();

        return this;
    }


    then<TResult>(onResolve?:(value:TValue) => TResult | PromiseLike<TResult>, onReject?:(error:any) => void | TResult | PromiseLike<TResult>) {
        let handler:ResultHandler<TValue> = new ResultHandler<TValue>(onResolve);

        if (this._isCompleted) {
            if (is.notEmpty(this._error) && is.notEmpty(onReject)) onReject(this._error);
            else handler.handle(this.value);
        } else {
            if (is.notEmpty(onResolve)) this._handlers.push(handler);
            if (is.notEmpty(onReject) )this._errorHandlers.push(onReject);
        }

        return handler.result;
    }


    catch(onReject:(error:any) => void):IPromise<TValue> {
        if (is.notEmpty(onReject)) this._errorHandlers.push(onReject);
        return this;
    }


    all(results:IPromise<TValue>[]):IPromise<TValue[]> { return this._all(results, false); }
    race(results:IPromise<TValue>[]):IPromise<TValue> { return this._all(results, true); }


    /**
     * Wait for all the given results to complete and collect their values.
     *
     * Returns a result which will complete once all the results in a list are
     * complete. If any of the results in the list completes with an error,
     * the returned result also completes with an error. Otherwise the value
     * of the returned result will be a list of all the values that were produced.
     *
     * If `isRace` is true, the result completes with an error immediately on
     * the first error from one of the results. Otherwise all results must
     * complete before the returned result is completed (still with the first
     * error to occur, the remaining errors are silently dropped).
     */
    private _all(results:IPromise<TValue>[], isRace:boolean = false):IPromise<any> {
        let returnResult:Result<any[]> = new Result<any[]>();
        let values:any[] = [];  // Collects the values. Set to null on error.
        let total:number;
        let remaining:number = 0;  // How many results are we waiting for.
        let error:Error;   // The first error from a result.

        /// Handle an error from any of the results.
        let handleError = (theError:Error):void => {
            let isFirstError:boolean = (is.notEmpty(values));
            values = null;
            remaining--;
            if (isFirstError) {
                if (remaining === 0 || isRace) returnResult.reject(theError);
                else error = theError;
            } else if (remaining === 0 && !isRace) returnResult.reject(error);
        };

        /// As each result completes, put its value into the corresponding
        /// position in the list of values.
        remaining = results.length;
        total = results.length;
        results.forEach((result:IPromise<any>):boolean => {
            if (is.empty(result)) return true;

            result.then((value:any):any => {
                remaining--;

                if (is.notEmpty(values)) {
                    if (isRace && values.length === 0) {
                        returnResult.resolve(value);
                    } else {
                        values.push(value);
                        if (remaining === 0) returnResult.resolve(values);
                    }
                } else if (remaining === 0 && !isRace) returnResult.reject(error);

                return value;
            }, handleError);

            return true;
        });

        if (total === 0) returnResult.resolve(values);

        return returnResult;
    }


    /*====================================================================*
     START: Private Methods
     *====================================================================*/
    _handleComplete() {
        for (let i = 0; i < this._handlers.length; i++) {
            this._handlers[i].handle(this.value);
        }
    }


    _handleError():void {
        if (is.empty(this._error)) return;
        if (this._errorHandlers.length === 0) console.log(this._error, 'Result._handleError()');

        for (let i = 0; i < this._errorHandlers.length; i++) {
            try { this._errorHandlers[i](this._error); }
            catch (e) { console.log(e, 'Result._handleError()'); }
        }
    }


    /*====================================================================*
     START: Static Methods
     *====================================================================*/
    static reject(error?:any):IPromise<any> {
        let result:Result<any> = new Result<any>();
        let errorFinal:Error = typeof error === 'string' ? new Error(error) : error;
        result.reject(errorFinal || new Error('Unspecified error'));
        return result;
    }


    static resolve<TValue>(value?:TValue):IPromise<TValue> {
        let result:Result<TValue> = new Result<TValue>();
        result.resolve(value);
        return result;
    }


    static all<TValue>(results:IPromise<TValue>[]):IPromise<TValue[]> {
        let result:Result<TValue> = new Result<TValue>();
        return result.all(results);
    }


    static race<TValue>(results:IPromise<TValue>[]):IPromise<TValue> {
        let result:Result<TValue> = new Result<TValue>();
        return result.race(results);
    }
} // End class


export default Result;
