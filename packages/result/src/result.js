"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const resultHandler_1 = require("./resultHandler");
class Result {
    constructor() {
        if (arguments.length > 0 && is_1.default.notEmpty(arguments[0]))
            this.debugInfo = arguments[0];
        this.isHandled = false;
        this._handlers = [];
        this._errorHandlers = [];
        this._error = null;
        this._isCompleted = false;
    }
    get isCompleted() { return this._isCompleted; }
    get value() { return this._value; }
    set value(value) { this._value = value; }
    resolve(value) {
        if (this._isCompleted)
            return this;
        this._isCompleted = true;
        this._value = value;
        this._handleComplete();
        return this;
    }
    reject(error) {
        if (this._isCompleted)
            return;
        this._isCompleted = true;
        let errorFinal = typeof error === 'string' ? new Error(error) : error;
        this._error = (errorFinal || new Error('Unspecified error'));
        this._handleError();
        return this;
    }
    then(onResolve, onReject) {
        let handler = new resultHandler_1.default(onResolve);
        if (this._isCompleted) {
            if (is_1.default.notEmpty(this._error) && is_1.default.notEmpty(onReject))
                onReject(this._error);
            else
                handler.handle(this.value);
        }
        else {
            if (is_1.default.notEmpty(onResolve))
                this._handlers.push(handler);
            if (is_1.default.notEmpty(onReject))
                this._errorHandlers.push(onReject);
        }
        return handler.result;
    }
    catch(onReject) {
        if (is_1.default.notEmpty(onReject))
            this._errorHandlers.push(onReject);
        return this;
    }
    all(results) { return this._all(results, false); }
    race(results) { return this._all(results, true); }
    _all(results, isRace = false) {
        let returnResult = new Result();
        let values = [];
        let total;
        let remaining = 0;
        let error;
        let handleError = (theError) => {
            let isFirstError = (is_1.default.notEmpty(values));
            values = null;
            remaining--;
            if (isFirstError) {
                if (remaining === 0 || isRace)
                    returnResult.reject(theError);
                else
                    error = theError;
            }
            else if (remaining === 0 && !isRace)
                returnResult.reject(error);
        };
        remaining = results.length;
        total = results.length;
        results.forEach((result) => {
            if (is_1.default.empty(result))
                return true;
            result.then((value) => {
                remaining--;
                if (is_1.default.notEmpty(values)) {
                    if (isRace && values.length === 0) {
                        returnResult.resolve(value);
                    }
                    else {
                        values.push(value);
                        if (remaining === 0)
                            returnResult.resolve(values);
                    }
                }
                else if (remaining === 0 && !isRace)
                    returnResult.reject(error);
                return value;
            }, handleError);
            return true;
        });
        if (total === 0)
            returnResult.resolve(values);
        return returnResult;
    }
    _handleComplete() {
        for (let i = 0; i < this._handlers.length; i++) {
            this._handlers[i].handle(this.value);
        }
    }
    _handleError() {
        if (is_1.default.empty(this._error))
            return;
        if (this._errorHandlers.length === 0)
            console.log(this._error, 'Result._handleError()');
        for (let i = 0; i < this._errorHandlers.length; i++) {
            try {
                this._errorHandlers[i](this._error);
            }
            catch (e) {
                console.log(e, 'Result._handleError()');
            }
        }
    }
    static reject(error) {
        let result = new Result();
        let errorFinal = typeof error === 'string' ? new Error(error) : error;
        result.reject(errorFinal || new Error('Unspecified error'));
        return result;
    }
    static resolve(value) {
        let result = new Result();
        result.resolve(value);
        return result;
    }
    static all(results) {
        let result = new Result();
        return result.all(results);
    }
    static race(results) {
        let result = new Result();
        return result.race(results);
    }
}
exports.Result = Result;
exports.default = Result;
//# sourceMappingURL=result.js.map