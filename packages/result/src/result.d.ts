import ResultHandler from './resultHandler';
import { IPromise } from './iPromise';
export declare class Result<TValue> implements IPromise<TValue> {
    constructor();
    isHandled: boolean;
    readonly isCompleted: boolean;
    value: TValue;
    _value: TValue;
    _handlers: ResultHandler<TValue>[];
    _errorHandlers: {
        (error: any): void;
    }[];
    _error: any;
    _isCompleted: boolean;
    debugInfo: string;
    resolve(value?: TValue): IPromise<TValue>;
    reject(error?: any): IPromise<TValue>;
    then<TResult>(onResolve?: (value: TValue) => TResult | PromiseLike<TResult>, onReject?: (error: any) => void | TResult | PromiseLike<TResult>): Result<any>;
    catch(onReject: (error: any) => void): IPromise<TValue>;
    all(results: IPromise<TValue>[]): IPromise<TValue[]>;
    race(results: IPromise<TValue>[]): IPromise<TValue>;
    private _all(results, isRace?);
    _handleComplete(): void;
    _handleError(): void;
    static reject(error?: any): IPromise<any>;
    static resolve<TValue>(value?: TValue): IPromise<TValue>;
    static all<TValue>(results: IPromise<TValue>[]): IPromise<TValue[]>;
    static race<TValue>(results: IPromise<TValue>[]): IPromise<TValue>;
}
export default Result;
