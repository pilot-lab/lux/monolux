"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const result_1 = require("./result");
class ResultHandler {
    constructor(onDone) {
        this.onDone = onDone;
        this.result = new result_1.default();
    }
    handle(value) {
        let returnValue = this.onDone(value);
        if (returnValue instanceof result_1.default) {
            returnValue.then((value) => {
                if (is_1.default.notEmpty(this.result))
                    this.result.resolve(value);
            });
        }
        else if (is_1.default.notEmpty(this.result))
            this.result.resolve(returnValue);
    }
}
exports.ResultHandler = ResultHandler;
exports.default = ResultHandler;
//# sourceMappingURL=resultHandler.js.map