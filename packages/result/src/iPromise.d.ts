export interface IPromise<TValue> extends PromiseLike<TValue> {
    then<TResult>(onResolve?: (value: TValue) => TResult | PromiseLike<TResult>, onReject?: (reason: any) => void | TResult | PromiseLike<TResult>): PromiseLike<TResult>;
}
export default IPromise;
