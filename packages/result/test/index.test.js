"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const alsatian_1 = require("alsatian");
const index_1 = require("../index");
let Tests = class Tests {
    test01(testValue) {
        const result = new index_1.Result();
        result.then((value) => {
            alsatian_1.Expect(value).toBe(testValue);
        });
        result.resolve(testValue);
        const promise = result;
    }
    test02(testValue) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = new index_1.Result();
            setTimeout(() => {
                console.log('done!');
                result.resolve(null);
            }, 3000);
            const promiselike = result;
            yield promiselike;
            console.log('The waiting is over!');
        });
    }
    test03(testValues) {
        const results = [];
        for (let i = 0; i < testValues.length; i++) {
            const result = new index_1.Result();
            results.push(result);
        }
        index_1.Result.all(results).then((values) => {
            alsatian_1.Expect(values.length).toBe(testValues.length);
        });
        for (let i = 0; i < results.length; i++) {
            results[i].resolve(testValues[i]);
        }
    }
};
__decorate([
    alsatian_1.TestCase(1),
    alsatian_1.TestCase(2),
    alsatian_1.TestCase(3),
    alsatian_1.Test('Complete with value')
], Tests.prototype, "test01", null);
__decorate([
    alsatian_1.TestCase(null),
    alsatian_1.AsyncTest("Retrieving data"),
    alsatian_1.Timeout(5000)
], Tests.prototype, "test02", null);
__decorate([
    alsatian_1.TestCase([1, 2, 3]),
    alsatian_1.TestCase(['a', 'b', 'c']),
    alsatian_1.TestCase([1, 'a', 1, 'b']),
    alsatian_1.Test('Complete all')
], Tests.prototype, "test03", null);
Tests = __decorate([
    alsatian_1.TestFixture('Result Tests')
], Tests);
exports.Tests = Tests;
//# sourceMappingURL=index.test.js.map