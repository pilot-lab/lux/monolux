import {
    AsyncTest,
    Setup,
    SetupFixture,
    Teardown,
    TeardownFixture,
    Test,
    TestCase,
    TestFixture,
    Timeout,
    Expect
} from 'alsatian';
import { IPromise, Result } from '../index';


@TestFixture('Result Tests')
export class Tests {
    @TestCase(1)
    @TestCase(2)
    @TestCase(3)
    @Test('Complete with value')
    test01(testValue: number) {
        const result = new Result<number>();

        result.then((value: number) => {
            Expect(value).toBe(testValue);
        });

        result.resolve(testValue);

        const promise:PromiseLike<number> = <IPromise<number>>result;
    }

    @TestCase(null)
    @AsyncTest("Retrieving data")
    @Timeout(5000) // Alsatian will now wait 5 seconds before failing
    async test02(testValue:any) {
        const result:Result<any> = new Result<any>();

        setTimeout(() => {
            console.log('done!');
            result.resolve(null);
        }, 3000);

        const promiselike:IPromise<any> = result;
        await promiselike;

        console.log('The waiting is over!');
    }


    @TestCase([1, 2, 3])
    @TestCase(['a', 'b', 'c'])
    @TestCase([1, 'a', 1, 'b'])
    @Test('Complete all')
    test03(testValues: any[]) {
        const results: Result<any>[] = [];

        for (let i = 0; i < testValues.length; i++) {
            const result = new Result<any>();
            results.push(result);
        }

        Result.all(results).then((values: any[]) => {
            Expect(values.length).toBe(testValues.length);
        });

        for (let i = 0; i < results.length; i++) {
            results[i].resolve(testValues[i]);
        }
    }
}
