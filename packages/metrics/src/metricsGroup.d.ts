import IMetrics from "./interfaces/iMetrics";
import IMetricsGroup from "./interfaces/iMetricsGroup";
export declare class MetricsGroup implements IMetricsGroup {
    constructor(element: IMetrics, key?: number);
    readonly min: number[];
    private _min;
    readonly max: number[];
    private _max;
    readonly elements: IMetrics[];
    private _elements;
    _key: number;
    readonly key: number;
    readonly size: number;
    readonly center: number[];
    add(element: IMetrics): this;
    isNear(values: number[], thresholdDistance?: number): boolean;
}
export default MetricsGroup;
