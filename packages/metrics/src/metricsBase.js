"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const signals_1 = require("@pilotlab/signals");
const metrics_1 = require("./metrics");
class MetricsBase {
    constructor(...values) {
        this._changed = new signals_1.Signal();
        this._isSignalChanged = false;
        this._setSchema();
        let valuesFinal = [];
        let length = this._schema.length;
        for (let i = 0; i < length; i++) {
            valuesFinal[i] = 0;
        }
        if (values && values.length > 0) {
            length = (values.length <= this._schema.length) ? values.length : this._schema.length;
            for (let i = 0; i < length; i++) {
                valuesFinal[i] = values[i];
            }
        }
        this._array = valuesFinal;
        this._isForceChanges = true;
    }
    get schema() { return this._schema; }
    _setSchema() { this._schema = []; }
    get array() { return this._array; }
    get isEmpty() { return true; }
    get clone() { return this; }
    get magnitude() { return metrics_1.default.magnitude(this.array); }
    get magnitudeSquared() { return metrics_1.default.magnitudeSquared(this.array); }
    get abs() { metrics_1.default.to(this, metrics_1.default.abs(this.array)); return this; }
    get set() { return this.internalSet; }
    get isSignalChanged() { return this._isSignalChanged; }
    set isSignalChanged(value) { this._isSignalChanged = value; }
    get changed() { return this._changed; }
    _onChanged() {
        if (!this._isSignalChanged)
            return;
        this._changed.dispatch(this);
    }
    to(values) { metrics_1.default.to(this, values); return this; }
    equals(values) { return metrics_1.default.equals(this.array, values); }
    add(values) {
        const c = metrics_1.default.add(this.array, values);
        if (this.equals(c))
            return this;
        metrics_1.default.to(this, c);
        this._onChanged();
        return this;
    }
    addNumber(value) {
        if (value === 0)
            return this;
        metrics_1.default.to(this, metrics_1.default.addNumber(this.array, value));
        this._onChanged();
        return this;
    }
    subtract(values) {
        const c = metrics_1.default.subtract(this.array, values);
        if (this.equals(c))
            return this;
        metrics_1.default.to(this, c);
        this._onChanged();
        return this;
    }
    subtractNumber(value) {
        if (value === 0)
            return this;
        metrics_1.default.to(this, metrics_1.default.subtractNumber(this.array, value));
        this._onChanged();
        return this;
    }
    multiply(values) {
        const c = metrics_1.default.multiply(this.array, values);
        if (this.equals(c))
            return this;
        metrics_1.default.to(this, c);
        this._onChanged();
        return this;
    }
    multiplyNumber(value) {
        if (value === 1)
            return this;
        metrics_1.default.to(this, metrics_1.default.multiplyNumber(this.array, value));
        this._onChanged();
        return this;
    }
    divide(values) {
        const c = metrics_1.default.divide(this.array, values);
        if (this.equals(c))
            return this;
        metrics_1.default.to(this, c);
        this._onChanged();
        return this;
    }
    divideNumber(value) {
        if (value === 0 || value === 1)
            return this;
        metrics_1.default.to(this, metrics_1.default.divideNumber(this.array, value));
        this._onChanged();
        return this;
    }
    dot(values) { return metrics_1.default.dot(this.array, values); }
    distanceSquared(values) { return metrics_1.default.distanceSquared(this.array, values); }
    distance(values) { return metrics_1.default.distance(this.array, values); }
    min(values) {
        const c = metrics_1.default.min(this.array, values, true);
        if (this.equals(c))
            return this;
        metrics_1.default.to(this, c);
        this._onChanged();
        return this;
    }
    max(values) {
        const c = metrics_1.default.max(this.array, values, true);
        if (this.equals(c))
            return this;
        metrics_1.default.to(this, c);
        this._onChanged();
        return this;
    }
    clamp(min, max) {
        const c = metrics_1.default.clamp(this.array, min, max, true);
        if (this.equals(c))
            return this;
        metrics_1.default.to(this, c);
        this._onChanged();
        return this;
    }
    _setValues(indices, values) {
        let isChanged = false;
        for (let i = 0, length = indices.length; i < length; i++) {
            if (this._isForceChanges || this._array[indices[i]] !== values[i])
                isChanged = true;
            this._array[indices[i]] = values[i];
        }
        if (isChanged)
            this._onChanged();
    }
}
exports.MetricsBase = MetricsBase;
exports.default = MetricsBase;
//# sourceMappingURL=metricsBase.js.map