"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const animation_1 = require("@pilotlab/animation");
const is_1 = require("@pilotlab/is");
class Metrics {
    static get tolerance() { return 0.0000001; }
    static to(instance, values) {
        const length = values.length;
        for (let i = 0; i < length; i++) {
            instance.array[i] = values[i];
        }
        return instance;
    }
    static equals(a, b, tolerance) {
        if (!tolerance)
            tolerance = Metrics.tolerance;
        for (let i = 0, len = a.length; i < len; i++) {
            if (Math.abs(a[i] - b[i]) > tolerance)
                return false;
        }
        return true;
    }
    static getArray(args, schema) {
        if (args.length < 1)
            return [];
        let pos = [];
        let isArray = Array.isArray(args[0]) || ArrayBuffer.isView(args[0]);
        if (typeof args[0] === "number") {
            pos = Array.prototype.slice.call(args);
        }
        else if (typeof args[0] === "object" && !isArray) {
            let a = schema;
            let p = args[0];
            for (let i = 0; i < a.length; i++) {
                if ((p.length && i >= p.length) || !(a[i] in p))
                    break;
                pos.push(p[a[i]]);
            }
        }
        else if (isArray)
            pos = [].slice.call(args[0]);
        return pos;
    }
    static magnitudeSquared(value) { return Metrics.dot(value, value); }
    static magnitude(value) { return Math.sqrt(Metrics.dot(value, value)); }
    static abs(value) {
        const length = value.length;
        for (let i = 0; i < length; i++) {
            value[i] = Math.abs(value[i]);
        }
        return value;
    }
    static add(a, b, isModifyA = true) {
        const length = b.length > a.length ? a.length : b.length;
        const c = isModifyA ? a : new Array(length);
        for (let i = 0; i < length; i++) {
            c[i] = (a[i] + b[i]);
        }
        return c;
    }
    static addNumber(a, value, isModifyA = true) {
        const length = a.length;
        const c = isModifyA ? a : new Array(length);
        for (let i = 0; i < length; i++) {
            c[i] = (a[i] + value);
        }
        return c;
    }
    static subtract(a, b, isModifyA = true) {
        const length = b.length > a.length ? a.length : b.length;
        const c = isModifyA ? a : new Array(length);
        for (let i = 0; i < length; i++) {
            c[i] = (a[i] - b[i]);
        }
        return c;
    }
    static difference(a, b, isModifyA = true) {
        const length = b.length > a.length ? a.length : b.length;
        const c = isModifyA ? a : new Array(length);
        for (let i = 0; i < length; i++) {
            c[i] = Math.abs(a[i] - b[i]);
        }
        return c;
    }
    static subtractNumber(a, value, isModifyA = true) {
        const length = a.length;
        const c = isModifyA ? a : new Array(length);
        for (let i = 0; i < length; i++) {
            c[i] = (a[i] - value);
        }
        return c;
    }
    static multiply(a, b, isModifyA = true) {
        const length = b.length > a.length ? a.length : b.length;
        const c = isModifyA ? a : new Array(length);
        for (let i = 0; i < length; i++) {
            if (c[i] !== 1)
                c[i] = (a[i] * b[i]);
        }
        return c;
    }
    static multiplyNumber(a, value, isModifyA = true) {
        const length = a.length;
        const c = isModifyA ? a : new Array(length);
        for (let i = 0; i < length; i++) {
            c[i] = (a[i] * value);
        }
        return c;
    }
    static divide(a, b, isModifyA = true) {
        const length = b.length > a.length ? a.length : b.length;
        const c = isModifyA ? a : new Array(length);
        for (let i = 0; i < length; i++) {
            if (b[i] !== 0 && b[i] !== 1)
                c[i] = (a[i] / b[i]);
        }
        return c;
    }
    static divideNumber(a, value, isModifyA = true) {
        const length = a.length;
        const c = isModifyA ? a : new Array(length);
        for (let i = 0; i < length; i++) {
            if (value !== 0 && value !== 1)
                c[i] = (a[i] / value);
        }
        return c;
    }
    static dot(a, b) {
        let dotValue = 0.0;
        const length = b.length > a.length ? a.length : b.length;
        for (let i = 0; i < length; i++)
            dotValue += a[i] * b[i];
        return dotValue;
    }
    static distanceSquared(a, b) {
        const difference = Metrics.subtract(a, b);
        return Metrics.magnitudeSquared(difference);
    }
    static distance(a, b) { return Math.sqrt(Metrics.distanceSquared(a, b)); }
    static min(a, b, isModifyA = true) {
        const length = b.length > a.length ? a.length : b.length;
        const c = isModifyA ? a : new Array(length);
        for (let i = 0; i < length; i++) {
            c[i] = Math.min(a[i], b[i]);
        }
        return c;
    }
    static max(a, b, isModifyA = true) {
        const length = b.length > a.length ? a.length : b.length;
        const c = isModifyA ? a : new Array(length);
        for (let i = 0; i < length; i++) {
            c[i] = Math.max(a[i], b[i]);
        }
        return c;
    }
    static clamp(values, min, max, isModifyValues = true) {
        let length = values.length > max.length ? max.length : values.length;
        const c = isModifyValues ? values : new Array(length);
        for (let i = 0; i < length; i++) {
            c[i] = Math.min(values[i], max[i]);
        }
        length = c.length > min.length ? min.length : c.length;
        for (let i = 0; i < length; i++) {
            c[i] = Math.max(c[i], min[i]);
        }
        return c;
    }
    static largestValue(values) {
        if (values.length < 1)
            return 0;
        const length = values.length;
        let value = values[0];
        for (let i = 1; i < length; i++) {
            value = Math.max(value, values[i]);
        }
        return value;
    }
    static smallestValue(values) {
        if (values.length < 1)
            return 0;
        const length = values.length;
        let value = values[0];
        for (let i = 1; i < length; i++) {
            value = Math.min(value, values[i]);
        }
        return value;
    }
    static sortDownwardLeftToRight(a, b) {
        return a.y - b.y || a.x - b.x;
    }
    static sortRightwardTopToBottom(a, b) {
        return a.x - b.x || a.y - b.y;
    }
    static go(start, target, durationSpeed, ease, repeatCount = 0, animationKey) {
        let duration = 0;
        if (is_1.default.notEmpty(durationSpeed) && typeof durationSpeed === 'number' && durationSpeed >= 0) {
            duration = durationSpeed;
        }
        else if (durationSpeed instanceof animation_1.Speed && durationSpeed.type === animation_1.SpeedType.DURATION) {
            duration = durationSpeed.duration;
        }
        else {
            let difference = this.largestValue(this.difference(start, target, false));
            duration = animation_1.Animation.getDuration(durationSpeed, 0, difference);
        }
        if (is_1.default.empty(ease))
            ease = animation_1.Animation.validateSpeed(durationSpeed).ease;
        let length = start.length < target.length ? start.length : target.length;
        let animationValues = [];
        for (let i = 0; i < length; i++) {
            animationValues.push(new animation_1.AnimationValue(start[i], target[i]));
        }
        let animation = new animation_1.Animation(animationValues, duration, ease, repeatCount, animationKey);
        animation.ticked.listen(() => {
            for (let i = 0; i < length; i++) {
                start[i] = animation.values[i].current;
            }
        });
        animation.completed.listenOnce(() => {
            for (let i = 0; i < length; i++) {
                start[i] = target[i];
            }
        });
        animation_1.Animation.goAnimation(animation);
        return animation;
    }
}
exports.default = Metrics;
//# sourceMappingURL=metrics.js.map