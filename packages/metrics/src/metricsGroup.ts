import Metrics from "./metrics";
import IMetrics from "./interfaces/iMetrics";
import IMetricsGroup from "./interfaces/iMetricsGroup";


export class MetricsGroup implements IMetricsGroup {
    constructor(element:IMetrics, key?:number) {
        this._key = key;

        this._min = element.array;
        this._max = element.array;

        element.internalSet = this;
        this._elements = [];
        this._elements.push(element);
    }


    get min():number[] { return this._min; }
    private _min:number[];


    get max():number[] { return this._max; }
    private _max:number[];


    get elements():IMetrics[] { return this._elements; }
    private _elements:IMetrics[];


    _key:number;
    get key() { return this._key; }


    get size():number { return Metrics.distanceSquared(this._min, this._max); }



    get center():number[] {
        let c:number[] = [];
        const length:number = this._min.length > this._max.length ? this._max.length : this._min.length;
        for (let i = 0; i < length; i++) {
            c[i] = ((this._max[i] - this._min[i]) / 2) + this._min[i];
        }
        return c;
    }


    add(element:IMetrics):this {
        this._min = Metrics.min(this._min, element.array, false);
        this._max = Metrics.max(this._max, element.array, false);
        element.internalSet = this;
        this._elements.push(element);
        return this;
    }


    isNear(values:number[], thresholdDistance:number = 20):boolean {
        let valuesClamped:number[] = Metrics.clamp(values, this._min, this._max, false);
        let distance:number = Metrics.distance(valuesClamped, values);
        return distance < thresholdDistance;
    }
}


export default MetricsGroup;
