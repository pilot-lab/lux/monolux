import { Signal } from "@pilotlab/signals";
import Metrics from "./metrics";
import IMetrics from "./interfaces/iMetrics";
import IMetricsGroup from "./interfaces/iMetricsGroup";


export abstract class MetricsBase implements IMetrics {
    protected constructor(...values:number[]) {
        this._isSignalChanged = false;
        this._setSchema();

        let valuesFinal:number[] = [];
        let length:number = this._schema.length;
        for (let i = 0; i < length; i++) {
            valuesFinal[i] = 0;
        }

        if (values && values.length > 0) {
            length = (values.length <= this._schema.length) ? values.length : this._schema.length;
            for (let i = 0; i < length; i++) {
                valuesFinal[i] = values[i];
            }
        }

        this._array = valuesFinal;
        this._isForceChanges = true;
    }


    /**
     * Should we signal a changed event, even when newly assigned
     * values are the same as the previous values?
     */
    protected _isForceChanges:boolean;


    protected _schema:string[];
    get schema():string[] { return this._schema; }
    protected _setSchema():void { this._schema = []; }


    protected readonly _array:number[];
    get array():number[] { return this._array; }


    get isEmpty():boolean { return true; }
    get clone():IMetrics { return this; }


    get magnitude():number { return Metrics.magnitude(this.array); }
    get magnitudeSquared():number { return Metrics.magnitudeSquared(this.array); }
    get abs():this { Metrics.to(this, Metrics.abs(this.array)); return this; }


    internalSet:IMetricsGroup;
    get set():IMetricsGroup { return this.internalSet; }


    /*====================================================================*
     START: Signals
     *====================================================================*/
    get isSignalChanged():boolean { return this._isSignalChanged; }
    set isSignalChanged(value:boolean) { this._isSignalChanged = value; }
    private _isSignalChanged:boolean;
    get changed():Signal<this> { return this._changed; }
    private _changed:Signal<this> = new Signal<this>();
    protected _onChanged() {
        if (!this._isSignalChanged) return;
        this._changed.dispatch(this);
    }


    /*====================================================================*
     START: Public Methods
     *====================================================================*/
    /**
     * Update the values of this instance
     * @param values an array of numbers
     */
    to(values:number[]):this { Metrics.to(this, values); return this; }


    equals(values:number[]):boolean { return Metrics.equals(this.array, values); }


    add(values:number[]):this {
        const c:number[] = Metrics.add(this.array, values);
        if (this.equals(c)) return this;
        Metrics.to(this, c);
        this._onChanged();
        return this;
    }


    addNumber(value:number):this {
        if (value === 0) return this;
        Metrics.to(this, Metrics.addNumber(this.array, value));
        this._onChanged();
        return this;
    }


    subtract(values:number[]):this {
        const c:number[] = Metrics.subtract(this.array, values);
        if (this.equals(c)) return this;
        Metrics.to(this, c);
        this._onChanged();
        return this;
    }


    subtractNumber(value:number):this {
        if (value === 0) return this;
        Metrics.to(this, Metrics.subtractNumber(this.array, value));
        this._onChanged();
        return this;
    }


    multiply(values:number[]):this {
        const c:number[] = Metrics.multiply(this.array, values);
        if (this.equals(c)) return this;
        Metrics.to(this, c);
        this._onChanged();
        return this;
    }


    multiplyNumber(value:number):this {
        if (value === 1) return this;
        Metrics.to(this, Metrics.multiplyNumber(this.array, value));
        this._onChanged();
        return this;
    }


    divide(values:number[]):this {
        const c:number[] = Metrics.divide(this.array, values);
        if (this.equals(c)) return this;
        Metrics.to(this, c);
        this._onChanged();
        return this;
    }


    divideNumber(value:number):this {
        if (value === 0 || value === 1) return this;
        Metrics.to(this, Metrics.divideNumber(this.array, value));
        this._onChanged();
        return this;
    }


    dot(values:number[]):number { return Metrics.dot(this.array, values); }


    distanceSquared(values:number[]):number { return Metrics.distanceSquared(this.array, values); }
    distance(values:number[]):number { return Metrics.distance(this.array, values); }



   min(values:number[]):this {
       const c:number[] = Metrics.min(this.array, values, true);
       if (this.equals(c)) return this;
       Metrics.to(this, c);
       this._onChanged();
       return this;

    }


    max(values:number[]):this {
        const c:number[] = Metrics.max(this.array, values, true);
        if (this.equals(c)) return this;
        Metrics.to(this, c);
        this._onChanged();
        return this;
    }


    clamp(min:number[], max:number[]):this {
        const c:number[] = Metrics.clamp(this.array, min, max, true);
        if (this.equals(c)) return this;
        Metrics.to(this, c);
        this._onChanged();
        return this;
    }


    /*====================================================================*
     START: Protected Methods
     *====================================================================*/
    protected _setValues(indices:number[], values:number[]):void {
        let isChanged:boolean = false;
        for (let i = 0, length = indices.length; i < length; i++) {
            if (this._isForceChanges || this._array[indices[i]] !== values[i]) isChanged = true;
            this._array[indices[i]] = values[i];
        }

        if (isChanged) this._onChanged();
    }
}


export default MetricsBase;
