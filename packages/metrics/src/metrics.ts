import IMetrics from "./interfaces/iMetrics";
import IPoint from "./interfaces/iPoint";
import {
    Animation,
    AnimationValue,
    IAnimation,
    IAnimationEaseFunction,
    ISpeed,
    Speed,
    SpeedType
} from "@pilotlab/animation";
import is from "@pilotlab/is";


export default class Metrics {
    /*====================================================================*
     START: Static Properties
     *====================================================================*/
    static get tolerance():number { return 0.0000001; }


    /*====================================================================*
     START: Static Methods
     *====================================================================*/
    /**
     * NOTE:
     *
     * Setting the isModifyA parameter to false on these static methods
     * has a negative performance impact.
     */


    /**
     * Update the values of the given instance
     * @param instance an object of type IMetrics that we want to update with the provided values
     * @param values a list of numbers, an array of numbers, or an object with properties matching the given schema
     */
    static to(instance:IMetrics, values:number[]):IMetrics {
        const length:number = values.length;
        for (let i = 0; i < length; i++) {
            instance.array[i] = values[i];
        }

        return instance;
    }


    /**
     * Check if two number arrays are equal, within a tolerance
     * @returns boolean
     */
    static equals(a:number[], b:number[], tolerance?:number):boolean {
        if (!tolerance) tolerance = Metrics.tolerance;
        for (let i = 0, len = a.length; i < len; i++) {
            if (Math.abs(a[i] - b[i]) > tolerance) return false;
        }
        return true;
    }


    /**
     * Convert different kinds of parameters (arguments, array, object) into an array of numbers
     * @param args a list of numbers, an array of number, or an object with {x,y,z,w} properties
     * @param schema an array of strings representing the expected properties in an object representation of the array
     */
    static getArray(args:any[], schema:string[]):number[] {
        if (args.length < 1) return [];

        let pos:number[] = [];
        let isArray:boolean = Array.isArray(args[0]) || ArrayBuffer.isView(args[0]);

        /// positional arguments: x,y,z,w,...
        if (typeof args[0] === "number") {
            pos = Array.prototype.slice.call(args);

            /// as an object of {x, y?, z?, w?}
        } else if (typeof args[0] === "object" && !isArray) {
            let a = schema;
            let p = args[0];
            for (let i = 0; i < a.length; i++) {
                if ((p.length && i >= p.length) || !(a[i] in p)) break; // check for length and key
                pos.push(p[a[i]]);
            }

            /// as an array of values
        } else if (isArray) pos = [].slice.call(args[0]);

        return pos;
    }


    static magnitudeSquared(value:number[]) { return Metrics.dot(value, value); }
    static magnitude(value:number[]):number { return Math.sqrt(Metrics.dot(value, value)); }


    static abs(value:number[]):number[] {
        const length:number = value.length;
        for (let i:number = 0; i < length; i++) { value[i] = Math.abs(value[i]); }
        return value;
    }


    /**
     * Add `b` to `a`
     * @returns new number[]
     */
    static add(a:number[], b:number[], isModifyA:boolean = true):number[] {
        const length:number = b.length > a.length ? a.length : b.length;
        const c:number[] = isModifyA ? a : new Array(length);
        for (let i:number = 0; i < length; i++) { c[i] = (a[i] + b[i]); }
        return c;
    }


    /**
     * Add a numerical value to `a`
     * @returns a new number array with the result or, if `isModifyA` is true, a modified version of `a`
     */
    static addNumber(a:number[], value:number, isModifyA:boolean = true):number[] {
        const length:number = a.length;
        const c:number[] = isModifyA ? a : new Array(length);
        for (let i:number = 0; i < length; i++) { c[i] = (a[i] + value); }
        return c;
    }


    /**
     * Subtract `b` from `a`
     * @returns `a`
     */
    static subtract(a:number[], b:number[], isModifyA:boolean = true):number[] {
        const length:number = b.length > a.length ? a.length : b.length;
        const c:number[] = isModifyA ? a : new Array(length);
        for (let i:number = 0; i < length; i++) { c[i] = (a[i] - b[i]); }
        return c;
    }


    /**
     * Get the absolute difference `b` and `a`
     * @returns `a`
     */
    static difference(a:number[], b:number[], isModifyA:boolean = true):number[] {
        const length:number = b.length > a.length ? a.length : b.length;
        const c:number[] = isModifyA ? a : new Array(length);
        for (let i:number = 0; i < length; i++) { c[i] = Math.abs(a[i] - b[i]); }
        return c;
    }


    /**
     * Subtract a numerical value from `a`
     * @returns a new number array with the result or, if `isModifyA` is true, a modified version of `a`
     */
    static subtractNumber(a:number[], value:number, isModifyA:boolean = true):number[] {
        const length:number = a.length;
        const c:number[] = isModifyA ? a : new Array(length);
        for (let i:number = 0; i < length; i++) { c[i] = (a[i] - value); }
        return c;
    }


    /**
     * Multiply `b` with vector `a`
     * @returns a new number array with the result or, if `isModifyA` is true, a modified version of `a`
     */
    static multiply(a:number[], b:number[], isModifyA:boolean = true):number[] {
        const length:number = b.length > a.length ? a.length : b.length;
        const c:number[] = isModifyA ? a : new Array(length);
        for (let i:number = 0; i < length; i++) { if (c[i] !== 1) c[i] = (a[i] * b[i]); }
        return c;
    }


    /**
     * Multiply `a` by a number
     * @returns `a`
     */
    static multiplyNumber(a:number[], value:number, isModifyA:boolean = true):number[] {
        const length:number = a.length;
        const c:number[] = isModifyA ? a : new Array(length);
        for (let i:number = 0; i < length; i++) { c[i] = (a[i] * value); }
        return c;
    }


    /**
     * Divide `b` by `a`
     * @returns a new number array with the result or, if `isModifyA` is true, a modified version of `a`
     */
    static divide(a:number[], b:number[], isModifyA:boolean = true):number[] {
        const length:number = b.length > a.length ? a.length : b.length;
        const c:number[] = isModifyA ? a : new Array(length);
        for (let i:number = 0; i < length; i++) { if (b[i] !== 0 && b[i] !== 1) c[i] = (a[i] / b[i]); }
        return c;
    }


    /**
     * Divide `a` by a number
     * @returns a new number array with the result or, if `isModifyA` is true, a modified version of `a`
     */
    static divideNumber(a:number[], value:number, isModifyA:boolean = true):number[] {
        const length:number = a.length;
        const c:number[] = isModifyA ? a : new Array(length);
        for (let i:number = 0; i < length; i++) { if (value !== 0 && value !== 1) c[i] = (a[i] / value); }
        return c;
    }


    /**
     * Get the scalar product of two number arrays
     */
    static dot(a:number[], b:number[]):number {
        let dotValue:number = 0.0;
        const length:number = b.length > a.length ? a.length : b.length;
        for (let i:number = 0; i < length; i++) dotValue += a[i] * b[i];
        return dotValue;
    }


    /**
     * Get the squared distance between `a` and `b`
     */
    static distanceSquared(a:number[], b:number[]):number {
        const difference:number[] = Metrics.subtract(a, b);
        return Metrics.magnitudeSquared(difference);
    }


    /**
     * The distance between two points
     */
    static distance(a:number[], b:number[]):number { return Math.sqrt(Metrics.distanceSquared(a, b)); }


    /**
     * Get a new number[] that has the minimum dimensional values of `a` and `b`
     *@returns a new number array with the result or, if `isModifyA` is true, a modified version of `a`
     */
    static min(a:number[], b:number[], isModifyA:boolean = true):number[] {
        const length:number = b.length > a.length ? a.length : b.length;
        const c:number[] = isModifyA ? a : new Array(length);
        for (let i:number = 0; i < length; i++) {
            c[i] = Math.min(a[i], b[i]);
        }
        return c;
    }


    /**
     * Get a new number[] that has the maximum dimensional values of `a` and `b`
     * @returns `a`
     */
    static max(a:number[], b:number[], isModifyA:boolean = true):number[] {
        const length:number = b.length > a.length ? a.length : b.length;
        const c:number[] = isModifyA ? a : new Array(length);
        for (let i:number = 0; i < length; i++) {
            c[i] = Math.max(a[i], b[i]);
        }
        return c;
    }


    static clamp(values:number[], min:number[], max:number[], isModifyValues:boolean = true) {
        let length:number = values.length > max.length ? max.length : values.length;
        const c:number[] = isModifyValues ? values : new Array(length);
        for (let i:number = 0; i < length; i++) {
            c[i] = Math.min(values[i], max[i]);
        }

        length = c.length > min.length ? min.length : c.length;
        for (let i:number = 0; i < length; i++) {
            c[i] = Math.max(c[i], min[i]);
        }

        return c;
    }


    static largestValue(values:number[]):number {
        if (values.length < 1) return 0;

        const length:number = values.length;
        let value:number = values[0];
        for (let i:number = 1; i < length; i++) {
            value = Math.max(value, values[i]);
        }
        return value;
    }


    static smallestValue(values:number[]):number {
        if (values.length < 1) return 0;

        const length:number = values.length;
        let value:number = values[0];
        for (let i:number = 1; i < length; i++) {
            value = Math.min(value, values[i]);
        }

        return value;
    }


    /// Compare and sort functions
    /// Top-down, then left-to-right
    /// Logical OR (||) returns expr1 if it can be converted to true; otherwise, returns expr2.
    /// So, when used with Boolean values, || returns true if either operand is true; if both are false, returns false.
    static sortDownwardLeftToRight(a:IPoint, b:IPoint):number {
        return a.y - b.y || a.x - b.x;
    }


    //---- Left-to-right, then top-down
    static sortRightwardTopToBottom(a:IPoint, b:IPoint):number {
        return a.x - b.x || a.y - b.y;
    }


    /// Animation
    static go(
        start:number[],
        target:number[],
        durationSpeed?:(number | ISpeed),
        ease?:IAnimationEaseFunction,
        repeatCount:number = 0,
        animationKey?:string,
    ):IAnimation {
        let duration:number = 0;

        if (is.notEmpty(durationSpeed) && typeof durationSpeed === 'number' && durationSpeed >= 0) {
            duration = durationSpeed;
        } else if (durationSpeed instanceof Speed && (<ISpeed>durationSpeed).type === SpeedType.DURATION) {
            duration = (<ISpeed>durationSpeed).duration;
        } else {
            let difference:number = this.largestValue(this.difference(start, target, false));
            duration = Animation.getDuration(durationSpeed, 0, difference);
        }

        if (is.empty(ease)) ease = Animation.validateSpeed(durationSpeed).ease;

        let length:number = start.length < target.length ? start.length : target.length;
        let animationValues:AnimationValue[] = [];
        for (let i = 0; i < length; i++) {
            animationValues.push(new AnimationValue(start[i], target[i]));
        }

        let animation:IAnimation = new Animation(
            animationValues,
            duration,
            ease,
            repeatCount,
            animationKey
        );

        animation.ticked.listen(() => {
            for (let i = 0; i < length; i++) {
                start[i] = animation.values[i].current;
            }
        });

        animation.completed.listenOnce(() => {
            for (let i = 0; i < length; i++) {
                start[i] = target[i];
            }
        });

        Animation.goAnimation(animation);
        return animation;
    }
}
