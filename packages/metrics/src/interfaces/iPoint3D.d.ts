import IPoint from './iPoint';
export default interface IPoint3D extends IPoint {
    z: number;
}
