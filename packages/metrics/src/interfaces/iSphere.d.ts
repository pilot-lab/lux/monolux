import IPoint3D from './iPoint3D';
export default interface ISphere extends IPoint3D {
    radius: number;
}
