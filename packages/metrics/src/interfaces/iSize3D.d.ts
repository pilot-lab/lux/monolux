import ISize from "./iSize";
export default interface ISize3D extends ISize {
    depth: number;
}
