import IMetrics from "./iMetrics";


export default interface IMetricsGroup {
    readonly key:(number | string);
    readonly size:number;
    readonly min:number[];
    readonly max:number[];
    readonly center:number[];
    elements:IMetrics[];


    add(element:IMetrics):this;
    isNear(values:number[], thresholdDistance?:number):boolean;
}
