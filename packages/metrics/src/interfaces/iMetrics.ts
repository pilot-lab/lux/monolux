import { Signal } from "@pilotlab/signals";
import IMetricsGroup from "./iMetricsGroup";


export default interface IMetrics {
    /*====================================================================*
     START: Internal
     *====================================================================*/
    internalSet:IMetricsGroup;


    /*====================================================================*
     START: Properties
     *====================================================================*/
    readonly schema:string[];
    readonly array:number[];
    readonly isEmpty:boolean;
    readonly clone:IMetrics;
    readonly set:IMetricsGroup;
    isSignalChanged:boolean;
    magnitude:number;
    magnitudeSquared:number;
    abs:this;


    /*====================================================================*
     START: Signals
     *====================================================================*/
    readonly changed:Signal<this>;


    /*====================================================================*
     START: Methods
     *====================================================================*/
    to(values:number[]):IMetrics;
    equals(values:number[]):boolean;
    add(values:number[]):this;
    subtract(values:number[]):this;
    multiply(values:number[]):this;
    multiplyNumber(value:number):this;
    divide(values:number[]):this;
    dot(values:number[]):number;
    distanceSquared(values:number[]):number;
    distance(values:number[]):number;
    min(values:number[]):this;
    max(values:number[]):this;
    clamp(min:number[], max:number[]):this;
}
