"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const metrics_1 = require("./metrics");
class MetricsGroup {
    constructor(element, key) {
        this._key = key;
        this._min = element.array;
        this._max = element.array;
        element.internalSet = this;
        this._elements = [];
        this._elements.push(element);
    }
    get min() { return this._min; }
    get max() { return this._max; }
    get elements() { return this._elements; }
    get key() { return this._key; }
    get size() { return metrics_1.default.distanceSquared(this._min, this._max); }
    get center() {
        let c = [];
        const length = this._min.length > this._max.length ? this._max.length : this._min.length;
        for (let i = 0; i < length; i++) {
            c[i] = ((this._max[i] - this._min[i]) / 2) + this._min[i];
        }
        return c;
    }
    add(element) {
        this._min = metrics_1.default.min(this._min, element.array, false);
        this._max = metrics_1.default.max(this._max, element.array, false);
        element.internalSet = this;
        this._elements.push(element);
        return this;
    }
    isNear(values, thresholdDistance = 20) {
        let valuesClamped = metrics_1.default.clamp(values, this._min, this._max, false);
        let distance = metrics_1.default.distance(valuesClamped, values);
        return distance < thresholdDistance;
    }
}
exports.MetricsGroup = MetricsGroup;
exports.default = MetricsGroup;
//# sourceMappingURL=metricsGroup.js.map