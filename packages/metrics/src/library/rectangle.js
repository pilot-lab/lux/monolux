"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const point_1 = require("./point");
const metrics_1 = require("../metrics");
class Rectangle extends point_1.default {
    _setSchema() { this._schema = Rectangle.schema; }
    get width() { return this._array[2]; }
    set width(value) { this._setValues([2], [value]); }
    get height() { return this._array[3]; }
    set height(value) { this._setValues([3], [value]); }
    get isEmpty() { return is_1.default.empty(this.x) || is_1.default.empty(this.y) || is_1.default.empty(this.width) || is_1.default.empty(this.height); }
    get clone() { return new Rectangle(this.x, this.y, this.width, this.height); }
    static get empty() { return new Rectangle(); }
    static get schema() { return ["x", "y", "width", "height"]; }
    static getArray(...args) { return metrics_1.default.getArray(args, Rectangle.schema); }
}
exports.default = Rectangle;
//# sourceMappingURL=rectangle.js.map