import MetricsBase from "../metricsBase";
import IPoint from "../interfaces/iPoint";
export default class Point extends MetricsBase implements IPoint {
    protected _setSchema(): void;
    x: number;
    y: number;
    readonly isEmpty: boolean;
    readonly clone: Point;
    static readonly empty: Point;
    static readonly schema: string[];
    static getArray(...args: number[]): number[];
}
