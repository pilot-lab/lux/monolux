import is from "@pilotlab/is";
import IPoint3D from "../interfaces/iPoint3D";
import Point from "./point";
import Metrics from "../metrics";


export default class Point3D extends Point implements IPoint3D {
    protected _setSchema():void { this._schema = Point3D.schema; }


    get z():number { return this._array[2]; }
    set z(value:number) { this._setValues([2], [value]); }


    get isEmpty():boolean {  return is.empty(this.x) || is.empty(this.y) || is.empty(this.z); }
    get clone():Point3D { return new Point3D(this.x, this.y, this.z); }


    /*====================================================================*
     START: Static Methods
     *====================================================================*/
    static get empty():Point3D { return new Point3D(); }
    static get schema():string[] { return ["x", "y", "z"]; }
    static getArray(...args:number[]):number[] { return Metrics.getArray(args, Point3D.schema); }
}
