"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const point3D_1 = require("./point3D");
const metrics_1 = require("../metrics");
class Vector extends point3D_1.default {
    _setSchema() { this._schema = Vector.schema; }
    get w() { return this._array[3]; }
    set w(value) { this._setValues([3], [value]); }
    get isEmpty() { return is_1.default.empty(this.x) || is_1.default.empty(this.y) || is_1.default.empty(this.z) || is_1.default.empty(this.w); }
    get clone() { return new Vector(this.x, this.y, this.z, this.w); }
    static get empty() { return new Vector(); }
    static get schema() { return ["x", "y", "z", "w"]; }
    static getArray(...args) { return metrics_1.default.getArray(args, Vector.schema); }
}
exports.default = Vector;
//# sourceMappingURL=vector.js.map