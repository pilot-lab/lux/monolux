import is from "@pilotlab/is";
import IVector from "../interfaces/iVector";
import Point3D from "./point3D";
import Metrics from "../metrics";


export default class Vector extends Point3D implements IVector {
    protected _setSchema():void { this._schema = Vector.schema; }


    get w():number { return this._array[3]; }
    set w(value:number) { this._setValues([3], [value]); }


    get isEmpty():boolean {  return is.empty(this.x) || is.empty(this.y) || is.empty(this.z) || is.empty(this.w); }
    get clone():Vector { return new Vector(this.x, this.y, this.z, this.w); }


    /*====================================================================*
     START: Static Methods
     *====================================================================*/
    static get empty():Vector { return new Vector(); }
    static get schema():string[] { return ["x", "y", "z", "w"]; }
    static getArray(...args:number[]):number[] { return Metrics.getArray(args, Vector.schema); }
}
