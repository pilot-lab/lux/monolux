import is from "@pilotlab/is";
import MetricsBase from "../metricsBase";
import Metrics from "../metrics";
import ISize from "../interfaces/iSize";


export default class Size extends MetricsBase implements ISize {
    protected _setSchema():void { this._schema = Size.schema; }


    get width():number { return this._array[0]; }
    set width(value:number) { this._setValues([0], [value]); }


    get height():number { return this._array[1]; }
    set height(value:number) { this._setValues([1], [value]); }


    get isEmpty():boolean {  return is.empty(this.width) || is.empty(this.width); }
    get clone():Size { return new Size(this.width, this.height); }


    /*====================================================================*
     START: Static Methods
     *====================================================================*/
    static get empty():Size { return new Size(); }
    static get schema():string[] { return ["width", "height"]; }
    static getArray(...args:number[]):number[] { return Metrics.getArray(args, Size.schema); }
}
