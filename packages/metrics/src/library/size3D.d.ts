import Size from "./size";
import ISize3D from "../interfaces/iSize3D";
export default class Size3D extends Size implements ISize3D {
    protected _setSchema(): void;
    depth: number;
    readonly isEmpty: boolean;
    readonly clone: Size3D;
    static readonly empty: Size3D;
    static readonly schema: string[];
    static getArray(...args: number[]): number[];
}
