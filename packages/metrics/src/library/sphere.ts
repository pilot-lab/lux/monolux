import is from "@pilotlab/is";
import ISphere from "../interfaces/iSphere";
import Point3D from "./point3D";
import Metrics from "../metrics";


export default class Sphere extends Point3D implements ISphere {
    protected _setSchema():void { this._schema = Sphere.schema; }


    get radius():number { return this._array[3]; }
    set radius(value:number) { this._setValues([3], [value]); }


    get isEmpty():boolean {  return is.empty(this.x) || is.empty(this.y) || is.empty(this.z) || is.empty(this.radius); }
    get clone():Sphere { return new Sphere(this.x, this.y, this.z, this.radius); }


    /*====================================================================*
     START: Static Methods
     *====================================================================*/
    static get empty():Sphere { return new Sphere(); }
    static get schema():string[] { return ["x", "y", "z", "radius"]; }
    static getArray(...args:number[]):number[] { return Metrics.getArray(args, Sphere.schema); }
}
