import is from "@pilotlab/is";
import IBounds from "../interfaces/iBounds";
import Metrics from "../metrics";
import Point3D from "./point3D";


export default class Bounds extends Point3D implements IBounds {
    constructor(...values:any[]) {
        super(...values);
        this._arrayPrevious = this.array.slice();
        this.pivotX = 0.5;
        this.pivotY = 0.5;
        this.pivotZ = 0.5;
        this.scaleAll = 1;
    }


    protected _setSchema():void { this._schema = Bounds.schema; }


    /*====================================================================*
     START: Properties
     *====================================================================*/
    reset():this {
        this.x = null;
        this.y = null;
        this.z = null;
        this.width = null;
        this.height = null;
        this.depth = null;
        this.pivotX = 0.5;
        this.pivotY = 0.5;
        this.pivotZ = 0.5;
        this.scaleAll = 1;

        return this;
    }


    get previous():number[] { return this._arrayPrevious; }
    protected readonly _arrayPrevious:number[];


    get position():number[] { return [this.array[0], this.array[1], this.array[2]]; }
    set position(values:number[]) { this._setValues([0, 1, 2], values); }


    get size():number[] { return [this.array[3], this.array[4], this.array[5]]; }
    set size(values:number[]) { this._setValues([3, 4, 5], values); }


    get pivot():number[] { return [this.array[6], this.array[7], this.array[8]]; }
    set pivot(values:number[]) { this._setValues([6, 7, 8], values); }


    get scale():number[] { return [this.array[9], this.array[10], this.array[11]]; }
    set scale(values:number[]) { this._setValues([9, 10, 11], values); }
    set scaleAll(value:number) { this.scaleX = value; this.scaleY = value; this.scaleZ = value; this._onChanged(); }


    get pivotX():number { return this._array[6]; }
    set pivotX(value:number) { this._setValues([6], [value]); }


    get pivotY():number { return this._array[7]; }
    set pivotY(value:number) { this._setValues([7], [value]); }


    get pivotZ():number { return this._array[8]; }
    set pivotZ(value:number) { this._setValues([8], [value]); }


    get scaleX():number { return this._array[9]; }
    set scaleX(value:number) { this._setValues([9], [value]); }


    get scaleY():number { return this._array[10]; }
    set scaleY(value:number) { this._setValues([10], [value]); }


    get scaleZ():number { return this._array[11]; }
    set scaleZ(value:number) { this._setValues([11], [value]); }


    get leftUnscaled():number { return this.isEmpty ? 0 : this.x - (this.widthUnscaled * this.pivotX); }
    set leftUnscaled(value:number) { this.x = this.isEmpty ? value : value + (this.widthUnscaled * this.pivotX); this._onChanged(); }
    get left():number { return this.x - (this.width * this.pivotX); }
    set left(value:number) {
        if (is.empty(this._array[0])) this.x = value;
        else this.x = value + (this.width * this.pivotX);
    }


    get rightUnscaled():number { return this.isEmpty ? 1 : this.leftUnscaled + this.widthUnscaled; }
    set rightUnscaled(value:number) { this.leftUnscaled = value - this.widthUnscaled; }
    get right():number { return this.isEmpty ? 1 : this.left + this.width; }
    set right(value:number) { this.left = value - this.width; }


    get topUnscaled():number { return this.isEmpty ? 0 : this.y - (this.heightUnscaled * this.pivotY); }
    set topUnscaled(value:number) { this.y = this.isEmpty ? value : value + (this.heightUnscaled * this.pivotY); this._onChanged(); }
    get top():number { return this.y - (this.height * this.pivotY); }
    set top(value:number) {
        if (is.empty(this._array[1])) this.y = value;
        else this.y = value + (this.height * this.pivotY);
    }


    get bottomUnscaled():number { return this.isEmpty ? 1 : this.topUnscaled + this.heightUnscaled; }
    set bottomUnscaled(value:number) { this.topUnscaled = value - this.heightUnscaled; }
    get bottom():number { return this.isEmpty ? 1 : this.top + this.height; }
    set bottom(value:number) { this.top = value - this.height; }


    get frontUnscaled():number { return this.isEmpty ? 0 : this.z - (this.depthUnscaled * this.pivotZ); }
    set frontUnscaled(value:number) { this.z = this.isEmpty ? value : value + (this.depthUnscaled * this.pivotZ); this._onChanged(); }
    get front():number { return this.z - (this.depth * this.pivotZ); }
    set front(value:number) {
        if (is.empty(this._array[2])) this.z = value;
        else this.z = value + (this.depth * this.pivotZ);
    }


    get backUnscaled():number { return this.isEmpty ? 1 : this.frontUnscaled + this.depthUnscaled; }
    get back():number { return this.isEmpty ? 1 : this.front + this.depth; }


    get topLeftFrontUnscaled():number[] { return [this.leftUnscaled, this.topUnscaled, this.frontUnscaled]; }
    get topRightFrontUnscaled():number[] { return [this.rightUnscaled, this.topUnscaled, this.frontUnscaled]; }
    get bottomLeftFrontUnscaled():number[] { return [this.leftUnscaled, this.bottomUnscaled, this.frontUnscaled]; }
    get bottomRightFrontUnscaled():number[] { return [this.rightUnscaled, this.bottomUnscaled, this.frontUnscaled]; }
    get topLeftFront():number[] { return [this.left, this.top, this.front]; }
    get topRightFront():number[] { return [this.right, this.top, this.front]; }
    get bottomLeftFront():number[] { return [this.left, this.bottom, this.front]; }
    get bottomRightFront():number[] { return [this.right, this.bottom, this.front]; }


    get topLeftBackUnscaled():number[] { return [this.leftUnscaled, this.topUnscaled, this.backUnscaled]; }
    get topRightBackUnscaled():number[] { return [this.rightUnscaled, this.topUnscaled, this.backUnscaled]; }
    get bottomLeftBackUnscaled():number[] { return [this.leftUnscaled, this.bottomUnscaled, this.backUnscaled]; }
    get bottomRightBackUnscaled():number[] { return [this.rightUnscaled, this.bottomUnscaled, this.backUnscaled]; }
    get topLeftBack():number[] { return [this.left, this.top, this.back]; }
    get topRightBack():number[] { return [this.right, this.top, this.back]; }
    get bottomLeftBack():number[] { return [this.left, this.bottom, this.back]; }
    get bottomRightBack():number[] { return [this.right, this.bottom, this.back]; }


    get widthUnscaled():number { return this.width; }
    set widthUnscaled(value:number) { this.width = value; this._onChanged(); }
    get width():number { return this._array[3] * this.scaleX; }
    set width(value:number) {
        /// Set the size dimension, if it hasn't been set already.
        /// Otherwise, modify the scale dimension.
        if (is.empty(this._array[3])) this._setValues([3], [value]);
        else this.scaleX = value / this._array[3];
    }


    get heightUnscaled():number { return this.height; }
    set heightUnscaled(value:number) { this.height = value; this._onChanged(); }
    get height():number { return this._array[4] * this.scaleX; }
    set height(value:number) {
        /// Set the size dimension, if it hasn't been set already.
        /// Otherwise, modify the scale dimension.
        if (is.empty(this._array[4])) this._setValues([4], [value]);
        else this.scaleY = value /this._array[4];
    }


    get depthUnscaled():number { return this.depth; }
    set depthUnscaled(value:number) { this.depth = value; this._onChanged(); }
    get depth():number { return this._array[5] * this.scaleX; }
    set depth(value:number) {
        /// Set the size dimension, if it hasn't been set already.
        /// Otherwise, modify the scale dimension.
        if (is.empty(this._array[5])) this._setValues([5], [value]);
        else this.scaleZ = value / this._array[5];
    }


    get centerXUnscaled():number { return this.widthUnscaled >> 1 ; }
    get centerYUnscaled():number { return this.heightUnscaled >> 1; }
    get centerZUnscaled():number { return this.depthUnscaled >> 1; }
    get centerX():number { return this.width >> 1 ; }
    get centerY():number { return this.height >> 1; }
    get centerZ():number { return this.depth >> 1; }


    get radiusUnscaled():number { return this.widthUnscaled >> 1; }
    set radiusUnscaled(value:number) {
        const dimension:number = value << 1;
        this._setValues([3, 4, 5], [dimension, dimension, dimension]);
    }
    get radius():number { return this.width >> 1; }
    set radius(value:number) {
        const dimension:number = value << 1;
        this.width = dimension;
        this.height = dimension;
        this.depth = dimension;
    }


    get isEmpty():boolean {
        return is.empty(this.x) ||
            is.empty(this.y) ||
            is.empty(this.z) ||
            is.empty(this.width) ||
            is.empty(this.height) ||
            is.empty(this.depth);
    }


    get clone():Bounds { return new Bounds(
        this.x,
        this.y,
        this.z,
        this.width,
        this.height,
        this.depth,
        this.pivotX,
        this.pivotY,
        this.pivotZ,
        this.scaleX,
        this.scaleY,
        this.scaleZ
    ); }


    /*====================================================================*
     START: Methods
     *====================================================================*/
    setPrevious(values?:number[]):this {
        if (!values || values.length < 1) values = this.array.slice();
        const length:number = values.length <= this._schema.length ? values.length : this._schema.length;
        for (let i = 0; i < length; i++) { this._arrayPrevious[i] = values[i] }
        return this;
    }


    intersects(cube:IBounds):boolean {
        return !(
            cube.left > this.right ||
            cube.right < this.left ||
            cube.top > this.bottom ||
            cube.bottom < this.top ||
            cube.front > this.back ||
            cube.back < this.front
        );
    }


    has(cube:IBounds):boolean {
        return (
            cube.left > this.left &&
            cube.right < this.right &&
            cube.top > this.top &&
            cube.bottom < this.bottom &&
            cube.front > this.front &&
            cube.back < this.back
        );
    }


    canFit(cube:IBounds) {
        return (
            this.width >= cube.width &&
            this.height >= cube.height &&
            this.depth >= cube.depth
        );
    }


    /*====================================================================*
     START: Static Methods
     *====================================================================*/
    static get empty():Bounds { return new Bounds(); }
    static get schema():string[] { return ["x", "y", "z", "width", "height", "depth", "pivotX", "pivotY", "pivotZ", "scaleX", "scaleY", "scaleZ"]; }
    static getArray(...args:number[]):number[] { return Metrics.getArray(args, Bounds.schema); }
} // End of class
