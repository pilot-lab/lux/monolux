import is from "@pilotlab/is";
import MetricsBase from "../metricsBase";
import Metrics from "../metrics";
import IPoint from "../interfaces/iPoint";


export default class Point extends MetricsBase implements IPoint {
    protected _setSchema():void { this._schema = Point.schema; }


    get x():number { return this._array[0]; }
    set x(value:number) { this._setValues([0], [value]); }


    get y():number { return this._array[1]; }
    set y(value:number) { this._setValues([1], [value]); }


    get isEmpty():boolean {  return is.empty(this.x) || is.empty(this.y); }
    get clone():Point { return new Point(this.x, this.y); }


    /*====================================================================*
     START: Static Methods
     *====================================================================*/
    static get empty():Point { return new Point(); }
    static get schema():string[] { return ["x", "y"]; }
    static getArray(...args:number[]):number[] { return Metrics.getArray(args, Point.schema); }
}
