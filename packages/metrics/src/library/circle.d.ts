import ICircle from "../interfaces/iCircle";
import Point from "./point";
export default class Circle extends Point implements ICircle {
    protected _setSchema(): void;
    radius: number;
    readonly isEmpty: boolean;
    readonly clone: Circle;
    static readonly empty: Circle;
    static readonly schema: string[];
    static getArray(...args: number[]): number[];
}
