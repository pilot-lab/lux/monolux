import MetricsBase from "../metricsBase";
import ISize from "../interfaces/iSize";
export default class Size extends MetricsBase implements ISize {
    protected _setSchema(): void;
    width: number;
    height: number;
    readonly isEmpty: boolean;
    readonly clone: Size;
    static readonly empty: Size;
    static readonly schema: string[];
    static getArray(...args: number[]): number[];
}
