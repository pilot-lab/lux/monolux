"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const size_1 = require("./size");
const metrics_1 = require("../metrics");
class Size3D extends size_1.default {
    _setSchema() { this._schema = Size3D.schema; }
    get depth() { return this._array[2]; }
    set depth(value) { this._setValues([2], [value]); }
    get isEmpty() { return is_1.default.empty(this.width) || is_1.default.empty(this.width) || is_1.default.empty(this.depth); }
    get clone() { return new Size3D(this.width, this.height, this.depth); }
    static get empty() { return new Size3D(); }
    static get schema() { return ["width", "height", "depth"]; }
    static getArray(...args) { return metrics_1.default.getArray(args, Size3D.schema); }
}
exports.default = Size3D;
//# sourceMappingURL=size3D.js.map