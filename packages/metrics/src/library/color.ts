import is from "@pilotlab/is";
import Numbers from "@pilotlab/numbers";
import IColor from "../interfaces/iColor";
import MetricsBase from "../metricsBase";
import Metrics from "../metrics";


export default class Color extends MetricsBase implements IColor {
    protected _setSchema():void { this._schema = Color.schema; }


    get r():number { return this._array[0]; }
    set r(value:number) { this._setValues([0], [value]); }


    get g():number { return this._array[1]; }
    set g(value:number) { this._setValues([1], [value]); }


    get b():number { return this._array[2]; }
    set b(value:number) { this._setValues([2], [value]); }


    get a():number { return this._array[3]; }
    set a(value:number) { this._setValues([3], [value]); }


    get isEmpty():boolean {  return is.empty(this.r) || is.empty(this.g) || is.empty(this.b) || is.empty(this.a); }
    get clone():Color { return new Color(this.r, this.g, this.b, this.a); }
    get normalized():number[] { return Color.normalize(this.array); }


    /*====================================================================*
     START: Static Methods
     *====================================================================*/
    static get empty():Color { return new Color(); }
    static get schema():string[] { return ["r", "g", "b", "a"]; }
    static getArray(...args:number[]):number[] { return Metrics.getArray(args, Color.schema); }
    static get random():Color { return new Color(Numbers.randomByte(), Numbers.randomByte(), Numbers.randomByte()); }


    static normalize(color:number[]):number[] { return [color[0] / 255, color[1] / 255, color[2] / 255, color.length > 3 ? color[3] : 1]; }


    static rgbToHexBase(r:number, g:number, b:number):string { return `${((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1)}`; }
    static rgbToHex(r:number, g:number, b:number):string { return `#${this.rgbToHexBase(r, g, b)}`; }
    static rgbToHexNumber(r:number, g:number, b:number):number {
        let hexString:string = `0x${this.rgbToHexBase(r, g, b)}`;
        return parseInt(hexString, 16);
    }


    static isValidHexString(value:string):boolean {
        return (/^(#[a-fA-F0-9]{3,6})$/i.test(value));
    }


    /**
     * Return an instance of the Color class from a hex string.
     * String may be in the format '#000000', '000000', '#000', or '000'.
     */
    static hexToRgb(colorHexIn:string):number[] {
        if (!this.isValidHexString(colorHexIn)) {
            return [255, 255, 255];
        }

        //* START: Normalize the string
        let colorHex = colorHexIn.toLowerCase();

        if (colorHex.indexOf('#') > -1) colorHex = colorHex.substring(1);
        if (colorHex.length === 3) {
            let newHex:string = colorHex.substring(0, 1) + colorHex.substring(0, 1);
            newHex += colorHex.substring(1, 2) + colorHex.substring(1, 2);
            newHex += colorHex.substring(2, 3) + colorHex.substring(2, 3);
            colorHex = newHex;
        }
        //* END: Normalize the string

        const arrBuff = new ArrayBuffer(4);
        const vw = new DataView(arrBuff);
        vw.setUint32(0, parseInt(colorHex, 16), false);
        const arrByte = new Uint8Array(arrBuff);

        const r:number = arrByte[1];
        const g:number = arrByte[2];
        const b:number = arrByte[3];

        return [r, g, b];
    }


    static hsbToRgb(hue:number, saturation:number, brightness:number, isNormalize:boolean = true):number[] {
        let max_hue = 360.0;
        let min_saturation = 0;
        let max_saturation = 100.0;
        let min_brightness = 0;
        let max_brightness = 100.0;

        hue = hue % max_hue;
        brightness = Math.max(min_brightness, Math.min(brightness, max_brightness)) / max_brightness * (isNormalize ? 1.0 : 255.0);

        if (saturation <= min_saturation) {
            brightness = Math.round(brightness);
            return [brightness, brightness, brightness];
        } else if (saturation > max_saturation) saturation = max_saturation;

        saturation = saturation / max_saturation;

        /// SEE: http://d.hatena.ne.jp/ja9/20100903/128350434
        let hi = Math.floor(hue / 60.0) % 6,
            f = (hue / 60.0) - hi,
            p = brightness * (1 - saturation),
            q = brightness * (1 - f * saturation),
            t = brightness * (1 - (1 - f) * saturation);

        let r = 0,
            g = 0,
            b = 0;

        switch (hi) {
            case 0:
                r = brightness;
                g = t;
                b = p;
                break;
            case 1:
                r = q;
                g = brightness;
                b = p;
                break;
            case 2:
                r = p;
                g = brightness;
                b = t;
                break;
            case 3:
                r = p;
                g = q;
                b = brightness;
                break;
            case 4:
                r = t;
                g = p;
                b = brightness;
                break;
            case 5:
                r = brightness;
                g = p;
                b = q;
                break;
            default:
                break;
        }

        if (!isNormalize) {
            r = Math.round(r);
            g = Math.round(g);
            b = Math.round(b);
        }

        return [r, g, b];
    }


    static rgbToHsb(r:number, g:number, b:number, isInputNormalized:boolean = true) {
        let rr, gg, bb;

        if (!isInputNormalized) {
            r /= 255;
            g /= 255;
            b /= 255;
        }

        let h, s;
        let v = Math.max(r, g, b);
        let diff = v - Math.min(r, g, b);
        let diffc = function (c:number) {
            return (v - c) / 6 / diff + 1 / 2;
        };

        if (diff === 0) {
            h = s = 0;
        } else {
            s = diff / v;
            rr = diffc(r);
            gg = diffc(g);
            bb = diffc(b);

            if (r === v) {
                h = bb - gg;
            } else if (g === v) {
                h = (1 / 3) + rr - bb;
            } else if (b === v) {
                h = (2 / 3) + gg - rr;
            }
            if (h < 0) {
                h += 1;
            } else if (h > 1) {
                h -= 1;
            }
        }

        return [Math.round(h * 360), Math.round(s * 360), Math.round(v * 360)];
    }


    static adjustWhiteBalance(color:number[], whiteBalance:number[], isNormalize:boolean = true):number[] {
        const colorAdjusted:number[] = [color[0], color[1], color[2], color.length > 3 ? color[3] : 1];

        colorAdjusted[0] = color[0] * ((isNormalize ? 1.0 : 255.0) / whiteBalance[0]);
        colorAdjusted[1] = color[1] * ((isNormalize ? 1.0 : 255.0) / whiteBalance[1]);
        colorAdjusted[2] = color[2] * ((isNormalize ? 1.0 : 255.0) / whiteBalance[2]);

        return colorAdjusted;
    };


    /**
     * In the YIQ color space, each RGB channel is scaled in accordance to its visual impact.
     * Once everything is scaled and normalized, the resulting value will be in a range
     * between 0 and 255, where lower data are darker.
     */
    static toYIQ(color:number[]):number {
        const yiq:number = ((color[0] * 299) + (color[1] * 587) + (color[2] * 114)) / 1000;
        return Math.round(yiq);
    }


    /**
     * Is the color darker than a neutral gray?
     */
    static isDark(color:(string | number[])):boolean {
        let colorRgb:number[];

        if (typeof color === 'string') colorRgb = this.hexToRgb(color);
        else colorRgb = color;

        const yiq:number = this.toYIQ(colorRgb);
        return (yiq < 128);
    }



    static isNearMatch(colorA:number[], colorB:number[], rangeAllowed:number):boolean {
        let isMatch:boolean = false;
        let redDif:number = Math.abs(colorA[0] - colorB[0]);
        let blueDif:number = Math.abs(colorA[1] - colorB[1]);
        let greenDif:number = Math.abs(colorA[2] - colorB[2]);

        if (redDif <= rangeAllowed && blueDif <= rangeAllowed && greenDif <= rangeAllowed) {
            isMatch = true;
        }

        return isMatch;
    }


    static difference(colorA:number[], colorB:number[]):number[] {
        let redDif:number = Math.abs(colorA[0] - colorB[0]);
        let blueDif:number = Math.abs(colorA[1] - colorB[1]);
        let greenDif:number = Math.abs(colorA[2] - colorB[2]);
        return [ redDif, greenDif, blueDif ];
    }


    /**
     * Use alpha blending to brighten the colors but don't use it
     * directly. Instead derive an opaque color that we can use.
     * If we use a color with alpha blending directly we won't be able
     * to paint over whatever color was in the background and there
     * would be shadows of that color showing through.
     */
    static blend(colorFront:number[], colorBack:number[], alpha:number):number[] {
        let opacity:number = (alpha * 255.0);

        let frontColor:number[] = [colorFront[0], colorFront[1], colorFront[2]];
        let backColor:number[] = [colorBack[0], colorBack[1], colorBack[2]];

        let frontRed:number = frontColor[0];
        let frontGreen:number = frontColor[1];
        let frontBlue:number = frontColor[2];
        let backRed:number = backColor[0];
        let backGreen:number = backColor[1];
        let backBlue:number = backColor[2];

        let newRed:number = frontRed * opacity / 255.0 + backRed * ((255.0 - opacity) / 255.0);
        let newGreen:number = frontGreen * opacity / 255.0 + backGreen * ((255.0 - opacity) / 255.0);
        let newBlue:number = frontBlue * opacity / 255.0 + backBlue * ((255.0 - opacity) / 255.0);

        return [ Math.round(newRed), Math.round(newGreen), Math.round(newBlue)];
    }


    /**
     * Return a color of the same hue that contrasts with the given color by the specified amount.
     * @param backgroundColor The existing color with which we need to contrast.
     * @param valueDifference The desired difference in contrast expressed as a value between 0 and 1.
     * @param isPreferLighter Should we first try to find a color that is lighter than the given color?
     */
    static getContrastingColor(backgroundColor:number[], valueDifference:number = 0.12, isPreferLighter:boolean = true):number[] {
        if (is.empty(valueDifference)) valueDifference = 0.12;
        if (isPreferLighter) {
            let foregroundColor:number[] = [ 255, 255, 255 ];
            let newColor:number[] = this.blend(foregroundColor, backgroundColor, valueDifference);

            if (this.toYIQ(newColor) > 160) return this.getContrastingColor(backgroundColor, valueDifference, false);
            return newColor;
        } else {
            let foregroundColor:number[] = [ 0, 0, 0 ];
            let newColor:number[] = this.blend(foregroundColor, backgroundColor, valueDifference);
            if (this.toYIQ(newColor) < 80) return this.getContrastingColor(backgroundColor, valueDifference, true);
            return newColor;
        }
    }


    static getHighContrastColor(backgroundColor:number[]):number[] {
        let yiq:number = this.toYIQ(backgroundColor);
        return (yiq >= 128) ? [ 0, 0, 0 ] : [ 255, 255, 255 ];
    }
}
