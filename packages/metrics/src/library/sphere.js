"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const point3D_1 = require("./point3D");
const metrics_1 = require("../metrics");
class Sphere extends point3D_1.default {
    _setSchema() { this._schema = Sphere.schema; }
    get radius() { return this._array[3]; }
    set radius(value) { this._setValues([3], [value]); }
    get isEmpty() { return is_1.default.empty(this.x) || is_1.default.empty(this.y) || is_1.default.empty(this.z) || is_1.default.empty(this.radius); }
    get clone() { return new Sphere(this.x, this.y, this.z, this.radius); }
    static get empty() { return new Sphere(); }
    static get schema() { return ["x", "y", "z", "radius"]; }
    static getArray(...args) { return metrics_1.default.getArray(args, Sphere.schema); }
}
exports.default = Sphere;
//# sourceMappingURL=sphere.js.map