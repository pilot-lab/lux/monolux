"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const numbers_1 = require("@pilotlab/numbers");
const metricsBase_1 = require("../metricsBase");
const metrics_1 = require("../metrics");
class Color extends metricsBase_1.default {
    _setSchema() { this._schema = Color.schema; }
    get r() { return this._array[0]; }
    set r(value) { this._setValues([0], [value]); }
    get g() { return this._array[1]; }
    set g(value) { this._setValues([1], [value]); }
    get b() { return this._array[2]; }
    set b(value) { this._setValues([2], [value]); }
    get a() { return this._array[3]; }
    set a(value) { this._setValues([3], [value]); }
    get isEmpty() { return is_1.default.empty(this.r) || is_1.default.empty(this.g) || is_1.default.empty(this.b) || is_1.default.empty(this.a); }
    get clone() { return new Color(this.r, this.g, this.b, this.a); }
    get normalized() { return Color.normalize(this.array); }
    static get empty() { return new Color(); }
    static get schema() { return ["r", "g", "b", "a"]; }
    static getArray(...args) { return metrics_1.default.getArray(args, Color.schema); }
    static get random() { return new Color(numbers_1.default.randomByte(), numbers_1.default.randomByte(), numbers_1.default.randomByte()); }
    static normalize(color) { return [color[0] / 255, color[1] / 255, color[2] / 255, color.length > 3 ? color[3] : 1]; }
    static rgbToHexBase(r, g, b) { return `${((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1)}`; }
    static rgbToHex(r, g, b) { return `#${this.rgbToHexBase(r, g, b)}`; }
    static rgbToHexNumber(r, g, b) {
        let hexString = `0x${this.rgbToHexBase(r, g, b)}`;
        return parseInt(hexString, 16);
    }
    static isValidHexString(value) {
        return (/^(#[a-fA-F0-9]{3,6})$/i.test(value));
    }
    static hexToRgb(colorHexIn) {
        if (!this.isValidHexString(colorHexIn)) {
            return [255, 255, 255];
        }
        let colorHex = colorHexIn.toLowerCase();
        if (colorHex.indexOf('#') > -1)
            colorHex = colorHex.substring(1);
        if (colorHex.length === 3) {
            let newHex = colorHex.substring(0, 1) + colorHex.substring(0, 1);
            newHex += colorHex.substring(1, 2) + colorHex.substring(1, 2);
            newHex += colorHex.substring(2, 3) + colorHex.substring(2, 3);
            colorHex = newHex;
        }
        const arrBuff = new ArrayBuffer(4);
        const vw = new DataView(arrBuff);
        vw.setUint32(0, parseInt(colorHex, 16), false);
        const arrByte = new Uint8Array(arrBuff);
        const r = arrByte[1];
        const g = arrByte[2];
        const b = arrByte[3];
        return [r, g, b];
    }
    static hsbToRgb(hue, saturation, brightness, isNormalize = true) {
        let max_hue = 360.0;
        let min_saturation = 0;
        let max_saturation = 100.0;
        let min_brightness = 0;
        let max_brightness = 100.0;
        hue = hue % max_hue;
        brightness = Math.max(min_brightness, Math.min(brightness, max_brightness)) / max_brightness * (isNormalize ? 1.0 : 255.0);
        if (saturation <= min_saturation) {
            brightness = Math.round(brightness);
            return [brightness, brightness, brightness];
        }
        else if (saturation > max_saturation)
            saturation = max_saturation;
        saturation = saturation / max_saturation;
        let hi = Math.floor(hue / 60.0) % 6, f = (hue / 60.0) - hi, p = brightness * (1 - saturation), q = brightness * (1 - f * saturation), t = brightness * (1 - (1 - f) * saturation);
        let r = 0, g = 0, b = 0;
        switch (hi) {
            case 0:
                r = brightness;
                g = t;
                b = p;
                break;
            case 1:
                r = q;
                g = brightness;
                b = p;
                break;
            case 2:
                r = p;
                g = brightness;
                b = t;
                break;
            case 3:
                r = p;
                g = q;
                b = brightness;
                break;
            case 4:
                r = t;
                g = p;
                b = brightness;
                break;
            case 5:
                r = brightness;
                g = p;
                b = q;
                break;
            default:
                break;
        }
        if (!isNormalize) {
            r = Math.round(r);
            g = Math.round(g);
            b = Math.round(b);
        }
        return [r, g, b];
    }
    static rgbToHsb(r, g, b, isInputNormalized = true) {
        let rr, gg, bb;
        if (!isInputNormalized) {
            r /= 255;
            g /= 255;
            b /= 255;
        }
        let h, s;
        let v = Math.max(r, g, b);
        let diff = v - Math.min(r, g, b);
        let diffc = function (c) {
            return (v - c) / 6 / diff + 1 / 2;
        };
        if (diff === 0) {
            h = s = 0;
        }
        else {
            s = diff / v;
            rr = diffc(r);
            gg = diffc(g);
            bb = diffc(b);
            if (r === v) {
                h = bb - gg;
            }
            else if (g === v) {
                h = (1 / 3) + rr - bb;
            }
            else if (b === v) {
                h = (2 / 3) + gg - rr;
            }
            if (h < 0) {
                h += 1;
            }
            else if (h > 1) {
                h -= 1;
            }
        }
        return [Math.round(h * 360), Math.round(s * 360), Math.round(v * 360)];
    }
    static adjustWhiteBalance(color, whiteBalance, isNormalize = true) {
        const colorAdjusted = [color[0], color[1], color[2], color.length > 3 ? color[3] : 1];
        colorAdjusted[0] = color[0] * ((isNormalize ? 1.0 : 255.0) / whiteBalance[0]);
        colorAdjusted[1] = color[1] * ((isNormalize ? 1.0 : 255.0) / whiteBalance[1]);
        colorAdjusted[2] = color[2] * ((isNormalize ? 1.0 : 255.0) / whiteBalance[2]);
        return colorAdjusted;
    }
    ;
    static toYIQ(color) {
        const yiq = ((color[0] * 299) + (color[1] * 587) + (color[2] * 114)) / 1000;
        return Math.round(yiq);
    }
    static isDark(color) {
        let colorRgb;
        if (typeof color === 'string')
            colorRgb = this.hexToRgb(color);
        else
            colorRgb = color;
        const yiq = this.toYIQ(colorRgb);
        return (yiq < 128);
    }
    static isNearMatch(colorA, colorB, rangeAllowed) {
        let isMatch = false;
        let redDif = Math.abs(colorA[0] - colorB[0]);
        let blueDif = Math.abs(colorA[1] - colorB[1]);
        let greenDif = Math.abs(colorA[2] - colorB[2]);
        if (redDif <= rangeAllowed && blueDif <= rangeAllowed && greenDif <= rangeAllowed) {
            isMatch = true;
        }
        return isMatch;
    }
    static difference(colorA, colorB) {
        let redDif = Math.abs(colorA[0] - colorB[0]);
        let blueDif = Math.abs(colorA[1] - colorB[1]);
        let greenDif = Math.abs(colorA[2] - colorB[2]);
        return [redDif, greenDif, blueDif];
    }
    static blend(colorFront, colorBack, alpha) {
        let opacity = (alpha * 255.0);
        let frontColor = [colorFront[0], colorFront[1], colorFront[2]];
        let backColor = [colorBack[0], colorBack[1], colorBack[2]];
        let frontRed = frontColor[0];
        let frontGreen = frontColor[1];
        let frontBlue = frontColor[2];
        let backRed = backColor[0];
        let backGreen = backColor[1];
        let backBlue = backColor[2];
        let newRed = frontRed * opacity / 255.0 + backRed * ((255.0 - opacity) / 255.0);
        let newGreen = frontGreen * opacity / 255.0 + backGreen * ((255.0 - opacity) / 255.0);
        let newBlue = frontBlue * opacity / 255.0 + backBlue * ((255.0 - opacity) / 255.0);
        return [Math.round(newRed), Math.round(newGreen), Math.round(newBlue)];
    }
    static getContrastingColor(backgroundColor, valueDifference = 0.12, isPreferLighter = true) {
        if (is_1.default.empty(valueDifference))
            valueDifference = 0.12;
        if (isPreferLighter) {
            let foregroundColor = [255, 255, 255];
            let newColor = this.blend(foregroundColor, backgroundColor, valueDifference);
            if (this.toYIQ(newColor) > 160)
                return this.getContrastingColor(backgroundColor, valueDifference, false);
            return newColor;
        }
        else {
            let foregroundColor = [0, 0, 0];
            let newColor = this.blend(foregroundColor, backgroundColor, valueDifference);
            if (this.toYIQ(newColor) < 80)
                return this.getContrastingColor(backgroundColor, valueDifference, true);
            return newColor;
        }
    }
    static getHighContrastColor(backgroundColor) {
        let yiq = this.toYIQ(backgroundColor);
        return (yiq >= 128) ? [0, 0, 0] : [255, 255, 255];
    }
}
exports.default = Color;
//# sourceMappingURL=color.js.map