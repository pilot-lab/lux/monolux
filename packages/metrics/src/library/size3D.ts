import is from "@pilotlab/is";
import Size from "./size";
import Metrics from "../metrics";
import ISize3D from "../interfaces/iSize3D";


export default class Size3D extends Size implements ISize3D{
    protected _setSchema():void { this._schema = Size3D.schema; }


    get depth():number { return this._array[2]; }
    set depth(value:number) { this._setValues([2], [value]); }


    get isEmpty():boolean {  return is.empty(this.width) || is.empty(this.width) || is.empty(this.depth); }
    get clone():Size3D { return new Size3D(this.width, this.height, this.depth); }


    /*====================================================================*
     START: Static Methods
     *====================================================================*/
    static get empty():Size3D { return new Size3D(); }
    static get schema():string[] { return ["width", "height", "depth"]; }
    static getArray(...args:number[]):number[] { return Metrics.getArray(args, Size3D.schema); }
}
