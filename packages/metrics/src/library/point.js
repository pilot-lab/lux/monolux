"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const metricsBase_1 = require("../metricsBase");
const metrics_1 = require("../metrics");
class Point extends metricsBase_1.default {
    _setSchema() { this._schema = Point.schema; }
    get x() { return this._array[0]; }
    set x(value) { this._setValues([0], [value]); }
    get y() { return this._array[1]; }
    set y(value) { this._setValues([1], [value]); }
    get isEmpty() { return is_1.default.empty(this.x) || is_1.default.empty(this.y); }
    get clone() { return new Point(this.x, this.y); }
    static get empty() { return new Point(); }
    static get schema() { return ["x", "y"]; }
    static getArray(...args) { return metrics_1.default.getArray(args, Point.schema); }
}
exports.default = Point;
//# sourceMappingURL=point.js.map