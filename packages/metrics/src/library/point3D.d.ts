import IPoint3D from "../interfaces/iPoint3D";
import Point from "./point";
export default class Point3D extends Point implements IPoint3D {
    protected _setSchema(): void;
    z: number;
    readonly isEmpty: boolean;
    readonly clone: Point3D;
    static readonly empty: Point3D;
    static readonly schema: string[];
    static getArray(...args: number[]): number[];
}
