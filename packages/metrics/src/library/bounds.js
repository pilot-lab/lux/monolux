"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const metrics_1 = require("../metrics");
const point3D_1 = require("./point3D");
class Bounds extends point3D_1.default {
    constructor(...values) {
        super(...values);
        this._arrayPrevious = this.array.slice();
        this.pivotX = 0.5;
        this.pivotY = 0.5;
        this.pivotZ = 0.5;
        this.scaleAll = 1;
    }
    _setSchema() { this._schema = Bounds.schema; }
    reset() {
        this.x = null;
        this.y = null;
        this.z = null;
        this.width = null;
        this.height = null;
        this.depth = null;
        this.pivotX = 0.5;
        this.pivotY = 0.5;
        this.pivotZ = 0.5;
        this.scaleAll = 1;
        return this;
    }
    get previous() { return this._arrayPrevious; }
    get position() { return [this.array[0], this.array[1], this.array[2]]; }
    set position(values) { this._setValues([0, 1, 2], values); }
    get size() { return [this.array[3], this.array[4], this.array[5]]; }
    set size(values) { this._setValues([3, 4, 5], values); }
    get pivot() { return [this.array[6], this.array[7], this.array[8]]; }
    set pivot(values) { this._setValues([6, 7, 8], values); }
    get scale() { return [this.array[9], this.array[10], this.array[11]]; }
    set scale(values) { this._setValues([9, 10, 11], values); }
    set scaleAll(value) { this.scaleX = value; this.scaleY = value; this.scaleZ = value; this._onChanged(); }
    get pivotX() { return this._array[6]; }
    set pivotX(value) { this._setValues([6], [value]); }
    get pivotY() { return this._array[7]; }
    set pivotY(value) { this._setValues([7], [value]); }
    get pivotZ() { return this._array[8]; }
    set pivotZ(value) { this._setValues([8], [value]); }
    get scaleX() { return this._array[9]; }
    set scaleX(value) { this._setValues([9], [value]); }
    get scaleY() { return this._array[10]; }
    set scaleY(value) { this._setValues([10], [value]); }
    get scaleZ() { return this._array[11]; }
    set scaleZ(value) { this._setValues([11], [value]); }
    get leftUnscaled() { return this.isEmpty ? 0 : this.x - (this.widthUnscaled * this.pivotX); }
    set leftUnscaled(value) { this.x = this.isEmpty ? value : value + (this.widthUnscaled * this.pivotX); this._onChanged(); }
    get left() { return this.x - (this.width * this.pivotX); }
    set left(value) {
        if (is_1.default.empty(this._array[0]))
            this.x = value;
        else
            this.x = value + (this.width * this.pivotX);
    }
    get rightUnscaled() { return this.isEmpty ? 1 : this.leftUnscaled + this.widthUnscaled; }
    set rightUnscaled(value) { this.leftUnscaled = value - this.widthUnscaled; }
    get right() { return this.isEmpty ? 1 : this.left + this.width; }
    set right(value) { this.left = value - this.width; }
    get topUnscaled() { return this.isEmpty ? 0 : this.y - (this.heightUnscaled * this.pivotY); }
    set topUnscaled(value) { this.y = this.isEmpty ? value : value + (this.heightUnscaled * this.pivotY); this._onChanged(); }
    get top() { return this.y - (this.height * this.pivotY); }
    set top(value) {
        if (is_1.default.empty(this._array[1]))
            this.y = value;
        else
            this.y = value + (this.height * this.pivotY);
    }
    get bottomUnscaled() { return this.isEmpty ? 1 : this.topUnscaled + this.heightUnscaled; }
    set bottomUnscaled(value) { this.topUnscaled = value - this.heightUnscaled; }
    get bottom() { return this.isEmpty ? 1 : this.top + this.height; }
    set bottom(value) { this.top = value - this.height; }
    get frontUnscaled() { return this.isEmpty ? 0 : this.z - (this.depthUnscaled * this.pivotZ); }
    set frontUnscaled(value) { this.z = this.isEmpty ? value : value + (this.depthUnscaled * this.pivotZ); this._onChanged(); }
    get front() { return this.z - (this.depth * this.pivotZ); }
    set front(value) {
        if (is_1.default.empty(this._array[2]))
            this.z = value;
        else
            this.z = value + (this.depth * this.pivotZ);
    }
    get backUnscaled() { return this.isEmpty ? 1 : this.frontUnscaled + this.depthUnscaled; }
    get back() { return this.isEmpty ? 1 : this.front + this.depth; }
    get topLeftFrontUnscaled() { return [this.leftUnscaled, this.topUnscaled, this.frontUnscaled]; }
    get topRightFrontUnscaled() { return [this.rightUnscaled, this.topUnscaled, this.frontUnscaled]; }
    get bottomLeftFrontUnscaled() { return [this.leftUnscaled, this.bottomUnscaled, this.frontUnscaled]; }
    get bottomRightFrontUnscaled() { return [this.rightUnscaled, this.bottomUnscaled, this.frontUnscaled]; }
    get topLeftFront() { return [this.left, this.top, this.front]; }
    get topRightFront() { return [this.right, this.top, this.front]; }
    get bottomLeftFront() { return [this.left, this.bottom, this.front]; }
    get bottomRightFront() { return [this.right, this.bottom, this.front]; }
    get topLeftBackUnscaled() { return [this.leftUnscaled, this.topUnscaled, this.backUnscaled]; }
    get topRightBackUnscaled() { return [this.rightUnscaled, this.topUnscaled, this.backUnscaled]; }
    get bottomLeftBackUnscaled() { return [this.leftUnscaled, this.bottomUnscaled, this.backUnscaled]; }
    get bottomRightBackUnscaled() { return [this.rightUnscaled, this.bottomUnscaled, this.backUnscaled]; }
    get topLeftBack() { return [this.left, this.top, this.back]; }
    get topRightBack() { return [this.right, this.top, this.back]; }
    get bottomLeftBack() { return [this.left, this.bottom, this.back]; }
    get bottomRightBack() { return [this.right, this.bottom, this.back]; }
    get widthUnscaled() { return this.width; }
    set widthUnscaled(value) { this.width = value; this._onChanged(); }
    get width() { return this._array[3] * this.scaleX; }
    set width(value) {
        if (is_1.default.empty(this._array[3]))
            this._setValues([3], [value]);
        else
            this.scaleX = value / this._array[3];
    }
    get heightUnscaled() { return this.height; }
    set heightUnscaled(value) { this.height = value; this._onChanged(); }
    get height() { return this._array[4] * this.scaleX; }
    set height(value) {
        if (is_1.default.empty(this._array[4]))
            this._setValues([4], [value]);
        else
            this.scaleY = value / this._array[4];
    }
    get depthUnscaled() { return this.depth; }
    set depthUnscaled(value) { this.depth = value; this._onChanged(); }
    get depth() { return this._array[5] * this.scaleX; }
    set depth(value) {
        if (is_1.default.empty(this._array[5]))
            this._setValues([5], [value]);
        else
            this.scaleZ = value / this._array[5];
    }
    get centerXUnscaled() { return this.widthUnscaled >> 1; }
    get centerYUnscaled() { return this.heightUnscaled >> 1; }
    get centerZUnscaled() { return this.depthUnscaled >> 1; }
    get centerX() { return this.width >> 1; }
    get centerY() { return this.height >> 1; }
    get centerZ() { return this.depth >> 1; }
    get radiusUnscaled() { return this.widthUnscaled >> 1; }
    set radiusUnscaled(value) {
        const dimension = value << 1;
        this._setValues([3, 4, 5], [dimension, dimension, dimension]);
    }
    get radius() { return this.width >> 1; }
    set radius(value) {
        const dimension = value << 1;
        this.width = dimension;
        this.height = dimension;
        this.depth = dimension;
    }
    get isEmpty() {
        return is_1.default.empty(this.x) ||
            is_1.default.empty(this.y) ||
            is_1.default.empty(this.z) ||
            is_1.default.empty(this.width) ||
            is_1.default.empty(this.height) ||
            is_1.default.empty(this.depth);
    }
    get clone() {
        return new Bounds(this.x, this.y, this.z, this.width, this.height, this.depth, this.pivotX, this.pivotY, this.pivotZ, this.scaleX, this.scaleY, this.scaleZ);
    }
    setPrevious(values) {
        if (!values || values.length < 1)
            values = this.array.slice();
        const length = values.length <= this._schema.length ? values.length : this._schema.length;
        for (let i = 0; i < length; i++) {
            this._arrayPrevious[i] = values[i];
        }
        return this;
    }
    intersects(cube) {
        return !(cube.left > this.right ||
            cube.right < this.left ||
            cube.top > this.bottom ||
            cube.bottom < this.top ||
            cube.front > this.back ||
            cube.back < this.front);
    }
    has(cube) {
        return (cube.left > this.left &&
            cube.right < this.right &&
            cube.top > this.top &&
            cube.bottom < this.bottom &&
            cube.front > this.front &&
            cube.back < this.back);
    }
    canFit(cube) {
        return (this.width >= cube.width &&
            this.height >= cube.height &&
            this.depth >= cube.depth);
    }
    static get empty() { return new Bounds(); }
    static get schema() { return ["x", "y", "z", "width", "height", "depth", "pivotX", "pivotY", "pivotZ", "scaleX", "scaleY", "scaleZ"]; }
    static getArray(...args) { return metrics_1.default.getArray(args, Bounds.schema); }
}
exports.default = Bounds;
//# sourceMappingURL=bounds.js.map