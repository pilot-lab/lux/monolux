"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const point_1 = require("./point");
const metrics_1 = require("../metrics");
class Circle extends point_1.default {
    _setSchema() { this._schema = Circle.schema; }
    get radius() { return this._array[2]; }
    set radius(value) { this._setValues([2], [value]); }
    get isEmpty() { return is_1.default.empty(this.x) || is_1.default.empty(this.y) || is_1.default.empty(this.radius); }
    get clone() { return new Circle(this.x, this.y, this.radius); }
    static get empty() { return new Circle(); }
    static get schema() { return ["x", "y", "radius"]; }
    static getArray(...args) { return metrics_1.default.getArray(args, Circle.schema); }
}
exports.default = Circle;
//# sourceMappingURL=circle.js.map