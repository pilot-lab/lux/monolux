"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const point_1 = require("./point");
const metrics_1 = require("../metrics");
class Point3D extends point_1.default {
    _setSchema() { this._schema = Point3D.schema; }
    get z() { return this._array[2]; }
    set z(value) { this._setValues([2], [value]); }
    get isEmpty() { return is_1.default.empty(this.x) || is_1.default.empty(this.y) || is_1.default.empty(this.z); }
    get clone() { return new Point3D(this.x, this.y, this.z); }
    static get empty() { return new Point3D(); }
    static get schema() { return ["x", "y", "z"]; }
    static getArray(...args) { return metrics_1.default.getArray(args, Point3D.schema); }
}
exports.default = Point3D;
//# sourceMappingURL=point3D.js.map