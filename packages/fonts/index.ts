import FontDescriptions from './src/fontDescription';
import {FontWeight, FontStyle} from './src/fontEnums';
import FontManager from './src/fontManager';
import FontMetrics from './src/fontMetrics';
import StyleFont from './src/styleFont';


export {
    FontDescriptions,
    FontWeight,
    FontStyle,
    FontManager,
    FontMetrics,
    StyleFont
};



