export interface Font {
    fontFamily:string;
    src:string;

    /**
     * Set this before initializing src.
     */
    onload:() => void;
    onerror:() => void;
    toStyleNode():any;
    metrics:FontMetricsExtended;
    measureText(text:string, fontSize:number):FontMetricsMeasureText;
}


export declare let Font:{
    prototype:Font;
    new ():Font;
};


export interface FontMetricsCore {
    leading:number;
    ascent:number;
    descent:number;
}


export interface FontMetricsExtended extends FontMetricsCore {
    /**
     * The font-indicated number of units per em.
     */
    quadsize:number;

    /**
     * See: http://www.microsoft.com/typography/otspec/os2.htm#wtc
     */
    weightclass:number;
}


export interface FontMetricsMeasureText extends FontMetricsCore {
    fontsize:number;
    width:number;
    height:number;
    bounds:FontBounds;
}


export interface FontBounds {
    minx:number;
    maxx:number;
    miny:number;
    maxy:number;
}
