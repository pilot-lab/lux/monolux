# vue-ts-webpack

Project template with VueJS, Babel, Stylus, PostCSS, and Webpack

Setup: run `npm install`

Scripts:

`pwp build` - will build your project into the /dist folder

`pwp start` - will start a local dev server at localhost:8080

Enjoy!


