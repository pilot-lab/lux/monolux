import {
    DataType,
    AttributeCollection,
    Attributes,
    AttributeChangeOptions,
    IAttribute
} from '@pilotlab/data';
import { FontWeight, FontStyle } from './fontEnums';


export class StyleFont extends AttributeCollection {
    constructor(
        data?:(StyleFont | AttributeCollection | Attributes | Object | string),
        label:string = 'Font Style'
    ) {
        super('styleFont', null, DataType.COLLECTION, label);

        this.p_isSystemFont = this.attributes.get('isSystemFont', true, DataType.BOOLEAN, label, AttributeChangeOptions.zero);
        this.p_family = this.attributes.get('family', 'Arial', DataType.STRING, label, AttributeChangeOptions.zero);
        this.p_size = this.attributes.get('size', 10, DataType.NUMBER_DOUBLE, label, AttributeChangeOptions.zero);
        this.p_style = this.attributes.get('style', 0, DataType.NUMBER_INT, label, AttributeChangeOptions.zero);
        this.p_weight = this.attributes.get('weight', 400, DataType.NUMBER_INT, label, AttributeChangeOptions.zero);
    }


    get isSystemFont():boolean { return this.p_isSystemFont.value; }
    set isSystemFont(value:boolean) { this.p_isSystemFont.set(value, AttributeChangeOptions.save.durationZero); }
    protected p_isSystemFont:IAttribute;


    get family():string { return this.p_family.value; }
    set family(value:string) { this.p_family.set(value, AttributeChangeOptions.save.durationZero); }
    protected p_family:IAttribute;


    get size():number { return this.p_size.value; }
    set size(value:number) { this.p_size.set(value, AttributeChangeOptions.save.durationZero); }
    protected p_size:IAttribute;


    get style():FontStyle { return this.p_style.value; }
    get styleString():string {
        let style:string = 'normal';
        switch (this.p_style.value) {
            case FontStyle.ITALIC:
                style = 'italic';
                break;
            case FontStyle.OBLIQUE:
                style = 'oblique';
                break;
            default:
                style = 'normal';
                break;
        }

        return style;
    }
    set style(value:FontStyle) { this.p_style.set(value, AttributeChangeOptions.save.durationZero); }
    protected p_style:IAttribute;


    get weight():FontWeight { return this.p_weight.value; }
    get weightString():string {
        let weight:string = 'normal';
        switch (this.p_weight.value) {
            case FontWeight.BLACK:
                weight = '900';
                break;
            case FontWeight.BOLD:
                weight = 'bold';
                break;
            case FontWeight.EXTRA_BOLD:
                weight = '800';
                break;
            case FontWeight.EXTRA_LIGHT:
                weight = '200';
                break;
            case FontWeight.HEAVY:
                weight = '1000';
                break;
            case FontWeight.LIGHT:
                weight = '300';
                break;
            case FontWeight.MEDIUM:
                weight = '500';
                break;
            case FontWeight.SEMI_BOLD:
                weight = '600';
                break;
            case FontWeight.THIN:
                weight = '100';
                break;
            default:
                weight = 'normal';
                break;
        }

        return weight;
    }
    set weight(value:FontWeight) { this.p_weight.set(value, AttributeChangeOptions.save.durationZero); }
    protected p_weight:IAttribute;


    get fontName():string {
        if (this.isSystemFont) return this.family;
        else return this.family + '-' + this.style + '-' + this.weight;
    }
} // End Class


export default StyleFont;
