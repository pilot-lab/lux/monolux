export const enum FontWeight {
    THIN = 100,
    EXTRA_LIGHT = 200,
    LIGHT = 300,
    NORMAL = 400,
    MEDIUM = 500,
    SEMI_BOLD = 600,
    BOLD = 700,
    EXTRA_BOLD = 800,
    BLACK = 900,
    HEAVY = 1000
} // End enum


export const enum FontStyle {
    NORMAL = 0,
    ITALIC = 1,
    OBLIQUE = 2
} // End enum
