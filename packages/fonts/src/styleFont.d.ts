import { AttributeCollection, Attributes, IAttribute } from '@pilotlab/data';
import { FontWeight, FontStyle } from './fontEnums';
export declare class StyleFont extends AttributeCollection {
    constructor(data?: (StyleFont | AttributeCollection | Attributes | Object | string), label?: string);
    isSystemFont: boolean;
    protected p_isSystemFont: IAttribute;
    family: string;
    protected p_family: IAttribute;
    size: number;
    protected p_size: IAttribute;
    style: FontStyle;
    readonly styleString: string;
    protected p_style: IAttribute;
    weight: FontWeight;
    readonly weightString: string;
    protected p_weight: IAttribute;
    readonly fontName: string;
}
export default StyleFont;
