import is from '@pilotlab/is';
import { IRectangle, Cube } from '@pilotlab/data';
import { Signal } from '@pilotlab/signals';
import { Font, FontMetricsMeasureText } from '../components/fontjs/font';
import { FontMetrics } from './fontMetrics';


export class FontDescription {
    constructor(name:string = 'Arial', src?:string) {
        this.p_font = new Font();
        this.p_font.onload = ():void => { this.p_isLoaded = true; this.loaded.dispatch(true); };
        this.p_font.onerror = ():void => { this.loaded.dispatch(false); };
        this.p_font.fontFamily = name;
        if (is.notEmpty(src)) this.p_font.src = src;
    }


    /*====================================================================*
     START: Properties
     *====================================================================*/
    protected p_font:Font;


    get isLoaded():boolean { return this.p_isLoaded; }
    protected p_isLoaded:boolean = false;


    get name():string { return this.p_font.fontFamily; }


    get src():string { return this.p_font.src; }
    set src(value:string) { this.p_font.src = value; }


    get metrics():FontMetrics {
        let metrics:FontMetrics = new FontMetrics(
            this.p_font.metrics.ascent,
            this.p_font.metrics.descent,
            this.p_font.metrics.leading,
            this.p_font.metrics.quadsize,
            this.p_font.metrics.weightclass
        );
        return metrics;
    }


    /*====================================================================*
     START: Signals
     *====================================================================*/
    loaded:Signal<boolean> = new Signal<boolean>(true);


    /*====================================================================*
     START: Methods
     *====================================================================*/
    measureText(text:string, fontSize:number):IRectangle {
        let rect:IRectangle = new IRectangle();
        if (is.notEmpty(this.p_font)) {
            let metrics:FontMetricsMeasureText = this.p_font.measureText(text, fontSize);
            rect = new Cube(metrics.bounds.minx, metrics.bounds.miny, 0, metrics.width, metrics.height, 0);
        }
        return rect;
    }
} // End of class


export default FontDescription;
