import { MapList } from '@pilotlab/collections';
import { Initializable } from '@pilotlab/initializable';
import { IPromise } from '@pilotlab/result';
import { FontDescription } from './fontDescription';
import { FontMetrics } from './fontMetrics';
import StyleFont from './styleFont';
export declare class FontManager extends Initializable {
    constructor();
    protected p_onInitializeStarted(result: IPromise<any>, args: any[]): IPromise<any>;
    registered: MapList<string, FontDescription>;
    private _registerOnInitialized;
    private _fontSVGDefs;
    readonly FontSVGDefs: SVGDefsElement;
    getFontMetrics(fontStyle: StyleFont): FontMetrics;
    registerFont(fontStyle: StyleFont, src: string): IPromise<StyleFont>;
}
export default FontManager;
