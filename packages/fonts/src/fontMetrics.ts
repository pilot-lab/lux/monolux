import { FontWeight } from './fontEnums';


export class FontMetrics {
    constructor(ascent?:number, descent?:number, leading?:number, unitsPerEm?:number, weightClass?:number) {
        this.ascent = ascent;
        this.descent = descent;
        this.leading = leading;
        this.unitsPerEm = unitsPerEm;
        this.weightClass = weightClass;
    }


    ascent:number;
    descent:number;
    leading:number;


    /**
     * See: https://www.microsoft.com/typography/otspec/TTCH01.htm
     */
    unitsPerEm:number;


    /**
     * See: http://www.microsoft.com/typography/otspec/os2.htm#wtc
     */
    weightClass:FontWeight;
} // End class


export default FontMetrics;
