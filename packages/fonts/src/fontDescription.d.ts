import { IRectangle } from '@pilotlab/data';
import { Signal } from '@pilotlab/signals';
import { Font } from '../components/fontjs/font';
import { FontMetrics } from './fontMetrics';
export declare class FontDescription {
    constructor(name?: string, src?: string);
    protected p_font: Font;
    readonly isLoaded: boolean;
    protected p_isLoaded: boolean;
    readonly name: string;
    src: string;
    readonly metrics: FontMetrics;
    loaded: Signal<boolean>;
    measureText(text: string, fontSize: number): IRectangle;
}
export default FontDescription;
