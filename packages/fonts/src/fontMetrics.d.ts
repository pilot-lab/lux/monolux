import { FontWeight } from './fontEnums';
export declare class FontMetrics {
    constructor(ascent?: number, descent?: number, leading?: number, unitsPerEm?: number, weightClass?: number);
    ascent: number;
    descent: number;
    leading: number;
    unitsPerEm: number;
    weightClass: FontWeight;
}
export default FontMetrics;
