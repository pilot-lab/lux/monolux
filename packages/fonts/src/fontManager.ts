import is from '@pilotlab/is';
import { MapList } from '@pilotlab/collections';
import { Initializable } from '@pilotlab/initializable';
import { IPromise, Result } from '@pilotlab/result';
import { FontDescription } from './fontDescription';
import { FontMetrics } from './fontMetrics';
import StyleFont from './styleFont';


export class FontManager extends Initializable {
    constructor() {
        super();
        this.registered = new MapList<string, FontDescription>();
        this.initialize();
    }


    protected p_onInitializeStarted(result:IPromise<any>, args:any[]):IPromise<any> {
        Result.all(this._registerOnInitialized).then((returnList:any[]) => {
            let isSuccess:boolean = is.notEmpty(returnList);
            result.resolve(isSuccess);
        });

        return result;
    }


    /*====================================================================*
     STAT: Properties
     *====================================================================*/
    registered:MapList<string, FontDescription>;


    private _registerOnInitialized:IPromise<StyleFont>[] = [];
    private _fontSVGDefs:SVGDefsElement;
    get FontSVGDefs():SVGDefsElement { return this._fontSVGDefs; }


    /*====================================================================*
     START: Static Methods
     *====================================================================*/
    getFontMetrics(fontStyle:StyleFont):FontMetrics {
        let font:FontDescription = this.registered.get(fontStyle.fontName);
        if (is.notEmpty(font)) {
            return font.metrics;
        } else {
            let metrics:FontMetrics = new FontMetrics();
            let textSpan = document.createElement('span');
            textSpan.innerText = 'Hg';
            textSpan.style.fontFamily = fontStyle.family;
            textSpan.style.fontSize = fontStyle.size.toString() + 'px';
            textSpan.style.fontWeight = fontStyle.weightString;
            textSpan.style.fontStyle = fontStyle.styleString;

            let block = document.createElement('div');
            block.style.display = 'inline-block';
            block.style.height = '0px';
            block.style.width = '1px';

            let div = document.createElement('div');
            div.appendChild(textSpan);
            div.appendChild(block);

            document.body.appendChild(div);

            try {
                block.style.verticalAlign = 'baseline';
                metrics.ascent = block.offsetTop - textSpan.offsetTop;

                block.style.verticalAlign = 'bottom';
                let ascentPlusDescent:number = block.offsetTop - textSpan.offsetTop;

                metrics.descent = ascentPlusDescent - metrics.ascent;
            } finally { document.body.removeChild(div); }

            return metrics;
        }
    }


    /*====================================================================*
     START: Public Methods
     *====================================================================*/
    registerFont(fontStyle:StyleFont, src:string):IPromise<StyleFont> {
        let result:IPromise<StyleFont> = new Result<StyleFont>();
        fontStyle.isSystemFont = false;
        let fontName:string = fontStyle.fontName;
        let font:FontDescription = new FontDescription(fontName, src);

        this.registered.set(fontName, font);
        if (!this.isInitialized) this._registerOnInitialized.push(result);

        let xmlns:string = 'http://www.w3.org/2000/svg';
        let style:SVGStyleElement;

        if (this._fontSVGDefs == null) {
            this._fontSVGDefs = <SVGDefsElement>document.createElementNS(xmlns, 'defs');
            style = <SVGStyleElement>document.createElementNS(xmlns, 'style');
            style.setAttribute('type', 'text/css');
        } else {
            style = <SVGStyleElement>this._fontSVGDefs.getElementsByTagNameNS(xmlns, "style")[0]
        }

        let fontFace =
            "  @font-face {\n" +
            "    font-family: " + fontStyle.family + ";\n" +
            "    font-style: " + fontStyle.styleString + ";\n" +
            "    font-weight: " + fontStyle.weightString + ";\n" +
            "    src: url('" + src + "');\n" +
            "  }\n";

        style.textContent += fontFace;

        this._fontSVGDefs.appendChild(style);

        font.loaded.listen((isSuccess:boolean) => {
            if (isSuccess) result.resolve(fontStyle);
            else result.reject('Font ' + fontName + ' could not be loaded.');
        });
        return result;
    }
} // End of class


export default FontManager;
