import is from '@pilotlab/is';
import { Debug } from '@pilotlab/debug';
import { Result, IPromise } from '@pilotlab/result';


import {
    AsyncTest,
    Setup,
    SetupFixture,
    Teardown,
    TeardownFixture,
    Test,
    TestCase,
    TestFixture,
    Timeout,
    Expect
} from 'alsatian';


import {
    IData,
    IDataOwner,
    IDataSetter,
    IDataStore,
    Data,
    KeyAutoGenerationType,
    DataSetter
} from '../index';
import {
    AttributeCreateOptions,
    DataType,
    IAttribute,
    IAttributes,
    IAttributeSetReturn,
    AttributeEventArgs,
    AttributeCollection,
    AttributeChangeOptions,
    AttributeRoot,
    AttributeRange,
    IColor,
    Color,
    Point,
    AttributeDouble
} from '../index';


@TestFixture("Data Tests")
export class Tests {
    @TestCase(null)
    @AsyncTest("attribute types")
    @Timeout(5000) // Alsatian will now wait 5 seconds before failing
    async testTypes(testValue:any) {
        const promise:Promise<any> = new Promise((resolve, reject) => {
            class Testy extends AttributeCollection {
                position:Point;
                offset:Point;
                color:Color;

                initialize(data?:(IAttributes | Object | string)):IPromise<any> {
                    if (is.empty(data)) data = {
                        position: { x: 23, y: 42, z: 51 },
                        offset: { key: 'offset', value: { x: 20, y:40 }, dataType: DataType.POINT, label: 'User Name' },
                        color: { r: 120, g: 130, b: 140 }
                    };

                    return super.initialize(data);
                }
            }

            const o:Testy = new Testy('testy', null, 'Attribute Types', false, false);
            o.initialize({
                position: { x: 67, y: 42, z: 51 },
                offset: { key: 'offset', value: { x: 20, y:40 }, dataType: DataType.POINT, label: 'User Name' },
                color: { r: 120, g: 130, b: 140 }
            });


            o.position.initialized.listenOnce(() => {
                console.log(`o.position.x: ${o.position.x}`);
            });

            console.log(o.value.get('position').toObject());
            console.log(`o.value.get('position').value.get('x').value: ${o.value.get('position').value.get('x').value}`);
            console.log(`typeof o.value.get('position'): ${o.value.get('position') instanceof Point}`);
            console.log(`o.value.get('position').x: ${o.value.get('position').x}`);

            // console.log(o.toObject());
            console.log(`o.color.g: ${o.color.g}`);
            let returnValue:IAttributeSetReturn = o.position.attributes.set('x', 520, DataType.NUMBER, null, null, AttributeChangeOptions.signal);
            console.log(`isChanged: ${returnValue.isChanged}, o.position.x: ${returnValue.attribute.value}`);

            // console.log(`o.attributes.get('point').x: ${(<Point>o.attributes.get('point')).attributes.get('x').value}`);

            o.position.changed.listen((args:AttributeEventArgs<Point>) => {
                console.log(`point changed, attribute: ${args.node.key}, value: ${args.node.value}`);
            });
            o.position.x = 1280;

            resolve();
        });

        const result:any = await promise;
    }

    @TestCase(null)
    @AsyncTest("Data instantiation")
    @Timeout(5000) // Alsatian will now wait 5 seconds before failing
    async testInstantiation(testValue:any) {
        const promise:Promise<any> = new Promise((resolve, reject) => {
            const d:Data = new Data({
                position: { x: 67, y: 42, z: 51 },
                offset: { key: 'offset', value: { x: 20, y:40 }, dataType: DataType.POINT, label: 'User Name' },
                color: { r: 120, g: 130, b: 140 }
            });

            Debug.data(d.toObject());

            const d2:Data = new Data({name: 'billy bob', age: 24, isMarried: true, pet: { type: 'dog', name: 'squatchie', color: 'blue' }});

            d2.changed.listen((args:AttributeEventArgs<Data>) => {
                Debug.log(`data was changed. key: ${args.node.key}, value: ${args.node.value}`);
            });

            d2.attributes.get('age').changed.listen((args:AttributeEventArgs<AttributeDouble>) => {
                Debug.info(`Age was changed. The new age is ${args.node.value}`);
            });

            Debug.data(d2.attributes.toObject());

            d2.update({name: 'Martha Ray', age: 52, isMarried: false});
            Debug.data(d2.attributes.toObject());

            d2.update({age: 41, isMarried: 'boo', pet: { type: 'cat' }});
            Debug.data(d2.attributes.toObject());

            d2.attributes.set('pet/color', 'purple');
            Debug.data(d2.attributes.toObject());

            resolve();
        });

        const result:any = await promise;
    }


    @TestCase()
    @AsyncTest("Creating attributes")
    @Timeout(5000) // Alsatian will now wait 5 seconds before failing
    async testCreating(testValue:any) {
        const promise:Promise<any> = new Promise((resolve, reject) => {
            const data:Data = new Data();

            const projects:IAttribute = data.attributes.get('projects', new AttributeCreateOptions(null, DataType.COLLECTION, 'My Projects'));
            const project01:IAttribute = projects.attributes.get('project01', new AttributeCreateOptions(null, DataType.COLLECTION, 'Project 01'));
            const project02:IAttribute = projects.attributes.get('project02', new AttributeCreateOptions(null, DataType.COLLECTION, 'Project 02'));
            const project01Name:IAttribute = project01.attributes.get('project01Name', new AttributeCreateOptions('The coolest project around', DataType.STRING, 'Project Name'));

            const color01:Color = new Color([255, 0, 0]);
            Debug.log(color01.value.get('r').dataType.toString());
            const objColor01:any = color01.attributes.toObject();
            Debug.data(objColor01);
            const color02:Color = project01.attributes.get('color02', new AttributeCreateOptions(new Color([120, 90, 51]), DataType.COLOR, 'Label Color'));
            Debug.data(color02.toObject());
            const color03:Color = <Color>Color.create.fromAny({dataType: 'color', value: { r: 62, g: 100, b: 0 } });
            Debug.log(`color03.r: ${color03.r}`);
            Debug.data(color03.r);

            const color01a:Color = <Color>Color.create.fromObject({ dataType: 'color', value: objColor01 });
            Debug.data(color01a.toObject());

            Debug.info('objColor01:');
            Debug.data(objColor01);
            const color04:Color = project01.attributes.get('color04', new AttributeCreateOptions(color01a, DataType.COLOR, 'Label Color'));
            Debug.log(color04.value.get('r').dataType.toString());
            Debug.log(color04.r.toString());

            const point:IAttribute = project01.attributes.get('point', new AttributeCreateOptions(Point.create.fromObject({ dataType: 'point', value: {x: 100, y: 25} }), DataType.POINT, 'Position'));

            Debug.data(data.attributes.toObject());
            Debug.data(data.attributes.toObject(true, true));

            resolve();
        });

        const result:any = await promise;
    }


    @TestCase()
    @AsyncTest("Range")
    @Timeout(5000) // Alsatian will now wait 5 seconds before failing
    async testRange(testValue:any) {
        const promise:Promise<any> = new Promise((resolve, reject) => {
            const range:AttributeRange = new AttributeRange([20, 0, 30]);

            Debug.log(`is range empty: ${range.isEmpty}`);

            Debug.log(`min: ${range.min}, max: ${range.max}, current: ${range.current}, step: ${range.step}, stepped: ${range.stepped}`);

            range.min = 100;
            range.max = 10;
            range.min = 200;
            range.max = 500;
            range.current = 32.56;
            range.step = 5;

            Debug.log(`min: ${range.min}, max: ${range.max}, current: ${range.current}, step: ${range.step}, stepped: ${range.stepped}`);

            range.min = -10000;
            range.max = 10000;
            range.current = 28.25;
            range.step = 20;

            Debug.log(`min: ${range.min}, max: ${range.max}, current: ${range.current}, step: ${range.step}, stepped: ${range.stepped}`);

            range.current = 32.5;

            Debug.log(`min: ${range.min}, max: ${range.max}, current: ${range.current}, step: ${range.step}, stepped: ${range.stepped}`);

            resolve();
        });

        const result:any = await promise;
    }
}
