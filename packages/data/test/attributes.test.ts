import {
    AsyncTest,
    Setup,
    SetupFixture,
    Teardown,
    TeardownFixture,
    Test,
    TestCase,
    TestFixture,
    Timeout,
    Expect
} from 'alsatian';


import {
    NodeEventArgs,
    AttributeBase,
    AttributesBase,
    Attribute,
    AttributeChangeActions,
    Attributes,
    AttributesFactoryBase,
    AttributesFactory,
    AttributeUpdateTracker,
    AttributeCreateOptions,
    DataType,
    AttributesWrapType,
    AttributeEventArgs,
    AttributeFactoryBase,
    AttributeFactory,
    AttributeRange,
    AttributeReader,
    AttributeRoot,
    AttributeSetReturn,
    DataTools,
    AttributeChangeOptions,
    IAttribute,
    IAttributeChangeOptions,
    IAttributes,
    IAttributeUpdateTracker,
    IAttributeFactory,
    IAttributesFactory,
    IAttributeSetReturn,
    AttributeBoolean,
    AttributeCollection,
    AttributeDouble,
    AttributeInt,
    AttributeStringJson,
    AttributeString,
    SortDirection,
    Sorters,
    Color,
    Cube,
    Point,
    Size,
    Vector,
    IColor,
    ICoordinateSystem,
    ICube,
    IMatrix2,
    IMatrix3,
    IMatrix4,
    IPoint,
    IPoint3D,
    IQuaternion,
    IRectangle,
    ISize,
    ISize3D,
    IType,
    IVector,
    Types
} from '../index';


import Debug from '@pilotlab/debug';
import is from '@pilotlab/is';
import { IPromise, Result } from '@pilotlab/result';


import { IAnimationEventArgs } from '@pilotlab/animation';


@TestFixture("Attribute Tests")
export class Tests {
    @TestCase(null)
    @Test("Detect data types")
    testDataTypeDetection() {
        const attribute:IAttribute = new Attribute(10, DataType.NUMBER_DOUBLE);
        Debug.log(`data type: ${Types.isColor({ r: 100, g: 100, b: 100 })}`);
        Debug.log(`data type: ${Types.isPoint({ x: 10, y: 10 })}`);
    }

    @TestCase(null)
    @Test("instantiation")
    test00() {
        const attribute:IAttribute = new AttributeDouble(10, 'My number', 'num');
        Expect(attribute.key).toBe('num');
        Expect(attribute.value).toBe(10);
        Expect(attribute.dataType).toBe(DataType.NUMBER_DOUBLE);
        Expect(DataTools.getDataType(attribute.value)).toBe(DataType.NUMBER);

        const attributeBoolean:IAttribute = new AttributeBoolean(true, 'is that thing truee', 'attributeBoolean');
        Expect(attributeBoolean.value).toBe(true);
        Expect(attributeBoolean.key).toBe('attributeBoolean');

        const attributeRoot:AttributeRoot = new AttributeRoot(null, 'root');
        Expect(attributeRoot.key).toBe('root');
        Expect(attributeRoot.isRoot).toBe(true);
    }

    @TestCase(null)
    @TestCase(undefined)
    @TestCase()
    @Test("is empty true")
    testIsEmptyTrue(testValue:any) {
        let attribute:IAttribute = new Attribute(testValue);

        Expect(is.empty(attribute.value)).toBe(true);
        Expect(attribute.isEmpty).toBe(true);
    }


    @TestCase(null)
    @AsyncTest("set")
    @Timeout(5000) // Alsatian will now wait 5 seconds before failing
    async test02(testValue:any) {
        const promise:Promise<any> = new Promise((resolve, reject) => {
            const attributeString:IAttribute = new AttributeString('boo', 'my string label');

            attributeString.isSignalChange = true;
            attributeString.changed.listen(() => {
                Debug.log(`new value of attributeString: ${attributeString.value}`);
                resolve();
            });

            attributeString.set('foo');
            attributeString.set('bar', new AttributeChangeOptions(AttributeChangeActions.NONE));
        });

        const result:any = await promise;
    }


    @TestCase(null)
    @AsyncTest("factory")
    @Timeout(5000) // Alsatian will now wait 5 seconds before failing
    async test03(testValue:any) {
        const promise:Promise<any> = new Promise((resolve, reject) => {
            let factory:AttributeFactory = new AttributeFactory();
            let attribute:any = factory.fromObject({ value: 'boo', dataType: DataType.STRING });
            Debug.log(`value: ${attribute.value}, dataType: ${attribute.dataType}`);

            let factoryCollection:AttributesFactory = new AttributesFactory(factory);
            let attribute02:any = factoryCollection.node.fromObject({ value: 10, dataType: DataType.NUMBER });
            Debug.log(`value: ${attribute02.value}, dataType: ${attribute02.dataType}`);

            let attribute2:IAttribute = new Attribute(new Attributes(), DataType.COLLECTION);
            attribute2.attributes.set('r', 22);
            Debug.log('r: ' + attribute2.attributes.get('r').value);

            resolve();
        });

        const result:any = await promise;
    }


    @TestCase(null)
    @AsyncTest("attribute collection")
    @Timeout(5000) // Alsatian will now wait 5 seconds before failing
    async test04(testValue:any) {
        const promise:Promise<any> = new Promise((resolve, reject) => {
            const attribute:any = new AttributeCollection({ stringy: "boo" });
            Debug.data(attribute.toObject());

            Debug.log(`dataType: ${DataTools.getDataType(255).toString()}`);

            const color2:Color = new Color([0, 200, 0]);
            const color:Color = <Color>attribute.attributes.get('color01', new AttributeCreateOptions(color2.attributes, DataType.COLOR, 'Color 1'));
            const color3:Color = <Color>attribute.attributes.set('color01', color2, DataType.COLOR, 'Color 1').attribute;
            Debug.log(`g1: ${color.g}, g2: ${color2.g}, g3: ${color3.g}`);
            Debug.log(`color.r: ${color.r}`);
            Debug.data(color.toObject());

            resolve();
        });

        const result:any = await promise;
    }


    @TestCase(null)
    @AsyncTest("dynamic getters and setters")
    @Timeout(5000) // Alsatian will now wait 5 seconds before failing
    async test05(testValue:any) {
        const promise:Promise<any> = new Promise((resolve, reject) => {
            class Testy extends AttributeCollection {
                name:string;

                // initialize(data?:(IAttributes | Object | string)):IPromise<any> {
                //     if (is.empty(data)) data = {
                //         name: 'Jake'
                //     };
                //
                //     return super.initialize(data);
                // }
            }

            const o:Testy = new Testy({ name: 'Bill' }, 'testy', 'Attribute Types', false, false);
            o.initialize({
                name: 'Jennifer'
            });

            Debug.data(o.attributes.get('name').toObject());

            Debug.log(`o.value.get('name'): ${o.value.get('name').value}`);

            resolve();
        });

        const result:any = await promise;
    }


    @TestCase(null)
    @AsyncTest("attribute types")
    @Timeout(5000) // Alsatian will now wait 5 seconds before failing
    async testTypes(testValue:any) {
        const promise:Promise<any> = new Promise((resolve, reject) => {
            class Testy extends AttributeCollection {
                position:Point;
                offset:Point;
                color:Color;

                initialize(data?:(IAttributes | Object | string)):IPromise<any> {
                    if (is.empty(data)) data = {
                        position: { x: 23, y: 42, z: 51 },
                        offset: { key: 'offset', value: { x: 20, y:40 }, dataType: DataType.POINT, label: 'User Name' },
                        color: { r: 120, g: 130, b: 140 }
                    };

                    return super.initialize(data);
                }
            }

            const o:Testy = new Testy('testy', null, 'Attribute Types', false, false);
            o.initialize({
                position: { x: 67, y: 42, z: 51 },
                offset: { key: 'offset', value: { x: 20, y:40 }, dataType: DataType.POINT, label: 'User Name' },
                color: { r: 120, g: 130, b: 140 }
            });

            Debug.log(`o.position.attributes.get('x').value: ${o.position.attributes.get('x').value}`);
            o.position.initialized.listenOnce(() => {
                Debug.log(`o.position.x on initialized: ${o.position.x}`);
            });
            Debug.log(`o.position.x outside initialized: ${o.position.x}`);

            Debug.data(o.value.get('position').toObject());
            Debug.log(`o.value.get('position').value.get('x').value: ${o.value.get('position').value.get('x').value}`);
            Debug.log(`typeof o.value.get('position'): ${o.value.get('position') instanceof Point}`);
            Debug.log(`o.value.get('position').x: ${o.value.get('position').x}`);

            Debug.data(o.attributes.toObject());
            Debug.log(`o.color.g: ${o.color.g}`);
            let returnValue:IAttributeSetReturn = o.position.attributes.set('x', 520, DataType.NUMBER, null, null, AttributeChangeOptions.signal);
            Debug.log(`isChanged: ${returnValue.isChanged}, o.position.x: ${returnValue.attribute.value}`);

            o.position.isSignalChange = true;
            o.position.changed.listen((args:AttributeEventArgs<Point>) => {
                Debug.log(`point changed, attribute: ${args.node.key}, value: ${args.node.value}`);
            });
            o.position.x = 1280;

            resolve();
        });

        const result:any = await promise;
    }


    @TestCase(null)
    @AsyncTest("particle class")
    @Timeout(5000) // Alsatian will now wait 5 seconds before failing
    async testParticles(testValue:any) {
        const promise:Promise<any> = new Promise((resolve, reject) => {
            class Particle extends AttributeCollection {
                // constructor(data?:(IAttributes | Object | string)) {
                //     super(null, null, null, false, true);
                //     // this.attributes.set('position', new Point(23, 42, 51), DataType.POINT_3D, null, null, AttributeChangeOptions.noSaveOrSignal);
                //     // this.attributes.set('offset', { x: 20, y: 40 }, DataType.POINT, null, null, AttributeChangeOptions.noSaveOrSignal);
                //     // this.attributes.set('color', new Color(120, 130, 140), DataType.COLOR, null, null, AttributeChangeOptions.noSaveOrSignal);
                //     // if (is.notEmpty(data)) this.attributes.update(data)
                // }

                // position:Point;
                // offset:Point;
                // color:Color;
            }

            const p:any = new AttributeCollection({
                position: { x: 67, y: 42, z: 51 },
                offset: { value: { x: 20, y:40 }, dataType: DataType.POINT, label: 'User Name' },
                color: { r: 120, g: 130, b: 140 }
            });

            p.position.initialized.listenOnce(() => {
                Debug.log(`p.position.x: ${p.position.x}`);
            });

            Debug.log(`p.offset.y: ${p.offset.y}`);
            Debug.log(`p.offset.label: ${p.offset.label}`);

            p.attributes.deleteByKey('offset');

            Debug.log('deleted offset');
            Debug.data(p.toObject());

            // Debug.data(p.value.get('position').toObject());
            // Debug.log(`p.value.get('position').value.get('x').value: ${p.value.get('position').value.get('x').value}`);
            // Debug.log(`p.value.get('position') instanceof Point}: ${p.value.get('position') instanceof Point}`);
            // Debug.log(`p.value.get('position').x: ${p.value.get('position').x}`);
            //
            // Debug.data(p.toObject());
            // Debug.log(`p.color.g: ${p.color.g}`);
            // let returnValue:IAttributeSetReturn = p.position.attributes.set('x', 520, DataType.NUMBER, null, null, AttributeChangeOptions.signal);
            // Debug.log(`isChanged: ${returnValue.isChanged}, p.position.x: ${returnValue.attribute.value}`);
            //
            // p.position.isSignalChange = true;
            // p.position.changed.listen((args:AttributeEventArgs<Point>) => {
            //     Debug.log(`point changed, attribute: ${args.node.key}, value: ${args.node.value}`);
            // });
            // p.position.x = 1280;
            //
            // const p2:Particle = new Particle();
            // Debug.log(`p2.position.x: ${p2.position.x}`);
            // Debug.log(`p.offset.y: ${p.offset.y}`);

            resolve();
        });

        const result:any = await promise;
    }


    @TestCase(null)
    @AsyncTest("tools")
    @Timeout(5000) // Alsatian will now wait 5 seconds before failing
    async testTools(testValue:any) {
        const promise:Promise<any> = new Promise((resolve, reject) => {
            Debug.log(`DataTools.getDataType({ age: 24 }): ${DataTools.getDataType({ age: 24 })}`);

            resolve();
        });

        const result:any = await promise;
    }


    @TestCase(null)
    @AsyncTest("animation")
    @Timeout(5000) // Alsatian will now wait 5 seconds before failing
    async testAnimation(testValue:any) {
        const promise:Promise<any> = new Promise((resolve, reject) => {
            let attribute:AttributeDouble = new AttributeDouble(0, 'animation test', 'test00');
            let returnValue:IAttributeSetReturn = attribute.set(100, new AttributeChangeOptions(AttributeChangeActions.SIGNAL_CHANGE, 3));
            Debug.log(`isChanged: ${returnValue.isChanged}`);
            Debug.log(`returnValue.animation.animations.length: ${returnValue.animation.animations.size}`);

            if (returnValue.animation.animations.size > 0) {
                returnValue.animation.animations.item(0).ticked.listen((args:IAnimationEventArgs) => { Debug.info(`${attribute.label}: ${args.values[0].current}`)});
                returnValue.animation.animations.item(0).completed.listen(() => { Debug.log(`animation complete`); resolve(); });
            } else resolve();
        });

        const result:any = await promise;
    }


    @TestCase(null)
    @AsyncTest("int")
    @Timeout(5000) // Alsatian will now wait 5 seconds before failing
    async testInt(testValue:any) {
        const promise:Promise<any> = new Promise((resolve, reject) => {
            let attributeInt:AttributeInt = new AttributeInt(23, 'int attribute', 'int00');

            Debug.data(attributeInt.toObject());

            attributeInt.set(300);

            Debug.log(`${attributeInt.label}: ${attributeInt.value}`);

            attributeInt.set(112);
            Debug.log(`${attributeInt.label}: ${attributeInt.value}`);

            Debug.data(attributeInt.toObject());

            resolve();
        });

        const result:any = await promise;
    }


    @TestCase(null)
    @AsyncTest("collection")
    @Timeout(5000) // Alsatian will now wait 5 seconds before failing
    async testCollection(testValue:any) {
        const promise:Promise<any> = new Promise((resolve, reject) => {
            let attributeCollection:AttributeCollection = new AttributeCollection();
            let attributeItem1:IAttribute = attributeCollection.attributes.set('item_1_0', new Attributes(), DataType.COLLECTION, 'item 1.0').attribute;
            attributeItem1.attributes.set('item_1_1', 100, DataType.NUMBER, 'item 1.1');

            Debug.log(`key: ${attributeItem1.key}`);
            Debug.log(`attributes.size: ${attributeItem1.attributes.list.size}`);
            Debug.data(attributeCollection.toObject());

            attributeCollection = new AttributeCollection({
                item_1_0: {
                    label: 'item 1.0',
                    value: {
                        item_1_1: {
                            label: 'item 1.1',
                            value: 100,
                            dataType: 'number'
                        }
                    },
                    dataType: 'collection'
                }
            });

            Debug.data(attributeCollection.toObject());

            let data:any = [
                {
                    key: 'item1',
                    dataType: 'collection',
                    label: 'item 1',
                    value: [
                        { label: 'item 1_1', value: 11 },
                        {
                            key: 'item1_2',
                            label: 'item 1_2',
                            dataType: 'collection',
                            value: {
                                item_1_2_1: { dataType: 'number_double', label: 'A cool number', value: 121 },
                                item_1_2_2: 122
                            }
                        },
                        { label: 'item 1_3', value: 13 }
                    ]
                },
                {
                    key: 'item2',
                    label: 'item 2',
                    value: [
                        { label: 'item 2_1', value: 21 },
                        { label: 'item 2_2', value: 22 }
                    ]
                },
                { label: 'item 3', value: 3 }
            ];

            attributeCollection = new AttributeCollection(data);
            attributeCollection.key = 'mykey';
            attributeCollection.label = 'A great label';
            attributeCollection.attributes.update(data).then(() => {
                Debug.log('attributeCollection');
                Debug.data(attributeCollection.attributes.toObject(true, true, true));
            });

            Debug.log(`attributeCollection.attributes.array.length: ${attributeCollection.attributes.array.length}`);

            resolve();
        });

        const result:any = await promise;
    }
}
