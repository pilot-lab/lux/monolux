import is from '@pilotlab/is';
import { Debug } from '@pilotlab/debug';
import { Result, IPromise } from '@pilotlab/result';


import {
    AsyncTest,
    Setup,
    SetupFixture,
    Teardown,
    TeardownFixture,
    Test,
    TestCase,
    TestFixture,
    Timeout,
    Expect
} from 'alsatian';


import {
    IData,
    IDataOwner,
    IDataSetter,
    IDataStore,
    Data,
    KeyAutoGenerationType,
    DataSetter
} from '../index';
import {
    AttributeCreateOptions,
    DataType,
    IAttribute,
    IAttributes,
    IAttributeSetReturn,
    AttributeEventArgs,
    AttributeCollection,
    AttributeChangeOptions,
    AttributeRoot,
    IColor,
    Color,
    Point
} from '../index';
import DataTypes from '../src/dataTypes';


@TestFixture("Factory Tests")
export class Tests {
    @TestCase()
    @AsyncTest("Factory")
    @Timeout(5000) // Alsatian will now wait 5 seconds before failing
    async test02(testValue:any) {
        const promise:Promise<any> = new Promise((resolve, reject) => {
            const dataTypes:DataTypes = new DataTypes();

            // dataManager.registerAttributeType(scopeColor, 'Color', ['color']);
            // let color:IColor = dataManager.instance('Color', new AttributeCreateOptions([160, 20, 42], DataType.CUBE, 'My Cool Color', null, 'color0011'));
            // console.log(color.toObject());
            // console.log(`label: ${color.label}, key: ${color.key}, dataType: ${color.dataType}`);

            // let instance = Object.create(scopeColor['Color'].prototype);
            // instance.constructor.apply(instance, [160, 20, 42]);
            // console.log(instance.toObject());

            // dataManager.registerAttributeType('Color');
            //
            // const color:IColor = dataManager.instance('Color', new AttributeCreateOptions({r: 150, g: 20, b: 240, a: 0.5}, DataType.COLOR, 'My Awesome Color', null, 'color001'));
            // Debug.data(color.toObject());

            resolve();
        });

        const result:any = await promise;
    }
}
