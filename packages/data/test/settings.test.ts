import {
    AsyncTest,
    Setup,
    SetupFixture,
    Teardown,
    TeardownFixture,
    Test,
    TestCase,
    TestFixture,
    Timeout,
    Expect
} from 'alsatian';


import {
    NodeEventArgs,
    AttributeBase,
    AttributesBase,
    Attribute,
    AttributeChangeActions,
    Attributes,
    AttributesFactoryBase,
    AttributesFactory,
    AttributeUpdateTracker,
    AttributeCreateOptions,
    DataType,
    AttributesWrapType,
    AttributeEventArgs,
    AttributeFactoryBase,
    AttributeFactory,
    AttributeRange,
    AttributeReader,
    AttributeRoot,
    AttributeSetReturn,
    DataTools,
    AttributeChangeOptions,
    IAttribute,
    IAttributeChangeOptions,
    IAttributes,
    IAttributeUpdateTracker,
    IAttributeFactory,
    IAttributesFactory,
    IAttributeSetReturn,
    AttributeBoolean,
    AttributeCollection,
    AttributeDouble,
    AttributeInt,
    AttributeSelect,
    AttributeStringJson,
    AttributeString,
    SortDirection,
    Sorters,
    Color,
    Cube,
    Point,
    Size,
    Vector,
    IColor,
    ICoordinateSystem,
    ICube,
    IMatrix2,
    IMatrix3,
    IMatrix4,
    IPoint,
    IPoint3D,
    IQuaternion,
    IRectangle,
    ISize,
    ISize3D,
    IType,
    IVector,
    Types,
    Data
} from '../index';


import Debug from '@pilotlab/debug';
import is from '@pilotlab/is';
import { IPromise, Result } from '@pilotlab/result';


import { IAnimationEventArgs } from '@pilotlab/animation';


@TestFixture("Attribute Tests: Settings")
export class Tests {
    @TestCase(null)
    @AsyncTest("settings")
    @Timeout(5000) // Alsatian will now wait 5 seconds before failing
    async testSettings(testValue:any) {
        const promise:Promise<any> = new Promise((resolve, reject) => {
            const velocityMax:number = 50;
            const sceneSettings:any = {
                presetLabels: {
                    label: 'preset',
                    dataType: 'select',
                    isEditable: true,
                    selected: 'rain',
                    value: {
                        rain: { label: 'rain', dataType: 'boolean', value: true },
                        bubbles: { label: 'bubbles', dataType: 'boolean', value: false },
                        depth: { label: 'depth mapping', dataType: 'boolean', value: false },
                        performance: { label: 'performance testing', dataType: 'boolean', value: false },
                    }
                },

                presets: { dataType: 'collection', isHidden: true, value: {} },

                spawns: {
                    label: 'spawns',
                    dataType: 'collection',
                    value: {
                        spawnInterval: {
                            label: 'spawn interval',
                            dataType: 'range',
                            value: { current: 0, min: 0, max: 6, step: 0 }
                        },
                        spawnsPerInterval: {
                            label: 'spawns per interval',
                            dataType: 'range',
                            value: { current: 10, min: 1, max: 30, step: 1 }
                        },
                        spawnXMin: {
                            label: 'spawn x min',
                            dataType: 'range',
                            value: { current: 0, min: 0, max: 1, step: 0 }
                        },
                        spawnXMax: {
                            label: 'spawn x max',
                            dataType: 'range',
                            value: { current: 1, min: 0, max: 1, step: 0 }
                        },
                        spawnYMin: {
                            label: 'spawn y min',
                            dataType: 'range',
                            value: { current: 0, min: 0, max: 1, step: 0 }
                        },
                        spawnYMax: {
                            label: 'spawn y max',
                            dataType: 'range',
                            value: { current: 1, min: 0, max: 1, step: 0 }
                        },
                    }
                },

                lifespan: {
                    label: 'lifespan',
                    dataType: 'collection',
                    value: {
                        lifespanMin: {
                            label: 'lifespan min',
                            dataType: 'range',
                            value: { current: 0, min: 0, max: 120, step: 0 }
                        },
                        lifespanMax: {
                            label: 'lifespan max',
                            dataType: 'range',
                            value: { current: 0, min: 0, max: 120, step: 0 }
                        },
                    }
                },

               size: {
                    label: 'size',
                    dataType: 'collection',
                    value: {
                       radiusMin: {
                           label: 'radius min',
                           dataType: 'range',
                           value: { current: 4, min: 1, max: 200, step: 0 }
                       },

                       radiusMax: {
                           label: 'radius max',
                           dataType: 'range',
                           value: { current: 56, min: 1, max: 200, step: 0 }
                       },
                   }
                },

                color: {
                    label: 'color',
                    dataType: 'collection',
                    value: {
                        hueMin: {
                            label: 'hue min',
                            dataType: 'range',
                            value: { current: 0, min: 0, max: 360, step: 1 }
                        },

                        hueMax: {
                            label: 'hue max',
                            dataType: 'range',
                            value: { current: 360, min: 0, max: 360, step: 1 }
                        },

                        hueCycleStep: {
                            label: 'hue cycle step',
                            dataType: 'range',
                            value: { current: 1, min: 0, max: 10, step: 1 }
                        },

                        saturation: {
                            dataType: 'range',
                            value: { current: 40, min: 0, max: 360, step: 1 }
                        },

                        brightness: {
                            dataType: 'range',
                            value: { current: 100, min: 0, max: 100, step: 1 }
                        },
                    }
                },

                physics: {
                    label: 'physics',
                    dataType: 'collection',
                    value: {
                        velocity: {
                            label: 'velocity',
                            dataType: 'collection',
                            value: {
                                velocityXMin: {
                                    label: 'x min',
                                    dataType: 'range',
                                    value: { current: 0, min: -velocityMax, max: velocityMax, step: 0 }
                                },

                                velocityXMax: {
                                    label: 'x max',
                                    dataType: 'range',
                                    value: { current: 0, min: -velocityMax, max: velocityMax, step: 0 }
                                },

                                velocityYMin: {
                                    label: 'y min',
                                    dataType: 'range',
                                    value: { current: 0, min: -velocityMax, max: velocityMax, step: 0 }
                                },

                                velocityYMax: {
                                    label: 'y max',
                                    dataType: 'range',
                                    value: { current: 0, min: -velocityMax, max: velocityMax, step: 0 }
                                },
                            }
                        },

                        wind: {
                            key: 'wind',
                            label: 'wind',
                            dataType: 'collection',
                            value: {
                                turbulenceScale: {
                                    label: 'turbulence scale',
                                    dataType: 'range',
                                    value: { current: 100, min: 1, max: 100, step: 0 }
                                },

                                turbulenceStrength: {
                                    label: 'turbulence strength',
                                    dataType: 'range',
                                    value: { current: 0, min: 0, max: 1, step: 0 }
                                },

                                velocityX: {
                                    label: 'velocity x',
                                    dataType: 'range',
                                    value: { current: 0, min: -1, max: 1, step: 0 }
                                },

                                velocityY: {
                                    label: 'velocity y',
                                    dataType: 'range',
                                    value: { current: 0, min: -1, max: 1, step: 0 }
                                },
                            }
                        },

                        boundaryConstraintType: {
                            label: 'constraints',
                            dataType: 'select',
                            value: {
                                block: { dataType: 'boolean', value: true },
                                kill: { dataType: 'boolean', value: false },
                            }
                        },

                        collisions: { dataType: 'boolean', value: false },
                    }
                },

                effects: { label: 'effects', value: {
                        trail: {
                            label: 'particle trails',
                            dataType: 'range',
                            value: { current: 0, min: 0, max: 1, step: 0 }
                        },
                    }
                },
            };
            const settings:AttributeCollection = new AttributeCollection();
            settings.attributes.update(sceneSettings);
            Debug.data(settings.attributes.toObject(true, true));

            const scenes:any = {
                scenes: {
                    label: 'scene',
                    dataType: 'collection',
                    isEditable: false,
                    value: {
                        particles: { label: 'particle storm', dataType: 'boolean', value: true },
                        bubbles: { label: 'bubbles', dataType: 'boolean', value: false },
                        depth: { label: 'depth mapping', dataType: 'boolean', value: false },
                        performance: { label: 'performance testing', dataType: 'boolean', value: false },
                    }
                },

                scene: { label: 'scene settings', dataType: 'collection', value: {} },
            };

            const data:AttributeCollection = new AttributeCollection({}, 'data one', 'data');//new Data(scenes, true, null, true, false, 'dataOne', 'data one');

            data.attributes.update(scenes);
            Debug.data(data.toObject());

            const scenes2:any = {
                scenes: {
                    label: 'scene A',
                    dataType: 'collection',
                    isEditable: true,
                    value: {
                        particles: { label: 'particle shower', dataType: 'boolean', value: false },
                        bubbles: { label: 'bubbledeeboo', dataType: 'boolean', value: true },
                        depth: { label: 'depth of field', dataType: 'boolean', value: false },
                        performance: { label: 'endurance', dataType: 'boolean', value: false },
                    }
                },

                scene: { label: 'what a scene', dataType: 'collection', value: {} },
            };

            data.attributes.get('scenes').attributes.get('particles').set(false);
            Debug.data(data.toObject());

            data.attributes.update(scenes2).then(() => {
                Debug.data(data.attributes.toObject());
            });

            data.key = 'data';
            Debug.log(`key: ${data.key}`);
            Debug.data(data.copy.toObject());

            resolve();
        });

        const result:any = await promise;
    }
}
