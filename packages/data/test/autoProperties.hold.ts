// import {
//     AsyncTest,
//     Setup,
//     SetupFixture,
//     Teardown,
//     TeardownFixture,
//     Test,
//     TestCase,
//     TestFixture,
//     Timeout,
//     Expect
// } from 'alsatian';
//
//
// import {
//     IData,
//     IDataOwner,
//     IDataSetter,
//     IDataStore,
//     Data,
//     KeyAutoGenerationType,
//     DataUpdateHandlerSequence,
//     DataSetter
// } from '../index';
//
//
// export class Data {
//     [key:string]:any;
// }
//
// export class DataPoint extends Data {
//     x:number;
//     y:number = 4;
// }
//
//
// export interface IModel<TData extends Data> {
//     data:TData;
// }
//
//
// export class Model<TData extends Data> implements IModel<TData> {
//     constructor(data:any = {}) {
//         this.data = <TData>data;
//
//         Object.keys(data).forEach((key:string) => {
//             Object.defineProperty(this, key, {
//                 get: () => { return this.data[key]; },
//                 set: (value) => { this.data[key] = value; },
//                 enumerable: true,
//                 configurable: true
//             });
//         });
//     }
//
//     data:TData;
//
//     toStringy():string {
//         let returnString:string = '';
//         Object.keys(this).forEach((key:string) => { returnString += this.data[key] + ' : ' });
//         return returnString;
//     }
// }
//
//
// export class PointTest {}
//
//
// export interface IAttributeCollection<TGet, TSet> {
//     get:TGet;
//     set:TSet;
// }
//
//
// function prop(target: Object, name: string) {
//     Object.defineProperty(target, name, {
//         get: function() { return this["_" + name]; },
//         set: function(value) { this["_" + name] = value; },
//         enumerable: true,
//         configurable: true
//     });
// }
//
// function createTest():any {
//     const data:any = {
//         attributes: [
//             {key: 'x', value: 2, dataType: 'number_double'},
//             {key: 'y', value: 6, dataType: 'number_double'}
//         ]
//     };
//
//     data.children.forEach((attribute:any) => {
//         Object.defineProperty(PointTest.prototype, attribute.key, {
//             get: function() { return this['_' + attribute.key]; },
//             set: function(value) { this['_' + attribute.key] = value; },
//             enumerable: true,
//             configurable: true
//         });
//
//         return true;
//     });
// }
//
//
// class Person {
//     @prop first:string = 'Kevin';
//     @prop last:string = 'Russ';
//
//     name() { return `${this.first} ${this.last}` }
// }
//
//
// function getProp<T, K extends keyof T>(obj:T, key:K) {
//     return obj[key];
// }
//
//
// // a function used to define targetObject's descriptor from SourceObject's properties
// function defineGetter(sourceObject:any, targetObject:any):any {
//     Object.keys(sourceObject).forEach(function (key) {
//         Object.defineProperty(targetObject.prototype, key, {
//             get: function () {
//                 return getProp(sourceObject, key);
//             },
//             enumerable: true,
//             configurable: true
//         })
//     });
//
//     return targetObject;
// }
//
//
// class Form {
//     name: string;
//     label: string;
//     value: any;
//     sequence: number;
//     // width: number;
//     type: number;
//
//     get info() {
//         return `name is ${this.name}, label is ${this.label}`;
//     }
// }
//
//
// @TestFixture("Attribute Tests")
// export class Tests {
//     @TestCase(2, 2)
//     @TestCase(2, 3)
//     @TestCase(3, 3)
//     @Test("addition tests")
//     addTest(x: number, y: number) {
//         let person = new Person();
//         console.log('name: ' + person.name());
//         Expect(person.name()).toBe('Kevin Russ');
//
//         // let attributeX:Attribute('x', x, DataType.NUMBER_DOUBLE);
//         // let attributeY:Attribute('y', y, DataType.NUMBER_DOUBLE);
//         // point.get =
//         //
//         // Expect(point.get.x === x && point.get.y === y).toBe(true);
//
//         let form1:Form = defineGetter({
//             name: '1',
//             label: '2',
//             value: 3,
//             sequence: 4,
//             width: 5,
//             type: 6
//         }, Form);
//
//         console.log(form1.info);
//     }
//
//
//     @Test("Model tests")
//     test02() {
//         let point:Model<DataPoint> = new Model<DataPoint>({x: 3, y: 4});
//         point.data.y = 2;
//
//         console.log('x: ' + point.data.x + ', y: ' + point.data.y);
//         Expect(point.data.x).toBe(3);
//
//         console.log(point.toStringy());
//
//         let point2 = new Model<any>({x: 3, y: 4});
//         point2.data.x = 3;
//         point2.data.y = 4;
//
//         console.log('x: ' + point2.data.x + ', y: ' + point2.data.y);
//         Expect(point2.data.x).toBe(3);
//
//         let data:DataPoint = new DataPoint();
//         data.x = 5;
//         data.y = 10;
//         let point3:Model<DataPoint> = new Model<DataPoint>(data);
//         console.log('x: ' + point3.data.x + ', y: ' + point3.data.y);
//
//
//         // type Size = { width:number; height:number };
//         // let size:Model<Size> = new Model<Size>({width: 112, height: 24});
//         // console.log('width:' + size.data.width);
//     }
//
//
//     @TestCase(/[ \t]+/)
//     @AsyncTest("RegExp")
//     @Timeout(5000) // Alsatian will now wait 5 seconds before failing
//     async test03(testValue:any) {
//         const promise:Promise<any> = new Promise((resolve, reject) => {
//             // let attributes:IAttribute = new Attribute('myKey', new Attributes(), DataType.COLLECTION);
//             // let attributeTest:any = new AttributeBaseTest<any, any, any, any>();
//             // let attributeBase:any = new AttributeBase<any, any, any, any>();
//             // let attribute:any = Attribute.create.fromObject({ key: 'rule01', value: 'boo', dataType: DataType.STRING });
//             // console.log(`attribute key: ${attribute.key}, value: ${attribute.value}, dataType: ${DataType[attribute.dataType]}`)
//             resolve();
//         });
//
//         const result:any = await promise;
//     }
// }
