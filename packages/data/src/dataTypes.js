"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const collections_1 = require("@pilotlab/collections");
const dataTypeDescriptions_1 = require("./dataTypeDescriptions");
class DataTypeDescription {
    constructor(src, className, type, parentType, aliases, validator) {
        this.src = src;
        this.type = type;
        this.validator = validator;
        this.p_descriptions = new collections_1.List();
        this.aliases = [];
    }
    validate(value) {
        let type = '';
        for (let i = 0; i < this.p_descriptions.size; i++) {
            let typeDescription = this.p_descriptions.item(i);
            type = typeDescription.validate(value);
            if (is_1.default.notEmpty(type, true))
                break;
        }
        if (is_1.default.empty(type, true) && this.validate(value))
            type = this.type;
        return type;
    }
    registerDataType(src, className, type, parentType, aliases, validator) {
        if (is_1.default.empty(type, true) || is_1.default.empty(validator))
            return;
        let typeDescription = new DataTypeDescription(src, className, type, parentType, aliases, validator);
        this.p_descriptions.add(typeDescription);
        return typeDescription;
    }
}
exports.DataTypeDescription = DataTypeDescription;
class DataTypes {
    constructor() {
        this.p_descriptions = new collections_1.MapList();
        dataTypeDescriptions_1.default.types.forEach((dataTypeDescription) => {
            console.log(`${dataTypeDescription.className}`);
            let parent;
            // if (is.notEmpty(dataTypeDescription.parentType)) {
            //     parent = this.p_descriptions.get(dataTypeDescription.parentType);
            // }
        });
        //
        // let isCollectionType = (value:any) => {
        //     return is.notEmpty(value) && (value instanceof AttributesBase || typeof value === 'object' || Array.isArray(value))
        // };
        //
        // let typeAny:DataTypeDescription = new DataTypeDescription('/', 'any', (value:any) => true);
        // typeAny.registerDataType('/', 'null', (value:any) => { return value === null; });
        // typeAny.registerDataType('/', 'undefined', (value:any) => { return typeof value === 'undefined' });
        // typeAny.registerDataType('/', 'boolean', (value:any) => { return typeof value === 'boolean' });
        // typeAny.registerDataType('/', 'number', (value:any) => { return is.number(value) });
        // typeAny.registerDataType('/', 'string', (value:any) => { return typeof value === 'string' });
        //
        // let typeCollection:DataTypeDescription = typeAny.registerDataType('/', 'collection', isCollectionType);
        // let typeColor:DataTypeDescription = typeCollection.registerDataType('/', 'color', (value:any) => {
        //     return (
        //         is.notEmpty(value) &&
        //         typeof value === 'object' &&
        //         value.hasOwnProperty('r') &&
        //         value.hasOwnProperty('g') &&
        //         value.hasOwnProperty('b')
        //     );
        // });
        //
        // let typePoint:DataTypeDescription = typeCollection.registerDataType('/', 'point', (value:any) => {
        //     return (
        //         is.notEmpty(value) &&
        //         typeof value === 'object' &&
        //         value.hasOwnProperty('x') &&
        //         value.hasOwnProperty('y')
        //     );
        // });
        //
        // let typePoint3d:DataTypeDescription = typePoint.registerDataType('/', 'point_3d', (value:any) => {
        //     return (
        //         is.notEmpty(value) &&
        //         value.hasOwnProperty('x') &&
        //         value.hasOwnProperty('y') &&
        //         value.hasOwnProperty('z')
        //     );
        // });
        //
        // let testValue:any;
        // console.log(`typeof: ${typeof testValue}`);
        // console.log(`data type: ${typeAny.validate(testValue)}!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!`);
    }
} // End class
exports.DataTypes = DataTypes;
exports.default = DataTypes;
//# sourceMappingURL=dataTypes.js.map