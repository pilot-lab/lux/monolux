"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const strings_1 = require("@pilotlab/strings");
let DataTypeDescriptions = {
    types: [
        {
            src: "attribute",
            className: "Attribute",
            aliases: ["any"],
            validator: (value) => true,
            subTypes: [
                { type: "null", validator: (value) => value === null },
                { type: "undefined", validator: (value) => typeof value === "undefined" },
                { src: "library/attributeBoolean", className: "AttributeBoolean", aliases: ["boolean", "bool"], validator: (value) => typeof value === "boolean" },
                {
                    src: "library/attributeDouble",
                    className: "AttributeDouble",
                    aliases: ["number", "double", "float"],
                    validator: (value) => is_1.default.number(value),
                    subTypes: [
                        { className: "AttributeRange", aliases: ["range"], validator: (value) => value.hasOwnProperty("min") && value.hasOwnProperty("max") }
                    ]
                },
                { src: "library/attributeDataTime", className: "AttributeDataTime", aliases: ["data", "time", "date_time"], validator: (value) => value instanceof Date },
                { src: "library/attributeRegExp", className: "AttributeRegExp", aliases: ["regexp", "reg_exp"], validator: (value) => value instanceof RegExp },
                { src: "library/attributeFunction", className: "AttributeFunction", aliases: ["function"], validator: (value) => typeof value === "function" },
                {
                    src: "library/attributeString",
                    className: "AttributeString",
                    aliases: ["string"],
                    validator: (value) => typeof value === "string",
                    subTypes: [
                        { className: "AttributeStringHexValue", aliases: ["string_hex", "hex", "hex_value"], validator: (value) => strings_1.default.isValidHexString(value) },
                        { className: "AttributeStringHtml", aliases: ["string_html", "html"], validator: (value) => value.toLowerCase().indexOf("<html>") > -1 },
                        { className: "AttributeStringSvg", aliases: ["string_svg", "svg"], validator: (value) => strings_1.default.isSvg(strings_1.default.stringToXmlDocument(value)) },
                        { className: "AttributeStringXml", aliases: ["string_xml", "xml"], validator: (value) => strings_1.default.isXml(strings_1.default.stringToXmlDocument(value)) },
                        {
                            className: "AttributeStringDataUrl",
                            aliases: ["string_data", "string_data_url", "string_data_uri", "data", "data_url", "data_uri"],
                            validator: (value) => strings_1.default.isDataUrl(value)
                        },
                        { className: "AttributeStringBase64", aliases: ["string_base64", "base64"], validator: (value) => strings_1.default.isBase64(value) },
                        { className: "AttributeStringUri", aliasees: ["string_uri", "string_url", "uri", "url"], validator: (value) => strings_1.default.isValidUri(value) }
                    ]
                },
                {
                    src: "library/attributeCollection",
                    className: "AttributeCollection",
                    aliases: ["collection"],
                    validator: (value) => {
                        return (is_1.default.notEmpty(value) &&
                            typeof value === "object");
                    },
                    subTypes: [
                        {
                            src: "library/types/color",
                            className: "Color",
                            validator: (value) => {
                                return (value.hasOwnProperty("r") &&
                                    value.hasOwnProperty("g") &&
                                    value.hasOwnProperty("b"));
                            }
                        },
                        {
                            src: "library/types/point",
                            className: "Point",
                            validator: (value) => { return value.hasOwnProperty("x") && value.hasOwnProperty("y"); },
                            subTypes: [
                                {
                                    aliases: ["point3d", "point_3d"],
                                    validator: (value) => { return value.hasOwnProperty("z") && !value.hasOwnProperty("w"); }
                                },
                                {
                                    src: "library/types/vector",
                                    className: "Vector",
                                    aliases: ["vector"],
                                    validator: (value) => { return value.hasOwnProperty("w"); }
                                }
                            ]
                        },
                        {
                            src: "library/attributeActions",
                            className: "AttributeActions",
                            validator: (value) => {
                                return (value.hasOwnProperty("action"));
                            }
                        },
                        {
                            src: "library/attributeSelect",
                            className: "AttributeSelect",
                            validator: (value) => {
                                return (value.hasOwnProperty("selected"));
                            }
                        },
                        {
                            src: "library/types/size",
                            className: "Size",
                            validator: (value) => {
                                return (value.hasOwnProperty("width") &&
                                    value.hasOwnProperty("height"));
                            },
                            subTypes: [
                                {
                                    aliases: ["size_3d"],
                                    validator: (value) => {
                                        return value.hasOwnProperty("depth");
                                    }
                                }
                            ]
                        },
                        {
                            src: "library/types/bounds",
                            className: "Bounds",
                            validator: (value) => {
                                return (value.hasOwnProperty("position") &&
                                    value.hasOwnProperty("pivot") &&
                                    value.hasOwnProperty("size") &&
                                    value.hasOwnProperty("scale"));
                            }
                        }
                    ]
                }
            ]
        }
    ]
};
exports.default = DataTypeDescriptions;
//# sourceMappingURL=dataTypeDescriptions.js.map