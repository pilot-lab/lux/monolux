import is from '@pilotlab/is';
import Strings from '@pilotlab/strings';


let DataTypeDescriptions = {
    types: [
        {
            src: "attribute",
            className: "Attribute",
            aliases: ["any"],
            validator: (value:any) => true,
            subTypes: [
                { type: "null", validator: (value:any) => value === null },
                { type: "undefined", validator: (value:any) => typeof value === "undefined" },
                { src: "library/attributeBoolean", className: "AttributeBoolean", aliases: ["boolean", "bool"], validator: (value:any) => typeof value === "boolean" },
                {
                    src: "library/attributeDouble",
                    className: "AttributeDouble",
                    aliases: ["number", "double", "float"],
                    validator: (value:any) => is.number(value),
                    subTypes: [
                        { className: "AttributeRange", aliases: ["range"], validator: (value:any) => value.hasOwnProperty("min") && value.hasOwnProperty("max") }
                    ]
                },
                { src: "library/attributeDataTime", className: "AttributeDataTime", aliases: ["data", "time", "date_time"], validator: (value:any) => value instanceof Date },
                { src: "library/attributeRegExp", className: "AttributeRegExp", aliases: ["regexp", "reg_exp"], validator: (value:any) => value instanceof RegExp },
                { src: "library/attributeFunction", className: "AttributeFunction", aliases: ["function"], validator: (value:any) => typeof value === "function" },
                {
                    src: "library/attributeString",
                    className: "AttributeString",
                    aliases: ["string"],
                    validator: (value:any) => typeof value === "string",
                    subTypes: [
                        { className: "AttributeStringHexValue", aliases: ["string_hex", "hex", "hex_value"], validator: (value:any) => Strings.isValidHexString(value) },
                        { className: "AttributeStringHtml", aliases: ["string_html", "html"], validator: (value:any) => value.toLowerCase().indexOf("<html>") > -1 },
                        { className: "AttributeStringSvg", aliases: ["string_svg", "svg"], validator: (value:any) => Strings.isSvg(Strings.stringToXmlDocument(value)) },
                        { className: "AttributeStringXml", aliases: ["string_xml", "xml"], validator: (value:any) => Strings.isXml(Strings.stringToXmlDocument(value)) },
                        {
                            className: "AttributeStringDataUrl",
                            aliases: ["string_data", "string_data_url", "string_data_uri", "data", "data_url", "data_uri"],
                            validator: (value:any) => Strings.isDataUrl(value)
                        },
                        { className: "AttributeStringBase64", aliases: ["string_base64", "base64"],  validator: (value:any) => Strings.isBase64(value) },
                        { className: "AttributeStringUri", aliasees: ["string_uri", "string_url", "uri", "url"], validator: (value:any) => Strings.isValidUri(value) }
                    ]
                },
                {
                    src: "library/attributeCollection",
                    className: "AttributeCollection",
                    aliases: ["collection"],
                    validator: (value:any) => {
                        return (
                            is.notEmpty(value) &&
                            typeof value === "object"
                        )
                    },
                    subTypes: [
                        {
                            src: "library/types/color",
                            className: "Color",
                            validator: (value:any) => {
                                return (
                                    value.hasOwnProperty("r") &&
                                    value.hasOwnProperty("g") &&
                                    value.hasOwnProperty("b")
                                );
                            }
                        },
                        {
                            src: "library/types/point",
                            className: "Point",
                            validator: (value:any) => { return value.hasOwnProperty("x") && value.hasOwnProperty("y"); },
                            subTypes: [
                                {
                                    aliases: ["point3d", "point_3d"],
                                    validator: (value:any) => { return value.hasOwnProperty("z") && !value.hasOwnProperty("w"); }
                                },
                                {
                                    src: "library/types/vector",
                                    className: "Vector",
                                    aliases: ["vector"],
                                    validator: (value:any) => { return value.hasOwnProperty("w"); }
                                }
                            ]
                        },
                        {
                            src: "library/attributeActions",
                            className: "AttributeActions",
                            validator: (value:any) => {
                                return (
                                    value.hasOwnProperty("action")
                                );
                            }
                        },
                        {
                            src: "library/attributeSelect",
                            className: "AttributeSelect",
                            validator: (value:any) => {
                                return (
                                    value.hasOwnProperty("selected")
                                );
                            }
                        },
                        {
                            src: "library/types/size",
                            className: "Size",
                            validator: (value:any) => {
                                return (
                                    value.hasOwnProperty("width") &&
                                    value.hasOwnProperty("height")
                                );
                            },
                            subTypes: [
                                {
                                    aliases: ["size_3d"],
                                    validator: (value:any) => {
                                        return value.hasOwnProperty("depth");
                                    }
                                }
                            ]
                        },
                        {
                            src: "library/types/bounds",
                            className: "Bounds",
                            validator: (value:any) => {
                                return (
                                    value.hasOwnProperty("position") &&
                                    value.hasOwnProperty("pivot") &&
                                    value.hasOwnProperty("size") &&
                                    value.hasOwnProperty("scale")
                                );
                            }
                        }
                    ]
                }
            ]
        }
    ]
};


export default DataTypeDescriptions;
