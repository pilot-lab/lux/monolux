import { List, MapList } from '@pilotlab/collections';
export interface IDataTypeDescription {
    src?: string;
    className: string;
    aliases?: string[];
    validator: (value: any) => boolean;
}
export declare class DataTypeDescription implements IDataTypeDescription {
    constructor(src: string, className: string, type: string, parentType: string, aliases: string[], validator: (value: any) => boolean);
    src: string;
    className: string;
    type: string;
    parentType: string;
    aliases: string[];
    validator: (value: any) => boolean;
    protected p_descriptions: List<DataTypeDescription>;
    validate(value: any): string;
    registerDataType(src: string, className: string, type: string, parentType: string, aliases: string[], validator: (value: any) => boolean): DataTypeDescription;
}
export declare class DataTypes {
    constructor();
    protected p_descriptions: MapList<string, IDataTypeDescription>;
}
export default DataTypes;
