export declare enum KeyAutoGenerationType {
    NONE = 0,
    GLOBALLY_UNIQUE = 1,
    SESSION_UNIQUE = 2,
}
