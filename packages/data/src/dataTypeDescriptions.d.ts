declare let DataTypeDescriptions: {
    types: {
        src: string;
        className: string;
        aliases: string[];
        validator: (value: any) => boolean;
        subTypes: ({
            type: string;
            validator: (value: any) => boolean;
        } | {
            src: string;
            className: string;
            aliases: string[];
            validator: (value: any) => boolean;
        } | {
            src: string;
            className: string;
            aliases: string[];
            validator: (value: any) => boolean;
            subTypes: {
                className: string;
                aliases: string[];
                validator: (value: any) => any;
            }[];
        } | {
            src: string;
            className: string;
            aliases: string[];
            validator: (value: any) => boolean;
            subTypes: ({
                className: string;
                aliases: string[];
                validator: (value: any) => boolean;
            } | {
                className: string;
                aliasees: string[];
                validator: (value: any) => boolean;
            })[];
        } | {
            src: string;
            className: string;
            aliases: string[];
            validator: (value: any) => boolean;
            subTypes: ({
                src: string;
                className: string;
                validator: (value: any) => any;
            } | {
                src: string;
                className: string;
                validator: (value: any) => any;
                subTypes: ({
                    aliases: string[];
                    validator: (value: any) => boolean;
                } | {
                    src: string;
                    className: string;
                    aliases: string[];
                    validator: (value: any) => any;
                })[];
            } | {
                src: string;
                className: string;
                validator: (value: any) => any;
                subTypes: {
                    aliases: string[];
                    validator: (value: any) => any;
                }[];
            })[];
        })[];
    }[];
};
export default DataTypeDescriptions;
