import is from '@pilotlab/is';
import { Strings } from '@pilotlab/strings';
import { DataType } from './attributes/attributeEnums';
import AttributeBase from './attributes/attributeBase';
import AttributesBase from './attributes/attributesBase';


export class DataTools {
    static getDataType(value:any):(DataType | string) {
        let dataType:(DataType | string) = DataType.ANY;

        // if (is.notEmpty(value) && value.hasOwnProperty('value')) value = value['value'];
        if (value instanceof AttributeBase) (<AttributeBase<any, any, any, any>>value).dataType;

        //----- NOTE: There's really no way to determine whether the value is intended to represent
        //----- a URI here, since even a simple string is a valid URI.

        if (is.undefined(value)) return DataType.UNDEFINED;
        else if (is.empty(value, false)) return DataType.NULL;
        else if (value instanceof AttributesBase || typeof value === 'object') {
            if (
                value.hasOwnProperty('r') &&
                value.hasOwnProperty('g') &&
                value.hasOwnProperty('b')
            ) {
                dataType = DataType.COLOR;
            } else if (
                value.hasOwnProperty('min') &&
                value.hasOwnProperty('max')
            ) {
                dataType = DataType.RANGE;
            } else if (
                value.hasOwnProperty('x') &&
                value.hasOwnProperty('y')
            ) {
                if (
                    value.hasOwnProperty('w')
                ) {
                    dataType = DataType.VECTOR;
                } else {
                    if (value.hasOwnProperty('z')) dataType = DataType.POINT_3D;
                    else dataType = DataType.POINT;
                }
            } else if (
                value.hasOwnProperty('width') &&
                value.hasOwnProperty('height')
            ) {
                if (value.hasOwnProperty('depth')) dataType = DataType.SIZE_3D;
                else dataType = DataType.SIZE;
            } else if (
                value.hasOwnProperty('position') &&
                value.hasOwnProperty('pivot') &&
                value.hasOwnProperty('size') &&
                value.hasOwnProperty('scale')
            ) {
                dataType = DataType.CUBE;
            } else if (Array.isArray(value)) dataType = DataType.ARRAY;
            else if (value.hasOwnProperty('value')) dataType = DataType.OBJECT;
            else dataType = DataType.COLLECTION;
        } else if (value instanceof Date) dataType = DataType.DATE_TIME;
        else if (value instanceof RegExp) dataType = DataType.REG_EXP;
        else {
            switch (typeof value) {
                case 'boolean':
                    dataType = DataType.BOOLEAN;
                    break;
                case 'number':
                    dataType = DataType.NUMBER;
                    break;
                case 'function':
                    dataType = DataType.FUNCTION;
                    break;
                case 'string':
                    if (Strings.isValidHexString(value)) dataType = DataType.STRING_HEX_VALUE;
                    else if (value.toLowerCase().indexOf('<html>') > -1) dataType = DataType.STRING_HTML;
                    else if (Strings.isSvg(Strings.stringToXmlDocument(value))) dataType = DataType.STRING_SVG;
                    else if (Strings.isXml(Strings.stringToXmlDocument(value))) dataType = DataType.STRING_XML;
                    else if (Strings.isDataUrl(value)) dataType = DataType.STRING_DATA_URL;
                    else if (Strings.isBase64(value)) dataType = DataType.STRING_BASE64;
                    else if (Strings.isValidUri(value)) dataType = DataType.STRING_URI;
                    else dataType = DataType.STRING;
                    break;
                case 'object':
                    dataType = DataType.OBJECT;
                    break;
                default:
                    dataType = DataType.ANY;
            }
        }

        return dataType;
    }


    static setCookie(name:string, value:string, expireMilliseconds?:number):void {
        /// expire is optional and should be in milliseconds
        let cookie:string = name + '=' + encodeURI(value);

        if (expireMilliseconds) {
            let today:Date = new Date();
            let expireDate = new Date();
            expireDate.setTime(today.getTime() + expireMilliseconds);
            cookie = cookie.concat(';expires=' + expireDate.toUTCString());
        }

        document.cookie = cookie;
    }


    static getCookie(name:string) {
        let re:RegExp = new RegExp('[; ]' + name + '=([^\\s;]*)');
        let sMatch = (' ' + document.cookie).match(re);
        if (name && sMatch) return decodeURI(sMatch[1]);
        return null;
    }
} // End Class


export default DataTools;
