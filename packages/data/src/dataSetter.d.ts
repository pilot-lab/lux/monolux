import { Initializable } from '@pilotlab/initializable';
import { IPromise, Result } from '@pilotlab/result';
import { DataType } from './attributes/attributeEnums';
import IAttribute from './attributes/interfaces/iAttribute';
import IAttributeChangeOptions from './attributes/interfaces/iAttributeChangeOptions';
import { Data } from './data';
import { IDataOwner } from './interfaces/iDataOwner';
import { IDataSetter } from './interfaces/iDataSetter';
export declare class DataSetter<TOwner extends IDataOwner, TData extends Data, TDataSetter extends DataSetter<any, any, any>> extends Initializable implements IDataSetter {
    constructor();
    initialize(dataOwner: TOwner, data: TData, changeOptions?: IAttributeChangeOptions, isBatchStarted?: boolean): IPromise<any>;
    protected p_onInitializeStarted(result: Result<any>, args: any[]): IPromise<any>;
    readonly owner: TOwner;
    protected p_owner: TOwner;
    protected p_data: TData;
    readonly changeOptions: IAttributeChangeOptions;
    protected p_changeOptions: IAttributeChangeOptions;
    protected p_isBatchStarted: boolean;
    register(key: string, defaultValue?: any, dataType?: DataType): IAttribute;
    create(dataOwner: TOwner, data: TData, changeOptions: IAttributeChangeOptions, isBatchStarted?: boolean): TDataSetter;
    start(changeOptions?: IAttributeChangeOptions, isBatch?: boolean): TDataSetter;
    end(): void;
    protected p_update(attribute: IAttribute, value: any, changeOptions?: IAttributeChangeOptions): TDataSetter;
}
export default DataSetter;
