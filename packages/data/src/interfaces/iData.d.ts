import { Signal } from '@pilotlab/signals';
import IAttribute from '../attributes/interfaces/iAttribute';
import IAttributes from '../attributes/interfaces/iAttributes';
import IAttributeChangeOptions from '../attributes/interfaces/iAttributeChangeOptions';
import IAttributeUpdateTracker from '../attributes/interfaces/iAttributeUpdateTracker';
import AttributeEventArgs from '../attributes/attributeEventArgs';
import { ProgressTracker } from '@pilotlab/progress';
import { IInitializable } from '@pilotlab/initializable';
import { IPromise } from "@pilotlab/result";
import IDataStore from "./iDataStore";
export interface IData extends IInitializable {
    readonly revision: string;
    /**
     * An SHA-1 hash representing the precise contents of the associated blob or text.
     * Can be used to check the uniqueness of a data object to avoid storing duplicates.
     */
    readonly hash: IAttribute;
    isAutoHash: boolean;
    store: IDataStore;
    isSaveDataTypes: boolean;
    /**
     * Toggles auto-save functionality.
     * Can be used to pause auto-saving while changes to the data are batched.
     */
    isAutoSave: boolean;
    storeSet: Signal<IDataStore>;
    /**
     * Dispatched after a response is received back from the data store when data has been saved.
     * @type {Signal<any>} Completes with the response from the data store.
     */
    saved: Signal<any>;
    saveTriggered: Signal<AttributeEventArgs<any>>;
    save(): IPromise<any>;
    autoSave(): void;
    delete(): IPromise<boolean>;
    update(data: (IData | IAttributes | Object | string), changeOptions?: IAttributeChangeOptions, progressTracker?: ProgressTracker): IAttributeUpdateTracker;
}
export default IData;
