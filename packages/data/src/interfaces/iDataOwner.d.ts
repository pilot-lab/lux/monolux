import IData from './iData';
export interface IDataOwner {
    data: IData;
}
export default IDataOwner;
