import { IInitializable } from '@pilotlab/initializable';
import { IAttributeChangeOptions } from '../attributes/interfaces/iAttributeChangeOptions';
import { IDataOwner } from './iDataOwner';
import IData from './iData';
export interface IDataSetter extends IInitializable {
    owner: IDataOwner;
    changeOptions: IAttributeChangeOptions;
    create(dataOwner: IDataOwner, data: IData, changeOptions: IAttributeChangeOptions, isBatchStarted?: boolean): IDataSetter;
    start(changeOptions?: IAttributeChangeOptions, isBatch?: boolean): IDataSetter;
    end(): void;
}
export default IDataSetter;
