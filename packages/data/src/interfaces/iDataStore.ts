import { MapList } from '@pilotlab/collections';
import { IPromise } from '@pilotlab/result';
import { Signal } from '@pilotlab/signals';


export interface IDataStore {
    connect():IPromise<boolean>


    updated:Signal<any>;


    /**
     * Adds a new value to the data store, or updates the object if it already exists.
     * The boolean returned in the result reflects the success or failure of the operation.
     * @param data {Object} The value to be added or updated in the data store.
     */
    save(data:any, path?:string):IPromise<any>;


    /**
     * Saves all the key/value pairs provided by the map.
     * When all the values have been saved to the store, returns a completed Result
     * containing a list of boolean values, representing the success or failure of the
     * save action for each item.
     */
    saveAll(data:MapList<string, any>):IPromise<any>;


    /**
     * This returns a Result which completes when the data object has been retrieved.
     * The Attribute returned in the Result will provided the requested data.
     * @param key {string} The key for the requested value
     */
    get(path?:string):IPromise<any>;


    /**
     * This returns a Result which completes when the data object that's been deleted.
     * The boolean returned in the result reflects the success or failure of the operation.
     * NOTE: Remember to check the data store for references to this data before removing.
     */
    delete(path?:string):IPromise<boolean>;
} // End of class


export default IDataStore;
