import { NodeFactoryBase } from '@pilotlab/nodes';
import AttributeCreateOptions from './attributeCreateOptions';
import IAttributesFactory from './interfaces/iAttributesFactory';
import IAttributeFactory from './interfaces/iAttributeFactory';
import IAttribute from './interfaces/iAttribute';
export declare abstract class AttributeFactoryBase<TAttribute extends IAttribute> extends NodeFactoryBase<TAttribute> implements IAttributeFactory {
    instance(options?: AttributeCreateOptions): TAttribute;
    readonly collection: IAttributesFactory;
    fromAny(value: (TAttribute | Object | string), ...args: any[]): TAttribute;
    fromObject(obj: any, ...args: any[]): TAttribute;
    fromArray(values: any[], ...args: any[]): TAttribute;
    getBaseDataType(dataType: string): string;
}
export default AttributeFactoryBase;
