import { NodeChangeType, NodeEventArgs } from '@pilotlab/nodes';
import { IPromise } from '@pilotlab/result';
import AttributeBase from './attributeBase';
import IAttributeChangeOptions from './interfaces/iAttributeChangeOptions';
import AttributeChangeOptions from './attributeChangeOptions';


export class AttributeEventArgs<TAttribute extends AttributeBase<any, any, any, any>> extends NodeEventArgs<TAttribute> {
    constructor(
        attribute:TAttribute,
        changeType:NodeChangeType,
        changeOptions:IAttributeChangeOptions = AttributeChangeOptions.default,
        result?:IPromise<any>,
        isMultipleNodeChange:boolean = false
    ) {
        super(attribute, changeType, changeOptions, result, isMultipleNodeChange);
    }

    changeOptions:IAttributeChangeOptions;
} // End class


export default AttributeEventArgs;
