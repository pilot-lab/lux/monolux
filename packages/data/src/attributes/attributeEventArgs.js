"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const nodes_1 = require("@pilotlab/nodes");
const attributeChangeOptions_1 = require("./attributeChangeOptions");
class AttributeEventArgs extends nodes_1.NodeEventArgs {
    constructor(attribute, changeType, changeOptions = attributeChangeOptions_1.default.default, result, isMultipleNodeChange = false) {
        super(attribute, changeType, changeOptions, result, isMultipleNodeChange);
    }
} // End class
exports.AttributeEventArgs = AttributeEventArgs;
exports.default = AttributeEventArgs;
//# sourceMappingURL=attributeEventArgs.js.map