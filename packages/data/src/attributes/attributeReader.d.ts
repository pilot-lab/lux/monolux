import { Signal } from '@pilotlab/signals';
import { NodeEventArgs } from '@pilotlab/nodes';
import AttributeBase from './attributeBase';
export declare class AttributeReader<TValue> {
    constructor(attribute: AttributeBase<any, any, any, any>);
    protected p_attribute: AttributeBase<any, any, any, any>;
    readonly value: TValue;
    readonly valuePrevious: TValue;
    readonly changed: Signal<NodeEventArgs<any>>;
}
export default AttributeReader;
