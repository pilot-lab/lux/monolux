import { IPromise, Result } from '@pilotlab/result';
import { ProgressTracker, ProgressCommand } from '@pilotlab/progress';
import IAttribute from './interfaces/iAttribute';
import IAttributes from './interfaces/iAttributes';
import IAttributeChangeOptions from './interfaces/iAttributeChangeOptions';
import { IAnimationBatch } from '@pilotlab/animation';
import IAttributeUpdateTracker from './interfaces/iAttributeUpdateTracker';
export declare class AttributeUpdateTracker implements IAttributeUpdateTracker {
    constructor(attributesNew: IAttributes, changeOptions?: IAttributeChangeOptions, progressTracker?: ProgressTracker, attributesChanged?: IAttributes);
    progressTracker: ProgressTracker;
    protected p_progressTracker: ProgressTracker;
    attributesNew: IAttributes;
    changeOptions: IAttributeChangeOptions;
    animation: IAnimationBatch;
    isAllowAttributeCreation: boolean;
    readonly result: Result<IAttributeUpdateTracker>;
    protected p_result: Result<any>;
    readonly isChanged: boolean;
    protected p_isChanged: boolean;
    readonly attributesChanged: IAttributes;
    protected p_attributesChanged: IAttributes;
    /**
     * This is resolved once the tracker has finished updating all attributes,
     * but before animations are initialized after the update cycle.
     */
    then(onDone: (value: IAttributeUpdateTracker) => any, onError?: (error: Error) => void): IPromise<any>;
    update(attributesToUpdate: IAttributes): void;
    protected p_queueCommand(command: ProgressCommand, key?: string): void;
    protected p_queueAttributeUpdate(attributeToUpdate: IAttribute, attributeNew: IAttribute): void;
    protected p_queueAttributesUpdate(attributesToUpdate: IAttributes, attributeNew: IAttribute): void;
}
export default AttributeUpdateTracker;
