import { IAnimationBatch } from '@pilotlab/animation';
import IAttribute from './interfaces/iAttribute';
import IAttributeSetReturn from './interfaces/iAttributeSetReturn';
import IAttributeUpdateTracker from './interfaces/iAttributeUpdateTracker';
export declare class AttributeSetReturn<TAttribute extends IAttribute> implements IAttributeSetReturn {
    constructor(attribute: TAttribute, isChanged: boolean);
    attribute: TAttribute;
    isChanged: boolean;
    animation: IAnimationBatch;
    updateTracker: IAttributeUpdateTracker;
}
export default AttributeSetReturn;
