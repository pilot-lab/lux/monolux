"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const attributeFactoryBase_1 = require("./attributeFactoryBase");
const attributesFactory_1 = require("./attributesFactory");
const attributeEnums_1 = require("./attributeEnums");
const scopeActions = require("./library/attributeActions");
const scopeAttributeCollection = require("./library/attributeCollection");
const scopeBoolean = require("./library/attributeBoolean");
const scopeByte = require("./library/attributeByte");
const scopeDouble = require("./library/attributeDouble");
const scopeInt = require("./library/attributeInt");
const scopeSelect = require("./library/attributeSelect");
const scopeString = require("./library/attributeString");
const scopeFunction = require("./library/attributeFunction");
const scopeDateTime = require("./library/attributeDateTime");
const scopeRegExp = require("./library/attributeRegExp");
const scopeRange = require("./library/attributeRange");
const scopeAttribute = require("./attribute");
class AttributeFactory extends attributeFactoryBase_1.default {
    constructor() {
        super();
        /// Register data types.
        this.register(scopeActions, 'AttributeActions', ['actions'], { baseType: attributeEnums_1.DataType.COLLECTION });
        this.register(scopeAttributeCollection, 'AttributeCollection', ['collection', 'attributes', 'nodes', 'children', 'array'], { baseType: attributeEnums_1.DataType.COLLECTION });
        this.register(scopeBoolean, 'AttributeBoolean', ['boolean', 'bool'], { baseType: attributeEnums_1.DataType.ANY });
        this.register(scopeByte, 'AttributeByte', ['numberbyte', 'number_byte', 'byte'], { baseType: attributeEnums_1.DataType.NUMBER });
        this.register(scopeDouble, 'AttributeDouble', ['numberdouble', 'number_double', 'double', 'float', 'number'], { baseType: attributeEnums_1.DataType.NUMBER });
        this.register(scopeInt, 'AttributeInt', ['numberint', 'number_int', 'int', 'integer'], { baseType: attributeEnums_1.DataType.NUMBER });
        this.register(scopeString, 'AttributeString', ['string'], { baseType: attributeEnums_1.DataType.STRING });
        this.register(scopeString, 'AttributeStringJson', ['stringjson', 'string_json'], { baseType: attributeEnums_1.DataType.STRING });
        this.register(scopeString, 'AttributeStringBase64', ['stringbase64', 'string_base64', 'base64'], { baseType: attributeEnums_1.DataType.STRING });
        this.register(scopeString, 'AttributeStringBinary', ['stringbinary', 'string_binary', 'binary'], { baseType: attributeEnums_1.DataType.STRING });
        this.register(scopeString, 'AttributeStringDataUrl', ['stringdataurl', 'string_data_url', 'dataurl', 'data_url'], { baseType: attributeEnums_1.DataType.STRING });
        this.register(scopeString, 'AttributeStringHexValue', ['stringhexvalue', 'stringhex', 'string_hex_value', 'string_hex', 'hex'], { baseType: attributeEnums_1.DataType.STRING });
        this.register(scopeString, 'AttributeStringHtml', ['stringhtml', 'string_html', 'html'], { baseType: attributeEnums_1.DataType.STRING });
        this.register(scopeString, 'AttributeStringJavascript', ['stringjavascript', 'string_javascript', 'javascript'], { baseType: attributeEnums_1.DataType.STRING });
        this.register(scopeString, 'AttributeStringRegExp', ['stringregexp', 'string_reg_exp', 'regexp', 'reg_exp'], { baseType: attributeEnums_1.DataType.STRING });
        this.register(scopeString, 'AttributeStringSvg', ['stringsvg', 'string_svg', 'svg'], { baseType: attributeEnums_1.DataType.STRING });
        this.register(scopeString, 'AttributeStringUri', ['stringuri', 'string_uri', 'stringurl', 'string_url', 'uri', 'url'], { baseType: attributeEnums_1.DataType.STRING });
        this.register(scopeString, 'AttributeStringXml', ['stringxml', 'string_xml', 'xml'], { baseType: attributeEnums_1.DataType.STRING });
        this.register(scopeFunction, 'AttributeFunction', ['function'], { baseType: attributeEnums_1.DataType.ANY });
        this.register(scopeDateTime, 'AttributeDateTime', ['dateTime', 'date_time', 'date', 'time'], { baseType: attributeEnums_1.DataType.ANY });
        this.register(scopeRegExp, 'AttributeRegExp', ['regexp', 'reg_exp'], { baseType: attributeEnums_1.DataType.ANY });
        this.register(scopeSelect, 'AttributeSelect', ['select'], { baseType: attributeEnums_1.DataType.COLLECTION });
        this.register(scopeRange, 'AttributeRange', ['range'], { baseType: attributeEnums_1.DataType.COLLECTION });
        this.register(scopeAttribute, 'Attribute', ['any', 'object', 'null', 'undefined'], { baseType: attributeEnums_1.DataType.ANY });
    }
    get collection() {
        if (is_1.default.empty(this.p_collection))
            this.p_collection = new attributesFactory_1.default(this);
        return this.p_collection;
    }
} // End class
exports.AttributeFactory = AttributeFactory;
exports.default = AttributeFactory;
//# sourceMappingURL=attributeFactory.js.map