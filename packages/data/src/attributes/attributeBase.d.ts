import { Signal } from '@pilotlab/signals';
import { IPromise, Result } from '@pilotlab/result';
import { NodeType, NodeBase } from '@pilotlab/nodes';
import { AttributeChangeActions, AttributesWrapType, DataType } from './attributeEnums';
import { IAnimationBatch } from '@pilotlab/animation';
import IAttributes from './interfaces/iAttributes';
import IAttributeFactory from './interfaces/iAttributeFactory';
import IAttributeChangeOptions from './interfaces/iAttributeChangeOptions';
import IAttributeSetReturn from './interfaces/iAttributeSetReturn';
import AttributeEventArgs from './attributeEventArgs';
import Attributes from './attributes';
import AttributeRoot from './attributeRoot';
import INodeChangeOptions from "../../../nodes/src/interfaces/iNodeChangeOptions";
export declare abstract class AttributeBase<TValue, TNode extends AttributeBase<any, any, any, any>, TNodes extends Attributes, TRoot extends AttributeRoot> extends NodeBase<TNode, TNodes, TRoot> {
    protected constructor(factory: IAttributeFactory, value?: any, dataType?: (DataType | string), label?: string, key?: string, nodeType?: NodeType, isInitialize?: boolean);
    protected p_onInitializeStarted(result: Result<any>, args: any[]): IPromise<any>;
    /**
     * All data associated with this entity will be saved to the provided attribute collection,
     * and all change events will be propagated through that collection.
     */
    protected p_wrapAttributes(value?: any, wrapType?: AttributesWrapType): void;
    readonly create: IAttributeFactory;
    protected p_factory: IAttributeFactory;
    protected p_returnValue: IAttributeSetReturn;
    /**
     * A UI-friendly title for the attribute that can be presented to users.
     * If no label is set, the key of the node will be returned.
     */
    label: string;
    protected p_label: string;
    /**
     * A brief description of that attribute that can be used to supply info to the user
     * about how the attribute should be used.
     */
    summary: string;
    protected p_summary: string;
    /**
     * The value of the associated data.
     * This property cannot be set directly. You must use the set() method to change the value.
     * This allows attributes to be wrapped in an AttributeReader instance, which effectively
     * renders them immutable.
     */
    value: TValue;
    readonly formatted: TValue;
    protected p_value: any;
    protected p_valueTarget: any;
    readonly isCollection: boolean;
    readonly attributes: IAttributes;
    readonly children: TNodes;
    readonly valuePrevious: TValue;
    protected p_valuePrevious: any;
    readonly dataType: (DataType | string);
    protected p_dataType: (DataType | string);
    /**
     * Attributes will be considered empty if no key or value has been set.
     */
    readonly isEmpty: boolean;
    /**
     * If this function returns true, the attribute will be omitted from data transfer objects
     * when saving data to the data store.
     */
    omitFromDataStore: (value: TValue) => boolean;
    protected p_omitFromDataStore: (value: TValue) => boolean;
    formatter: (value: any) => TValue;
    protected p_formatter: (value: any) => TValue;
    protected p_format(value: any): TValue;
    readonly copy: TNode;
    /**
     * An automatically generated unique ID that is used when animating attribute values.
     */
    readonly animationKey: string;
    private _animationKey;
    protected p_animation: IAnimationBatch;
    isEditable: boolean;
    protected p_isEditable: boolean;
    isHidden: boolean;
    protected p_isHidden: boolean;
    isAllowSave: boolean;
    protected p_isAllowSave: boolean;
    /**
     * Should we allow accessor properties to be generated automatically when
     * a new child Attribute is added to our Attribute collection?
     */
    isAllowAutoProperties: boolean;
    protected p_isAllowAutoPropterties: boolean;
    readonly changed: Signal<AttributeEventArgs<TNode>>;
    protected p_changed: Signal<AttributeEventArgs<TNode>>;
    internalChanged(args: AttributeEventArgs<TNode>): void;
    /**
     * Save the value to the attribute.
     */
    set(value: any, changeOptions?: IAttributeChangeOptions): IAttributeSetReturn;
    /**
     * Interrupt any animated value transitions that are in effect.
     */
    interruptAnimation(): void;
    /**
     * Manually trigger a dispatch on the 'changed' signal.
     */
    doChanged(action: AttributeChangeActions): void;
    toObject(isIncludeDataTypes?: boolean, isIncludeLabels?: boolean, appendToObject?: any, isForceInclude?: boolean): any;
    internalAttached(changeOptions: INodeChangeOptions): void;
}
export default AttributeBase;
