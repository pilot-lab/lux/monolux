import { NodesFactoryBase } from '@pilotlab/nodes';
import IAttributeFactory from './interfaces/iAttributeFactory';
import IAttributesFactory from './interfaces/iAttributesFactory';
import IAttribute from './interfaces/iAttribute';
import IAttributes from './interfaces/iAttributes';
export declare abstract class AttributesFactoryBase<TAttributes extends IAttributes> extends NodesFactoryBase<TAttributes> implements IAttributesFactory {
    constructor(nodeFactory: IAttributeFactory);
    readonly node: IAttributeFactory;
    protected p_node: IAttributeFactory;
    instance(parent?: IAttribute, ...args: any[]): TAttributes;
    fromAny(value: (IAttribute | IAttributes | Object | string | Array<any>), parent?: IAttribute, ...args: any[]): TAttributes;
    fromObject(value: any, parent?: IAttribute, ...args: any[]): TAttributes;
    fromArray(values: any[], parent?: IAttribute, ...args: any[]): TAttributes;
    fromAttributes(collectionIn: TAttributes): TAttributes;
}
export default AttributesFactoryBase;
