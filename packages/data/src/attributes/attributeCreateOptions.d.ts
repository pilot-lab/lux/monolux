import { DataType } from './attributeEnums';
import { NodeCreateOptions } from '@pilotlab/nodes';
import IAttributeCreateOptions from './interfaces/iAttributeCreateOptions';
import IAttributeChangeOptions from './interfaces/iAttributeChangeOptions';
export declare class AttributeCreateOptions extends NodeCreateOptions implements IAttributeCreateOptions {
    constructor(value?: any, dataType?: (DataType | string), label?: string, index?: number, key?: string, changeOptions?: IAttributeChangeOptions, omitFromDataStore?: (value: any) => boolean, isEditable?: boolean, isHidden?: boolean);
    value: any;
    dataType: (DataType | string);
    isEditable: boolean;
    isHidden: boolean;
    omitFromDataStore: (value: any) => boolean;
    changeOptions: IAttributeChangeOptions;
}
export default AttributeCreateOptions;
