"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const debug_1 = require("@pilotlab/debug");
const nodes_1 = require("@pilotlab/nodes");
const attribute_1 = require("./attribute");
const attributeCreateOptions_1 = require("./attributeCreateOptions");
const dataTools_1 = require("../dataTools");
const attributeEnums_1 = require("./attributeEnums");
const attributeChangeOptions_1 = require("./attributeChangeOptions");
const attributeBase_1 = require("./attributeBase");
const attributesBase_1 = require("./attributesBase");
class AttributeFactoryBase extends nodes_1.NodeFactoryBase {
    instance(options) {
        if (is_1.default.notEmpty(options)) {
            let dataType = options.dataType;
            if (is_1.default.empty(dataType, true))
                dataType = attributeEnums_1.DataType.ANY;
            /// IMPORTANT: We need to initialize the new attribute with any value that was
            /// passed in. If the new attribute is to be a collection, it must be initialized
            /// with an object representing the proper schema, otherwise we won't be able
            /// to properly set child values later.
            let attribute;
            if (this.getBaseDataType(dataType) === attributeEnums_1.DataType.COLLECTION) {
                let value = options.value;
                if (value instanceof attributesBase_1.default)
                    value = value.toObject(true, true, true);
                else if (value instanceof attributeBase_1.default)
                    value = value.attributes.toObject(true, true, true);
                attribute = this.p_factoryManager.instance(dataType, value);
            }
            else {
                attribute = this.p_factoryManager.instance(dataType);
            }
            if (is_1.default.empty(attribute, false))
                return new attribute_1.default();
            if (is_1.default.notEmpty(options)) {
                if (is_1.default.notEmpty(options.key))
                    attribute.key = options.key;
                if (is_1.default.notEmpty(options.label))
                    attribute.label = options.label;
                if (is_1.default.notEmpty(options.isEditable))
                    attribute.isEditable = options.isEditable;
                if (is_1.default.notEmpty(options.isHidden))
                    attribute.isHidden = options.isHidden;
                attribute.set(options.value, options.changeOptions);
            }
            return attribute;
        }
        else
            return new attribute_1.default();
    }
    get collection() { return null; }
    fromAny(value, ...args) {
        let node;
        if (value instanceof attributeBase_1.default) {
            node = value.copy;
        }
        else if (value instanceof attributesBase_1.default) {
            node = this.fromObject(value.toObject(true, true, true));
        }
        else if (typeof value === 'string') {
            node = this.fromString(value, ...args);
        }
        else if (Array.isArray(value)) {
            node = this.fromArray(value, ...args);
        }
        else
            node = this.fromObject(value, ...args);
        if (is_1.default.empty(node))
            return this.instance();
        return node;
    }
    fromObject(obj, ...args) {
        try {
            if (is_1.default.empty(obj))
                return null;
            let key;
            let value;
            let dataType;
            let label;
            let isEditable = false;
            let isHidden = false;
            let index;
            let hasValueProperty = true;
            if (obj.hasOwnProperty('key'))
                key = obj['key'];
            else if (obj.hasOwnProperty('id'))
                key = obj['id'];
            else if (obj.hasOwnProperty('_id'))
                key = obj['_id'];
            if (obj.hasOwnProperty('value')) {
                value = obj['value'];
                if (is_1.default.notEmpty(value) && value.hasOwnProperty('value')) {
                    if (is_1.default.notEmpty(key, true))
                        value['key'] = key;
                    return this.fromAny(value);
                }
            }
            else if (obj.hasOwnProperty('children'))
                value = obj['children'];
            else if (obj.hasOwnProperty('attributes'))
                value = obj['attributes'];
            else if (obj.hasOwnProperty('nodes'))
                value = obj['nodes'];
            else {
                hasValueProperty = false;
            }
            if (obj.hasOwnProperty('label'))
                label = obj['label'];
            if (obj.hasOwnProperty('isEditable')) {
                if (typeof obj['isEditable'] === 'string') {
                    isEditable = obj['isEditable'].toLowerCase() === 'true';
                }
                else {
                    isEditable = obj['isEditable'];
                }
            }
            if (obj.hasOwnProperty('isHidden')) {
                if (typeof obj['isHidden'] === 'string') {
                    isHidden = obj['isHidden'].toLowerCase() === 'true';
                }
                else {
                    isHidden = obj['isHidden'];
                }
            }
            if (obj.hasOwnProperty('index') && is_1.default.number(parseFloat('' + obj['index']))) {
                index = parseFloat('' + obj['index']);
            }
            if (hasValueProperty) {
                /// The object has a value property, so we'll assume it's
                /// describing an attribute.
                if (obj.hasOwnProperty('dataType') && is_1.default.notEmpty(obj['dataType'], true)) {
                    if (typeof obj['dataType'] === 'string') {
                        dataType = obj['dataType'].toLowerCase();
                    }
                    else {
                        dataType = dataTools_1.default.getDataType(value);
                    }
                }
                else {
                    dataType = dataTools_1.default.getDataType(value);
                }
            }
            else if (is_1.default.notEmpty(key, true) || is_1.default.notEmpty(label, true) || (value.hasOwnProperty('dataType') && is_1.default.notEmpty(obj['dataType'], true))) {
                /// The object doesn't have a value property, but it has a label and/or
                /// a dataType property, so it appears to be describing an attribute.
                /// In this case, we'll give it a dataType of ANY.
                dataType = attributeEnums_1.DataType.ANY;
                value = null;
            }
            else {
                /// The object doesn't have a value property, it must describe a collection,
                /// so pass the entire object as the value.
                dataType = attributeEnums_1.DataType.COLLECTION;
                value = this.fromObject({ value: obj });
            }
            return this.instance(new attributeCreateOptions_1.default(value, dataType, label, index, key, attributeChangeOptions_1.default.zero, obj, isEditable, isHidden));
        }
        catch (e) {
            debug_1.default.error(e, 'Attribute.create.fromObject(...)');
        }
    }
    fromArray(values, ...args) {
        let attributeNew = this.instance(new attributeCreateOptions_1.default(null, attributeEnums_1.DataType.COLLECTION));
        for (let i = 0, length = values.length; i < length; i++) {
            let value = values[i];
            let attributeChild = this.fromAny(value);
            if (is_1.default.notEmpty(attributeChild)) {
                attributeNew.attributes.add(attributeChild);
            }
        }
        return attributeNew;
    }
    getBaseDataType(dataType) {
        const dataTypeDescription = this.p_factoryManager.getDescription(dataType);
        return is_1.default.notEmpty(dataTypeDescription) && is_1.default.notEmpty(dataTypeDescription.baseType) ? dataTypeDescription.baseType : 'any';
    }
} // End class
exports.AttributeFactoryBase = AttributeFactoryBase;
exports.default = AttributeFactoryBase;
//# sourceMappingURL=attributeFactoryBase.js.map