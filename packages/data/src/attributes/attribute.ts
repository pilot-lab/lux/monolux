import is from '@pilotlab/is';
import { NodeType } from '@pilotlab/nodes';
import Debug from '@pilotlab/debug';
import { DataType } from './attributeEnums';
import Attributes from './attributes';
import AttributeBase from './attributeBase';
import IAttributeFactory from './interfaces/iAttributeFactory';
import AttributeFactory from './attributeFactory';
import AttributeRoot from './attributeRoot';


export class Attribute
    extends AttributeBase<any, Attribute, Attributes, AttributeRoot> {
    constructor(value?:any, label?:string, key?:string) {
        super(Attribute.create, value, DataType.NUMBER_DOUBLE, label, key, NodeType.BASIC, true);
    }


    /*====================================================================*
     START: Static
     *====================================================================*/
    static get create():IAttributeFactory {
        if (is.empty(this._factory)) {
            this._factory = new AttributeFactory();
        }
        return this._factory;
    }
    private static _factory:IAttributeFactory;


    static get empty():any { return new Attribute(); }


    get isEmpty():boolean {
        if (is.undefined(this.p_value)) return true;
        if (is.empty(this.p_value, false)) return true;
        if (is.empty(this.key, true)) return true;
        if (is.empty(this.dataType, true)) return true;
        if (this.dataType === DataType.UNDEFINED) return true;
        if (this.dataType === DataType.NULL) return true;

        return false;
    }
} // End class


export default Attribute;
