"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const debug_1 = require("@pilotlab/debug");
const nodes_1 = require("@pilotlab/nodes");
const attributeBase_1 = require("./attributeBase");
const attributesBase_1 = require("./attributesBase");
const attributeEnums_1 = require("./attributeEnums");
const attributeChangeOptions_1 = require("./attributeChangeOptions");
const attributeCreateOptions_1 = require("./attributeCreateOptions");
class AttributesFactoryBase extends nodes_1.NodesFactoryBase {
    constructor(nodeFactory) {
        super(nodeFactory);
    }
    get node() { return this.p_node; }
    instance(parent, ...args) { return null; }
    fromAny(value, parent, ...args) {
        let collection;
        if (value instanceof attributesBase_1.default) {
            collection = this.fromObject(value.toObject(true, true));
        }
        else if (value instanceof attributeBase_1.default) {
            collection = this.fromObject(value.children.toObject(true, true));
        }
        else if (typeof value === 'string') {
            collection = this.fromString(value, parent, null, ...args);
        }
        else if (Array.isArray(value)) {
            collection = this.fromArray(value, parent, ...args);
        }
        else
            collection = this.fromObject(value, parent, ...args);
        if (is_1.default.empty(collection))
            return this.instance(parent, ...args);
        return collection;
    }
    fromObject(value, parent, ...args) {
        try {
            let collection = this.instance(parent);
            if (is_1.default.empty(value))
                return collection;
            if (value instanceof attributesBase_1.default) {
                collection = value.copy;
                collection.internalParent = parent;
                return collection;
            }
            if (value instanceof attributeBase_1.default) {
                collection = value.attributes.copy;
                collection.internalParent = parent;
                return collection;
            }
            for (let propertyKey in value) {
                if (!value.hasOwnProperty(propertyKey))
                    continue;
                let attributeNew;
                let propertyValue = value[propertyKey];
                if (is_1.default.empty(propertyValue)) {
                    /// We got a propertyKey with no associated propertyValue, so just create a new empty node.
                    attributeNew = collection.get(propertyKey, new attributeCreateOptions_1.default(null, attributeEnums_1.DataType.ANY));
                    continue;
                }
                attributeNew = this.node.fromAny({ key: propertyKey, value: propertyValue });
                if (is_1.default.notEmpty(attributeNew))
                    collection.add(attributeNew);
            }
            return collection;
        }
        catch (e) {
            debug_1.default.error(e, 'AttributesFactoryBase.create.fromObject(...)');
            debug_1.default.data(value);
        }
    }
    fromArray(values, parent, ...args) {
        let collection = this.instance();
        for (let i = 0, length = values.length; i < length; i++) {
            let value = values[i];
            let attribute = this.node.fromAny(value);
            if (is_1.default.notEmpty(attribute))
                collection.add(attribute);
        }
        return collection;
    }
    fromAttributes(collectionIn) {
        let collection = this.instance();
        if (is_1.default.empty(collectionIn) || !(collectionIn instanceof attributesBase_1.default))
            return collection;
        collection.update(collectionIn, attributeChangeOptions_1.default.noSaveOrSignal.durationZero);
        return collection;
    }
} // End class
exports.AttributesFactoryBase = AttributesFactoryBase;
exports.default = AttributesFactoryBase;
//# sourceMappingURL=attributesFactoryBase.js.map