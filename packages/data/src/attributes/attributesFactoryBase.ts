import is from '@pilotlab/is';
import Debug from '@pilotlab/debug';
import { NodesFactoryBase } from '@pilotlab/nodes';
import AttributeBase from './attributeBase';
import AttributesBase from './attributesBase';
import { DataType } from './attributeEnums';
import AttributeChangeOptions from './attributeChangeOptions';
import AttributeCreateOptions from './attributeCreateOptions';
import IAttributeFactory from './interfaces/iAttributeFactory';
import IAttributesFactory from './interfaces/iAttributesFactory';
import IAttribute from './interfaces/iAttribute';
import IAttributes from './interfaces/iAttributes';


export abstract class AttributesFactoryBase<TAttributes extends IAttributes>
extends NodesFactoryBase<TAttributes>
implements IAttributesFactory {
    constructor(nodeFactory:IAttributeFactory) {
        super(nodeFactory);
    }


    get node():IAttributeFactory { return this.p_node; }
    protected p_node:IAttributeFactory;


    instance(parent?:IAttribute, ...args:any[]):TAttributes { return null; }


    fromAny(value:(IAttribute | IAttributes | Object | string | Array<any>), parent?:IAttribute, ...args:any[]):TAttributes {
        let collection:TAttributes;

        if (value instanceof AttributesBase) {
            collection = this.fromObject(value.toObject(true, true));
        } else if (value instanceof AttributeBase) {
            collection = this.fromObject(value.children.toObject(true, true));
        } else if (typeof value === 'string') {
            collection = this.fromString(value, parent, null, ...args);
        } else if (Array.isArray(value)) {
            collection = this.fromArray(value, parent, ...args);
        } else collection = this.fromObject(value, parent, ...args);

        if (is.empty(collection)) return this.instance(parent, ...args);
        return collection;
    }


    fromObject(value:any, parent?:IAttribute, ...args:any[]):TAttributes {
        try {
            let collection:TAttributes = this.instance(parent);
            if (is.empty(value)) return collection;

            if (value instanceof AttributesBase) {
                collection = value.copy;
                collection.internalParent = parent;
                return collection;
            }

            if (value instanceof AttributeBase) {
                collection = <TAttributes>value.attributes.copy;
                collection.internalParent = parent;
                return collection;
            }

            for (let propertyKey in value) {
                if (!value.hasOwnProperty(propertyKey)) continue;

                let attributeNew:IAttribute;
                let propertyValue:any = value[propertyKey];


                if (is.empty(propertyValue)) {
                    /// We got a propertyKey with no associated propertyValue, so just create a new empty node.
                    attributeNew = <IAttribute>collection.get(propertyKey, new AttributeCreateOptions(null, DataType.ANY));
                    continue;
                }

                attributeNew = this.node.fromAny({ key: propertyKey, value: propertyValue });
                if (is.notEmpty(attributeNew)) collection.add(attributeNew);
            }

            return collection;
        } catch(e) {
            Debug.error(e, 'AttributesFactoryBase.create.fromObject(...)');
            Debug.data(value);
        }
    }


    fromArray(values:any[], parent?:IAttribute, ...args:any[]):TAttributes {
        let collection:TAttributes = this.instance();

        for (let i = 0, length = values.length; i < length; i++) {
            let value:any = values[i];
            let attribute:IAttribute = this.node.fromAny(value);
            if (is.notEmpty(attribute)) collection.add(attribute);
        }

        return collection;
    }


    fromAttributes(collectionIn:TAttributes):TAttributes {
        let collection:TAttributes = this.instance();
        if (is.empty(collectionIn) || !(collectionIn instanceof AttributesBase)) return collection;
        collection.update(collectionIn, AttributeChangeOptions.noSaveOrSignal.durationZero);
        return collection;
    }
} // End class


export default AttributesFactoryBase;
