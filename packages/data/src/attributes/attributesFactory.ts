import Attributes from './attributes';
import AttributesFactoryBase from './attributesFactoryBase';
import IAttribute from './interfaces/iAttribute';


export class AttributesFactory extends AttributesFactoryBase<Attributes> {
    instance(parent?:IAttribute, ...args:any[]):Attributes { return new Attributes(parent); }
} // End class


export default AttributesFactory;
