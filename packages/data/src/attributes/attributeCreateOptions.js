"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const attributeEnums_1 = require("./attributeEnums");
const nodes_1 = require("@pilotlab/nodes");
const attributeChangeOptions_1 = require("./attributeChangeOptions");
class AttributeCreateOptions extends nodes_1.NodeCreateOptions {
    constructor(value, dataType = attributeEnums_1.DataType.COLLECTION, label, index, key, changeOptions = attributeChangeOptions_1.default.default, omitFromDataStore = (value) => false, isEditable = false, isHidden = false) {
        super(label, key, index);
        this.value = value;
        this.dataType = dataType;
        this.isEditable = isEditable;
        this.isHidden = isHidden;
        this.changeOptions = changeOptions;
        this.omitFromDataStore = omitFromDataStore;
    }
} // End class
exports.AttributeCreateOptions = AttributeCreateOptions;
exports.default = AttributeCreateOptions;
//# sourceMappingURL=attributeCreateOptions.js.map