import is from '@pilotlab/is';
import Attribute from './attribute';
import AttributeFactoryBase from './attributeFactoryBase';
import IAttributesFactory from './interfaces/iAttributesFactory';
import AttributesFactory from './attributesFactory';
import IAttribute from './interfaces/iAttribute';
import IAttributeFactory from './interfaces/iAttributeFactory';
import { DataType } from './attributeEnums';


import * as scopeActions from './library/attributeActions';
import * as scopeAttributeCollection from './library/attributeCollection';
import * as scopeBoolean from './library/attributeBoolean';
import * as scopeByte from './library/attributeByte';
import * as scopeDouble from './library/attributeDouble';
import * as scopeInt from './library/attributeInt';
import * as scopeSelect from './library/attributeSelect';
import * as scopeString from './library/attributeString';
import * as scopeFunction from './library/attributeFunction';
import * as scopeDateTime from './library/attributeDateTime';
import * as scopeRegExp from './library/attributeRegExp';
import * as scopeRange from './library/attributeRange';
import * as scopeAttribute from './attribute';


export class AttributeFactory
extends AttributeFactoryBase<IAttribute> implements IAttributeFactory {
    constructor() {
        super();

        /// Register data types.
        this.register(scopeActions, 'AttributeActions', ['actions'], { baseType: DataType.COLLECTION });
        this.register(scopeAttributeCollection, 'AttributeCollection', ['collection', 'attributes', 'nodes', 'children', 'array'], { baseType: DataType.COLLECTION });
        this.register(scopeBoolean, 'AttributeBoolean', ['boolean', 'bool'], { baseType: DataType.ANY });
        this.register(scopeByte, 'AttributeByte', ['numberbyte', 'number_byte', 'byte'], { baseType: DataType.NUMBER });
        this.register(scopeDouble, 'AttributeDouble', ['numberdouble', 'number_double', 'double', 'float', 'number'], { baseType: DataType.NUMBER });
        this.register(scopeInt, 'AttributeInt', ['numberint', 'number_int', 'int', 'integer'], { baseType: DataType.NUMBER });
        this.register(scopeString, 'AttributeString', ['string'], { baseType: DataType.STRING });
        this.register(scopeString, 'AttributeStringJson', ['stringjson', 'string_json'], { baseType: DataType.STRING });
        this.register(scopeString, 'AttributeStringBase64', ['stringbase64', 'string_base64', 'base64'], { baseType: DataType.STRING });
        this.register(scopeString, 'AttributeStringBinary', ['stringbinary', 'string_binary', 'binary'], { baseType: DataType.STRING });
        this.register(scopeString, 'AttributeStringDataUrl', ['stringdataurl', 'string_data_url', 'dataurl', 'data_url'], { baseType: DataType.STRING });
        this.register(scopeString, 'AttributeStringHexValue', ['stringhexvalue', 'stringhex', 'string_hex_value', 'string_hex', 'hex'], { baseType: DataType.STRING });
        this.register(scopeString, 'AttributeStringHtml', ['stringhtml', 'string_html', 'html'], { baseType: DataType.STRING });
        this.register(scopeString, 'AttributeStringJavascript', ['stringjavascript', 'string_javascript', 'javascript'], { baseType: DataType.STRING });
        this.register(scopeString, 'AttributeStringRegExp', ['stringregexp', 'string_reg_exp', 'regexp', 'reg_exp'], { baseType: DataType.STRING });
        this.register(scopeString, 'AttributeStringSvg', ['stringsvg', 'string_svg', 'svg'], { baseType: DataType.STRING });
        this.register(scopeString, 'AttributeStringUri', ['stringuri', 'string_uri', 'stringurl', 'string_url', 'uri', 'url'], { baseType: DataType.STRING });
        this.register(scopeString, 'AttributeStringXml', ['stringxml', 'string_xml', 'xml'], { baseType: DataType.STRING });
        this.register(scopeFunction, 'AttributeFunction', ['function'], { baseType: DataType.ANY });
        this.register(scopeDateTime, 'AttributeDateTime', ['dateTime', 'date_time', 'date', 'time'], { baseType: DataType.ANY });
        this.register(scopeRegExp, 'AttributeRegExp', ['regexp', 'reg_exp'], { baseType: DataType.ANY });
        this.register(scopeSelect, 'AttributeSelect', ['select'], { baseType: DataType.COLLECTION });
        this.register(scopeRange, 'AttributeRange', ['range'], { baseType: DataType.COLLECTION });
        this.register(scopeAttribute, 'Attribute', ['any', 'object', 'null', 'undefined'], { baseType: DataType.ANY });
    }


    get collection():IAttributesFactory {
        if (is.empty(this.p_collection)) this.p_collection = new AttributesFactory(this);
        return this.p_collection;
    }
    protected p_collection:IAttributesFactory;
} // End class


export default AttributeFactory;
