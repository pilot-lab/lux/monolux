"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DataType;
(function (DataType) {
    DataType["UNDEFINED"] = "attribute";
    DataType["NULL"] = "null";
    DataType["ACTIONS"] = "actions";
    DataType["ANY"] = "any";
    DataType["ARRAY"] = "array";
    DataType["BOOLEAN"] = "boolean";
    DataType["COLLECTION"] = "collection";
    DataType["COLOR"] = "color";
    DataType["CUBE"] = "bounds";
    DataType["DATE_TIME"] = "date_time";
    DataType["FUNCTION"] = "function";
    DataType["OBJECT"] = "object";
    DataType["NUMBER"] = "number";
    DataType["NUMBER_DOUBLE"] = "number_double";
    DataType["NUMBER_INT"] = "number_int";
    DataType["NUMBER_BYTE"] = "number_byte";
    DataType["POINT"] = "point";
    DataType["POINT_3D"] = "point_3d";
    DataType["RANGE"] = "range";
    DataType["RECTANGLE"] = "rectangle";
    DataType["REG_EXP"] = "reg_exp";
    DataType["SELECT"] = "select";
    DataType["SIZE"] = "size";
    DataType["SIZE_3D"] = "size_3d";
    DataType["STRING"] = "string";
    DataType["STRING_BASE64"] = "string_base_64";
    DataType["STRING_BINARY"] = "string_binary";
    DataType["STRING_DATA_URL"] = "string_data_url";
    DataType["STRING_HEX_VALUE"] = "string_hex_value";
    DataType["STRING_HTML"] = "string_html";
    DataType["STRING_JAVASCRIPT"] = "string_javascript";
    DataType["STRING_JSON"] = "string_json";
    DataType["STRING_REG_EXP"] = "string_reg_exp";
    DataType["STRING_SVG"] = "string_svg";
    DataType["STRING_URI"] = "string_uri";
    DataType["STRING_XML"] = "string_xml";
    DataType["VECTOR"] = "vector";
})(DataType = exports.DataType || (exports.DataType = {})); // End enum
var AttributesWrapType;
(function (AttributesWrapType) {
    AttributesWrapType[AttributesWrapType["OVERWRITE_BASE"] = 0] = "OVERWRITE_BASE";
    AttributesWrapType[AttributesWrapType["CLEAR_BASE"] = 1] = "CLEAR_BASE";
    AttributesWrapType[AttributesWrapType["OVERWRITE_WRAPPED"] = 2] = "OVERWRITE_WRAPPED";
    AttributesWrapType[AttributesWrapType["CLEAR_WRAPPED"] = 3] = "CLEAR_WRAPPED";
})(AttributesWrapType = exports.AttributesWrapType || (exports.AttributesWrapType = {}));
var ModelUpdateHandlerSequence;
(function (ModelUpdateHandlerSequence) {
    ModelUpdateHandlerSequence[ModelUpdateHandlerSequence["BEFORE_UPDATE"] = 0] = "BEFORE_UPDATE";
    ModelUpdateHandlerSequence[ModelUpdateHandlerSequence["AFTER_UPDATE"] = 1] = "AFTER_UPDATE";
})(ModelUpdateHandlerSequence = exports.ModelUpdateHandlerSequence || (exports.ModelUpdateHandlerSequence = {})); // End enum
var IdAutoGenerationType;
(function (IdAutoGenerationType) {
    IdAutoGenerationType[IdAutoGenerationType["NONE"] = 0] = "NONE";
    IdAutoGenerationType[IdAutoGenerationType["GLOBALLY_UNIQUE"] = 1] = "GLOBALLY_UNIQUE";
    IdAutoGenerationType[IdAutoGenerationType["SESSION_UNIQUE"] = 2] = "SESSION_UNIQUE";
})(IdAutoGenerationType = exports.IdAutoGenerationType || (exports.IdAutoGenerationType = {}));
var AttributeChangeActions;
(function (AttributeChangeActions) {
    /// Extend the AttributeChangeActions enum from NodeBase,
    /// which includes NONE and SIGNAL_CHANGE
    AttributeChangeActions[AttributeChangeActions["NONE"] = 0] = "NONE";
    AttributeChangeActions[AttributeChangeActions["SIGNAL_CHANGE"] = 1] = "SIGNAL_CHANGE";
    AttributeChangeActions[AttributeChangeActions["SAVE"] = 2] = "SAVE";
})(AttributeChangeActions = exports.AttributeChangeActions || (exports.AttributeChangeActions = {})); // End enum
//# sourceMappingURL=attributeEnums.js.map