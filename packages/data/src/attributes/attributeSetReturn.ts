import { Animation, IAnimationBatch, AnimationBatch } from '@pilotlab/animation';
import IAttribute from './interfaces/iAttribute';
import IAttributeSetReturn from './interfaces/iAttributeSetReturn';
import IAttributeUpdateTracker from './interfaces/iAttributeUpdateTracker';


export class AttributeSetReturn<TAttribute extends IAttribute>
implements IAttributeSetReturn {
    constructor(attribute:TAttribute, isChanged:boolean) {
        this.attribute = attribute;
        this.isChanged = isChanged;
        this.animation = new AnimationBatch(Animation.animate, [], false);
    }


    attribute:TAttribute;
    isChanged:boolean = false;
    animation:IAnimationBatch;


    updateTracker:IAttributeUpdateTracker = null;
} // End class


export default AttributeSetReturn;
