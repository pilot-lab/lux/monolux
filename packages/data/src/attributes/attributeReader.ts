import { Signal } from '@pilotlab/signals';
import { NodeEventArgs } from '@pilotlab/nodes';
import AttributeBase from './attributeBase';


export class AttributeReader<TValue> {
    constructor(attribute:AttributeBase<any, any, any, any>) {
        this.p_attribute = attribute;
    }


    protected p_attribute:AttributeBase<any, any, any, any>;


    get value():TValue { return this.p_attribute.value; }
    get valuePrevious():TValue { return this.p_attribute.valuePrevious; }
    get changed():Signal<NodeEventArgs<any>> { return this.p_attribute.changed; }
} // End class


export default AttributeReader;
