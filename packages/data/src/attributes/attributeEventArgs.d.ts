import { NodeChangeType, NodeEventArgs } from '@pilotlab/nodes';
import { IPromise } from '@pilotlab/result';
import AttributeBase from './attributeBase';
import IAttributeChangeOptions from './interfaces/iAttributeChangeOptions';
export declare class AttributeEventArgs<TAttribute extends AttributeBase<any, any, any, any>> extends NodeEventArgs<TAttribute> {
    constructor(attribute: TAttribute, changeType: NodeChangeType, changeOptions?: IAttributeChangeOptions, result?: IPromise<any>, isMultipleNodeChange?: boolean);
    changeOptions: IAttributeChangeOptions;
}
export default AttributeEventArgs;
