import Attributes from './attributes';
import AttributeBase from './attributeBase';
import IAttributeFactory from './interfaces/iAttributeFactory';
import AttributeRoot from './attributeRoot';
export declare class Attribute extends AttributeBase<any, Attribute, Attributes, AttributeRoot> {
    constructor(value?: any, label?: string, key?: string);
    static readonly create: IAttributeFactory;
    private static _factory;
    static readonly empty: any;
    readonly isEmpty: boolean;
}
export default Attribute;
