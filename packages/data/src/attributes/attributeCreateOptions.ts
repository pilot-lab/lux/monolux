import { DataType } from './attributeEnums';
import { NodeCreateOptions } from '@pilotlab/nodes';
import IAttributeCreateOptions from './interfaces/iAttributeCreateOptions';
import IAttributeChangeOptions from './interfaces/iAttributeChangeOptions';
import AttributeChangeOptions from './attributeChangeOptions';


export class AttributeCreateOptions
extends NodeCreateOptions
implements IAttributeCreateOptions {
    constructor(
        value?:any,
        dataType:(DataType | string) = DataType.COLLECTION,
        label?:string,
        index?:number,
        key?:string,
        changeOptions:IAttributeChangeOptions = AttributeChangeOptions.default,
        omitFromDataStore:(value:any) => boolean = (value:any) => false,
        isEditable:boolean = false,
        isHidden:boolean = false
    ) {
        super(label, key, index);

        this.value = value;
        this.dataType = dataType;
        this.isEditable = isEditable;
        this.isHidden = isHidden;
        this.changeOptions = changeOptions;
        this.omitFromDataStore = omitFromDataStore;
    }


    value:any;
    dataType:(DataType | string);
    isEditable:boolean;
    isHidden:boolean;
    omitFromDataStore:(value:any) => boolean;
    changeOptions:IAttributeChangeOptions;
} // End class


export default AttributeCreateOptions;
