export enum DataType {
    UNDEFINED = 'attribute',
    NULL = 'null',
    ACTIONS = 'actions',
    ANY = 'any',
    ARRAY = 'array',
    BOOLEAN = 'boolean',
    COLLECTION = 'collection',
    COLOR = 'color',
    CUBE = 'bounds',
    DATE_TIME = 'date_time',
    FUNCTION = 'function',
    OBJECT = 'object',
    NUMBER = 'number',
    NUMBER_DOUBLE = 'number_double',
    NUMBER_INT = 'number_int',
    NUMBER_BYTE = 'number_byte',
    POINT = 'point',
    POINT_3D = 'point_3d',
    RANGE = 'range',
    RECTANGLE = 'rectangle',
    REG_EXP = 'reg_exp',
    SELECT = 'select',
    SIZE = 'size',
    SIZE_3D = 'size_3d',
    STRING = 'string',
    STRING_BASE64 = 'string_base_64',
    STRING_BINARY = 'string_binary',
    STRING_DATA_URL = 'string_data_url',
    STRING_HEX_VALUE = 'string_hex_value',
    STRING_HTML = 'string_html',
    STRING_JAVASCRIPT = 'string_javascript',
    STRING_JSON = 'string_json',
    STRING_REG_EXP = 'string_reg_exp',
    STRING_SVG = 'string_svg',
    STRING_URI = 'string_uri',
    STRING_XML = 'string_xml',
    VECTOR = 'vector'
} // End enum


export enum AttributesWrapType {
    OVERWRITE_BASE,
    CLEAR_BASE,
    OVERWRITE_WRAPPED,
    CLEAR_WRAPPED
}


export enum ModelUpdateHandlerSequence {
    BEFORE_UPDATE,
    AFTER_UPDATE
} // End enum


export enum IdAutoGenerationType {
    NONE,
    GLOBALLY_UNIQUE,
    SESSION_UNIQUE
}


export enum AttributeChangeActions {
    /// Extend the AttributeChangeActions enum from NodeBase,
    /// which includes NONE and SIGNAL_CHANGE
    NONE = 0,
    SIGNAL_CHANGE = 1 << 0,
    SAVE = 1 << 1
} // End enum

