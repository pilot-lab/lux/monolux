import is from '@pilotlab/is';
import { NodeType } from '@pilotlab/nodes';
import { DataType } from './attributeEnums';
import Attributes from './attributes';
import AttributeBase from './attributeBase';
import IAttributeFactory from './interfaces/iAttributeFactory';
import Attribute from './attribute';
import IAttributes from "./interfaces/iAttributes";


export class AttributeRoot
extends AttributeBase<Attributes, AttributeBase<any, any, any, any>, Attributes, AttributeRoot>  {
    constructor(
        data?:(IAttributes | Object | string),
        key?:string,
        label?:string
    ) {
        super(AttributeRoot.create,  data, DataType.COLLECTION, label, key, NodeType.ROOT, true);
    }


    static get create():IAttributeFactory {
        if (is.empty(this._factory)) this._factory = Attribute.create;
        return this._factory;
    }
    private static _factory:IAttributeFactory;
} // End class


export default AttributeRoot;
