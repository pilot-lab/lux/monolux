"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const attributes_1 = require("./attributes");
const attributesFactoryBase_1 = require("./attributesFactoryBase");
class AttributesFactory extends attributesFactoryBase_1.default {
    instance(parent, ...args) { return new attributes_1.default(parent); }
} // End class
exports.AttributesFactory = AttributesFactory;
exports.default = AttributesFactory;
//# sourceMappingURL=attributesFactory.js.map