"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const nodes_1 = require("@pilotlab/nodes");
const attribute_1 = require("../attribute");
const attributeBase_1 = require("../attributeBase");
const attributeEnums_1 = require("../attributeEnums");
class AttributeDateTime extends attributeBase_1.default {
    constructor(value = Date.now(), label, key) {
        super(attribute_1.default.create, value, attributeEnums_1.DataType.DATE_TIME, label, key, nodes_1.NodeType.BASIC, true);
    }
} // End class
exports.AttributeDateTime = AttributeDateTime;
exports.default = AttributeDateTime;
//# sourceMappingURL=attributeDateTime.js.map