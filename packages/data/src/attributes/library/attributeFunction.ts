import { NodeType } from '@pilotlab/nodes';
import Attribute from '../attribute';
import AttributeBase from '../attributeBase';
import Attributes from '../attributes';
import AttributeRoot from '../attributeRoot';
import { DataType } from '../attributeEnums';


export class AttributeFunction
    extends AttributeBase<Function, AttributeFunction, Attributes, AttributeRoot> {
    constructor(value:Function = () => {}, label?:string, key?:string) {
        super(Attribute.create, value, DataType.FUNCTION, label, key, NodeType.BASIC, true);
    }
} // End class


export default AttributeFunction;
