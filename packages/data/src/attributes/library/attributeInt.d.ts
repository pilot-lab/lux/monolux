import AttributeNumberBase from './attributeNumberBase';
export declare class AttributeInt extends AttributeNumberBase {
    constructor(value?: number, label?: string, key?: string);
}
export default AttributeInt;
