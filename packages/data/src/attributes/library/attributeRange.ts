import is from '@pilotlab/is';
import { NodeType } from '@pilotlab/nodes';
import AttributeBase from '../attributeBase';
import Attributes from '../attributes';
import AttributeRoot from '../attributeRoot';
import Attribute from '../attribute';
import { DataType } from '../attributeEnums';
import IAttributes from '../interfaces/iAttributes';
import AttributeDouble from './attributeDouble';


export interface IRangeAttributes extends IAttributes {
    readonly current:AttributeDouble;
    readonly min:AttributeDouble;
    readonly max:AttributeDouble;
    readonly step:AttributeDouble;
}


export class AttributeRange
extends AttributeBase<any, AttributeRange, Attributes, AttributeRoot> {
    constructor(value?:any, label?:string, key?:string, isInitialize:boolean = true) {
        super(Attribute.create, null, DataType.RANGE, label, key, NodeType.COLLECTION, false);

        if (is.empty(value, true)) {
            value = { current: 0, min: 0, max: 100, step: 0 };
        } else if (Array.isArray(value)) {
            value = {
                current: value[0],
                min: is.notEmpty(value[1]) ? value[1] : 0,
                max: is.notEmpty(value[2]) ? value[2] : 0,
                step: is.notEmpty(value[3]) ? value[3] : 0
            };
        }

        if (is.empty(value.current)) value.current = 0;
        if (is.empty(value.min)) value.min = 0;
        if (is.empty(value.max)) value.max = 0;
        if (is.empty(value.step)) value.step = 0;

        if (isInitialize) this.initialize(value, DataType.RANGE, NodeType.COLLECTION);
    }


    /*====================================================================*
     START: Property placeholders
     *====================================================================*/
    current:number;
    min:number;
    max:number;
    step:number;
    readonly attributes:IRangeAttributes;

    get stepped():number {
        if (this.step <= 0) return this.current;

        const overflow:number = (this.current % this.step);
        let current:number = this.current - overflow;

        if (overflow > this.step / 2) current += this.step;

        return current;
    }


    get isEmpty():boolean {  return is.empty(this.current) }
} // End of class


export default AttributeRange;
