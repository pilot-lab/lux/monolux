import { NodeType } from '@pilotlab/nodes';
import AttributeBase from '../attributeBase';
import Attributes from '../attributes';
import AttributeRoot from '../attributeRoot';
import Attribute from '../attribute';
import { DataType } from '../attributeEnums';


export class AttributeActions
    extends AttributeBase<any, AttributeActions, Attributes, AttributeRoot> {
    constructor(value?:any, label?:string, key?:string, isInitialize:boolean = true) {
        super(Attribute.create, null, DataType.ACTIONS, label, key, NodeType.COLLECTION, false);

        this.action = '';

        if (isInitialize) this.initialize(value, DataType.ACTIONS, NodeType.COLLECTION);
    }


    action:string;
} // End of class


export default AttributeActions;

