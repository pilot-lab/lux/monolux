"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const nodes_1 = require("@pilotlab/nodes");
const attributeBase_1 = require("../attributeBase");
const attribute_1 = require("../attribute");
const attributeChangeOptions_1 = require("../attributeChangeOptions");
const attributeEnums_1 = require("../attributeEnums");
class AttributeSelect extends attributeBase_1.default {
    constructor(value, label, key, isInitialize = true) {
        super(attribute_1.default.create, null, attributeEnums_1.DataType.SELECT, label, key, nodes_1.NodeType.COLLECTION, false);
        this.selected = 'none';
        if (isInitialize)
            this.initialize(value, attributeEnums_1.DataType.SELECT, nodes_1.NodeType.COLLECTION);
        this.attributes.isSignalChange = true;
        this.attributes.changed.listen((args) => {
            if (args.node.value) {
                this._singleSelect(args.node.key);
                this.selected = args.node.key;
                this.doChanged(attributeEnums_1.AttributeChangeActions.SIGNAL_CHANGE);
            }
        });
    }
    select(key) {
        this.attributes.get(key).set(true, attributeChangeOptions_1.default.save);
        return this;
    }
    _singleSelect(key) {
        for (let i = 0, length = this.attributes.list.size; i < length; i++) {
            const attribute = this.attributes.list.item(i);
            if (key !== attribute.key) {
                attribute.set(false, attributeChangeOptions_1.default.save);
            }
        }
    }
} // End of class
exports.AttributeSelect = AttributeSelect;
exports.default = AttributeSelect;
//# sourceMappingURL=attributeSelect.js.map