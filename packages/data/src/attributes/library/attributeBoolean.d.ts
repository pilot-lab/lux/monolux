import AttributeBase from '../attributeBase';
import Attributes from '../attributes';
import AttributeRoot from '../attributeRoot';
export declare class AttributeBoolean extends AttributeBase<boolean, AttributeBoolean, Attributes, AttributeRoot> {
    constructor(value?: boolean, label?: string, key?: string);
}
export default AttributeBoolean;
