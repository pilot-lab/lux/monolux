import AttributeBase from '../attributeBase';
import Attributes from '../attributes';
import AttributeRoot from '../attributeRoot';
export declare class AttributeFunction extends AttributeBase<Function, AttributeFunction, Attributes, AttributeRoot> {
    constructor(value?: Function, label?: string, key?: string);
}
export default AttributeFunction;
