import { NodeType } from '@pilotlab/nodes';
import Attribute from '../attribute';
import AttributeBase from '../attributeBase';
import Attributes from '../attributes';
import AttributeRoot from '../attributeRoot';
import { DataType } from '../attributeEnums';


export class AttributeBoolean
extends AttributeBase<boolean, AttributeBoolean, Attributes, AttributeRoot> {
    constructor(value:boolean = false, label?:string, key?:string) {
        super(Attribute.create, value, DataType.BOOLEAN, label, key, NodeType.BASIC, true);
    }
} // End class


export default AttributeBoolean;
