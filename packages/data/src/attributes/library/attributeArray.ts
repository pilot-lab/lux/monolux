import { NodeType } from '@pilotlab/nodes';
import Attribute from '../attribute';
import AttributeBase from '../attributeBase';
import Attributes from '../attributes';
import AttributeRoot from '../attributeRoot';
import { DataType } from '../attributeEnums';


export class AttributeArray
    extends AttributeBase<Array<any>, AttributeArray, Attributes, AttributeRoot> {
    constructor(value:any[] = [], label?:string, key?:string) {
        super(Attribute.create, value, DataType.ARRAY, label, key, NodeType.BASIC, true);
    }
} // End class


export default AttributeArray;
