import AttributeBase from '../attributeBase';
import Attributes from '../attributes';
import AttributeRoot from '../attributeRoot';
export declare class AttributeDateTime extends AttributeBase<number, AttributeDateTime, Attributes, AttributeRoot> {
    constructor(value?: number, label?: string, key?: string);
}
export default AttributeDateTime;
