import { NodeType } from '@pilotlab/nodes';
import { IPromise } from "@pilotlab/result";
import { DataType} from '../attributeEnums';
import Attribute from '../attribute';
import AttributeBase from '../attributeBase';
import Attributes from '../attributes';
import AttributeRoot from '../attributeRoot';
import IAttributes from '../interfaces/iAttributes'


export class AttributeCollection
extends AttributeBase<Attributes, AttributeCollection, Attributes, AttributeRoot> {
    constructor(
        data?:(IAttributes | Object | string | Array<any>),
        label?:string,
        key?:string,
        isRoot:boolean = false,
        isInitialize:boolean = true
    ) {
        super(Attribute.create, null, DataType.COLLECTION, label, key, NodeType.COLLECTION, false);
        this._isRoot = isRoot;
        if (isInitialize) this.initialize(data);
    }


    initialize(data?:(IAttributes | Object | string | Array<any>)):IPromise<any> {
        return super.initialize(data, DataType.COLLECTION, this._isRoot ? NodeType.ROOT : NodeType.COLLECTION);
    }


    private _isRoot:boolean;


    // get attributes():Attributes { return <Attributes>this.p_value; }
} // End class


export default AttributeCollection;
