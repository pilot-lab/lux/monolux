"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const nodes_1 = require("@pilotlab/nodes");
const attribute_1 = require("../attribute");
const attributeBase_1 = require("../attributeBase");
const attributeEnums_1 = require("../attributeEnums");
class AttributeFunction extends attributeBase_1.default {
    constructor(value = () => { }, label, key) {
        super(attribute_1.default.create, value, attributeEnums_1.DataType.FUNCTION, label, key, nodes_1.NodeType.BASIC, true);
    }
} // End class
exports.AttributeFunction = AttributeFunction;
exports.default = AttributeFunction;
//# sourceMappingURL=attributeFunction.js.map