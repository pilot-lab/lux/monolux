import AttributeNumberBase from './attributeNumberBase';
import AttributeCreateOptions from '../attributeCreateOptions';
export declare class AttributeDouble extends AttributeNumberBase {
    constructor(value?: number, label?: string, key?: string);
    static create(options: AttributeCreateOptions): AttributeDouble;
}
export default AttributeDouble;
