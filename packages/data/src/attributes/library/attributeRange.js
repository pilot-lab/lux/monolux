"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const nodes_1 = require("@pilotlab/nodes");
const attributeBase_1 = require("../attributeBase");
const attribute_1 = require("../attribute");
const attributeEnums_1 = require("../attributeEnums");
class AttributeRange extends attributeBase_1.default {
    constructor(value, label, key, isInitialize = true) {
        super(attribute_1.default.create, null, attributeEnums_1.DataType.RANGE, label, key, nodes_1.NodeType.COLLECTION, false);
        if (is_1.default.empty(value, true)) {
            value = { current: 0, min: 0, max: 100, step: 0 };
        }
        else if (Array.isArray(value)) {
            value = {
                current: value[0],
                min: is_1.default.notEmpty(value[1]) ? value[1] : 0,
                max: is_1.default.notEmpty(value[2]) ? value[2] : 0,
                step: is_1.default.notEmpty(value[3]) ? value[3] : 0
            };
        }
        if (is_1.default.empty(value.current))
            value.current = 0;
        if (is_1.default.empty(value.min))
            value.min = 0;
        if (is_1.default.empty(value.max))
            value.max = 0;
        if (is_1.default.empty(value.step))
            value.step = 0;
        if (isInitialize)
            this.initialize(value, attributeEnums_1.DataType.RANGE, nodes_1.NodeType.COLLECTION);
    }
    get stepped() {
        if (this.step <= 0)
            return this.current;
        const overflow = (this.current % this.step);
        let current = this.current - overflow;
        if (overflow > this.step / 2)
            current += this.step;
        return current;
    }
    get isEmpty() { return is_1.default.empty(this.current); }
} // End of class
exports.AttributeRange = AttributeRange;
exports.default = AttributeRange;
//# sourceMappingURL=attributeRange.js.map