import Numbers from '@pilotlab/numbers';
import { DataType } from '../attributeEnums';
import AttributeNumberBase from './attributeNumberBase';


export class AttributeByte extends AttributeNumberBase {
    constructor(value:number = 0, label?:string, key?:string) {
        super(value, DataType.NUMBER_BYTE, label, key);

        this.p_formatter = (value:number) => {
            value = Math.round(value);
            return Numbers.clamp(value, 0, 255);
        };
    }
} // End class


export default AttributeByte;
