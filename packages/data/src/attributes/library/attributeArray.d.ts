import AttributeBase from '../attributeBase';
import Attributes from '../attributes';
import AttributeRoot from '../attributeRoot';
export declare class AttributeArray extends AttributeBase<Array<any>, AttributeArray, Attributes, AttributeRoot> {
    constructor(value?: any[], label?: string, key?: string);
}
export default AttributeArray;
