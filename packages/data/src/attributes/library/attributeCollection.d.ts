import { IPromise } from "@pilotlab/result";
import AttributeBase from '../attributeBase';
import Attributes from '../attributes';
import AttributeRoot from '../attributeRoot';
import IAttributes from '../interfaces/iAttributes';
export declare class AttributeCollection extends AttributeBase<Attributes, AttributeCollection, Attributes, AttributeRoot> {
    constructor(data?: (IAttributes | Object | string | Array<any>), label?: string, key?: string, isRoot?: boolean, isInitialize?: boolean);
    initialize(data?: (IAttributes | Object | string | Array<any>)): IPromise<any>;
    private _isRoot;
}
export default AttributeCollection;
