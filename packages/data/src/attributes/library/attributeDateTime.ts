import { NodeType } from '@pilotlab/nodes';
import Attribute from '../attribute';
import AttributeBase from '../attributeBase';
import Attributes from '../attributes';
import AttributeRoot from '../attributeRoot';
import { DataType } from '../attributeEnums';


export class AttributeDateTime
    extends AttributeBase<number, AttributeDateTime, Attributes, AttributeRoot> {
    constructor(value:number = Date.now(), label?:string, key?:string) {
        super(Attribute.create, value, DataType.DATE_TIME, label, key, NodeType.BASIC, true);
    }
} // End class


export default AttributeDateTime;
