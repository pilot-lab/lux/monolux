import { NodeType } from '@pilotlab/nodes';
import Attribute from '../attribute';
import AttributeBase from '../attributeBase';
import Attributes from '../attributes';
import AttributeRoot from '../attributeRoot';
import { DataType } from '../attributeEnums';


export abstract class AttributeStringBase
    extends AttributeBase<string, AttributeStringBase, Attributes, AttributeRoot> {
    constructor(value:string = '', dataType:DataType = DataType.STRING, label?:string, key?:string) {
        super(Attribute.create, value, dataType, label, key, NodeType.BASIC, true);
    }
} // End class


export class AttributeString extends AttributeStringBase {
    constructor(value:string = '', label?:string, key?:string) {
        super(value, DataType.STRING, label, key);
    }
} // End class


export class AttributeStringJson extends AttributeStringBase {
    constructor(value:string = '{}', label?:string, key?:string) {
        super(value, DataType.STRING_JSON, label, key);
    }
} // End class


export class AttributeStringBase64 extends AttributeStringBase {
    constructor(value:string = '', label?:string, key?:string) {
        super(value, DataType.STRING_BASE64, label, key);
    }
} // End class


export class AttributeStringBinary extends AttributeStringBase {
    constructor(value:string = '', label?:string, key?:string) {
        super(value, DataType.STRING_BINARY, label, key);
    }
} // End class


export class AttributeStringDataUrl extends AttributeStringBase {
    constructor(value:string = '', label?:string, key?:string) {
        super(value, DataType.STRING_DATA_URL, label, key);
    }
} // End class


export class AttributeStringHexValue extends AttributeStringBase {
    constructor(value:string = '', label?:string, key?:string) {
        super(value, DataType.STRING_HEX_VALUE, label, key);
    }
} // End class


export class AttributeStringHtml extends AttributeStringBase {
    constructor(value:string = '', label?:string, key?:string) {
        super(value, DataType.STRING_HTML, label, key);
    }
} // End class


export class AttributeStringJavascript extends AttributeStringBase {
    constructor(value:string = '', label?:string, key?:string) {
        super(value, DataType.STRING_JAVASCRIPT, label, key);
    }
} // End class


export class AttributeStringRegExp extends AttributeStringBase {
    constructor(value:string = '', label?:string, key?:string) {
        super(value, DataType.STRING_REG_EXP, label, key);
    }
} // End class


export class AttributeStringSvg extends AttributeStringBase {
    constructor(value:string = '', label?:string, key?:string) {
        super(value, DataType.STRING_SVG, label, key);
    }
} // End class


export class AttributeStringUri extends AttributeStringBase {
    constructor(value:string = '', label?:string, key?:string) {
        super(value, DataType.STRING_URI, label, key);
    }
} // End class


export class AttributeStringXml extends AttributeStringBase {
    constructor(value:string = '', label?:string, key?:string) {
        super(value, DataType.STRING_XML, label, key);
    }
} // End class


export default AttributeString;
