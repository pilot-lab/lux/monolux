import { IAnimationEaseFunction, ISpeed } from '@pilotlab/animation';
import AttributeBase from '../attributeBase';
import Attributes from '../attributes';
import AttributeRoot from '../attributeRoot';
import { DataType } from '../attributeEnums';
import IAttributeChangeOptions from '../interfaces/iAttributeChangeOptions';
import IAttributeSetReturn from '../interfaces/iAttributeSetReturn';
export declare abstract class AttributeNumberBase extends AttributeBase<number, AttributeNumberBase, Attributes, AttributeRoot> {
    protected constructor(value?: any, dataType?: (DataType | string), label?: string, key?: string, isInitialize?: boolean);
    protected p_set(value: any, changeOptions: IAttributeChangeOptions, returnValue: IAttributeSetReturn): void;
    go(target: number, durationSpeed?: (number | ISpeed), ease?: IAnimationEaseFunction, repeatCount?: number): IAttributeSetReturn;
}
export default AttributeNumberBase;
