"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const animation_1 = require("@pilotlab/animation");
const nodes_1 = require("@pilotlab/nodes");
const attribute_1 = require("../attribute");
const attributeBase_1 = require("../attributeBase");
const attributeEnums_1 = require("../attributeEnums");
const attributeChangeOptions_1 = require("../attributeChangeOptions");
class AttributeNumberArray extends attributeBase_1.default {
    constructor(value, dataType, label, key, isInitialize = true) {
        super(attribute_1.default.create, value, dataType, label, key, nodes_1.NodeType.BASIC, isInitialize);
        this.p_formatter = (value) => {
            return value;
        };
    }
    /*====================================================================*
    START: Methods
    *====================================================================*/
    p_set(value, changeOptions, returnValue) {
        /// If we're running an animation, interrupt it.
        if (is_1.default.notEmpty(this.p_animation)) {
            this.p_animation.interrupt();
            this.p_animation = null;
        }
        /// Run this function where we simply need to set the value and update the returnValue.
        const setValue = () => {
            this.p_value = value;
            this.p_valueTarget = value;
            returnValue.isChanged = true;
            this.doChanged(changeOptions.action);
        };
        if (is_1.default.notEmpty(changeOptions.durationSpeed) && changeOptions.durationSpeed !== 0) {
            /// A duration or speed value was passed,
            /// so we may need to set up an animation.
            let duration = animation_1.Animation.getDuration(changeOptions.durationSpeed);
            if (duration > 0) {
                if (this.p_value !== value) {
                    let easeFinal = animation_1.Animation.validateSpeed(changeOptions.durationSpeed).ease;
                    this.p_valueTarget = value;
                    returnValue.animation.animations.clear();
                    returnValue.isChanged = true;
                    /// We need to animate
                    const animation = animation_1.Animation.go(this.p_value, value, duration, easeFinal, changeOptions.repeatCount, this.animationKey);
                    animation.ticked.listen((args) => {
                        this.p_value = args.values[0].current;
                        const action = changeOptions.action === attributeEnums_1.AttributeChangeActions.SAVE
                            ? attributeEnums_1.AttributeChangeActions.SIGNAL_CHANGE
                            : changeOptions.action;
                        this.doChanged(action);
                    }, this);
                    animation.completed.listenOnce(() => {
                        this.p_value = value;
                        this.doChanged(changeOptions.action);
                    });
                    returnValue.animation.animations.add(animation);
                    this.p_animation = returnValue.animation;
                }
                else if (changeOptions.action === attributeEnums_1.AttributeChangeActions.SAVE) {
                    this.doChanged(changeOptions.action);
                }
            }
            else
                setValue();
        }
        else
            setValue();
    }
    go(target, durationSpeed, ease, repeatCount = 0) {
        const options = new attributeChangeOptions_1.default(attributeEnums_1.AttributeChangeActions.SIGNAL_CHANGE, durationSpeed, ease);
        options.repeatCount = repeatCount;
        return this.set(target, options);
    }
} // End class
exports.AttributeNumberArray = AttributeNumberArray;
exports.default = AttributeNumberArray;
//# sourceMappingURL=attributeNumberArray.js.map