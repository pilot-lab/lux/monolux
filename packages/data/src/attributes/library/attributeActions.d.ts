import AttributeBase from '../attributeBase';
import Attributes from '../attributes';
import AttributeRoot from '../attributeRoot';
export declare class AttributeActions extends AttributeBase<any, AttributeActions, Attributes, AttributeRoot> {
    constructor(value?: any, label?: string, key?: string, isInitialize?: boolean);
    action: string;
}
export default AttributeActions;
