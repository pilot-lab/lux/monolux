"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const numbers_1 = require("@pilotlab/numbers");
const attributeEnums_1 = require("../attributeEnums");
const attributeNumberBase_1 = require("./attributeNumberBase");
class AttributeByte extends attributeNumberBase_1.default {
    constructor(value = 0, label, key) {
        super(value, attributeEnums_1.DataType.NUMBER_BYTE, label, key);
        this.p_formatter = (value) => {
            value = Math.round(value);
            return numbers_1.default.clamp(value, 0, 255);
        };
    }
} // End class
exports.AttributeByte = AttributeByte;
exports.default = AttributeByte;
//# sourceMappingURL=attributeByte.js.map