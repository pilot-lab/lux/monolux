"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const nodes_1 = require("@pilotlab/nodes");
const attributeBase_1 = require("../attributeBase");
const attribute_1 = require("../attribute");
const attributeEnums_1 = require("../attributeEnums");
class AttributeActions extends attributeBase_1.default {
    constructor(value, label, key, isInitialize = true) {
        super(attribute_1.default.create, null, attributeEnums_1.DataType.ACTIONS, label, key, nodes_1.NodeType.COLLECTION, false);
        this.action = '';
        if (isInitialize)
            this.initialize(value, attributeEnums_1.DataType.ACTIONS, nodes_1.NodeType.COLLECTION);
    }
} // End of class
exports.AttributeActions = AttributeActions;
exports.default = AttributeActions;
//# sourceMappingURL=attributeActions.js.map