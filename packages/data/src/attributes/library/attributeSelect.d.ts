import AttributeBase from '../attributeBase';
import Attributes from '../attributes';
import AttributeRoot from '../attributeRoot';
export declare class AttributeSelect extends AttributeBase<any, AttributeSelect, Attributes, AttributeRoot> {
    constructor(value?: any, label?: string, key?: string, isInitialize?: boolean);
    selected: string;
    select(key: string): this;
    protected _singleSelect(key: string): void;
}
export default AttributeSelect;
