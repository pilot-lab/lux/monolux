import { NodeType } from '@pilotlab/nodes';
import Attribute from '../attribute';
import AttributeBase from '../attributeBase';
import Attributes from '../attributes';
import AttributeRoot from '../attributeRoot';
import { DataType } from '../attributeEnums';


export class AttributeRegExp
    extends AttributeBase<RegExp, AttributeRegExp, Attributes, AttributeRoot> {
    constructor(value:RegExp = /(.*?)/, label?:string, key?:string) {
        super(Attribute.create, value, DataType.REG_EXP, label, key, NodeType.BASIC, true);
    }
} // End class


export default AttributeRegExp;
