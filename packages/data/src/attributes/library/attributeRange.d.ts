import AttributeBase from '../attributeBase';
import Attributes from '../attributes';
import AttributeRoot from '../attributeRoot';
import IAttributes from '../interfaces/iAttributes';
import AttributeDouble from './attributeDouble';
export interface IRangeAttributes extends IAttributes {
    readonly current: AttributeDouble;
    readonly min: AttributeDouble;
    readonly max: AttributeDouble;
    readonly step: AttributeDouble;
}
export declare class AttributeRange extends AttributeBase<any, AttributeRange, Attributes, AttributeRoot> {
    constructor(value?: any, label?: string, key?: string, isInitialize?: boolean);
    current: number;
    min: number;
    max: number;
    step: number;
    readonly attributes: IRangeAttributes;
    readonly stepped: number;
    readonly isEmpty: boolean;
}
export default AttributeRange;
