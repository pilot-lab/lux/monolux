"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const nodes_1 = require("@pilotlab/nodes");
const attribute_1 = require("../attribute");
const attributeBase_1 = require("../attributeBase");
const attributeEnums_1 = require("../attributeEnums");
class AttributeStringBase extends attributeBase_1.default {
    constructor(value = '', dataType = attributeEnums_1.DataType.STRING, label, key) {
        super(attribute_1.default.create, value, dataType, label, key, nodes_1.NodeType.BASIC, true);
    }
} // End class
exports.AttributeStringBase = AttributeStringBase;
class AttributeString extends AttributeStringBase {
    constructor(value = '', label, key) {
        super(value, attributeEnums_1.DataType.STRING, label, key);
    }
} // End class
exports.AttributeString = AttributeString;
class AttributeStringJson extends AttributeStringBase {
    constructor(value = '{}', label, key) {
        super(value, attributeEnums_1.DataType.STRING_JSON, label, key);
    }
} // End class
exports.AttributeStringJson = AttributeStringJson;
class AttributeStringBase64 extends AttributeStringBase {
    constructor(value = '', label, key) {
        super(value, attributeEnums_1.DataType.STRING_BASE64, label, key);
    }
} // End class
exports.AttributeStringBase64 = AttributeStringBase64;
class AttributeStringBinary extends AttributeStringBase {
    constructor(value = '', label, key) {
        super(value, attributeEnums_1.DataType.STRING_BINARY, label, key);
    }
} // End class
exports.AttributeStringBinary = AttributeStringBinary;
class AttributeStringDataUrl extends AttributeStringBase {
    constructor(value = '', label, key) {
        super(value, attributeEnums_1.DataType.STRING_DATA_URL, label, key);
    }
} // End class
exports.AttributeStringDataUrl = AttributeStringDataUrl;
class AttributeStringHexValue extends AttributeStringBase {
    constructor(value = '', label, key) {
        super(value, attributeEnums_1.DataType.STRING_HEX_VALUE, label, key);
    }
} // End class
exports.AttributeStringHexValue = AttributeStringHexValue;
class AttributeStringHtml extends AttributeStringBase {
    constructor(value = '', label, key) {
        super(value, attributeEnums_1.DataType.STRING_HTML, label, key);
    }
} // End class
exports.AttributeStringHtml = AttributeStringHtml;
class AttributeStringJavascript extends AttributeStringBase {
    constructor(value = '', label, key) {
        super(value, attributeEnums_1.DataType.STRING_JAVASCRIPT, label, key);
    }
} // End class
exports.AttributeStringJavascript = AttributeStringJavascript;
class AttributeStringRegExp extends AttributeStringBase {
    constructor(value = '', label, key) {
        super(value, attributeEnums_1.DataType.STRING_REG_EXP, label, key);
    }
} // End class
exports.AttributeStringRegExp = AttributeStringRegExp;
class AttributeStringSvg extends AttributeStringBase {
    constructor(value = '', label, key) {
        super(value, attributeEnums_1.DataType.STRING_SVG, label, key);
    }
} // End class
exports.AttributeStringSvg = AttributeStringSvg;
class AttributeStringUri extends AttributeStringBase {
    constructor(value = '', label, key) {
        super(value, attributeEnums_1.DataType.STRING_URI, label, key);
    }
} // End class
exports.AttributeStringUri = AttributeStringUri;
class AttributeStringXml extends AttributeStringBase {
    constructor(value = '', label, key) {
        super(value, attributeEnums_1.DataType.STRING_XML, label, key);
    }
} // End class
exports.AttributeStringXml = AttributeStringXml;
exports.default = AttributeString;
//# sourceMappingURL=attributeString.js.map