import { INodesFactory } from '@pilotlab/nodes';
import IAttributes from './iAttributes';
import IAttribute from './iAttribute';


export interface IAttributesFactory extends INodesFactory{
    instance(parent?:IAttribute, ...args:any[]):IAttributes;
    fromAny(data:(IAttributes | Object | string), parent?:IAttribute, ...args:any[]):IAttributes;
    fromString(data:string, parent?:IAttribute, parseString?:(data:string) => IAttributes, ...args:any[]):IAttributes;
    fromJson(json:string, parent?:IAttribute, ...args:any[]):IAttributes;
    fromObject(obj:Object, parent?:IAttribute, ...args:any[]):IAttributes;
    fromArray(value:any[], parent?:IAttribute, ...args:any[]):IAttributes;
    fromAttributes(collection:IAttributes, parent?:IAttribute, ...args:any[]):IAttributes;
} // End interface


export default IAttributesFactory;
