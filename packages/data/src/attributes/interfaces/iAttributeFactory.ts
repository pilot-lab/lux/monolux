import { INodeFactory } from '@pilotlab/nodes';
import IAttributesFactory from './iAttributesFactory';
import IAttributeCreateOptions from './iAttributeCreateOptions';
import IAttribute from './iAttribute';


export interface IAttributeFactory extends INodeFactory {
    collection:IAttributesFactory;


    getBaseDataType(dataType:string):string;


    instance(createOptions?:IAttributeCreateOptions):IAttribute;
    fromAny(value:(IAttribute | Object | string), ...args:any[]):IAttribute;
    fromString(value:string, parseString?:(data:string) => IAttribute, ...args:any[]):IAttribute;
    fromJson(value:string, ...args:any[]):IAttribute;
    fromObject(value:Object, ...args:any[]):IAttribute;
    fromArray(value:any[], ...args:any[]):IAttribute;
} // End interface


export default IAttributeFactory;
