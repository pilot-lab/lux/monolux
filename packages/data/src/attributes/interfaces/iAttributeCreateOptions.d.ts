import { INodeCreateOptions } from '@pilotlab/nodes';
import { DataType } from '../attributeEnums';
import IAttributeChangeOptions from './iAttributeChangeOptions';
export interface IAttributeCreateOptions extends INodeCreateOptions {
    value: any;
    dataType: (DataType | string);
    isEditable: boolean;
    isHidden: boolean;
    omitFromDataStore: (value: any) => boolean;
    changeOptions: IAttributeChangeOptions;
}
export default IAttributeCreateOptions;
