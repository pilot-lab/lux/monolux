import { IPromise } from '@pilotlab/result';
import { ProgressTracker } from '@pilotlab/progress';
import IAttributes from './iAttributes';
import IAttributeChangeOptions from './iAttributeChangeOptions';
import { IAnimationBatch } from '@pilotlab/animation';
export interface IAttributeUpdateTracker {
    progressTracker: ProgressTracker;
    attributesNew: IAttributes;
    changeOptions: IAttributeChangeOptions;
    animation: IAnimationBatch;
    result: IPromise<any>;
    isChanged: boolean;
    attributesChanged: IAttributes;
    then(onDone: (value: any) => any, onError?: (error: Error) => void): IPromise<any>;
    update(attributesToUpdate: IAttributes): void;
}
export default IAttributeUpdateTracker;
