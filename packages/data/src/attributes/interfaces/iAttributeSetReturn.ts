import { IAnimationBatch } from '@pilotlab/animation';
import IAttribute from './iAttribute';
import IAttributeUpdateTracker from './iAttributeUpdateTracker';


export interface IAttributeSetReturn {
    attribute:IAttribute;
    isChanged:boolean;
    animation:IAnimationBatch;
    updateTracker:IAttributeUpdateTracker;
} // End interface


export default IAttributeSetReturn;
