import { ISpeed } from '@pilotlab/animation';
import { INodeChangeOptions, NodeChangeInitiationType } from '@pilotlab/nodes';
import { AttributeChangeActions } from '../attributeEnums';
export interface IAttributeChangeOptions extends INodeChangeOptions {
    readonly durationZero: IAttributeChangeOptions;
    readonly durationDefault: IAttributeChangeOptions;
    setDuration(value: (number | ISpeed)): IAttributeChangeOptions;
    readonly noSaveOrSignal: IAttributeChangeOptions;
    readonly signal: IAttributeChangeOptions;
    readonly save: IAttributeChangeOptions;
    readonly isSignalChange: boolean;
    readonly isSave: boolean;
    readonly action: AttributeChangeActions;
    setAction(value: AttributeChangeActions): IAttributeChangeOptions;
    readonly changeInitiationType: NodeChangeInitiationType;
    setChangeInitiationType(value: NodeChangeInitiationType): IAttributeChangeOptions;
    readonly copy: IAttributeChangeOptions;
}
export default IAttributeChangeOptions;
