import { INodes } from '@pilotlab/nodes';
import { ProgressTracker } from '@pilotlab/progress';
import { Signal } from '@pilotlab/signals';
import IAttributeSetReturn from '../interfaces/iAttributeSetReturn';
import IAttributeChangeOptions from './iAttributeChangeOptions';
import IAttributeCreateOptions from './iAttributeCreateOptions';
import IAttribute from './iAttribute';
import IAttributeUpdateTracker from './iAttributeUpdateTracker';
import IAttributeFactory from './iAttributeFactory';
import AttributeEventArgs from '../attributeEventArgs';
import { DataType } from '../attributeEnums';
export interface IAttributes extends INodes {
    readonly parent: IAttribute;
    create: IAttributeFactory;
    copy: IAttributes;
    isAllowSave: boolean;
    saveTriggered: Signal<AttributeEventArgs<any>>;
    changed: Signal<AttributeEventArgs<any>>;
    /**
     * If an attribute exists at the given path, it will be returned.
     * Otherwise, a new attribute will first be created with the given
     * parameters, then returned.
     */
    getOrCreate(path: string, createOptions?: IAttributeCreateOptions, segmentCreateOptions?: IAttributeCreateOptions): IAttribute;
    get(path: string, createOptions?: IAttributeCreateOptions, changeOptions?: IAttributeChangeOptions): any;
    getOnly(paths: string[]): IAttributes;
    getAll(index?: string, options?: any, filter?: (data: any) => boolean, sort?: (a: any, b: any) => number): IAttributes;
    set(path: string, value?: any, dataType?: (DataType | string), label?: string, index?: number, changeOptions?: IAttributeChangeOptions): IAttributeSetReturn;
    deleteByKey(key: string, changeOptions?: IAttributeChangeOptions): IAttribute;
    interruptAnimationAll(): void;
    update(data: (IAttributes | Object | string), changeOptions?: IAttributeChangeOptions, progressTracker?: ProgressTracker): IAttributeUpdateTracker;
    updateTracked(updateTracker: IAttributeUpdateTracker): void;
    internalChanged(args: AttributeEventArgs<any>): void;
}
export default IAttributes;
