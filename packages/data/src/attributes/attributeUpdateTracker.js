"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const result_1 = require("@pilotlab/result");
const progress_1 = require("@pilotlab/progress");
const attributeEnums_1 = require("./attributeEnums");
const attributeChangeOptions_1 = require("./attributeChangeOptions");
const animation_1 = require("@pilotlab/animation");
class AttributeUpdateTracker {
    constructor(attributesNew, changeOptions = attributeChangeOptions_1.default.default, progressTracker, attributesChanged) {
        this.p_isChanged = false;
        this.attributesNew = attributesNew;
        this.changeOptions = changeOptions;
        this.p_result = new result_1.Result();
        if (is_1.default.notEmpty(progressTracker))
            this.progressTracker = progressTracker;
        /// IMPORTANT:
        /// Be sure to use a factory to create an empty collection here if no attributes were changed,
        /// or this code will break tests with UnhandledPromiseRejectionWarning.
        this.p_attributesChanged = is_1.default.notEmpty(attributesChanged) ? attributesChanged : attributesNew.create.collection.instance();
        this.animation = new animation_1.AnimationBatch(animation_1.Animation.animate, [], false);
        this.isAllowAttributeCreation = true;
    }
    get progressTracker() {
        if (is_1.default.empty(this.p_progressTracker)) {
            this.progressTracker = new progress_1.ProgressTracker();
        }
        return this.p_progressTracker;
    }
    set progressTracker(value) {
        if (is_1.default.empty(value))
            return;
        this.p_progressTracker = value;
        this.p_progressTracker.completed.listenOnce(() => {
            this.animation.initialize();
            this.p_result.resolve(this);
        });
    }
    get result() { return this.p_result; }
    get isChanged() { return this.p_isChanged; }
    get attributesChanged() { return this.p_attributesChanged; }
    /**
     * This is resolved once the tracker has finished updating all attributes,
     * but before animations are initialized after the update cycle.
     */
    then(onDone, onError) {
        return this.p_result.then(onDone, onError);
    }
    update(attributesToUpdate) {
        for (let i = 0; i < this.attributesNew.list.size; i++) {
            const attributeNew = this.attributesNew.list.item(i);
            const attributeToUpdate = is_1.default.notEmpty(attributesToUpdate) ? attributesToUpdate.get(attributeNew.key) : null;
            if (is_1.default.empty(attributeToUpdate) || attributeToUpdate.isEmpty) {
                if (this.isAllowAttributeCreation) {
                    attributesToUpdate.add(attributeNew);
                    this.p_isChanged = true;
                    this.p_attributesChanged.addByPath(attributeNew, attributeNew.path);
                }
                continue;
            }
            if (attributeToUpdate.create.getBaseDataType(attributeToUpdate.dataType) === attributeEnums_1.DataType.COLLECTION)
                this.p_queueAttributesUpdate(attributeToUpdate.value, attributeNew);
            this.p_queueAttributeUpdate(attributeToUpdate, attributeNew);
        }
    }
    p_queueCommand(command, key) {
        if (is_1.default.empty(command))
            return;
        if (is_1.default.empty(key))
            this.progressTracker.queueCommand(command, [this.attributesNew, this.changeOptions]);
        else {
            let dataAttribute = this.attributesNew.get(key);
            if (is_1.default.empty(dataAttribute) || dataAttribute.isEmpty)
                return;
            this.progressTracker.queueCommand(command, [dataAttribute.value, this]);
        }
    }
    p_queueAttributeUpdate(attributeToUpdate, attributeNew) {
        if (is_1.default.empty(attributeNew))
            attributeNew = this.attributesNew.get(attributeToUpdate.key);
        if (attributeNew.isEmpty)
            return;
        this.progressTracker.queue((args) => {
            let attributeSetReturn = attributeToUpdate.set(attributeNew.value, this.changeOptions);
            if (is_1.default.notEmpty(attributeSetReturn.animation) && attributeSetReturn.animation.animations.size > 0) {
                this.animation.animations.append(attributeSetReturn.animation.animations);
            }
            if (attributeSetReturn.isChanged) {
                this.p_isChanged = true;
                this.p_attributesChanged.addByPath(attributeToUpdate, attributeToUpdate.path);
            }
            args.result.resolve();
        }, this, 'updating ' + attributeToUpdate.key + ' attribute');
    }
    p_queueAttributesUpdate(attributesToUpdate, attributeNew) {
        if (is_1.default.empty(attributesToUpdate) || is_1.default.empty(attributeNew))
            return;
        if (attributeNew.isEmpty
            || attributeNew.create.getBaseDataType(attributeNew.dataType) !== attributeEnums_1.DataType.COLLECTION
            || is_1.default.empty(attributeNew.value)
            || attributeNew.value.size === 0)
            return;
        this.progressTracker.queue((args) => {
            let updateTracker = new AttributeUpdateTracker(attributeNew.value, this.changeOptions, args.subTracker, this.p_attributesChanged);
            attributesToUpdate.updateTracked(updateTracker);
            return updateTracker.result;
        }, this, 'updating ' + attributeNew.key + ' data');
    }
} // End class
exports.AttributeUpdateTracker = AttributeUpdateTracker;
exports.default = AttributeUpdateTracker;
//# sourceMappingURL=attributeUpdateTracker.js.map