"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const animation_1 = require("@pilotlab/animation");
class AttributeSetReturn {
    constructor(attribute, isChanged) {
        this.isChanged = false;
        this.updateTracker = null;
        this.attribute = attribute;
        this.isChanged = isChanged;
        this.animation = new animation_1.AnimationBatch(animation_1.Animation.animate, [], false);
    }
} // End class
exports.AttributeSetReturn = AttributeSetReturn;
exports.default = AttributeSetReturn;
//# sourceMappingURL=attributeSetReturn.js.map