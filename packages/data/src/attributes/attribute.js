"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const nodes_1 = require("@pilotlab/nodes");
const attributeEnums_1 = require("./attributeEnums");
const attributeBase_1 = require("./attributeBase");
const attributeFactory_1 = require("./attributeFactory");
class Attribute extends attributeBase_1.default {
    constructor(value, label, key) {
        super(Attribute.create, value, attributeEnums_1.DataType.NUMBER_DOUBLE, label, key, nodes_1.NodeType.BASIC, true);
    }
    /*====================================================================*
     START: Static
     *====================================================================*/
    static get create() {
        if (is_1.default.empty(this._factory)) {
            this._factory = new attributeFactory_1.default();
        }
        return this._factory;
    }
    static get empty() { return new Attribute(); }
    get isEmpty() {
        if (is_1.default.undefined(this.p_value))
            return true;
        if (is_1.default.empty(this.p_value, false))
            return true;
        if (is_1.default.empty(this.key, true))
            return true;
        if (is_1.default.empty(this.dataType, true))
            return true;
        if (this.dataType === attributeEnums_1.DataType.UNDEFINED)
            return true;
        if (this.dataType === attributeEnums_1.DataType.NULL)
            return true;
        return false;
    }
} // End class
exports.Attribute = Attribute;
exports.default = Attribute;
//# sourceMappingURL=attribute.js.map