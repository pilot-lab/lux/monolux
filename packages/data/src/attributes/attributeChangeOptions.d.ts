import { ISpeed, IAnimationEaseFunction } from '@pilotlab/animation';
import { NodeChangeOptions, NodeChangeInitiationType } from '@pilotlab/nodes';
import { IPromise, Result } from '@pilotlab/result';
import IAttributeChangeOptions from './interfaces/iAttributeChangeOptions';
import { AttributeChangeActions } from './attributeEnums';
export declare class AttributeChangeOptions extends NodeChangeOptions {
    constructor(action?: AttributeChangeActions, durationSpeed?: (number | ISpeed), ease?: IAnimationEaseFunction, changeInitiationType?: NodeChangeInitiationType, configuration?: Object, isInitialize?: boolean);
    protected p_onInitializeStarted(result: Result<any>, args: any[]): IPromise<any>;
    static readonly default: IAttributeChangeOptions;
    static readonly noSaveOrSignal: IAttributeChangeOptions;
    static readonly signal: IAttributeChangeOptions;
    static readonly save: IAttributeChangeOptions;
    static readonly zero: IAttributeChangeOptions;
    readonly action: AttributeChangeActions;
    setAction(value: AttributeChangeActions): IAttributeChangeOptions;
    private _action;
    setDuration(value: (number | ISpeed)): IAttributeChangeOptions;
    readonly durationZero: IAttributeChangeOptions;
    readonly durationDefault: IAttributeChangeOptions;
    readonly noSaveOrSignal: IAttributeChangeOptions;
    readonly signal: IAttributeChangeOptions;
    readonly save: IAttributeChangeOptions;
    readonly isSignalChange: boolean;
    readonly isSave: boolean;
    setChangeInitiationType(value: NodeChangeInitiationType): IAttributeChangeOptions;
    readonly copy: IAttributeChangeOptions;
}
export default AttributeChangeOptions;
