import { NodesBase, INodeChangeOptions } from '@pilotlab/nodes';
import { ProgressTracker } from '@pilotlab/progress';
import { Signal } from '@pilotlab/signals';
import { DataType } from './attributeEnums';
import IAttributeChangeOptions from './interfaces/iAttributeChangeOptions';
import IAttributeCreateOptions from './interfaces/iAttributeCreateOptions';
import IAttributes from './interfaces/iAttributes';
import IAttributeUpdateTracker from './interfaces/iAttributeUpdateTracker';
import IAttributeFactory from './interfaces/iAttributeFactory';
import AttributeEventArgs from './attributeEventArgs';
import IAttributeSetReturn from './interfaces/iAttributeSetReturn';
import AttributeBase from './attributeBase';
export declare abstract class AttributesBase<TAttribute extends AttributeBase<any, any, any, any>, TAttributes extends AttributesBase<any, any>> extends NodesBase<TAttribute> implements IAttributes {
    constructor(factory: IAttributeFactory, parent?: TAttribute, pathSegment?: string);
    readonly create: IAttributeFactory;
    protected p_factory: IAttributeFactory;
    readonly copy: TAttributes;
    saveTriggered: Signal<AttributeEventArgs<TAttribute>>;
    changed: Signal<AttributeEventArgs<TAttribute>>;
    /**
     * If an attribute exists at the given path, it will be returned.
     * Otherwise, if createOptions is supplied, a new attribute will
     * first be created with the given parameters, then returned.
     */
    getOrCreate(path: string, createOptions?: IAttributeCreateOptions, segmentCreateOptions?: IAttributeCreateOptions): TAttribute;
    get(path: string, createOptions?: IAttributeCreateOptions, changeOptions?: IAttributeChangeOptions): any;
    getOnly(paths: string[]): TAttributes;
    getAll(key?: string, options?: any, filter?: (data: any) => boolean, sort?: (a: any, b: any) => number): TAttributes;
    /**
     * If an attribute doesn't already exist at the given path, one will be created.
     */
    set(path: string, value: any, dataType?: (DataType | string), label?: string, index?: number, changeOptions?: IAttributeChangeOptions): IAttributeSetReturn;
    protected p_add(child: TAttribute, index?: number, changeOptions?: IAttributeChangeOptions): void;
    deleteByKey(key: string, changeOptions?: IAttributeChangeOptions): TAttribute;
    interruptAnimationAll(): void;
    isAllowSave: boolean;
    /**
     * <pre><code>
     *    /// Just quietly initialize the data entity, but don't save to the data store or signal a change.
     *    /// This is used when reloading data from the store to initialize the app.
     *
     *    myComponent.data.update(data, AttributeChangeOptions.noSaveOrSignal);
     *
     *    /// Update the data, and signal changes, but don't save to the data store.
     *    /// Used for data entities that don't need to be saved to a store.
     *
     *    myComponent.data.update(data, nodeChangeOptions.flags(AttributeChangeOptions.SIGNAL_CHANGE));
     *
     *    /// Update the data and save to the data store, but don’t signal a change.
     *    /// Used for batch saving, when you may want to signal changes only at the end of the process.
     *
     *    myComponent.data.update(data, nodeChangeOptions.flags(AttributeChangeOptions.SAVE));
     *
     *    /// And, finally, the default mode. Update the data, save to the store and signal the change.
     *
     *    myComponent.data.update(data, AttributeChangeOptions.saveAndSignal);
     * </code></pre>
     */
    update(data: (IAttributes | Object | Array<any> | string), changeOptions?: IAttributeChangeOptions, progressTracker?: ProgressTracker): IAttributeUpdateTracker;
    /**
     * Cascades data down through all attributes and attribute
     * collections to update any that need to be changed.
     */
    updateTracked(updateTracker: IAttributeUpdateTracker): void;
    internalAttachedAll(changeOptions: INodeChangeOptions): void;
}
export default AttributesBase;
