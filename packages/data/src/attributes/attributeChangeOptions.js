"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const nodes_1 = require("@pilotlab/nodes");
const attributeEnums_1 = require("./attributeEnums");
class AttributeChangeOptions extends nodes_1.NodeChangeOptions {
    constructor(action = attributeEnums_1.AttributeChangeActions.SIGNAL_CHANGE, durationSpeed, ease, changeInitiationType, configuration, isInitialize = true) {
        super(null, null, null, null, null, false);
        this._action = attributeEnums_1.AttributeChangeActions.SIGNAL_CHANGE;
        if (isInitialize)
            this.initialize(action, durationSpeed, ease, changeInitiationType, configuration);
    }
    p_onInitializeStarted(result, args) {
        const action = args[0];
        const durationSpeed = args[1];
        const ease = args[2];
        const changeInitiationType = args[3];
        const configuration = args[4];
        this._action = action;
        this.changeInitiationType = is_1.default.notEmpty(changeInitiationType) ? changeInitiationType : nodes_1.NodeChangeInitiationType.NONE;
        if (is_1.default.notEmpty(durationSpeed))
            this.durationSpeed = durationSpeed;
        if (is_1.default.notEmpty(ease))
            this.ease = ease;
        if (is_1.default.notEmpty(configuration))
            this.configuration = configuration;
        return result.resolve();
    }
    static get default() { return new AttributeChangeOptions(); }
    static get noSaveOrSignal() { return new AttributeChangeOptions(attributeEnums_1.AttributeChangeActions.NONE); }
    static get signal() { return new AttributeChangeOptions(attributeEnums_1.AttributeChangeActions.SIGNAL_CHANGE); }
    static get save() { return new AttributeChangeOptions(attributeEnums_1.AttributeChangeActions.SAVE); }
    static get zero() { return new AttributeChangeOptions(attributeEnums_1.AttributeChangeActions.NONE).durationZero; }
    get action() { return this._action; }
    setAction(value) {
        this._action = value;
        return this;
    }
    setDuration(value) {
        this.p_durationSpeed = value;
        return this;
    }
    get durationZero() { return this.copy.setDuration(0); }
    get durationDefault() { return this.copy.setDuration(null); }
    get noSaveOrSignal() { return this.copy.setAction(attributeEnums_1.AttributeChangeActions.NONE); }
    get signal() { return this.copy.setAction(attributeEnums_1.AttributeChangeActions.SIGNAL_CHANGE); }
    get save() { return this.copy.setAction(attributeEnums_1.AttributeChangeActions.SAVE); }
    get isSignalChange() { return this.action === attributeEnums_1.AttributeChangeActions.SIGNAL_CHANGE || this.action === attributeEnums_1.AttributeChangeActions.SAVE; }
    get isSave() { return this.action === attributeEnums_1.AttributeChangeActions.SAVE; }
    setChangeInitiationType(value) {
        this.changeInitiationType = value;
        return this;
    }
    get copy() {
        let changeOptionsCopy = new AttributeChangeOptions(this.action, this.durationSpeed, this.ease, this.changeInitiationType, this.configuration);
        changeOptionsCopy.repeatCount = this.repeatCount;
        return changeOptionsCopy;
    }
} // End class
exports.AttributeChangeOptions = AttributeChangeOptions;
exports.default = AttributeChangeOptions;
//# sourceMappingURL=attributeChangeOptions.js.map