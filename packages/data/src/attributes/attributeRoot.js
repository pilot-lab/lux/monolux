"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const nodes_1 = require("@pilotlab/nodes");
const attributeEnums_1 = require("./attributeEnums");
const attributeBase_1 = require("./attributeBase");
const attribute_1 = require("./attribute");
class AttributeRoot extends attributeBase_1.default {
    constructor(data, key, label) {
        super(AttributeRoot.create, data, attributeEnums_1.DataType.COLLECTION, label, key, nodes_1.NodeType.ROOT, true);
    }
    static get create() {
        if (is_1.default.empty(this._factory))
            this._factory = attribute_1.default.create;
        return this._factory;
    }
} // End class
exports.AttributeRoot = AttributeRoot;
exports.default = AttributeRoot;
//# sourceMappingURL=attributeRoot.js.map