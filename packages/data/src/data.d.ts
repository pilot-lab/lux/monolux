import { ProgressTracker } from '@pilotlab/progress';
import { IPromise, Result } from '@pilotlab/result';
import { Signal } from '@pilotlab/signals';
import IAttribute from './attributes/interfaces/iAttribute';
import IAttributes from './attributes/interfaces/iAttributes';
import IAttributeChangeOptions from './attributes/interfaces/iAttributeChangeOptions';
import IAttributeUpdateTracker from './attributes/interfaces/iAttributeUpdateTracker';
import AttributeBase from './attributes/attributeBase';
import AttributeEventArgs from './attributes/attributeEventArgs';
import AttributeRoot from './attributes/attributeRoot';
import Attributes from './attributes/attributes';
import IData from './interfaces/iData';
import IDataStore from './interfaces/iDataStore';
import { KeyAutoGenerationType } from "./dataEnums";
export declare class Data extends AttributeBase<Attributes, AttributeBase<any, any, any, any>, Attributes, AttributeRoot> implements IData {
    constructor(data?: (Data | IAttributes | Object | string), isStoreKey?: boolean, dataStore?: IDataStore, isSaveDataTypes?: boolean, isAutoSave?: boolean, key?: string, label?: string, progressTracker?: ProgressTracker, isInitialize?: boolean);
    initialize(data?: (Data | IAttributes | Object | string), dataStore?: IDataStore, isSaveDataTypes?: boolean, isAutoSave?: boolean, progressTracker?: ProgressTracker): IPromise<any>;
    protected p_onInitializeStarted(result: Result<any>, args: any[]): IPromise<any>;
    /**
     * If the key is stored, this will be a globally unique ID, used to identify the data
     * object associated with this entity in the data store.
     */
    key: string;
    isStoreKey: boolean;
    protected p_keyAutoGenerationType: KeyAutoGenerationType;
    /**
     * The revision ID, which may be managed by the database and can be used to determine
     * if the data associated with this entity is up-to-date.
     */
    readonly revision: string;
    private _revision;
    /**
     * An SHA-1 hash representing the precise contents of the associated blob or text.
     * Can be used to check the uniqueness of a data object to avoid storing duplicates.
     */
    readonly hash: IAttribute;
    isAutoHash: boolean;
    protected p_isAutoHash: boolean;
    /**
     * The data store where the data associated with this entity will be stored.
     */
    store: IDataStore;
    private _dataStore;
    /**
     * Determines whether a dataType property is saved with the data objects,
     * which can be used to verify the data's type when the object is deserialized.
     */
    isSaveDataTypes: boolean;
    private _isSaveDataTypes;
    /**
     * Toggles auto-save functionality.
     * Can be used to pause auto-saving while changes to the data are batched.
     */
    isAutoSave: boolean;
    private _isAutoSave;
    /**
     * This will be set to true if autoSave() was called while isAutoSave was false.
     * This triggers a save of the data immediately after isAutoSave is set to true.
     */
    private _isAutoSaveQueued;
    storeSet: Signal<IDataStore>;
    /**
     * Dispatched after a response is received back from the data store when data has been saved.
     * Completes with the response from the data store.
     */
    saved: Signal<any>;
    protected p_onChanged(args: AttributeEventArgs<Data>): void;
    saveTriggered: Signal<AttributeEventArgs<Data>>;
    update(data: (Data | Attributes | Object | string), changeOptions?: IAttributeChangeOptions, progressTracker?: ProgressTracker): IAttributeUpdateTracker;
    private _saveDebounceDelayInMilliseconds;
    private _saveCommand;
    save(): IPromise<any>;
    autoSave(): void;
    delete(): IPromise<boolean>;
}
export default Data;
