"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const strings_1 = require("@pilotlab/strings");
const attributeEnums_1 = require("./attributes/attributeEnums");
const attributeBase_1 = require("./attributes/attributeBase");
const attributesBase_1 = require("./attributes/attributesBase");
class DataTools {
    static getDataType(value) {
        let dataType = attributeEnums_1.DataType.ANY;
        // if (is.notEmpty(value) && value.hasOwnProperty('value')) value = value['value'];
        if (value instanceof attributeBase_1.default)
            value.dataType;
        //----- NOTE: There's really no way to determine whether the value is intended to represent
        //----- a URI here, since even a simple string is a valid URI.
        if (is_1.default.undefined(value))
            return attributeEnums_1.DataType.UNDEFINED;
        else if (is_1.default.empty(value, false))
            return attributeEnums_1.DataType.NULL;
        else if (value instanceof attributesBase_1.default || typeof value === 'object') {
            if (value.hasOwnProperty('r') &&
                value.hasOwnProperty('g') &&
                value.hasOwnProperty('b')) {
                dataType = attributeEnums_1.DataType.COLOR;
            }
            else if (value.hasOwnProperty('min') &&
                value.hasOwnProperty('max')) {
                dataType = attributeEnums_1.DataType.RANGE;
            }
            else if (value.hasOwnProperty('x') &&
                value.hasOwnProperty('y')) {
                if (value.hasOwnProperty('w')) {
                    dataType = attributeEnums_1.DataType.VECTOR;
                }
                else {
                    if (value.hasOwnProperty('z'))
                        dataType = attributeEnums_1.DataType.POINT_3D;
                    else
                        dataType = attributeEnums_1.DataType.POINT;
                }
            }
            else if (value.hasOwnProperty('width') &&
                value.hasOwnProperty('height')) {
                if (value.hasOwnProperty('depth'))
                    dataType = attributeEnums_1.DataType.SIZE_3D;
                else
                    dataType = attributeEnums_1.DataType.SIZE;
            }
            else if (value.hasOwnProperty('position') &&
                value.hasOwnProperty('pivot') &&
                value.hasOwnProperty('size') &&
                value.hasOwnProperty('scale')) {
                dataType = attributeEnums_1.DataType.CUBE;
            }
            else if (Array.isArray(value))
                dataType = attributeEnums_1.DataType.ARRAY;
            else if (value.hasOwnProperty('value'))
                dataType = attributeEnums_1.DataType.OBJECT;
            else
                dataType = attributeEnums_1.DataType.COLLECTION;
        }
        else if (value instanceof Date)
            dataType = attributeEnums_1.DataType.DATE_TIME;
        else if (value instanceof RegExp)
            dataType = attributeEnums_1.DataType.REG_EXP;
        else {
            switch (typeof value) {
                case 'boolean':
                    dataType = attributeEnums_1.DataType.BOOLEAN;
                    break;
                case 'number':
                    dataType = attributeEnums_1.DataType.NUMBER;
                    break;
                case 'function':
                    dataType = attributeEnums_1.DataType.FUNCTION;
                    break;
                case 'string':
                    if (strings_1.Strings.isValidHexString(value))
                        dataType = attributeEnums_1.DataType.STRING_HEX_VALUE;
                    else if (value.toLowerCase().indexOf('<html>') > -1)
                        dataType = attributeEnums_1.DataType.STRING_HTML;
                    else if (strings_1.Strings.isSvg(strings_1.Strings.stringToXmlDocument(value)))
                        dataType = attributeEnums_1.DataType.STRING_SVG;
                    else if (strings_1.Strings.isXml(strings_1.Strings.stringToXmlDocument(value)))
                        dataType = attributeEnums_1.DataType.STRING_XML;
                    else if (strings_1.Strings.isDataUrl(value))
                        dataType = attributeEnums_1.DataType.STRING_DATA_URL;
                    else if (strings_1.Strings.isBase64(value))
                        dataType = attributeEnums_1.DataType.STRING_BASE64;
                    else if (strings_1.Strings.isValidUri(value))
                        dataType = attributeEnums_1.DataType.STRING_URI;
                    else
                        dataType = attributeEnums_1.DataType.STRING;
                    break;
                case 'object':
                    dataType = attributeEnums_1.DataType.OBJECT;
                    break;
                default:
                    dataType = attributeEnums_1.DataType.ANY;
            }
        }
        return dataType;
    }
    static setCookie(name, value, expireMilliseconds) {
        /// expire is optional and should be in milliseconds
        let cookie = name + '=' + encodeURI(value);
        if (expireMilliseconds) {
            let today = new Date();
            let expireDate = new Date();
            expireDate.setTime(today.getTime() + expireMilliseconds);
            cookie = cookie.concat(';expires=' + expireDate.toUTCString());
        }
        document.cookie = cookie;
    }
    static getCookie(name) {
        let re = new RegExp('[; ]' + name + '=([^\\s;]*)');
        let sMatch = (' ' + document.cookie).match(re);
        if (name && sMatch)
            return decodeURI(sMatch[1]);
        return null;
    }
} // End Class
exports.DataTools = DataTools;
exports.default = DataTools;
//# sourceMappingURL=dataTools.js.map