"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const initializable_1 = require("@pilotlab/initializable");
const attributeEnums_1 = require("./attributes/attributeEnums");
const attributeChangeOptions_1 = require("./attributes/attributeChangeOptions");
const attributeCreateOptions_1 = require("./attributes/attributeCreateOptions");
class DataSetter extends initializable_1.Initializable {
    constructor() { super(); }
    initialize(dataOwner, data, changeOptions = attributeChangeOptions_1.default.default.durationZero, isBatchStarted = false) { return this.p_initialize([dataOwner, data, changeOptions, isBatchStarted]); }
    p_onInitializeStarted(result, args) {
        this.p_owner = args[0];
        this.p_data = args[1];
        this.p_changeOptions = args[2];
        this.p_isBatchStarted = args[4];
        return result.resolve();
    }
    /*====================================================================*
     START: Properties
     *====================================================================*/
    get owner() { return this.p_owner; }
    get changeOptions() { return this.p_changeOptions; }
    /*====================================================================*
     START: Methods
     *====================================================================*/
    register(key, defaultValue, dataType) {
        return this.p_data.children.get(key, new attributeCreateOptions_1.default(defaultValue, dataType));
    }
    create(dataOwner, data, changeOptions, isBatchStarted = false) {
        let dataSetter = new DataSetter();
        dataSetter.initialize(dataOwner, data, changeOptions, isBatchStarted);
        return dataSetter;
    }
    start(changeOptions, isBatch = false) {
        if (isBatch)
            this.p_isBatchStarted = true;
        if (is_1.default.empty(changeOptions))
            changeOptions = this.changeOptions;
        else
            this.p_changeOptions = changeOptions;
        return this.create(this.owner, this.p_data, changeOptions, isBatch);
    }
    end() {
        if (!this.p_isBatchStarted)
            return;
        this.p_isBatchStarted = false;
        if (this.changeOptions.isSave)
            this.p_data.autoSave();
    }
    p_update(attribute, value, changeOptions) {
        if (is_1.default.empty(changeOptions))
            changeOptions = this.changeOptions;
        attribute.set(value, this.p_isBatchStarted ? changeOptions.copy.setAction(attributeEnums_1.AttributeChangeActions.NONE) : changeOptions);
        return this.create(this.owner, this.p_data, changeOptions, this.p_isBatchStarted);
    }
} // End class
exports.DataSetter = DataSetter;
exports.default = DataSetter;
//# sourceMappingURL=dataSetter.js.map