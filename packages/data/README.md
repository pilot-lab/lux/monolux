# @pilotlab/data

## Usage

New attribute collections must be initialized at instantiation, with an object or array representing the desired schema for the collection, along with default values.

``` js
const schema:any = {
    label: "User",
    value: {
        name: "",
        age: -1,
        superpowers: [ Superpowers.NONE ],
        strength: 0.5
    }
};

const attributeHero:ISuperHero = new AttributeCollection(schema);
```

In addition to providing default values, this initialization allows the attribute to later be updated and validated against the schema.

``` js
attributeHero.update({ name: "Bernard", age: 53, superpowers: [ Superpowers.FREEZE_RAY ], eyeColor: #f00 });
```

In the case above, the `eyeColor` property will be ignored, since it wasn't specified in the initial schema.

Now, we can listen for changes to the attribute and respond accordingly.

``` js
attributeHero.strength.changed.listen((args:AttributeEventArgs<ISuperHero>) => {
    if (args.attribute.value <= 0) args.attribute.parent.stun();
});
```
