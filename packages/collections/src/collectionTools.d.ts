import { ICompareFunction, IEqualsFunction } from '@pilotlab/function-types';
export declare class CollectionTools {
    static defaultCompare<T>(a: T, b: T): number;
    static defaultEquals<T>(a: T, b: T): boolean;
    static defaultToString(item: any): string;
    static makeString<T>(item: T, join?: string): string;
    static isFunction(func: any): boolean;
    static isUndefined(obj: any): boolean;
    static isString(obj: any): boolean;
    static reverseCompareFunction<T>(compareFunction: ICompareFunction<T>): ICompareFunction<T>;
    static compareToEquals<T>(compareFunction: ICompareFunction<T>): IEqualsFunction<T>;
}
export default CollectionTools;
