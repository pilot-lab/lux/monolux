"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const arrayTools_1 = require("./arrayTools");
class List {
    constructor(items) {
        this.p_array = [];
        if (is_1.default.notEmpty(items)) {
            for (let i = 0; i < items.length; i++) {
                this.add(items[i]);
            }
        }
    }
    static fromArray(array) {
        let list = new List();
        for (let i = 0; i < array.length; i++) {
            list.add(array[i]);
        }
        return list;
    }
    get isEmpty() { return this.p_array.length <= 0; }
    get size() { return this.p_array.length; }
    get first() { return this.p_array[0]; }
    get last() { return this.p_array[this.p_array.length]; }
    get array() { return this.p_array; }
    item(index) {
        let item = this.p_array[index];
        return is_1.default.notEmpty(item) ? item : null;
    }
    indexOf(item, fromIndex = 0) { return this.p_array.indexOf(item, fromIndex); }
    add(item, index) {
        if (is_1.default.empty(index))
            index = this.p_array.length;
        if (index < 0 || index > this.p_array.length || is_1.default.empty(item))
            return;
        this.p_array.splice(index, 0, item);
    }
    append(list) {
        if (is_1.default.empty(list))
            return;
        list.forEach((item) => {
            if (is_1.default.notEmpty) {
                this.add(item);
                return true;
            }
            else
                return false;
        });
    }
    copy() {
        let list = new List();
        for (let index = 0; index < this.p_array.length; index++) {
            list.add(this.p_array[index]);
        }
        return list;
    }
    forEach(callback) {
        this.p_array.forEach(callback);
    }
    sort(compareFunction) {
        this.p_array.sort(compareFunction);
    }
    replace(item, index) { this.p_array.splice(index, 1, item); }
    search(item, equalsFunction) {
        if (is_1.default.empty(equalsFunction))
            equalsFunction = (a, b) => { return a === b; };
        if (is_1.default.empty(item))
            return -1;
        for (let index = 0; index < this.p_array.length; index++) {
            if (equalsFunction(this.p_array[index], item))
                return index;
        }
        return -1;
    }
    has(item) { return (this.p_array.indexOf(item) >= 0); }
    delete(item, equalsFunction) {
        if (is_1.default.empty(equalsFunction))
            equalsFunction = (a, b) => { return a === b; };
        if (this.p_array.length < 1 || is_1.default.empty(item))
            return false;
        for (let index = 0; index < this.p_array.length; index++) {
            if (equalsFunction(this.p_array[index], item)) {
                this.p_array.splice(index, 1);
                return true;
            }
        }
        return false;
    }
    deleteAtIndex(index) {
        if (index < 0 || index >= this.p_array.length)
            return undefined;
        let item = this.p_array[index];
        this.p_array.splice(index, 1);
        return item;
    }
    clear() { this.p_array.splice(0, this.p_array.length); }
    equals(other, equalsFunction) {
        if (is_1.default.empty(equalsFunction))
            equalsFunction = (a, b) => { return a === b; };
        if (!(other instanceof List))
            return false;
        if (this.size !== other.size)
            return false;
        for (let index = 0; index < this.p_array.length; index++) {
            if (!equalsFunction(this.p_array[index], other.array[index]))
                return false;
        }
        return true;
    }
    toString() { return arrayTools_1.default.toString(this.copy().array); }
}
exports.List = List;
exports.default = List;
//# sourceMappingURL=list.js.map