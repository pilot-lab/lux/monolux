import { ICompareFunction, IEqualsFunction, ILoopFunction } from '@pilotlab/function-types';
export interface IList<T> {
    readonly isEmpty: boolean;
    readonly size: number;
    readonly first: T;
    readonly last: T;
    readonly array: Array<T>;
    item(index: number): T;
    has(item: T, equalsFunction?: IEqualsFunction<T>): boolean;
    indexOf(item: T, fromIndex?: number): number;
    add(item: T, index?: number): void;
    append(list: IList<T>): void;
    delete(item: T, equalsFunction?: IEqualsFunction<T>): void;
    deleteAtIndex(index: number): T;
    clear(): void;
    sort(compareFunction: ICompareFunction<T>): void;
    forEach(callback: ILoopFunction<T>): void;
}
export default IList;
