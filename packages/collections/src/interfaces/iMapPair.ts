export interface IMapPair<K, V> {
    key: K;
    value: V;
} // End interface


export default IMapPair;
