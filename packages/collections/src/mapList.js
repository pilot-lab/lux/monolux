"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const collectionTools_1 = require("./collectionTools");
class MapList {
    constructor(_toStringFunction) {
        this.table = {};
        this.p_length = 0;
        this._toString = _toStringFunction || collectionTools_1.default.defaultToString;
    }
    get size() { return this.p_length; }
    get(key) {
        let pair = this.table[this._toString(key)];
        if (collectionTools_1.default.isUndefined(pair))
            return undefined;
        return pair.value;
    }
    set(key, value) {
        if (collectionTools_1.default.isUndefined(key) || collectionTools_1.default.isUndefined(value))
            return undefined;
        let ret;
        let k = this._toString(key);
        let previousElement = this.table[k];
        if (collectionTools_1.default.isUndefined(previousElement)) {
            this.p_length++;
            ret = undefined;
        }
        else
            ret = previousElement.value;
        this.table[k] = {
            key: key,
            value: value
        };
        return ret;
    }
    delete(key) {
        let k = this._toString(key);
        let previousElement = this.table[k];
        if (!collectionTools_1.default.isUndefined(previousElement)) {
            delete this.table[k];
            this.p_length--;
            return previousElement.value;
        }
        return undefined;
    }
    get keys() {
        let array = [];
        for (let name in this.table) {
            if (this.table.hasOwnProperty(name)) {
                let pair = this.table[name];
                array.push(pair.key);
            }
        }
        return array;
    }
    get values() {
        let array = [];
        for (let name in this.table) {
            if (this.table.hasOwnProperty(name)) {
                let pair = this.table[name];
                array.push(pair.value);
            }
        }
        return array;
    }
    forEach(callback) {
        for (let name in this.table) {
            if (this.table.hasOwnProperty(name)) {
                let pair = this.table[name];
                let ret = callback(pair.value, pair.key);
                if (ret === false)
                    return;
            }
        }
    }
    has(key) { return !collectionTools_1.default.isUndefined(this.get(key)); }
    clear() {
        this.table = {};
        this.p_length = 0;
    }
    get isEmpty() { return this.p_length <= 0; }
    toString() {
        let toret = "{";
        this.forEach((k, v) => {
            toret = toret + "\n\t" + k.toString() + " : " + v.toString();
            return true;
        });
        return toret + "\n}";
    }
}
exports.MapList = MapList;
exports.default = MapList;
//# sourceMappingURL=mapList.js.map