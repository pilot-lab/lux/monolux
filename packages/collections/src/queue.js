"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const list_1 = require("./list");
class Queue {
    constructor() { this._list = new list_1.default(); }
    get isEmpty() { return this._list.size <= 0; }
    get size() { return this._list.size; }
    add(elem) { this._list.add(elem); }
    deleteFirst() {
        if (this._list.size !== 0) {
            let item = this._list.first;
            this._list.deleteAtIndex(0);
            return item;
        }
        return undefined;
    }
    deleteLast() {
        if (this._list.size !== 0) {
            let item = this._list.last;
            this._list.deleteAtIndex(this._list.size - 1);
            return item;
        }
        return undefined;
    }
    peek() {
        if (this._list.size !== 0)
            return this._list.first;
        return undefined;
    }
    has(elem) { return this._list.has(elem); }
    clear() { this._list.clear(); }
    forEach(callback) { this._list.forEach(callback); }
}
exports.Queue = Queue;
exports.default = Queue;
//# sourceMappingURL=queue.js.map