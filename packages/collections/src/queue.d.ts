import { ILoopFunction } from '@pilotlab/function-types';
export declare class Queue<T> {
    constructor();
    private _list;
    readonly isEmpty: boolean;
    readonly size: number;
    add(elem: T): void;
    deleteFirst(): T;
    deleteLast(): T;
    peek(): T;
    has(elem: T): boolean;
    clear(): void;
    forEach(callback: ILoopFunction<T>): void;
}
export default Queue;
