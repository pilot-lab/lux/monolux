import { ILoopFunction } from '@pilotlab/function-types';
export declare class Stack<T> {
    constructor();
    private _list;
    readonly isEmpty: boolean;
    push(elem: T): void;
    add(elem: T): void;
    pop(): T;
    peek(): T;
    readonly size: number;
    has(elem: T): boolean;
    clear(): void;
    forEach(callback: ILoopFunction<T>): void;
}
export default Stack;
