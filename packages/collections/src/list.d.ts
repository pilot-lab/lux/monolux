import { ICompareFunction, IEqualsFunction, ILoopFunction } from '@pilotlab/function-types';
import IList from './interfaces/iList';
export declare class List<T> implements IList<T> {
    constructor(items?: T[]);
    static fromArray<T>(array: T[]): List<T>;
    readonly isEmpty: boolean;
    readonly size: number;
    readonly first: T;
    readonly last: T;
    readonly array: Array<T>;
    protected p_array: Array<T>;
    item(index: number): T;
    indexOf(item: T, fromIndex?: number): number;
    add(item: T, index?: number): void;
    append(list: List<T>): void;
    copy(): List<T>;
    forEach(callback: ILoopFunction<T>): void;
    sort(compareFunction: ICompareFunction<T>): void;
    replace(item: T, index: number): void;
    search(item: T, equalsFunction?: IEqualsFunction<T>): number;
    has(item: T): boolean;
    delete(item: T, equalsFunction?: IEqualsFunction<T>): boolean;
    deleteAtIndex(index: number): T;
    clear(): void;
    equals(other: List<T>, equalsFunction?: IEqualsFunction<T>): boolean;
    toString(): string;
}
export default List;
