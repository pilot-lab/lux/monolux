"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const collectionTools_1 = require("./collectionTools");
class ArrayTools {
    static indexOf(array, item, equalsFunction) {
        let equals = equalsFunction || collectionTools_1.default.defaultEquals;
        let length = array.length;
        for (let i = 0; i < length; i++) {
            if (equals(array[i], item))
                return i;
        }
        return -1;
    }
    static lastIndexOf(array, item, equalsFunction) {
        let equals = equalsFunction || collectionTools_1.default.defaultEquals;
        let length = array.length;
        for (let i = length - 1; i >= 0; i--) {
            if (equals(array[i], item))
                return i;
        }
        return -1;
    }
    static has(array, item, equalsFunction) {
        return ArrayTools.indexOf(array, item, equalsFunction) >= 0;
    }
    static delete(array, item, equalsFunction) {
        let index = ArrayTools.indexOf(array, item, equalsFunction);
        if (index < 0)
            return false;
        array.splice(index, 1);
        return true;
    }
    static frequency(array, item, equalsFunction) {
        let equals = equalsFunction || collectionTools_1.default.defaultEquals;
        let length = array.length;
        let freq = 0;
        for (let i = 0; i < length; i++) {
            if (equals(array[i], item))
                freq++;
        }
        return freq;
    }
    static equals(array1, array2, equalsFunction) {
        let equals = equalsFunction || collectionTools_1.default.defaultEquals;
        if (array1.length !== array2.length)
            return false;
        let length = array1.length;
        for (let i = 0; i < length; i++) {
            if (!equals(array1[i], array2[i]))
                return false;
        }
        return true;
    }
    static copy(array) { return array.concat(); }
    static swap(array, i, j) {
        if (i < 0 || i >= array.length || j < 0 || j >= array.length)
            return false;
        let temp = array[i];
        array[i] = array[j];
        array[j] = temp;
        return true;
    }
    static toString(array) { return '[' + array.toString() + ']'; }
    static forEach(array, callback) {
        let length = array.length;
        for (let i = 0; i < length; i++) {
            if (callback(array[i]) === false)
                return;
        }
    }
}
exports.ArrayTools = ArrayTools;
exports.default = ArrayTools;
//# sourceMappingURL=arrayTools.js.map