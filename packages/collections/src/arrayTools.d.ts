import { IEqualsFunction } from '@pilotlab/function-types';
export declare class ArrayTools {
    static indexOf<T>(array: T[], item: T, equalsFunction?: IEqualsFunction<T>): number;
    static lastIndexOf<T>(array: T[], item: T, equalsFunction?: IEqualsFunction<T>): number;
    static has<T>(array: T[], item: T, equalsFunction?: IEqualsFunction<T>): boolean;
    static delete<T>(array: T[], item: T, equalsFunction?: IEqualsFunction<T>): boolean;
    static frequency<T>(array: T[], item: T, equalsFunction?: IEqualsFunction<T>): number;
    static equals<T>(array1: T[], array2: T[], equalsFunction?: IEqualsFunction<T>): boolean;
    static copy<T>(array: T[]): T[];
    static swap<T>(array: T[], i: number, j: number): boolean;
    static toString<T>(array: T[]): string;
    static forEach<T>(array: T[], callback: (item: T) => boolean): void;
}
export default ArrayTools;
