# collections

Generic lists, queues, and stacks.

## Install

`sudo npm install --save @pilotlab/collections`

## Usage

```
import {List, Queue, Stack} from '@pilotlab/collections';
