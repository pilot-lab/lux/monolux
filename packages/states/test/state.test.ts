import {
    AsyncTest,
    Setup,
    SetupFixture,
    Teardown,
    TeardownFixture,
    Test,
    TestCase,
    TestFixture,
    Timeout,
    Expect
} from 'alsatian';


import {
    AttributeBase,
    DataType
} from '@pilotlab/data';


import {
    Message,
    StateBase,
    State,
    States,
    StateFactory,
    IState
} from '../index';


import Debug from '@pilotlab/debug';
import is from '@pilotlab/is';
import { IPromise, Result } from '@pilotlab/result';


@TestFixture("State Tests")
export class Tests {

    @TestCase(null)
    @AsyncTest("Enter and exit states")
    @Timeout(5000) // Alsatian will now wait 5 seconds before failing
    async testEnterExit(testValue:any) {
        const promise:Promise<any> = new Promise((resolve, reject) => {
            const states:States = new States();
            states.add(new State(null, 'I am so very sad', 'sad'));
            states.set('happy', null, 'A very happy state');
            states.set('joyful', null, 'Oh joy of joys');

            states.entered.listen((state:State) => {
                Debug.log(`State entered: ${state.key}, label: ${state.label}`);
            }, false);

            states.exited.listen((state:State) => {
                Debug.log(`State exited: ${state.key}, label: ${state.label}`);
                resolve();
            }, false);

            states.enter('sad');
            states.push('happy');
            states.exit('happy');
            states.enter('joyful');
        });

        const result:any = await promise;
    }

}
