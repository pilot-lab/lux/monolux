import is from '@pilotlab/is';


export class Message {
    constructor(message:string, options?:Object) {
        this.message = message;
        if (is.notEmpty(options)) this.options = options;
    }


    message:string = '';
    options:Object = {};
} // End class


export default Message;
