import StatesBase from './statesBase';
import State from './state';
import IState from './interfaces/iState';
export declare class States extends StatesBase<State> {
    constructor(parent?: any);
    static readonly empty: IState;
}
export default States;
