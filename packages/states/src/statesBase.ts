import is from '@pilotlab/is';
import Debug from '@pilotlab/debug';
import {
    IAttributeChangeOptions,
    IAttributeSetReturn,
    IAttributeFactory,
    AttributesBase,
    AttributeChangeOptions,
    AttributeCreateOptions,
    DataType
} from '@pilotlab/data';
import { List, MapList, Stack } from '@pilotlab/collections';
import { Signal } from '@pilotlab/signals';
import Message from './message';
import StateFactory from './stateFactory';
import StateBase from './stateBase';
import IStates from './interfaces/iStates';


export abstract class StatesBase<TState extends StateBase<any>>
extends AttributesBase<TState, StatesBase<TState>>
implements IStates {
    protected constructor(
        factory:IAttributeFactory = new StateFactory(),
        parent?:TState,
        pathSegment:string = '.'
    ) {
        super(factory, parent ? parent : <TState>factory.instance(new AttributeCreateOptions(null, DataType.COLLECTION)), pathSegment);
        this.internalCurrentPrincipalIndex = 0;
        this.p_pathPrevious = '';
        this.p_pathLast = '';
    }


    /*====================================================================*
     START: Factory
     *====================================================================*/
    get create():StateFactory { return this.p_factory; }
    protected p_factory:StateFactory;


    /*====================================================================*
     START: Properties
     *====================================================================*/
    internalCurrentPrincipalIndex:number;
    protected p_mapPrincipal:MapList<string, any> = new MapList<string, any>();
    protected p_skipOnPrevious:List<string> = new List<string>();
    protected p_lastState:TState;


    get active():Stack<TState> { return this.p_active; }
    protected p_active:Stack<TState> = new Stack<TState>();


    get previous():TState {
        if (is.notEmpty(this.p_previousState)) return this.p_previousState;
        else return <TState>this.create.instance();
    }
    protected p_previousState:TState;


    get current():TState {
        if (this.p_active.size > 0 && this.p_active.peek().isCurrent) return this.p_active.peek();
        else this.create.instance();
    }


    get pathPrevious():string { return this.p_pathPrevious; }
    protected p_pathPrevious:string;


    get pathLast():string { return this.p_pathLast; }
    protected p_pathLast:string;


    /*====================================================================*
     START: Signals
     *====================================================================*/
    resetCompleted:Signal<any> = new Signal<any>();
    entered:Signal<TState> = new Signal<TState>();
    exited:Signal<TState> = new Signal<TState>();
    message:Signal<Message> = new Signal<Message>();


    /*====================================================================*
     START: Methods
     *====================================================================*/
    reset(
        baseStateName?:string,
        value?:any
    ):this {
        while (this.p_active.size > 0) {
            let state:TState = this.p_active.pop();
            state.internalIsOnStack = false;
            state.internalIsCurrent = false;
            state.internalPrevious = null;
            state.reset();
        }

        if (is.notEmpty(baseStateName)) this.push(baseStateName);
        this.resetCompleted.dispatch(value);
        this.p_onReset(value);
        return this;
    }
    protected p_onReset(value?:any) {}


    isCurrent(name:string):boolean {
        if (is.notEmpty(this.current) && is.notEmpty(this.current.key)) return this.current.key.localeCompare(name) === 0;
        else return false;
    }


    add(
        state:TState,
        index?:number,
        changeOptions:IAttributeChangeOptions = AttributeChangeOptions.default
    ):TState {
        try {
            if (is.empty(state) || is.empty(state.key)) return <TState>this.create.instance();
            let stateNew:TState = super.add(state, index, changeOptions);

            stateNew.entered.listen((stateEntered:TState) => { this.entered.dispatch(stateEntered);});
            stateNew.exited.listen((stateExited:TState) => { this.exited.dispatch(stateExited);});
            return stateNew;
        } catch (e) {
            Debug.error(e, 'StatesBase.add(...)');
            return null;
        }
    }


    set(
        path:string,
        value?:any,
        label?:string
    ):IAttributeSetReturn {
        return super.set(path, value, DataType.COLLECTION, label);
    }


    /*--------------------------------------------------------------------*
     START: Principal States
     *--------------------------------------------------------------------*/
    addPrincipal(
        path:string,
        value?:any,
        isSkipOnPrevious:boolean = false
    ):IAttributeSetReturn {
        this.p_mapPrincipal.set(path, value);
        if (isSkipOnPrevious) this.p_skipOnPrevious.add(path);
        return this.set(path, value);
    }


    removePrincipal(path:string):TState {
        if (this.p_skipOnPrevious.has(path)) this.p_skipOnPrevious.delete(path);
        const state:TState  = this.p_mapPrincipal.get(path);
        this.p_mapPrincipal.delete(path);
        this.deleteByKey(path);
        return state;
    }


    isPrincipal(path:string):boolean { return this.p_mapPrincipal.has(path); }


    getPrincipalIndex(path:string):number {
        if (this.isPrincipal(path)) return this.p_mapKeys.keys.indexOf(path);
        else return -1;
    }


    goFirst(
        value?:any,
        changeOptions:IAttributeChangeOptions = AttributeChangeOptions.default
    ):TState {
        this.internalCurrentPrincipalIndex = 0;
        let path:string = this.p_mapPrincipal.keys[this.internalCurrentPrincipalIndex];
        if (is.empty(path)) return <TState>this.create.instance();

        if (is.empty(value)) value = this.p_mapPrincipal.get(path);
        return this.push(path, value, changeOptions, true, true, true);
    }


    goNext(
        value?:any,
        changeOptions:IAttributeChangeOptions = AttributeChangeOptions.default,
        isCycle:boolean = false
    ):TState {
        this.internalCurrentPrincipalIndex++;
        if (this.internalCurrentPrincipalIndex > this.p_mapPrincipal.size - 1) {
            if (isCycle) this.internalCurrentPrincipalIndex = 0;
            else {
                this.internalCurrentPrincipalIndex = this.p_mapPrincipal.size - 1;
                return <TState>this.create.instance();
            }
        }
        let path:string = this.p_mapPrincipal.keys[this.internalCurrentPrincipalIndex];
        if (is.empty(path)) return <TState>this.create.instance();
        if (is.empty(value)) value = this.p_mapPrincipal.get(path);
        return this.push(path, value, changeOptions, true, true, true);
    }


    goPrevious(
        value?:any,
        changeOptions:IAttributeChangeOptions = AttributeChangeOptions.default,
        isCycle:boolean = false
    ):TState {
        this.internalCurrentPrincipalIndex--;
        if (this.internalCurrentPrincipalIndex < 0) {
            if (isCycle) this.internalCurrentPrincipalIndex = this.p_mapKeys.size - 1;
            else {
                this.internalCurrentPrincipalIndex = 0;
                return <TState>this.create.instance();
            }
        }
        let path:string = this.p_mapPrincipal.keys[this.internalCurrentPrincipalIndex];
        if (is.empty(path)) return <TState>this.create.instance();
        else if (this.p_skipOnPrevious.has(path)) {
            this.goPrevious(value, changeOptions, isCycle);
            return;
        }

        if (is.empty(value)) value = this.p_mapPrincipal.get(path);
        return this.push(path, value, changeOptions, true, true, true);
    }


    goLast(
        value?:any,
        changeOptions:IAttributeChangeOptions = AttributeChangeOptions.default
    ):TState {
        this.internalCurrentPrincipalIndex = this.p_mapPrincipal.size - 1;
        let path:string = this.p_mapPrincipal.keys[this.internalCurrentPrincipalIndex];
        if (is.empty(path)) return <TState>this.create.instance();

        if (is.empty(value)) value = this.p_mapPrincipal.get(path);
        return this.push(path, value, changeOptions, true, true, true);
    }


    /*--------------------------------------------------------------------*
     START: Push
     *--------------------------------------------------------------------*/
    internalPush(
        state:TState,
        value:any,
        changeOptions:IAttributeChangeOptions,
        isEnter:boolean = true,
        isExitPrevious:boolean = true,
        isPopAllActiveStates:boolean = true
    ):this {
        if (is.empty(state) || state.isEmpty) return this;

        //----- Update the state with provided data.
        if (is.notEmpty(value)) state.set(value, changeOptions);
        if (is.notEmpty(changeOptions.durationSpeed)) state.durationSpeed = changeOptions.durationSpeed;
        if (is.notEmpty(changeOptions.ease)) state.ease = changeOptions.ease;

        if (isPopAllActiveStates) this.popAll(isExitPrevious);
        //----- Don't push the same state onto the state stack on top of itself
        else if (this.p_active.size > 0 && this.p_active.peek().key.localeCompare(state.key) === 0) {
            //----- Just pop it off the stack, so we can push it back on.
            this.pop(changeOptions, false, false);
        }

        // try {
            if (is.notEmpty(this.p_lastState)) {
                state.internalPrevious = this.p_lastState;
                this.p_previousState = this.p_lastState;
            } else state.internalPrevious = this.create.instance();

            //----- Call ExitState on previous state
            if (isExitPrevious && this.p_active.size > 0 && this.p_active.peek().isCurrent) {
                this.p_active.peek().internalExit();
            }

            this.p_lastState = state;
            this.p_active.push(state);

            state.internalIsOnStack = true;

            if (isEnter) state.internalEnter();

            return this;
        // }
        // catch (e) {
        //     Debug.error(e, 'internalPush(...)');
        //     return this;
        // }
    }


    protected p_pushPath(
        path:string,
        value:any,
        changeOptions:IAttributeChangeOptions,
        isEnter:boolean,
        isExitPrev:boolean,
        isPopAll:boolean
    ):TState {
        if (is.empty(path)) return <TState>this.create.instance();

        let pathSegments:string[] = path.split(this.pathSegmentDelimiter);
        let segment:string;
        let states:StatesBase<TState> = this;
        let state:TState;
        let count:number = 0;

        while (pathSegments.length > 0) {
            segment = pathSegments.shift();
            state = states.internalPushSegment(segment, value, changeOptions, isEnter, isExitPrev, isPopAll);
            states = <StatesBase<TState>>state.children;

            count++;
        }

        return state;
    }


    internalPushSegment(
        segment:string,
        value:any,
        changeOptions:IAttributeChangeOptions,
        isEnter:boolean,
        isExitPrev:boolean,
        isPopAll:boolean
    ):TState {
        let state:TState = this.get(segment);
        this.internalPush(state, value, changeOptions, isEnter, isExitPrev, isPopAll);
        return state;
    }


    enter(
        path:string,
        value?:any,
        changeOptions:IAttributeChangeOptions = AttributeChangeOptions.default
    ):TState { return this.push(path, value, changeOptions, true, true, true); }
    push(
        path:string,
        value?:any,
        changeOptions:IAttributeChangeOptions = AttributeChangeOptions.default,
        isEnter:boolean = true,
        isExitPrev:boolean = true,
        isPopAll:boolean = false
    ):TState {
        //----- If this is a key state, set the current index to match.
        if (this.p_mapPrincipal.has(path) && path !== this.p_mapPrincipal.keys[this.internalCurrentPrincipalIndex]) {
            //----- The path of a key state was pushed and the current index isn't pointing to it,
            //----- so update the current index.
            this.internalCurrentPrincipalIndex = this.p_mapPrincipal.keys.indexOf(path);
        }

        this.p_pathPrevious = this.p_pathLast;
        this.p_pathLast = path;

        if (is.notEmpty(path) && path.indexOf(this.pathSegmentDelimiter) > -1) {
            return this.p_pushPath(path, value, changeOptions, isEnter, isExitPrev, isPopAll);
        } else {
            return this.internalPushSegment(path, value, changeOptions, isEnter, isExitPrev, isPopAll);
        }
    }


    /*--------------------------------------------------------------------*
     START: Pop
     *--------------------------------------------------------------------*/
    exitCurrent(changeOptions:IAttributeChangeOptions = AttributeChangeOptions.default):this { return this.pop(changeOptions); }
    pop(
        changeOptions:IAttributeChangeOptions = AttributeChangeOptions.default,
        isExit:boolean = true,
        isEnterPrevious:boolean = true
    ):this {
        let state:TState = this.p_active.peek();
        if (is.notEmpty(state)) this.p_active.pop();
        if (is.empty(state) || state.isEmpty) return this;

        if (isExit && state.isCurrent) {
            state.internalExit(changeOptions);
        }

        state.internalIsOnStack = false;
        state.internalPrevious = null;

        if (isEnterPrevious && this.p_active.size > 0) this.p_active.peek().internalEnter(null, changeOptions);

        return this;
    }


    popAllAbove(
        state:(TState | string),
        changeOptions:IAttributeChangeOptions = AttributeChangeOptions.default,
        isExitEach:boolean = true,
        isEnter:boolean = true
    ):this {
        if (typeof state === 'string') state = this.get(<string>state);
        if (is.empty(state) || (<TState>state).isEmpty) return;
        while (is.notEmpty(this.p_active.peek()) && this.p_active.peek() != state) {
            this.pop(changeOptions, isExitEach, false);
        }

        if (isEnter)  (<TState>state).internalEnter(null, changeOptions);

        return this;
    }


    exit(
        state:(TState | string),
        changeOptions:IAttributeChangeOptions = AttributeChangeOptions.default
    ):this { return this.popAllAboveInclusive(state, changeOptions); }
    popAllAboveInclusive(
        state:(TState | string),
        changeOptions:IAttributeChangeOptions = AttributeChangeOptions.default,
        isExitEach:boolean = true,
        isEnterPrev:boolean = true
    ):this {
        if (typeof state === 'string') state = this.get(<string>state);
        if (is.empty(state) || (<TState>state).isEmpty) return;
        this.popAllAbove((<TState>state), changeOptions, isExitEach, false);
        return this.pop(changeOptions, isExitEach, isEnterPrev);
    }


    popAll(
        isExitEach:boolean = true,
        changeOptions:IAttributeChangeOptions = AttributeChangeOptions.default
    ):this {
        while (this.p_active.size > 0) { this.pop(changeOptions, isExitEach, false) }
        return this;
    }


    swap(
        state:(TState | string),
        value?:any,
        changeOptions:IAttributeChangeOptions = AttributeChangeOptions.default,
        isExitCurrent:boolean = true
    ):this {
        if (typeof state === 'string') state = this.get(<string>state);
        if (is.empty(state) || (<TState>state).isEmpty) return;
        this.pop(changeOptions, isExitCurrent, false);
        this.internalPush((<TState>state), value, changeOptions, true, false, false);
        return this;
    }


    /*--------------------------------------------------------------------*
     START: Get
     *--------------------------------------------------------------------*/
    get(path:string):TState {
        let state:TState = <TState>super.get(path);
        if (is.empty(state)) state = <TState>this.create.instance();
        return state;
    }


    /*--------------------------------------------------------------------*
     START: BroadcastMessage
     *--------------------------------------------------------------------*/
    /**
     * Use to send messages about the state of the app and pass data.
     */
    broadcastMessage(message:string, data?:any):StatesBase<TState> {
        this.message.dispatch(new Message(message, data));
        return this;
    }
} // End class


export default StatesBase;
