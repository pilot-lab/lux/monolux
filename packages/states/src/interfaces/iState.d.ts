import { IAttributeChangeOptions, IAttribute, IAttributes } from '@pilotlab/data';
import { IAnimationEaseFunction } from '@pilotlab/animation';
import { Signal } from '@pilotlab/signals';
export interface IState extends IAttribute {
    readonly isEmpty: boolean;
    readonly isOnStack: boolean;
    internalIsOnStack: boolean;
    readonly isCurrent: boolean;
    internalIsCurrent: boolean;
    readonly previous: IState;
    durationSpeed: any;
    ease: IAnimationEaseFunction;
    readonly settings: IAttributes;
    internalPrevious: IState;
    readonly resetCompleted: Signal<IState>;
    reset(value?: any, changeOptions?: IAttributeChangeOptions): this;
    readonly entered: Signal<IState>;
    internalEnter(value?: any, changeOptions?: IAttributeChangeOptions): this;
    readonly exited: Signal<IState>;
    internalExit(changeOptions?: IAttributeChangeOptions): this;
    enter(value?: any, changeOptions?: IAttributeChangeOptions): this;
    exit(value?: any, changeOptions?: IAttributeChangeOptions): this;
    push(value?: any, changeOptions?: IAttributeChangeOptions, isEnterState?: boolean, isExitPrevious?: boolean, isPopAll?: boolean): this;
    popAllAbove(changeOptions?: IAttributeChangeOptions, isExitEach?: boolean, isEnterState?: boolean): this;
    popAllAboveInclusive(changeOptions?: IAttributeChangeOptions, isExitEach?: boolean, isEnterPrevious?: boolean): this;
}
export default IState;
