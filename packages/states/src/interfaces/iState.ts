import {
    IAttributeChangeOptions,
    IAttribute,
    IAttributes
} from '@pilotlab/data';
import { IAnimationEaseFunction } from '@pilotlab/animation';
import { Signal } from '@pilotlab/signals';


export interface IState extends IAttribute {
    /*====================================================================*
     START: Properties
     *====================================================================*/
    /**
     * Unlike attributes, states will only be considered empty if no key has been assigned,
     * even if no value has been assigned to the state.
     */
    readonly isEmpty:boolean;


    readonly isOnStack:boolean;
    internalIsOnStack:boolean;


    readonly isCurrent:boolean;
    internalIsCurrent:boolean;


    readonly previous:IState;
    durationSpeed:any;
    ease:IAnimationEaseFunction;


    readonly settings:IAttributes;


    /// Internal
    internalPrevious:IState;


    /*====================================================================*
     START: Signals (dispatched by StatesBase)
     *====================================================================*/
    readonly resetCompleted:Signal<IState>;
    reset(value?:any, changeOptions?:IAttributeChangeOptions):this;


    readonly entered:Signal<IState>;
    internalEnter(value?:any, changeOptions?:IAttributeChangeOptions):this;


    readonly exited:Signal<IState>;
    internalExit(changeOptions?:IAttributeChangeOptions):this;


    /*====================================================================*
     START: Methods
     *====================================================================*/
    /**
     * Enters the state, making it the active state on the stack.
     * Note: this method will update the currentKeyStateIndex of the state collection, if the state is a key state.
     */
    enter(value?:any, changeOptions?:IAttributeChangeOptions):this;
    exit(value?:any, changeOptions?:IAttributeChangeOptions):this;


    /**
     * Pushes the state to the top of the stack.
     * Note: this method will update the currentKeyStateIndex of the state collection, if the state is a key state.
     */
    push(
        value?:any,
        changeOptions?:IAttributeChangeOptions,
        isEnterState?:boolean,
        isExitPrevious?:boolean,
        isPopAll?:boolean
    ):this;


    /**
     * Pop all states that are above this one off the stack.
     */
    popAllAbove(
        changeOptions?:IAttributeChangeOptions,
        isExitEach?:boolean,
        isEnterState?:boolean
    ):this;


    /**
     * Pop all states that are above this one off the stack, then pop this one onto the top.
     */
    popAllAboveInclusive(
        changeOptions?:IAttributeChangeOptions,
        isExitEach?:boolean,
        isEnterPrevious?:boolean
    ):this;
} // End interface


export default IState;
