import is from '@pilotlab/is';
import {
    AttributeFactoryBase,
    AttributesFactoryBase,
    AttributeCreateOptions
} from '@pilotlab/data';
import State from './state';
import States from './states';


export class StatesFactory extends AttributesFactoryBase<States> {
    instance(parent?:State, ...args:any[]):States { return new States(parent); }
} // End class


export class StateFactory extends AttributeFactoryBase<State> {
    instance(createOptions?:AttributeCreateOptions):State {
        if (is.notEmpty(createOptions)) {
            return new State(
                createOptions.value,
                createOptions.label,
                createOptions.key,
            );
        } else return new State('');
    }


    get collection():StatesFactory { return new StatesFactory(this); }
} // End class


export default StateFactory;
