import StatesBase from './statesBase';
import State from './state';
import IState from './interfaces/iState';


export class States extends StatesBase<State> {
    constructor(parent?:any) { super(State.create, parent, '.'); }
    static get empty():IState { return <IState>State.create.instance(); }
} // End class


export default States;
