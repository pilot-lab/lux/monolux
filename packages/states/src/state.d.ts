import { IAttributeFactory } from '@pilotlab/data';
import StateBase from './stateBase';
export declare class State extends StateBase<State> {
    constructor(value?: any, label?: string, key?: string);
    static readonly create: IAttributeFactory;
    private static _factory;
    static readonly empty: any;
}
export default State;
