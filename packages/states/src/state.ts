import is from '@pilotlab/is';
import { IAttributeFactory } from '@pilotlab/data';
import StateBase from './stateBase';
import StateFactory from './stateFactory';


export class State extends StateBase<State> {
    constructor(value?:any, label?:string, key?:string) {
        super(State.create, value, label, key);
    }


    /*====================================================================*
     START: Static
     *====================================================================*/
    static get create():IAttributeFactory {
        if (is.empty(this._factory)) {
            this._factory = new StateFactory();
        }
        return this._factory;
    }
    private static _factory:IAttributeFactory;


    static get empty():any { return new State(); }
} // End class


export default State;
