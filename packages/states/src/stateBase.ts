import is from '@pilotlab/is';
import { IPromise, Result } from '@pilotlab/result';
import {
    AttributeBase,
    AttributeChangeOptions,
    IAttributeChangeOptions,
    IAttributeFactory,
    DataType,
    NodeType,
    AttributeCollection,
    Attributes
} from '@pilotlab/data';
import { IAnimationEaseFunction } from '@pilotlab/animation';
import { Signal } from '@pilotlab/signals';
import StatesBase from './statesBase';
import IState from './interfaces/iState';;


export abstract class StateBase<TState extends StateBase<any>>
extends AttributeBase<any, TState, StatesBase<TState>, TState>
implements IState {
    protected constructor(
        factory:IAttributeFactory,
        settings?:any,
        label?:string,
        key?:string
    ) {
        super(factory, new AttributeCollection(settings), DataType.ANY, label, key, NodeType.BASIC, true);
    }


    /*====================================================================*
     START: Properties
     *====================================================================*/
    /**
     * Unlike attributes, states will only be considered empty if no key has been assigned,
     * even if no value has been assigned to the state.
     */
    get isEmpty():boolean {  return is.empty(this.key); }


    get isOnStack():boolean { return this.internalIsOnStack; }
    set isOnStack(value:boolean) { this.internalIsOnStack = value; }
    internalIsOnStack:boolean = false;


    get isCurrent():boolean { return this.internalIsCurrent; }
    set isCurrent(value:boolean) { this.internalIsCurrent = value; }
    internalIsCurrent:boolean = false;


    get previous():TState {
        if (is.empty(this.internalPrevious)) return <TState>this.create.instance(); // Empty state.
        return this.internalPrevious;
    }
    internalPrevious:TState;


    get durationSpeed():any { return this.p_durationSpeed; }
    set durationSpeed(value:any) { this.p_durationSpeed = value; }
    protected p_durationSpeed:any;


    get ease():IAnimationEaseFunction { return this.p_ease; }
    set ease(value:IAnimationEaseFunction) { this.p_ease = value; }
    protected p_ease:IAnimationEaseFunction;


    get settings():Attributes { return this.value.attributes; }


    /*====================================================================*
     START: Signals (dispatched by States)
     *====================================================================*/
    resetCompleted:Signal<TState> = new Signal<TState>();
    reset(value?:any, changeOptions:IAttributeChangeOptions = AttributeChangeOptions.default):this {
        if (this.isEmpty) return this;
        if (is.notEmpty(value)) this.set(value, changeOptions);
        if (is.notEmpty(changeOptions.durationSpeed)) this.p_durationSpeed = changeOptions.durationSpeed;
        if (is.notEmpty(changeOptions.ease)) this.p_ease = changeOptions.ease;
        this.onReset();
        this.resetCompleted.dispatch(<TState><IState>this);
        return this;
    }
    protected onReset():void {}


    entered:Signal<TState> = new Signal<TState>(false);
    internalEnter(value?:any, changeOptions:IAttributeChangeOptions = AttributeChangeOptions.default):this {
        if (this.isEmpty) return this;
        this.onBeforeEnter();
        this.internalIsCurrent = true;
        if (is.notEmpty(value)) this.set(value, changeOptions);
        if (is.notEmpty(changeOptions.durationSpeed)) this.p_durationSpeed = changeOptions.durationSpeed;
        if (is.notEmpty(changeOptions.ease)) this.p_ease = changeOptions.ease;
        this.onEntered();
        this.entered.dispatch(<TState><IState>this);
        return this;
    }
    protected onBeforeEnter():void {}
    protected onEntered():void {}


    exited:Signal<TState> = new Signal<TState>();
    internalExit(changeOptions:IAttributeChangeOptions = AttributeChangeOptions.default):this {
        if (this.isEmpty) return this;
        this.internalIsCurrent = false;
        if (is.notEmpty(changeOptions.durationSpeed)) this.p_durationSpeed = changeOptions.durationSpeed;
        if (is.notEmpty(changeOptions.ease)) this.p_ease = changeOptions.ease;
        this.onBeforeExit(new Result()).then(() => {
            this.onExited();
            this.exited.dispatch(<TState><IState>this);
        });
        return this;
    }
    protected onBeforeExit(result:IPromise<any>):IPromise<any> { return result.resolve(); }
    protected onExited():void {}


    /*====================================================================*
     START: Methods
     *====================================================================*/
    /**
     * Enters the state, making it the active state on the stack.
     * Note: this method will update the currentPrincipalIndex of the state collection, if the state is a key state.
     */
    enter(
        value?:any,
        changeOptions:IAttributeChangeOptions = AttributeChangeOptions.default
    ):this { return this.push(value, changeOptions, true, true, true); }


    /**
     * Enters the state, making it the active state on the stack.
     * Note: this method will update the currentPrincipalIndex of the state collection, if the state is a key state.
     */
    exit(
        value?:any,
        changeOptions:IAttributeChangeOptions = AttributeChangeOptions.default
    ):this { return this.popAllAboveInclusive(changeOptions); }


    /**
     * Pushes the state to the top of the stack.
     * Note: this method will update the currentPrincipalIndex of the state collection, if the state is a key state.
     */
    push(
        value?:any,
        changeOptions:IAttributeChangeOptions = AttributeChangeOptions.default,
        isEnterState:boolean = true,
        isExitPrevious:boolean = true,
        isPopAll:boolean = false
    ):this {
        if (this.isEmpty || is.empty(this.parentCollection)) return this;
        this.parentCollection.internalPush(<TState><IState>this, value, changeOptions, isEnterState, isExitPrevious, isPopAll);
        this.p_checkPrincipals();
        return this;
    }


    /**
     * Pop all states that are above this one off the stack.
     */
    popAllAbove(
        changeOptions:IAttributeChangeOptions = AttributeChangeOptions.default,
        isExitEach:boolean = true,
        isEnterState:boolean = true
    ):this {
        if (this.isEmpty || !this.isOnStack || is.empty(this.parentCollection)) return this;
        if (!this.parentCollection.popAllAbove(<TState><IState>this, changeOptions, isExitEach, false)) return this;

        if (isEnterState) {
            if (!this.parentCollection.pop(changeOptions, false, false)) return this;
            if (!this.parentCollection.internalPush(<TState><IState>this, null, changeOptions, isEnterState, false, true)) return this;
        }

        return this;
    }


    /**
     * Pop all states that are above this one off the stack, then pop this one onto the top.
     */
    popAllAboveInclusive(
        changeOptions:IAttributeChangeOptions = AttributeChangeOptions.default,
        isExitEach:boolean = true,
        isEnterPrevious:boolean = true
    ):this {
        if (this.isEmpty || !this.isOnStack || is.empty(this.parentCollection)) return this;
        this.popAllAbove(changeOptions, isExitEach, false);
        this.parentCollection.pop(changeOptions, isExitEach, isEnterPrevious);
        return this;
    }


    //===== Protected
    protected p_checkPrincipals():void {
        let pathString:string = this.key;
        let currentState:TState = <TState><IState>this;

        while (is.notEmpty(currentState) && is.notEmpty(currentState.parentCollection)) {
            let keyStateIndex:number = (<StatesBase<TState>>currentState.parentCollection).getPrincipalIndex(pathString);
            if (keyStateIndex > -1) (<StatesBase<TState>>currentState.parentCollection).internalCurrentPrincipalIndex = keyStateIndex;

            currentState = <TState>currentState.parentCollection.parent;
            pathString = currentState.key + '/' + pathString;
        }
    }
} // End class


export default StateBase;
