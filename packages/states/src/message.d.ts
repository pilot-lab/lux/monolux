export declare class Message {
    constructor(message: string, options?: Object);
    message: string;
    options: Object;
}
export default Message;
