import { AttributeFactoryBase, AttributesFactoryBase, AttributeCreateOptions } from '@pilotlab/data';
import State from './state';
import States from './states';
export declare class StatesFactory extends AttributesFactoryBase<States> {
    instance(parent?: State, ...args: any[]): States;
}
export declare class StateFactory extends AttributeFactoryBase<State> {
    instance(createOptions?: AttributeCreateOptions): State;
    readonly collection: StatesFactory;
}
export default StateFactory;
