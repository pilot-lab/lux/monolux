# initializable

Base class for classes that support initialize methods.

## Install

sudo npm install --save @pilotlab/initializable

## Usage

```
import {IInitializable, Initializable} from '@pilotlab/initializable';
```
