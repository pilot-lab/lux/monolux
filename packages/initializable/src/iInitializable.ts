import { IPromise, Result } from '@pilotlab/result';
import { Signal } from '@pilotlab/signals';


export interface IInitializable {
    isInitialized:boolean;
    initialized:Signal<any>;
    initialize(...args:any[]):IPromise<any>;
    resetInitialization():void;
} // End interface


export default IInitializable;
