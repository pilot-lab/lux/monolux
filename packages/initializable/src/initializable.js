"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const result_1 = require("@pilotlab/result");
const signals_1 = require("@pilotlab/signals");
class Initializable {
    constructor() {
        this.p_isInitialized = false;
        this.initialized = new signals_1.Signal(true);
    }
    get isInitialized() { return this.p_isInitialized; }
    ;
    initialize(...args) {
        return this.p_initialize(args);
    }
    p_initialize(args) {
        let result = new result_1.Result();
        let resultFinal = new result_1.Result();
        if (this.isInitialized)
            return result_1.Result.resolve(this.p_initializedValue);
        result.then((value) => {
            if (this.p_isInitialized)
                return;
            this.p_initializedValue = value;
            this.p_isInitialized = true;
            resultFinal.resolve(value);
            this.p_onInitialized(value);
            this.initialized.dispatch(value);
        });
        this.p_onInitializeStarted(result, args);
        return resultFinal;
    }
    resetInitialization() {
        this.initialized.forget();
        this.p_isInitialized = false;
        this.p_initializedValue = null;
    }
    p_onInitializeStarted(result, args) {
        return result.resolve();
    }
    p_onInitialized(value) { }
}
exports.Initializable = Initializable;
exports.default = Initializable;
//# sourceMappingURL=initializable.js.map