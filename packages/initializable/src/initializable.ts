import { IPromise, Result } from '@pilotlab/result';
import { Signal } from '@pilotlab/signals';
import IInitializable from './iInitializable';


export class Initializable implements IInitializable {
    constructor() { this.initialized = new Signal<any>(true); }


    get isInitialized():boolean { return this.p_isInitialized; };
    protected p_isInitialized:boolean = false;


    initialized:Signal<any>;


    /**
     * Remembers whatever value was used to complete the Result after initialization
     */
    protected p_initializedValue:any;


    initialize(...args:any[]):any {
        return this.p_initialize(args);
    }


    protected p_initialize(args:any[]):IPromise<any> {
        let result:Result<any> = new Result<any>();
        let resultFinal:Result<any> = new Result<any>();
        if (this.isInitialized) return Result.resolve(this.p_initializedValue);

        result.then((value:any) => {
            if (this.p_isInitialized) return;
            this.p_initializedValue = value;
            this.p_isInitialized = true;
            resultFinal.resolve(value);
            this.p_onInitialized(value);
            this.initialized.dispatch(value);
        });

        this.p_onInitializeStarted(result, args);

        return resultFinal;
    }


    resetInitialization():void {
        this.initialized.forget();
        this.p_isInitialized = false;
        this.p_initializedValue = null;
    }


    protected p_onInitializeStarted(result:Result<any>, args:any[]):IPromise<any> {
        return result.resolve();
    }


    protected p_onInitialized(value:any):void {}
} // End class


export default Initializable;
