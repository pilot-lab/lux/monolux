import { IPromise } from '@pilotlab/result';
export declare class Identifier {
    static generate(): string;
    static generateAsync(): IPromise<string>;
    static generateStrong(isUuidFormat?: boolean): string;
    static generateRandomUuidFormat(): string;
    private static _MAX_SAFE_INTEGER;
    private static _sessionUniqueIntegerDefault;
    private static _sessionUniqueIntegerCategories;
    static getSessionUniqueInteger(category?: string): number;
    static toByteArray(id: string): number[];
    static encode(...args: number[]): string;
    static decode(encodedString: string): number[];
    static hashFromString(text: string): number;
    private static readonly _isSupportedPerformance;
    private static _getHashIds();
    private static _hashIds;
    private static _getHashIdsStrong();
    private static _hashIdsStrong;
}
export default Identifier;
