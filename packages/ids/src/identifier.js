"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
const result_1 = require("@pilotlab/result");
const hashIds_1 = require("./hashIds");
class Identifier {
    static generate() {
        let now = Date.now();
        let millisecondsPerYear = 31536000000;
        let millisecondsSince2015 = Math.floor(now - (millisecondsPerYear * 45));
        let perf = this._isSupportedPerformance
            ? Math.round(((window.performance.now() % 1) * 1000) / 10)
            : Math.round(Math.random() * 1000);
        return this._getHashIds().encode(millisecondsSince2015, perf);
    }
    static generateAsync() {
        let result = new result_1.Result();
        let generate = function () {
            let now = Date.now();
            let millisecondsPerYear = 31536000000;
            let millisecondsSince2015 = Math.floor(now - (millisecondsPerYear * 45));
            let perf = this._isSupportedPerformance
                ? Math.round(((window.performance.now() % 1) * 1000) / 10)
                : Math.round(Math.random() * 1000);
            let id = this._getHashIds().encode(millisecondsSince2015, perf);
            this.message(id);
        };
        let blob = new Blob([generate.toString()]);
        let blobURL = URL.createObjectURL(blob);
        let worker = new Worker(blobURL);
        worker.onmessage = function (e) {
            result.resolve(e.data);
        };
        worker.postMessage('');
        return result;
    }
    static generateStrong(isUuidFormat = false) {
        let now = Date.now();
        let millisecondsPerYear = 31536000000;
        let millisecondsSince2015 = Math.floor(now - (millisecondsPerYear * 45));
        let perf = this._isSupportedPerformance
            ? Math.round(((window.performance.now() % 1) * 1000) / 10)
            : Math.round(Math.random() * 1000);
        let id = this._getHashIdsStrong().encode(millisecondsSince2015, perf, Math.round((Math.random() * 1000000) * (Math.random() * 1000000)));
        if (isUuidFormat)
            id = id.substring(0, 8) + '-' + id.substring(8, 12) + '-' + id.substring(12, 16) + '-' + id.substring(16);
        return id;
    }
    static generateRandomUuidFormat() {
        function _p8(s) {
            let p = (Math.random().toString(16) + '000000000').substr(2, 8);
            return s ? "-" + p.substr(0, 4) + "-" + p.substr(4, 4) : p;
        }
        return _p8(false) + _p8(true) + _p8(true) + _p8(false);
    }
    static getSessionUniqueInteger(category) {
        if (is_1.default.empty(category)) {
            let sessionUniqueInteger = this._sessionUniqueIntegerDefault;
            if (sessionUniqueInteger >= this._MAX_SAFE_INTEGER) {
                sessionUniqueInteger = 0;
            }
            else
                sessionUniqueInteger++;
            this._sessionUniqueIntegerDefault = sessionUniqueInteger;
            return sessionUniqueInteger;
        }
        else {
            let sessionUniqueInteger = typeof this._sessionUniqueIntegerCategories[category] === 'number'
                ? this._sessionUniqueIntegerCategories[category] : 0;
            if (sessionUniqueInteger >= this._MAX_SAFE_INTEGER) {
                sessionUniqueInteger = 0;
            }
            else
                sessionUniqueInteger++;
            this._sessionUniqueIntegerCategories[category] = sessionUniqueInteger;
            return sessionUniqueInteger;
        }
    }
    static toByteArray(id) {
        let stripped = id.replace('-', '');
        let bytes = [];
        for (let i = 0; i < 16; i++) {
            let s = stripped.substring(i * 2, (i * 2) + 2);
            bytes.push(parseInt('0x' + s));
        }
        return bytes;
    }
    static encode(...args) {
        return this._getHashIds().encode(args);
    }
    static decode(encodedString) {
        return this._getHashIds().decode(encodedString);
    }
    static hashFromString(text) {
        let hash = 0;
        if (text.length == 0)
            return hash;
        for (let i = 0; i < text.length; i++) {
            let char = text.charCodeAt(i);
            hash = ((hash << 5) - hash) + char;
            hash = hash & hash;
        }
        return hash;
    }
    static get _isSupportedPerformance() {
        try {
            if (is_1.default.empty(window))
                return false;
            let win = window;
            const outcome = ('performance' in win);
            return false;
        }
        catch (e) {
            return false;
        }
    }
    static _getHashIds() {
        if (is_1.default.empty(this._hashIds))
            this._hashIds = new hashIds_1.default('salt', 10);
        return this._hashIds;
    }
    static _getHashIdsStrong() {
        if (is_1.default.empty(this._hashIdsStrong))
            this._hashIdsStrong = new hashIds_1.default('salt', 36);
        return this._hashIdsStrong;
    }
}
Identifier._MAX_SAFE_INTEGER = 9007199254740992;
Identifier._sessionUniqueIntegerDefault = 0;
Identifier._sessionUniqueIntegerCategories = {};
exports.Identifier = Identifier;
exports.default = Identifier;
//# sourceMappingURL=identifier.js.map