export declare class HashIds {
    constructor(salt?: string, minLength?: number, alphabet?: string);
    private _salt;
    private _minLength;
    private _alphabet;
    private _guards;
    private _seps;
    encode(...args: (number | number[])[]): string;
    decode(encodedString: string): number[];
    encodeHex(hex: any): string;
    decodeHex(encodedHex: string): string;
    private _encode(numbers);
    private _decode(encodedString, alphabet);
    private _shuffle(alphabet, salt);
    private _toAlphabet(input, alphabet);
    private _fromAlphabet(input, alphabet);
    private _escapeRegExp(s);
    private _parseInt(v, radix);
}
export default HashIds;
