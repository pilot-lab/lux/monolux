"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
class HashIds {
    constructor(salt = '', minLength = 0, alphabet) {
        this._alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        this._seps = 'cfhistuCFHISTU';
        let minAlphabetLength = 16;
        let sepDiv = 3.5;
        let guardDiv = 12;
        let errorAlphabetLength = 'error: alphabet must contain at least X unique characters';
        let errorAlphabetSpace = 'error: alphabet cannot contain spaces';
        let uniqueAlphabet = '';
        let sepsLength;
        let diff;
        this._salt = salt;
        this._minLength = minLength;
        if (is_1.default.notEmpty(alphabet))
            this._alphabet = alphabet;
        this._minLength = this._parseInt(minLength, 10) > 0 ? minLength : 0;
        this._salt = typeof salt === 'string' ? salt : '';
        if (typeof alphabet === 'string')
            this._alphabet = alphabet;
        for (let i = 0; i !== this._alphabet.length; i++) {
            if (uniqueAlphabet.indexOf(this._alphabet.charAt(i)) === -1) {
                uniqueAlphabet += this._alphabet.charAt(i);
            }
        }
        this._alphabet = uniqueAlphabet;
        if (this._alphabet.length < minAlphabetLength) {
            throw errorAlphabetLength.replace('X', '' + minAlphabetLength);
        }
        if (this._alphabet.search(' ') !== -1) {
            throw errorAlphabetSpace;
        }
        for (let _i = 0; _i !== this._seps.length; _i++) {
            let j = this._alphabet.indexOf(this._seps.charAt(_i));
            if (j === -1) {
                this._seps = this._seps.substr(0, _i) + ' ' + this._seps.substr(_i + 1);
            }
            else {
                this._alphabet = this._alphabet.substr(0, j) + ' ' + this._alphabet.substr(j + 1);
            }
        }
        this._alphabet = this._alphabet.replace(/ /g, '');
        this._seps = this._seps.replace(/ /g, '');
        this._seps = this._shuffle(this._seps, this._salt);
        if (!this._seps.length || this._alphabet.length / this._seps.length > sepDiv) {
            sepsLength = Math.ceil(this._alphabet.length / sepDiv);
            if (sepsLength > this._seps.length) {
                diff = sepsLength - this._seps.length;
                this._seps += this._alphabet.substr(0, diff);
                this._alphabet = this._alphabet.substr(diff);
            }
        }
        this._alphabet = this._shuffle(this._alphabet, this._salt);
        let guardCount = Math.ceil(this._alphabet.length / guardDiv);
        if (this._alphabet.length < 3) {
            this._guards = this._seps.substr(0, guardCount);
            this._seps = this._seps.substr(guardCount);
        }
        else {
            this._guards = this._alphabet.substr(0, guardCount);
            this._alphabet = this._alphabet.substr(guardCount);
        }
    }
    encode(...args) {
        let numbers = [];
        for (let i = 0; i < args.length; i++) {
            numbers[i] = args[i];
        }
        let ret = '';
        if (!numbers.length)
            return ret;
        if (numbers[0] && numbers[0].constructor === Array) {
            numbers = numbers[0];
            if (!numbers.length)
                return ret;
        }
        for (let i = 0; i !== numbers.length; i++) {
            numbers[i] = this._parseInt(numbers[i], 10);
            if (numbers[i] < 0)
                return ret;
        }
        return this._encode(numbers);
    }
    decode(encodedString) {
        let ret = [];
        if (!encodedString || !encodedString.length || typeof encodedString !== 'string')
            return ret;
        return this._decode(encodedString, this._alphabet);
    }
    encodeHex(hex) {
        hex = hex.toString();
        if (!/^[0-9a-fA-F]+$/.test(hex))
            return '';
        let numbers = hex.match(/[\w\W]{1,12}/g);
        for (let i = 0; i !== numbers.length; i++) {
            numbers[i] = parseInt('1' + numbers[i], 16);
        }
        return this.encode.apply(this, numbers);
    }
    decodeHex(encodedHex) {
        let ret = '';
        let numbers = this.decode(encodedHex);
        for (let i = 0; i !== numbers.length; i++) {
            ret += numbers[i].toString(16).substr(1);
        }
        return ret;
    }
    _encode(numbers) {
        let ret;
        let alphabet = this._alphabet;
        let numbersIdInt = 0;
        for (let i = 0; i !== numbers.length; i++) {
            numbersIdInt += numbers[i] % (i + 100);
        }
        ret = alphabet.charAt(numbersIdInt % alphabet.length);
        for (let _i2 = 0; _i2 !== numbers.length; _i2++) {
            let number = numbers[_i2];
            let buffer = ret + this._salt + alphabet;
            alphabet = this._shuffle(alphabet, buffer.substr(0, alphabet.length));
            let last = this._toAlphabet(number, alphabet);
            ret += last;
            if (_i2 + 1 < numbers.length) {
                number %= last.charCodeAt(0) + _i2;
                let sepsIndex = number % this._seps.length;
                ret += this._seps.charAt(sepsIndex);
            }
        }
        if (ret.length < this._minLength) {
            let guardIndex = (numbersIdInt + ret[0].charCodeAt(0)) % this._guards.length;
            let guard = this._guards[guardIndex];
            ret = guard + ret;
            if (ret.length < this._minLength) {
                guardIndex = (numbersIdInt + ret[2].charCodeAt(0)) % this._guards.length;
                guard = this._guards[guardIndex];
                ret += guard;
            }
        }
        let halfLength = parseInt('' + (alphabet.length / 2), 10);
        while (ret.length < this._minLength) {
            alphabet = this._shuffle(alphabet, alphabet);
            ret = alphabet.substr(halfLength) + ret + alphabet.substr(0, halfLength);
            let excess = ret.length - this._minLength;
            if (excess > 0) {
                ret = ret.substr(excess / 2, this._minLength);
            }
        }
        return ret;
    }
    _decode(encodedString, alphabet) {
        let ret = [], i = 0, r = new RegExp('[' + this._escapeRegExp(this._guards) + ']', 'g'), idBreakdown = encodedString.replace(r, ' '), idArray = idBreakdown.split(' ');
        if (idArray.length === 3 || idArray.length === 2) {
            i = 1;
        }
        idBreakdown = idArray[i];
        if (typeof idBreakdown[0] !== 'undefined') {
            let lottery = idBreakdown[0];
            idBreakdown = idBreakdown.substr(1);
            r = new RegExp('[' + this._escapeRegExp(this._seps) + ']', 'g');
            idBreakdown = idBreakdown.replace(r, ' ');
            idArray = idBreakdown.split(' ');
            for (let j = 0; j !== idArray.length; j++) {
                let subId = idArray[j];
                let buffer = lottery + this._salt + alphabet;
                alphabet = this._shuffle(alphabet, buffer.substr(0, alphabet.length));
                ret.push(this._fromAlphabet(subId, alphabet));
            }
            if (this._encode(ret) !== encodedString) {
                ret = [];
            }
        }
        return ret;
    }
    _shuffle(alphabet, salt) {
        let integer;
        if (!salt.length)
            return alphabet;
        for (let i = alphabet.length - 1, v = 0, p = 0, j = 0; i > 0; i--, v++) {
            v %= salt.length;
            p += integer = salt.charAt(v).charCodeAt(0);
            j = (integer + v + p) % i;
            let tmp = alphabet[j];
            alphabet = alphabet.substr(0, j) + alphabet.charAt(i) + alphabet.substr(j + 1);
            alphabet = alphabet.substr(0, i) + tmp + alphabet.substr(i + 1);
        }
        return alphabet;
    }
    _toAlphabet(input, alphabet) {
        let id = '';
        do {
            id = alphabet.charAt(input % alphabet.length) + id;
            input = this._parseInt(input / alphabet.length, 10);
        } while (input);
        return id;
    }
    _fromAlphabet(input, alphabet) {
        let number = 0;
        for (let i = 0; i < input.length; i++) {
            let pos = alphabet.indexOf(input[i]);
            number += pos * Math.pow(alphabet.length, input.length - i - 1);
        }
        return number;
    }
    _escapeRegExp(s) {
        return s.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
    }
    _parseInt(v, radix) {
        return (/^(\-|\+)?([0-9]+|Infinity)$/.test('' + v) ? parseInt('' + v, radix) : NaN);
    }
}
exports.HashIds = HashIds;
exports.default = HashIds;
//# sourceMappingURL=hashIds.js.map