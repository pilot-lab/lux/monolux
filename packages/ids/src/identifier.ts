import is from '@pilotlab/is';
import { IPromise, Result } from '@pilotlab/result';
import HashIds from './hashIds';


export class Identifier {
    static generate():string {
        let now:number = Date.now();
        let millisecondsPerYear:number = 31536000000;
        let millisecondsSince2015:number = Math.floor(now - (millisecondsPerYear * 45));
        let perf:number = this._isSupportedPerformance
            ? Math.round(((window.performance.now() % 1) * 1000) / 10)
            : Math.round(Math.random() * 1000);

        return this._getHashIds().encode(millisecondsSince2015, perf);
    }


    static generateAsync():IPromise<string> {
        let result:Result<string> = new Result<string>();

        let generate = function () {
            let now:number = Date.now();
            let millisecondsPerYear:number = 31536000000;
            let millisecondsSince2015:number = Math.floor(now - (millisecondsPerYear * 45));
            let perf:number = this._isSupportedPerformance
                ? Math.round(((window.performance.now() % 1) * 1000) / 10)
                : Math.round(Math.random() * 1000);
            let id:string = this._getHashIds().encode(millisecondsSince2015, perf);
            this.message(id)
        };

        let blob:Blob = new Blob([generate.toString()]);

        /// Obtain a blob URL reference to our worker 'file'.
        let blobURL:string = URL.createObjectURL(blob);

        let worker:Worker = new Worker(blobURL);
        worker.onmessage = function (e) {
            /// Logger message from worker.
            result.resolve(e.data);
        };

        //---- Start the worker.
        worker.postMessage('');

        return result;
    }


    static generateStrong(isUuidFormat:boolean = false):string {
        let now:number = Date.now();
        let millisecondsPerYear:number = 31536000000;
        let millisecondsSince2015:number = Math.floor(now - (millisecondsPerYear * 45));
        let perf:number = this._isSupportedPerformance
            ? Math.round(((window.performance.now() % 1) * 1000) / 10)
            : Math.round(Math.random() * 1000);
        let id:string = this._getHashIdsStrong().encode(millisecondsSince2015, perf, Math.round((Math.random() * 1000000) * (Math.random() * 1000000)));

        if (isUuidFormat) id = id.substring(0, 8) + '-' + id.substring(8, 12) + '-' + id.substring(12, 16) + '-' + id.substring(16);

        return id;
    }


    static generateRandomUuidFormat():string {
        function _p8(s:boolean) {
            let p = (Math.random().toString(16)+'000000000').substr(2, 8);
            return s ? "-" + p.substr(0, 4) + "-" + p.substr(4, 4) : p ;
        }

        return _p8(false) + _p8(true) + _p8(true) + _p8(false);
    }


    private static _MAX_SAFE_INTEGER:number = 9007199254740992;
    private static _sessionUniqueIntegerDefault:number = 0;
    private static _sessionUniqueIntegerCategories:any = {};
    static getSessionUniqueInteger(category?:string):number {
        if (is.empty(category)) {
            let sessionUniqueInteger:number = this._sessionUniqueIntegerDefault;

            ///- The maximum safe integer in JavaScript is 9007199254740992.
            if (sessionUniqueInteger >= this._MAX_SAFE_INTEGER) {
                sessionUniqueInteger = 0;
            } else sessionUniqueInteger++;

            this._sessionUniqueIntegerDefault = sessionUniqueInteger;

            return sessionUniqueInteger;
        } else {
            let sessionUniqueInteger:number = typeof this._sessionUniqueIntegerCategories[category] === 'number'
                ? this._sessionUniqueIntegerCategories[category] : 0;

            if (sessionUniqueInteger >= this._MAX_SAFE_INTEGER) {
                sessionUniqueInteger = 0;
            } else sessionUniqueInteger++;

            this._sessionUniqueIntegerCategories[category] = sessionUniqueInteger;

            return sessionUniqueInteger;
        }
    }


    static toByteArray(id:string):number[] {
        /// Performs exactly the same function as _uuid.parse(guid);
        /// Keeping here for future reference.
        let stripped:string = id.replace('-', '');

        let bytes:number[] = [];

        for (let i:number = 0; i < 16; i++) {
            let s = stripped.substring(i * 2, (i * 2) + 2);
            bytes.push(parseInt('0x' + s));
        }

        return bytes;
    }


    static encode(...args:number[]):string {
        return this._getHashIds().encode(args);
    }


    static decode(encodedString:string):number[] {
        return this._getHashIds().decode(encodedString);
    }


    static hashFromString(text:string):number {
        let hash:number = 0;
        if (text.length == 0) return hash;

        for (let i:number = 0; i < text.length; i++) {
            let char:number = text.charCodeAt(i);
            hash = ((hash << 5) - hash) + char;
            hash = hash & hash; // Convert to 32bit integer
        }

        return hash;
    }


    private static get _isSupportedPerformance():boolean {
        try {
            if (is.empty(window)) return false;

            let win:any = window;
            const outcome:any = ('performance' in win);
            return false;
        } catch(e) { return false; }
    }


    private static _getHashIds():HashIds {
        if (is.empty(this._hashIds)) this._hashIds = new HashIds('salt', 10);
        return this._hashIds;
    }
    private static _hashIds:HashIds;


    private static _getHashIdsStrong():HashIds {
        if (is.empty(this._hashIdsStrong)) this._hashIdsStrong = new HashIds('salt', 36);
        return this._hashIdsStrong;
    }
    private static _hashIdsStrong:HashIds;
} // End of class


export default Identifier;
