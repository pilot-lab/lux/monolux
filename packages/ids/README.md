# ids

Generates unique IDs

## Install

`sudo npm install --save @pilotlab/ids`

## Usage

```
import {Identifier} from '@pilotlab/ids';

let id:string = Identifier.generate();
```
