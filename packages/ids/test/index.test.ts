import {Expect, Test, TestCase, TestFixture} from 'alsatian';
import {Identifier} from '../index';
import {Debug} from '@pilotlab/debug';


@TestFixture('Identifier')
export class Tests {
    @Test('ID is generated')
    test01() {
        Debug.log(Identifier.generate(), 'generated ID');
        Debug.log(Identifier.generate(), 'generated ID');
        Debug.log(Identifier.generateStrong(), 'generated strong ID');
        Debug.log(Identifier.getSessionUniqueInteger(), 'session-unique ID');
        Debug.log(Identifier.getSessionUniqueInteger(), 'session-unique ID');
    }
}
