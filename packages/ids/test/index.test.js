"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const alsatian_1 = require("alsatian");
const index_1 = require("../index");
const lux_debug_1 = require("@pilotlab/debug");
let Tests = class Tests {
    test01() {
        lux_debug_1.Debug.log(index_1.Identifier.generate(), 'generated ID');
        lux_debug_1.Debug.log(index_1.Identifier.generate(), 'generated ID');
        lux_debug_1.Debug.log(index_1.Identifier.generateStrong(), 'generated strong ID');
        lux_debug_1.Debug.log(index_1.Identifier.getSessionUniqueInteger(), 'session-unique ID');
        lux_debug_1.Debug.log(index_1.Identifier.getSessionUniqueInteger(), 'session-unique ID');
    }
};
__decorate([
    alsatian_1.Test('ID is generated')
], Tests.prototype, "test01", null);
Tests = __decorate([
    alsatian_1.TestFixture('Identifier')
], Tests);
exports.Tests = Tests;
//# sourceMappingURL=index.test.js.map
