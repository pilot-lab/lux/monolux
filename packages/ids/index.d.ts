import HashIds from './src/hashIds';
import Identifier from './src/identifier';
export { HashIds, Identifier };
export default Identifier;
