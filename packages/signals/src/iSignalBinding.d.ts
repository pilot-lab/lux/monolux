export interface ISignalBinding<T> {
    context: any;
    priority: number;
    isActive: boolean;
    execute(param: T): any;
    detach(): (value: T) => void;
    isBound(): boolean;
    isOnce(): boolean;
    readonly listener: (value: T) => void;
    destroy(): void;
    toString(): string;
}
export default ISignalBinding;
