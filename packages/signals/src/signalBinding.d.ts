import ISignal from './iSignal';
import ISignalBinding from './iSignalBinding';
export declare class SignalBinding<T> implements ISignalBinding<T> {
    constructor(signal: ISignal<T>, listener: (value: T) => void, isOnce: boolean, listenerContext: any, priority?: number);
    readonly listener: (value: T) => void;
    private _listener;
    private readonly _isOnce;
    context: any;
    private _context;
    private _signal;
    priority: number;
    isActive: boolean;
    execute(param: T): any;
    detach(): (value: T) => void;
    isBound(): boolean;
    isOnce(): boolean;
    destroy(): void;
    toString(): string;
}
export default SignalBinding;
