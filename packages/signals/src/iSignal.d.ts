import ISignalBinding from './iSignalBinding';
export interface ISignal<T> {
    readonly isMemorize: boolean;
    isActive: boolean;
    readonly listenerCount: number;
    validateListener(listener: any, fnName: any): void;
    has(listener: (value: T) => void, context?: any): boolean;
    listen(listener: (value: T) => void, listenerContext?: any, priority?: number): ISignalBinding<T>;
    listenOnce(listener: (value: T) => void, listenerContext?: any, priority?: number): ISignalBinding<T>;
    delete(listener: (value: T) => void, context?: any): (value: T) => void;
    deleteAll(): void;
    halt(): void;
    dispatch(param: T): void;
    forget(): void;
    dispose(): void;
    toString(): string;
}
export default ISignal;
