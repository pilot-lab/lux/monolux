"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class SignalBinding {
    constructor(signal, listener, isOnce, listenerContext, priority = 0) {
        this.isActive = true;
        this._listener = listener;
        this._isOnce = isOnce;
        this._context = listenerContext;
        this._signal = signal;
        this.priority = priority;
    }
    get listener() { return this._listener; }
    get context() { return this._context; }
    set context(value) { this._context = value; }
    execute(param) {
        let handlerReturn;
        if (this.isActive && !!this._listener) {
            handlerReturn = this._listener.apply(this._context, [param]);
            if (this._isOnce) {
                this.detach();
            }
        }
        return handlerReturn;
    }
    detach() {
        return this.isBound() ? this._signal.delete(this._listener, this._context) : null;
    }
    isBound() { return (!!this._signal && !!this._listener); }
    isOnce() { return this._isOnce; }
    destroy() {
        delete this._signal;
        delete this._listener;
        delete this._context;
    }
    toString() {
        return '[SignalBinding isOnce:' + this._isOnce + ', isBound:' + this.isBound() + ', isActive:' + this.isActive + ']';
    }
}
exports.SignalBinding = SignalBinding;
exports.default = SignalBinding;
//# sourceMappingURL=signalBinding.js.map