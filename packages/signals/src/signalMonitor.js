"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const signal_1 = require("./signal");
class SignalMonitor {
    constructor(isListenOnce = true) {
        this._isListenOnce = isListenOnce;
        this.allSignalsDispatched = new signal_1.default(true);
        this._isEnabled = false;
        this._values = [];
        this._signals = [];
        this._signalsPending = [];
    }
    get signals() { return this._signals; }
    get signalsPending() { return this._signalsPending; }
    add(signal) {
        if (!signal || !(signal instanceof signal_1.default))
            return;
        this._signals.push(signal);
    }
    remove(signal) {
        try {
            this._signals.splice(this._signals.indexOf(signal), 1);
            this._signalsPending.splice(this._signals.indexOf(signal), 1);
        }
        catch (e) {
            console.log('Unable to remove Signal from SignalMonitor');
        }
        return signal;
    }
    start() {
        this._isEnabled = true;
        this._values = [];
        this._signalsPending.splice(0);
        if (this._signals.length < 1)
            return;
        const length = this._signals.length;
        for (let i = 0; i < length; i++) {
            let signal = this._signals[i];
            this._signalsPending.push(signal);
            signal.listenOnce((value) => {
                if (this._signalsPending.length > 0) {
                    this._values.push(value);
                    this._signalsPending.splice(this._signalsPending.indexOf(signal), 1);
                    if (this._signalsPending.length < 1) {
                        if (this._isEnabled) {
                            this.allSignalsDispatched.dispatch(this._values);
                        }
                        if (!this._isListenOnce)
                            this.start();
                    }
                }
            }, this);
        }
    }
    clear() {
        this._isEnabled = false;
        this._signals.splice(0);
        this._signalsPending.splice(0);
        this._values = [];
    }
}
exports.SignalMonitor = SignalMonitor;
exports.default = SignalMonitor;
//# sourceMappingURL=signalMonitor.js.map