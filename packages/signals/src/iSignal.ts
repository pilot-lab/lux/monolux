import ISignalBinding from './iSignalBinding';


/**
 * @desc      A TypeScript conversion of JS Signals by Miller Medeiros
 *            Released under the MIT license
 *            http://millermedeiros.github.com/js-signals/
 *
 * @version   1.0 - 7th March 2013
 *
 * @author    Richard Davey, TypeScript conversion
 * @author    Miller Medeiros, JS Signals
 * @author    Robert Penner, AS Signals
 *
 * @url       http://www.photonstorm.com
 */
export interface ISignal<T> {
    /**
     * If Signal should keep record of previously dispatched parameters and
     * automatically execute listener during `listen()`/`listenOnce()` if Signal was
     * already dispatched before.
     */
    readonly isMemorize:boolean;


    /**
     * If Signal is active and should broadcast events.
     * <p><strong>IMPORTANT:</strong> Setting this property during a dispatch will only affect the next dispatch,
     * if you want to stop the propagation of a signal use `halt()` instead.</p>
     */
    isActive:boolean;


    /**
     * @return {number} Number of listeners attached to the Signal.
     */
    readonly listenerCount:number;


    validateListener(listener:any, fnName:any):void;


    /**
     * Check if listener was attached to Signal.
     * @return {boolean} if Signal has the specified listener.
     */
    has(listener:(value:T) => void, context?:any):boolean;


    /**
     * Add a listener to the signal.
     * @param {(value:T) => void} listener Signal handler function.
     * @param {Object} [listenerContext] Context on which listener will be executed
     * (object that should represent the `this` variable inside listener function).
     * @param {Number} [priority] The priority level of the event listener.
     * Listeners with higher priority will be executed before listeners with lower priority.
     * Listeners with same priority level will be executed at the same order as they were added. (default = 0)
     * @return {SignalBinding<T>} An Object representing the binding between the Signal and listener.
     */
    listen(listener:(value:T) => void, listenerContext?:any, priority?:number):ISignalBinding<T>;


    /**
     * Add listener to the signal that should be deleted after first execution (will be executed only once).
     * @param {(value:T) => void} listener Signal handler function.
     * @param {Object} [listenerContext] Context on which listener will be executed
     * (object that should represent the `this` variable inside listener function).
     * @param {Number} [priority] The priority level of the event listener.
     * Listeners with higher priority will be executed before listeners with lower priority.
     * Listeners with same priority level will be executed at the same order as they were added. (default = 0)
     * @return {SignalBinding<T>} An Object representing the binding between the Signal and listener.
     */
    listenOnce(listener:(value:T) => void, listenerContext?:any, priority?:number):ISignalBinding<T>;


    /**
     * Remove a single listener from the dispatch queue.
     * @param {(value:T) => void} listener Handler function that should be deleted.
     * @param {Object} [context] Execution context (since you can add the same handler multiple times if executing in a different context).
     * @return {(value:T) => void} Listener handler function.
     */
    delete(listener:(value:T) => void, context?:any):(value:T) => void;


    /**
     * Remove all listeners from the Signal.
     */
    deleteAll():void;


    /**
     * Stop propagation of the event, blocking the dispatch to next listeners on the queue.
     * <p><strong>IMPORTANT:</strong> should be called only during signal dispatch, calling it before/after dispatch won't affect signal broadcast.</p>
     * @see Signal.prototype.disable
     */
    halt():void;


    /**
     * Dispatch/Broadcast Signal to all listeners added to the queue.
     * @param {...*} [params] Parameters that should be passed to each handler.
     */
    dispatch(param:T):void;


    /**
     * Forget memorized arguments.
     * @see Signal.isMemorize
     */
    forget():void;


    /**
     * Remove all bindings from signal and destroy any reference to external objects (destroy Signal object).
     * <p><strong>IMPORTANT:</strong> calling any method on the signal instance after calling dispose will throw errors.</p>
     */
    dispose():void;


    /**
     * @return {string} String representation of the object.
     */
    toString():string;
} // End class


export default ISignal;
