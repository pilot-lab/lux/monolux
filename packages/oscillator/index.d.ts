import { IPromise } from '@pilotlab/result';
import { Signal } from '@pilotlab/signals';
import { AnimationManager } from '@pilotlab/animation';
export declare class Oscillator {
    constructor(baseValue?: number, amplitude?: number, cycleDuration?: number, isEnabled?: boolean, animationManager?: AnimationManager);
    private _baseValue;
    private _amplitude;
    private _cycleDuration;
    private _animationManager;
    private _timer;
    isEnabled: boolean;
    ticked: Signal<number>;
    start(isReset?: boolean): void;
    stop(): void;
    tick(elapsed: number): IPromise<number>;
}
export default Oscillator;
