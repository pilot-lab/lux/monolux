import { is } from '@pilotlab/is';
import {Result, IPromise} from '@pilotlab/result';
import {Signal} from '@pilotlab/signals';
import {HiResTimer, HiResTimerCounterBrowser} from '@pilotlab/timer';
import {AnimationManager} from '@pilotlab/animation';


export class Oscillator {
    /**
     * The ValueOscillator uses the Sin function to smoothly 'float' a value within the range
     * of the provided amplitude above and below the base value.
     * @param baseValue - The base value around which the oscillation will cycle.
     * @param amplitude - The oscillating value will swing from -amplitude to +amplitude around the base.
     * @param cycleDuration - The time to complete one cycle in seconds.
     * @param isEnabled - Determines whether the oscillator is currently running.
     * @param animationManager - The AnimManager that will animate the oscillation.
     */
    constructor(
        baseValue:number = 0,
        amplitude:number = 20,
        cycleDuration:number = 1,
        isEnabled:boolean  = true,
        animationManager?:AnimationManager
    ) {
        this._baseValue = baseValue;
        this._amplitude = amplitude;
        this._cycleDuration = cycleDuration;
        if (this._cycleDuration <= 0) this._cycleDuration = 0.0001;

        if (is.notEmpty(animationManager)) {
            this._animationManager = animationManager;
        }

        //----- Use the property setter here
        //----- and make sure this comes after animationManager initialization.
        this.isEnabled = isEnabled;
    }


    private _baseValue:number = 0;
    private _amplitude:number = 40;
    private _cycleDuration:number = 1;
    private _animationManager:AnimationManager;
    private _timer:HiResTimer = new HiResTimer(new HiResTimerCounterBrowser());


    get isEnabled():boolean { return this._timer.isRunning; }
    set isEnabled(value:boolean) {
        if (this._timer.isRunning === value) return;

        if (value) this.start();
        else this.stop();
    }


    ticked:Signal<number> = new Signal<number>();


    start(isReset:boolean = false):void {
        if (is.empty(this._animationManager)) return;

        if (this._timer.isRunning) this._animationManager.ticked.listen(this.tick, this);

        this._timer.start();
    }


    stop():void {
        if (!this._timer.isRunning) return;

        this._timer.stop();
        this._animationManager.ticked.delete(this.tick);
    }


    /**
     * Since Sin ranges from -1 to +1, the value will float from -amplitude to +amplitude around the base,
     * and the time to complete one cycle of Sin  is 2 * PI - about 6.3 second, if no modifier is applied.
     */
    tick(elapsed:number):IPromise<number> {
        if (!this._timer.isRunning) return Result.resolve<number>(null);

        let value:number = this._baseValue + (this._amplitude * Math.sin(((2 * Math.PI) / this._cycleDuration) * (this._timer.elapsedMilliseconds / 1000)));
        this.ticked.dispatch(value);
        return Result.resolve<number>(value);
    }
} // End class


export default Oscillator;
