import {
    Setup,
    SetupFixture,
    Teardown,
    TeardownFixture,
    Test,
    TestCase,
    TestFixture,
    Expect
} from 'alsatian';
import { Detector } from '../index';


@TestFixture('Detector Tests')
export class Tests {
    @Setup
    public setup() {}

    @SetupFixture
    public setupFixture() {}

    @Teardown
    public teardown() {}

    @TeardownFixture
    public teardownFixture() {}

    @TestCase('')
    @TestCase(0)
    @TestCase('@pilotlab/strings')
    @TestCase('someothermodule')
    @Test('Detect whether a NodeJS module is installed')
    test01(testValue:any) {

    }
}
