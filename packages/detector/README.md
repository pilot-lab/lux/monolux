# detector

Use to determine context and capabilities of run-time environment.

## Install

`sudo npm install --save @pilotlab/detector`

## Usage

```
import Detector from @pilotlab/detector

let isSafariMobile:boolean = Detector.isSafariMobile;
```
