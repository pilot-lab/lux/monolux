export declare class Detector {
    static readonly isNodeJs: boolean;
    static readonly isTouch: boolean;
    static readonly isWindows: boolean;
    static readonly isSupportedWebAudio: boolean;
    static readonly isSupportedPerformance: boolean;
    static readonly isSupportedDeviceOrientationEvent: boolean;
    static readonly isChrome: boolean;
    static readonly isChromeApp: boolean;
    static readonly isSafari: boolean;
    static readonly isSafariMobile: boolean;
    static readonly isSafariMobileHomeScreenApp: boolean;
    static readonly isIE: boolean;
    static readonly isMobile: boolean;
    static readonly isTablet: boolean;
    static readonly isAppleMobile: boolean;
    static readonly isIPad: boolean;
    static readonly isAndroid: boolean;
    static readonly isBlackberry: boolean;
    static readonly isWindowsPhone: boolean;
}
export default Detector;
