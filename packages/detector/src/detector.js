"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const is_1 = require("@pilotlab/is");
class Detector {
    static get isNodeJs() {
        return Object.prototype.toString.call(typeof process !== 'undefined' ? process : 0) === '[object process]';
    }
    static get isTouch() {
        try {
            if (is_1.default.empty(window))
                return false;
            let win = window;
            return ('ontouchstart' in win);
        }
        catch (e) {
            return false;
        }
    }
    static get isWindows() { return is_1.default.notEmpty(navigator.userAgent.match(/Windows NT/i)); }
    static get isSupportedWebAudio() {
        try {
            if (is_1.default.empty(window))
                return false;
            let win = window;
            return ('webkitAudioContext' in win || 'AudioContext' in win);
        }
        catch (e) {
            return false;
        }
    }
    static get isSupportedPerformance() {
        try {
            if (is_1.default.empty(window))
                return false;
            let win = window;
            return ('performance' in win);
        }
        catch (e) {
            return false;
        }
    }
    static get isSupportedDeviceOrientationEvent() {
        try {
            if (is_1.default.empty(window))
                return false;
            let win = window;
            return ('deviceorientation' in win);
        }
        catch (e) {
            return false;
        }
    }
    static get isChrome() { return is_1.default.notEmpty(navigator.userAgent.match(/Chrome/i)); }
    static get isChromeApp() {
        try {
            if (is_1.default.empty(window))
                return false;
            let win = window;
            if (win.chrome && win.chrome.app && win.chrome.app.runtime) {
                return true;
            }
            else {
                return false;
            }
        }
        catch (e) {
            return false;
        }
    }
    static get isSafari() {
        return ((is_1.default.notEmpty(navigator.userAgent.match(/Safari/i)) || is_1.default.notEmpty(navigator.userAgent.match(/AppleWebKit/i))) && !Detector.isChrome);
    }
    static get isSafariMobile() {
        return (Detector.isSafari && is_1.default.notEmpty(navigator.userAgent.match(/Mobile/i)));
    }
    static get isSafariMobileHomeScreenApp() {
        let nav = navigator;
        return (Detector.isSafariMobile && nav.standalone);
    }
    static get isIE() {
        return (is_1.default.notEmpty(navigator.userAgent.match(/msie/i)) || is_1.default.notEmpty(navigator.userAgent.match(/trident/i)));
    }
    static get isMobile() { return (Detector.isAppleMobile || Detector.isAndroid || Detector.isWindowsPhone || Detector.isBlackberry); }
    static get isTablet() {
        let isTablet = false;
        if (Detector.isTouch && Detector.isWindows)
            isTablet = true;
        if (Detector.isIPad)
            return true;
        return isTablet;
    }
    static get isAppleMobile() { return is_1.default.notEmpty(navigator.userAgent.match(/iPhone|iPod/i)); }
    static get isIPad() { return is_1.default.notEmpty(navigator.userAgent.match(/iPad/i)); }
    static get isAndroid() { return is_1.default.notEmpty(navigator.userAgent.match(/Android/i)); }
    static get isBlackberry() { return is_1.default.notEmpty(navigator.userAgent.match(/BlackBerry/i)); }
    static get isWindowsPhone() { return is_1.default.notEmpty(navigator.userAgent.match(/IEMobile/i)); }
}
exports.Detector = Detector;
exports.default = Detector;
//# sourceMappingURL=detector.js.map