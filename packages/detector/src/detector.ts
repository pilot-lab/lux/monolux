import is from '@pilotlab/is';


export class Detector {
    /**
     * Is the code running in a NodeJS environment? If not, it may be running in a browser.
     */
    static get isNodeJs():boolean {
        /// Only Node.JS has a process variable that is of [[Class]] process
        return Object.prototype.toString.call(typeof process !== 'undefined' ? process : 0) === '[object process]';
    }


    static get isTouch():boolean {
        try {
            if (is.empty(window)) return false;
            let win:any = window;
            return ('ontouchstart' in win);
        } catch (e) { return false; }
    }


    static get isWindows():boolean { return is.notEmpty(navigator.userAgent.match(/Windows NT/i)); }


    static get isSupportedWebAudio():boolean {
        try {
            if (is.empty(window)) return false;
            let win:any = window;
            return ('webkitAudioContext' in win || 'AudioContext' in win);
        } catch (e) { return false; }
    }
    static get isSupportedPerformance():boolean {
        try {
            if (is.empty(window)) return false;
            let win: any = window;
            return ('performance' in win);
        } catch(e) { return false; }
    }
    static get isSupportedDeviceOrientationEvent():boolean {
        try {
            if (is.empty(window)) return false;
            let win:any = window;
            return ('deviceorientation' in win);
        } catch (e) { return false; }
    }


    static get isChrome():boolean { return is.notEmpty(navigator.userAgent.match(/Chrome/i)); }


    /**
     * Is the app running as a Google Chrome Packaged App?
     */
    static get isChromeApp():boolean {
        try {
            /// Check whether we're running in a Chrome Packaged App.
            if (is.empty(window)) return false;
            let win:any = window;
            if (win.chrome && win.chrome.app && win.chrome.app.runtime) {
                /// Running inside a Chrome App context
                return true;
            } else {
                /// Either not Chrome, or not as an app window
                return false;
            }
        } catch (e) { return false; }
    }


    static get isSafari():boolean {
        /**
         * The RegEx modifier /i indicates a case-insensitive search.
         */
        return ((is.notEmpty(navigator.userAgent.match(/Safari/i)) || is.notEmpty(navigator.userAgent.match(/AppleWebKit/i))) && !Detector.isChrome);
    }


    static get isSafariMobile():boolean {
        return (Detector.isSafari && is.notEmpty(navigator.userAgent.match(/Mobile/i)));
    }


    static get isSafariMobileHomeScreenApp():boolean {
        let nav:any = navigator;
        return (Detector.isSafariMobile && nav.standalone);
    }


    static get isIE():boolean {
        return (is.notEmpty(navigator.userAgent.match(/msie/i)) || is.notEmpty(navigator.userAgent.match(/trident/i)));
    }


    static get isMobile():boolean { return (Detector.isAppleMobile || Detector.isAndroid || Detector.isWindowsPhone || Detector.isBlackberry); }


    static get isTablet():boolean {
        let isTablet:boolean = false;

        /// If the device supports touch and it's not a Windows Surface,
        /// we're probably on a mobile device.
        if (Detector.isTouch && Detector.isWindows) isTablet = true;
        if (Detector.isIPad) return true;

        return isTablet;
    }


    static get isAppleMobile():boolean { return is.notEmpty(navigator.userAgent.match(/iPhone|iPod/i)); }
    static get isIPad():boolean { return is.notEmpty(navigator.userAgent.match(/iPad/i)); }
    static get isAndroid():boolean { return is.notEmpty(navigator.userAgent.match(/Android/i)); }
    static get isBlackberry():boolean { return is.notEmpty(navigator.userAgent.match(/BlackBerry/i)); }
    static get isWindowsPhone():boolean { return is.notEmpty(navigator.userAgent.match(/IEMobile/i)); }
} // End class


export default Detector;

