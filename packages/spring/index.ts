import is from '@pilotlab/is';
import { RangeScale } from '@pilotlab/range-scale';
import { Point, Vector } from '@pilotlab/attributes';
import { Signal } from '@pilotlab/signals';


export class Spring {
    constructor(maximumChaseDistance?:number) {
        if (is.notEmpty(maximumChaseDistance)) this.maximumChaseDistance = maximumChaseDistance;
        this._progressRangeScale = new RangeScale(-this.maximumChaseDistance, this.maximumChaseDistance, -1, 1);
    }


    /*====================================================================*
     START: Drag Zooming
     *====================================================================*/
    get isEnabled():boolean { return this._isEnabled; }
    set isEnabled(value:boolean) {
        if (value && !this._isEnabled) {
            this.chaseSpeed.x = this.catchProximity;
            this.chaseSpeed.y = this.catchProximity;
        }

        this._isEnabled = value;
    }
    private _isEnabled:boolean = false;


    tick() {
        if (this.isEnabled) {
            this._updateChase();
            this._applyValues();
            this.updated.dispatch(new Point(this.chaserPosition.x, this.chaserPosition.y));
        }
    }


    /*====================================================================*
     START: Properties and Members
     *====================================================================*/
    leaderPosition:Vector = new Vector(0, 0, 0);
    chaserPosition:Vector = new Vector(0, 0, 0);
    chaserRotation:number = 0;


    maximumChaseDistance:number = 10000;
    catchProximity:number = 1;
    chaseSpeed:Vector = new Vector(0, 0, 0);


    /**
     * Increased speed with distance.
     * 1 = no increase in speed. Higher numbers increase speed more with distance.
     */
    acceleration:number = 0.15;


    private _OFFSET_FACTOR:number = 1.0;


    get direction():number { return this._direction; }
    private _direction:number = 0;


    get isLeaderCaught():boolean {
        return this._isLeaderCaught;
    }
    private _isLeaderCaught:boolean = true;
    private _leaderCaughtTicks:number = 0;


    //----- Ticks to wait before registering a catch.
    //----- Prevents stuttering and false hits when the leader is in motion.
    private _WAIT_BEFORE_CATCHING_TICKS:number = 20;


    get velocityNormalized():Point {
        return new Point(
            this._progressRangeScale.output(this.leaderPosition.x - this.chaserPosition.x, false),
            this._progressRangeScale.output(this.leaderPosition.y - this.chaserPosition.y, false)
        )
    }
    private _progressRangeScale:RangeScale;


    /*====================================================================*
     START: Signals
     *====================================================================*/
    updated:Signal<Point> = new Signal<Point>();
    leaderCaught:Signal<Point> = new Signal<Point>();


    /*====================================================================*
     START: Public Methods
     *====================================================================*/
    startChasing(leaderX:number, leaderY:number, chaserX:number, chaserY:number):void {
        this.leaderPosition.x = leaderX;
        this.leaderPosition.y = leaderY;

        this.chaserPosition.x = chaserX;
        this.chaserPosition.y = chaserY;

        this.chaseSpeed.x = this.catchProximity;
        this.chaseSpeed.y = this.catchProximity;

        this.isEnabled = true;
        this._isLeaderCaught = false;
    }


    /*====================================================================*
     START: Private Methods
     *====================================================================*/
    private _updateChase():void {
        let vDiff:Vector = new Vector(this.leaderPosition.x - this.chaserPosition.x, this.leaderPosition.y - this.chaserPosition.y, 0);
        let vDirection:Vector = new Vector(vDiff.x, vDiff.y, 0);
        vDirection.normalize();
        vDiff = vDiff.abs;

        this.chaserPosition.x += vDirection.x * this.chaseSpeed.x;
        this.chaserPosition.y += vDirection.y * this.chaseSpeed.y;

        if (vDiff.x > this.maximumChaseDistance / this._OFFSET_FACTOR) {
            vDiff.x = this.maximumChaseDistance / this._OFFSET_FACTOR;
            this.chaserPosition.x = this.leaderPosition.x - ((this.maximumChaseDistance / this._OFFSET_FACTOR) * vDirection.x);
        }
        else if (vDiff.x < this.catchProximity) this.chaserPosition.x = this.leaderPosition.x;

        if (vDiff.y > this.maximumChaseDistance / this._OFFSET_FACTOR) {
            vDiff.y = this.maximumChaseDistance / this._OFFSET_FACTOR;
            this.chaserPosition.y = this.leaderPosition.y - ((this.maximumChaseDistance / this._OFFSET_FACTOR) * vDirection.y);
        }
        else if (vDiff.y < this.catchProximity) this.chaserPosition.y = this.leaderPosition.y;

        if (this.chaserPosition.x != this.leaderPosition.x && this.chaserPosition.y != this.leaderPosition.y) {
            this.chaserRotation = Math.atan2(
                    (this.chaserPosition.y - this.leaderPosition.y), (this.chaserPosition.x - this.leaderPosition.x)
                ) / (Math.PI / 180) - 90;
        }

        this.chaseSpeed = new Vector(
            vDiff.x * vDirection.x * this._OFFSET_FACTOR, vDiff.y * vDirection.y * this._OFFSET_FACTOR
        );

        //----- The chase speed is greater the farther the two points are from one another.
        this.chaseSpeed.x = Math.ceil(Math.abs(this.chaseSpeed.x) * this.acceleration);
        this.chaseSpeed.y = Math.ceil(Math.abs(this.chaseSpeed.y) * this.acceleration);
    }


    private _applyValues():void {
        let chaseSpeed:number = this.chaseSpeed.y;

        this._direction = -1;

        if (chaseSpeed > 0) {
            this._direction = 1;
            this._isLeaderCaught = false;
            this._leaderCaughtTicks = 0;
        } else if (chaseSpeed < 0) {
            this._isLeaderCaught = false;
            this._leaderCaughtTicks = 0;
        } else {
            this._direction = 0;

            if (!this._isLeaderCaught) {
                if (this._leaderCaughtTicks < this._WAIT_BEFORE_CATCHING_TICKS) this._leaderCaughtTicks += 1;
                else {
                    //----- The chaser caught the leader
                    this._leaderCaughtTicks = 0;
                    this._isLeaderCaught = true;
                    this.chaserPosition.x = this.leaderPosition.x;
                    this.chaserPosition.y = this.leaderPosition.y;
                    this.isEnabled = false;
                    this.leaderCaught.dispatch(new Point(this.leaderPosition.x, this.leaderPosition.y));
                }
            }
        }
    }
} // End of class


export default Spring;
