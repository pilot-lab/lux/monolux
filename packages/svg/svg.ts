export class Svg {
    static createNode(name:string, properties:any):SVGElement {
        const node:SVGElement = document.createElementNS('http://www.w3.org/2000/svg', name);
        this.setProperties(node, properties);
        return node;
    }


    static fromString(svg:string, properties:any):SVGElement {
        const placeholder = document.createElement('div');
        placeholder.innerHTML = svg;
        const node:SVGElement = <SVGElement>placeholder.firstChild;
        this.setProperties(node, properties);
        return node;
    }


    static setProperties(node:SVGElement, properties:any):SVGElement {
        const keys = Object.keys(properties);
        keys.forEach((key) => { node.setAttributeNS(null, key, properties[key]); });
        return node;
    }
} // End class


export default Svg;
