export declare class Svg {
    static createNode(name: string, properties: any): SVGElement;
    static fromString(svg: string, properties: any): SVGElement;
    static setProperties(node: SVGElement, properties: any): SVGElement;
}
export default Svg;
