/**
 * This is a found set of useful functions for manipulating SVG graphics.
 * The most interesting functions are the ones that parse SVG strings into simple
 * arrays of coordinate points. The remaining functions should probably be
 * moved into math utility classes.
 */

let bezier3Type:string = 'bezier3';
let lineType:string = 'line';


let mathAbs:Function = Math.abs;
let mathAsin:Function = Math.asin;
let mathCos:Function = Math.cos;
let mathMax:Function = Math.max;
let mathMin:Function = Math.min;
let mathPi:number = Math.PI;
let mathPow:Function = Math.pow;
let mathSin:Function = Math.sin;
let mathSqrt:Function = Math.sqrt;
let mathTan:Function = Math.tan;


let tolerance = 1e-6;


function x(p:number[]):number { return p[0]; }
function y(p:number[]):number { return p[1]; }
function toFloat(v:any):number { return parseFloat(v); }
function coordEqual(c1:number[], c2:number[]):boolean { return x(c1) === x(c2) && y(c1) === y(c2); }
function coordMax(c1:number[], c2:number[]):number[] { return [mathMax(x(c1), x(c2)), mathMax(y(c1), y(c2))]; }
function coordMin(c1:number[], c2:number[]):number[] { return [mathMin(x(c1), x(c2)), mathMin(y(c1), y(c2))]; }
function coordMultiply(c:number[], f:number):number[] { return [x(c) * f, y(c) * f]; }
function coordAdd(c1:number[], c2:number[]):number[] { return [x(c1) + x(c2), y(c1) + y(c2)]; }
function coordDot(c1:number[], c2:number[]):number { return x(c1) * x(c2) + y(c1) * y(c2); }
function coordLerp(c1:number[], c2:number[], t:number):number[] { return [x(c1) + (x(c2) - x(c1)) * t, y(c1) + (y(c2) - y(c1)) * t]; }


function linearRoot(p2:number, p1:number):number[] {
    let results:number[] = [];
    if (p2 !== 0) results.push(-p1 / p2);
    return results;
}


function quadRoots(p3:number, p2:number, p1:number):number[] {
    let results:number[] = [];

    if (mathAbs(p3) <= tolerance) return linearRoot(p2, p1);

    let a:number = p3;
    let b:number = p2 / a;
    let c:number = p1 / a;
    let d:number = b * b - 4 * c;
    if (d > 0) {
        let e:number = mathSqrt(d);
        results.push(0.5 * (-b + e));
        results.push(0.5 * (-b - e));
    } else if (d === 0) results.push(0.5 * -b);

    return results;
}


function cubeRoots(p4:number, p3:number, p2:number, p1:number):number[] {
    if (mathAbs(p4) <= tolerance) return quadRoots(p3, p2, p1);

    let results:number[] = [];

    let c3:number = p4;
    let c2:number = p3 / c3;
    let c1:number = p2 / c3;
    let c0:number = p1 / c3;

    let a:number = (3 * c1 - c2 * c2) / 3;
    let b:number = (2 * c2 * c2 * c2 - 9 * c1 * c2 + 27 * c0) / 27;
    let offset:number = c2 / 3;
    let discrim:number = b * b / 4 + a * a * a / 27;
    let halfB:number = b / 2;

    let tmp:number;
    if (discrim > 0) {
        let e:number = mathSqrt(discrim);
        tmp = -halfB + e;
        let root:number = tmp >= 0 ? mathPow(tmp, 1 / 3) : -mathPow(-tmp, 1 / 3);
        tmp = -halfB - e;
        if (tmp >= 0) root += mathPow(tmp, 1 / 3);
        else root -= mathPow(-tmp, 1 / 3);
        results.push(root - offset);
    } else if (discrim < 0) {
        let distance:number = mathSqrt(-a / 3);
        let angle:number = Math.atan2(mathSqrt(-discrim), -halfB) / 3;
        let cos:number = mathCos(angle);
        let sin:number = mathSin(angle);
        let sqrt3:number = mathSqrt(3);
        results.push(2 * distance * cos - offset);
        results.push(-distance * (cos + sqrt3 * sin) - offset);
        results.push(-distance * (cos - sqrt3 * sin) - offset);
    } else {
        if (halfB >= 0) tmp = -mathPow(halfB, 1 / 3);
        else tmp = mathPow(-halfB, 1 / 3);
        results.push(2 * tmp - offset);
        results.push(-tmp - offset);
    }

    return results;
}


function arcToCurve(cp1:number[], rx:number, ry:number, angle:number, large_arc:any, sweep:any, cp2:number[], recurse:any) {
    function rotate(cx:number, cy:number, r:number):number[] {
        let cos:number = mathCos(r);
        let sin:number = mathSin(r);
        return [
            cx * cos - cy * sin,
            cx * sin + cy * cos,
        ];
    }

    let x1:number = x(cp1);
    let y1:number = y(cp1);
    let x2:number = x(cp2);
    let y2:number = y(cp2);

    let rad:any = mathPi / 180 * (+angle || 0);
    let f1:any = 0;
    let f2:any = 0;
    let cx:any;
    let cy:any;
    let res:any[] = [];

    if (!recurse) {
        let xy:number[] = rotate(x1, y1, -rad);
        x1 = x(xy);
        y1 = y(xy);
        xy = rotate(x2, y2, -rad);
        x2 = x(xy);
        y2 = y(xy);

        let px:number = (x1 - x2) / 2;
        let py:number = (y1 - y2) / 2;
        let h:number = (px * px) / (rx * rx) + (py * py) / (ry * ry);
        if (h > 1) {
            h = mathSqrt(h);
            rx = h * rx;
            ry = h * ry;
        }

        let rx2:number = rx * rx;
        let ry2:number = ry * ry;

        let k:any = (large_arc === sweep ? -1 : 1)
            * mathSqrt(mathAbs((rx2 * ry2 - rx2 * py * py - ry2 * px * px) / (rx2 * py * py + ry2 * px * px)));

        cx = k * rx * py / ry + (x1 + x2) / 2;
        cy = k * -ry * px / rx + (y1 + y2) / 2;
        f1 = mathAsin((y1 - cy) / ry);
        f2 = mathAsin((y2 - cy) / ry);

        f1 = x1 < cx ? mathPi - f1 : f1;
        f2 = x2 < cx ? mathPi - f2 : f2;

        if (f1 < 0) f1 = mathPi * 2 + f1;
        if (f2 < 0) f2 = mathPi * 2 + f2;
        if (sweep && f1 > f2) f1 = f1 - mathPi * 2;
        if (!sweep && f2 > f1) f2 = f2 - mathPi * 2;
    } else {
        f1 = recurse[0];
        f2 = recurse[1];
        cx = recurse[2];
        cy = recurse[3];
    }

    let df:number = f2 - f1;
    if (mathAbs(df) > mathPi * 120 / 180) {
        let f2old:number = f2;
        let x2old:number = x2;
        let y2old:number = y2;

        f2 = f1 + mathPi * 120 / 180 * (sweep && f2 > f1 ? 1 : -1);
        x2 = cx + rx * mathCos(f2);
        y2 = cy + ry * mathSin(f2);
        res = arcToCurve([x2, y2], rx, ry, angle, 0, sweep, [x2old, y2old], [f2, f2old, cx, cy]);
    }

    df = f2 - f1;

    let c1:number = mathCos(f1);
    let s1:number = mathSin(f1);
    let c2:number = mathCos(f2);
    let s2:number = mathSin(f2);
    let t:number = mathTan(df / 4);
    let hx:number = 4 / 3 * rx * t;
    let hy:number = 4 / 3 * ry * t;
    let m1:number[] = [x1, y1];
    let m2:number[] = [x1 + hx * s1, y1 - hy * c1];
    let m3:number[] = [x2 + hx * s2, y2 - hy * c2];
    let m4:number[] = [x2, y2];
    m2[0] = 2 * m1[0] - m2[0];
    m2[1] = 2 * m1[1] - m2[1];

    function splitCurves(curves:any) {
        let result:any[] = [];
        while (curves.length > 0) {
            result.push([
                [curves[0], curves[1]],
                [curves[2], curves[3]],
                [curves[4], curves[5]],
            ]);
            curves.splice(0, 6);
        }
        return result;
    }

    if (recurse) {
        return splitCurves([m2, m3, m4].concat(res));
    } else {
        res = [m2, m3, m4].concat(res).join().split(',');
        let newres:any[] = [];
        for (let i:number = 0, ii = res.length; i < ii; i++) {
            newres[i] = i % 2 ? rotate(res[i - 1], res[i], rad)[1] : rotate(res[i], res[i + 1], rad)[0];
        }
        return splitCurves(newres);
    }
}


// Unpack an SVG path string into different curves and lines
//
// https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/d
export function splitSegments(polygon:string):any {
    if (typeof polygon !== 'string') {
        throw new Error('Polygon should be a path string');
    }

    let start:any = null;
    let position:any = null;
    let result:any[] = [];

    function stripWhitespace() { polygon = polygon.trim(); }

    function readCharSeq(n:number):number {
        let c:number = polygon.charCodeAt(n);
        while (c >= 48 && c <= 57) {
            n++;
            c = polygon.charCodeAt(n);
        }
        return n;
    }

    function readNumber():number {
        stripWhitespace();

        let start:number = 0;
        let end:number = 0;
        if (polygon[start] === ',') {
            start++;
            end++;
        }

        if (polygon[start] === '-') end++;

        end = readCharSeq(end);
        if (polygon[end] === '.') {
            end++;
            end = readCharSeq(end);
        }

        let s:string = polygon.substring(start, end);
        if (s !== '') {
            let num:number = toFloat(s);
            polygon = polygon.substring(end);
            if (polygon.length && polygon[0].toLowerCase() === 'e') {
                let expEnd:number = 0;
                if (polygon.length > 1 && polygon[1] === '-') {
                    expEnd = readCharSeq(2);
                } else expEnd = readCharSeq(1);
                let exp:number = toFloat(polygon.substring(1, expEnd));
                if (mathAbs(exp) > 0) {
                    num *= mathPow(10, exp);
                }
                polygon = polygon.substring(expEnd);
            }
            return num;
        } else throw new Error('Expected number: ' + polygon);
    }


    function readNumbers(n:any, fn:any):void {
        stripWhitespace();
        let index:number = 0;
        let c:number = polygon.charCodeAt(0);
        while ((c >= 48 && c <= 57) || c === 44 || c === 45) {
            let numbers:number[] = [];
            for (let i:number = 0; i < n; i++) { numbers.push(readNumber()); }
            fn(numbers, index);

            stripWhitespace();
            c = polygon.charCodeAt(0);
            index++;
        }
    }


    function readCoords(n:number, fn:any):void {
        readNumbers(n * 2, function (numbers:number[], index:number) {
            let coords:number[][] = [];
            for (let i:number = 0; i < n; i++) { coords.push(numbers.splice(0, 2)); }
            fn(coords, index);
        });
    }


    function pushType(itemType:any, offset?:any) {
        return function (c:any) {
            if (offset) {
                c = c.map(function (c:any) { return [x(c) + x(offset), y(c) + y(offset)]; });
            }
            c.unshift(position);
            result.push({
                type: itemType,
                coords: c,
            });
            position = c[c.length - 1];
        };
    }


    function calculateBezierControlPoint():any {
        let lastBezier:any = result[result.length - 1];
        let controlPoint:any = null;
        if (!lastBezier || lastBezier.type !== bezier3Type) {
            controlPoint = position;
        } else {
            // Calculate the mirror point of the last control point
            let lastPoint:any = lastBezier.coords[2];
            let xOffset:any = x(position) - x(lastPoint);
            let yOffset:any = y(position) - y(lastPoint);

            controlPoint = [x(position) + xOffset, y(position) + yOffset];
        }

        return controlPoint;
    }


    function handleArcSegment(relative:any):void {
        readNumbers(7, function (numbers:any) {
            let c2:any = coordAdd(numbers.slice(5, 7), relative);
            let args:any = [position].concat(numbers.slice(0, 5)).concat([c2]);
            let curve:any = arcToCurve.apply(null, args);
            for (let i:number = 0; i < curve.length; i++) { pushType(bezier3Type)(curve[i]); }
        });
    }


    function readSegment():any {
        stripWhitespace();
        if (polygon === '') return;

        let operator:any = polygon[0];
        polygon = polygon.substring(1);

        let pushLine:any = pushType(lineType);
        let origin:any = [0, 0];

        switch (operator) {
            case 'M':
                readCoords(1, function (c:any, i:any) {
                    if (i === 0) {
                        position = c[0];
                        if (!start) start = position;
                    } else pushType(lineType)(c);
                });
                break;
            case 'm':
                readCoords(1, function (c:any, i:any) {
                    if (i === 0) {
                        if (!position) position = c[0];
                        else position = coordAdd(c, position);

                        if (!start) start = position;
                    } else {
                        let c0:any = c[0];
                        pushType(lineType)([coordAdd(c0, position)]);
                    }
                });
                break;
            case 'C':
                readCoords(3, pushType(bezier3Type));
                break;
            case 'c':
                readCoords(3, pushType(bezier3Type, position));
                break;
            case 'S':
                readCoords(2, function (coords:any) {
                    let controlPoint = calculateBezierControlPoint();
                    coords.unshift(controlPoint);
                    pushType(bezier3Type)(coords);
                });
                break;
            case 's':
                readCoords(2, function (coords:any) {
                    let controlPoint = calculateBezierControlPoint();
                    coords = coords.map(function (c:any) { return coordAdd(c, position); });
                    coords.unshift(controlPoint);
                    pushType(bezier3Type)(coords);
                });
                break;
            case 'A':
                handleArcSegment(origin);
                break;
            case 'a':
                handleArcSegment(position);
                break;
            case 'L':
                readCoords(1, pushType(lineType));
                break;
            case 'l':
                readCoords(1, function (c:any) { pushLine([[x(c[0]) + x(position), y(c[0]) + y(position)]]); });
                break;
            case 'H':
                pushType(lineType)([[readNumber(), y(position)]]);
                break;
            case 'h':
                pushType(lineType, position)([[readNumber(), 0]]);
                break;
            case 'V':
                pushType(lineType)([[x(position), readNumber()]]);
                break;
            case 'v':
                pushType(lineType, position)([[0, readNumber()]]);
                break;
            case 'Z':
            case 'z':
                if (!coordEqual(position, start)) {
                    pushType(lineType)([start]);
                }
                break;
            default:
                throw new Error('Unknown operator: ' + operator);
        } // jscs:ignore validateIndentation
        // ^ (jscs bug)
    }

    while (polygon.length > 0) {
        readSegment();
    }

    // Remove zero-length lines
    for (let i:number = 0; i < result.length; i++) {
        let segment = result[i];
        if (segment.type === lineType && coordEqual(segment.coords[0], segment.coords[1])) {
            result.splice(i, 1);
            i--;
        }
    }

    return result;
}


function intersectBezier3Line(p1:any, p2:any, p3:any, p4:any, a1:any, a2:any):any {
    let result:any[] = [];

    let min:any = coordMin(a1, a2); // used to determine if point is on line segment
    let max:any = coordMax(a1, a2); // used to determine if point is on line segment

    // Start with Bezier using Bernstein polynomials for weighting functions:
    //     (1-t^3)P1 + 3t(1-t)^2P2 + 3t^2(1-t)P3 + t^3P4
    //
    // Expand and collect terms to form linear combinations of original Bezier
    // controls.  This ends up with a vector cubic in t:
    //     (-P1+3P2-3P3+P4)t^3 + (3P1-6P2+3P3)t^2 + (-3P1+3P2)t + P1
    //             /\                  /\                /\       /\
    //             ||                  ||                ||       ||
    //             c3                  c2                c1       c0

    // Calculate the coefficients
    let a:any = coordMultiply(p1, -1);
    let b:any = coordMultiply(p2, 3);
    let c:any = coordMultiply(p3, -3);
    let c3:any = coordAdd(a, coordAdd(b, coordAdd(c, p4)));

    a = coordMultiply(p1, 3);
    b = coordMultiply(p2, -6);
    c = coordMultiply(p3, 3);
    let c2 = coordAdd(a, coordAdd(b, c));

    a = coordMultiply(p1, -3);
    b = coordMultiply(p2, 3);
    let c1:any = coordAdd(a, b);

    let c0:any = p1;

    // Convert line to normal form: ax + by + c = 0
    // Find normal to line: negative inverse of original line's slope
    let n:any = [y(a1) - y(a2), x(a2) - x(a1)];

    // Determine new c coefficient
    let cl:any = x(a1) * y(a2) - x(a2) * y(a1);

    // ?Rotate each cubic coefficient using line for new coordinate system?
    // Find roots of rotated cubic
    let roots:any = cubeRoots(
        coordDot(n, c3),
        coordDot(n, c2),
        coordDot(n, c1),
        coordDot(n, c0) + cl
    );

    // Any roots in closed interval [0,1] are intersections on Bezier, but
    // might not be on the line segment.
    // Find intersections and calculate point coordinates
    for (let i:number = 0; i < roots.length; i++) {
        let t:any = roots[i];

        if (t >= 0 && t <= 1) {
            // We're within the Bezier curve
            // Find point on Bezier
            let p5:any = coordLerp(p1, p2, t);
            let p6:any = coordLerp(p2, p3, t);
            let p7:any = coordLerp(p3, p4, t);

            let p8:any = coordLerp(p5, p6, t);
            let p9:any = coordLerp(p6, p7, t);

            let p10:any = coordLerp(p8, p9, t);

            // See if point is on line segment
            // Had to make special cases for vertical and horizontal lines due
            // to slight errors in calculation of p10
            if (x(a1) === x(a2)) {
                if (y(min) <= y(p10) && y(p10) <= y(max)) result.push(p10);
            } else if (y(a1) === y(a2)) {
                if (x(min) <= x(p10) && x(p10) <= x(max)) result.push(p10);
            } else if (x(min) <= x(p10) && x(p10) <= x(max) && y(min) <= y(p10) && y(p10) <= y(max)) {
                result.push(p10);
            }
        }
    }

    return result;
}


function intersectLineLine(a1:any, a2:any, b1:any, b2:any) {
    let ua_t:any = (x(b2) - x(b1)) * (y(a1) - y(b1)) - (y(b2) - y(b1)) * (x(a1) - x(b1));
    let ub_t:any = (x(a2) - x(a1)) * (y(a1) - y(b1)) - (y(a2) - y(a1)) * (x(a1) - x(b1));
    let u_b:any  = (y(b2) - y(b1)) * (x(a2) - x(a1)) - (x(b2) - x(b1)) * (y(a2) - y(a1));

    if (u_b !== 0) {
        let ua = ua_t / u_b;
        let ub = ub_t / u_b;

        if (ua >= 0 && ua <= 1 && ub >= 0 && ub <= 1) {
            return [
                [
                    x(a1) + ua * (x(a2) - x(a1)),
                    y(a1) + ua * (y(a2) - y(a1)),
                ]
            ];
        }
    }

    return [];
}


function getIntersections(zero:any, point:any, shape:any):any {
    let coords:any = shape.coords;
    switch (shape.type) {
        case bezier3Type:
            return intersectBezier3Line(coords[0], coords[1], coords[2], coords[3], zero, point);
        case lineType:
            return intersectLineLine(coords[0], coords[1], zero, point);
        default:
            throw new Error('Unsupported shape type: ' + shape.type);
    } // jscs:ignore validateIndentation
    // ^ (jscs bug)
}


export function isInside(point:any, polygon:any):any {
    let segments:any;
    if (polygon && Array.isArray(polygon)) segments = polygon;
    else segments = splitSegments(polygon);

    let minX:number = 0;
    let minY:number = 0;
    for (let s:number = 0; s < segments.length; s++) {
        let coords:any = segments[s].coords;
        for (let c:number = 0; c < coords.length; c++) {
            let coord:any = coords[c];
            minX = Math.min(minX, x(coord));
            minY = Math.min(minY, y(coord));
        }
    }
    let zero:number[] = [minX - 10, minY - 10];

    let intersections:any[] = [];
    for (let i:number = 0; i < segments.length; i++) {
        let newIntersections:any = getIntersections(zero, point, segments[i]);
        for (let j:number = 0; j < newIntersections.length; j++) {
            let seen:boolean = false;
            let intersection:any = newIntersections[j];

            for (let k:number = 0; k < intersections.length; k++) {
                if (coordEqual(intersections[k], intersection)) {
                    seen = true;
                    break;
                }
            }

            if (!seen) intersections.push(intersection);
        }
    }

    return intersections.length % 2 === 1;
}
