"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const alsatian_1 = require("alsatian");
const index_1 = require("../index");
let Tests = class Tests {
    test01(testValue) {
        alsatian_1.Expect(index_1.default.empty(testValue)).toBe(true);
    }
    test02(testValue) { alsatian_1.Expect(index_1.default.notEmpty(testValue)).toBe(true); }
    test03(testValue) {
        alsatian_1.Expect(index_1.default.regExp(testValue)).toBe(true);
    }
    test04(testValue) {
        alsatian_1.Expect(index_1.default.regExp(testValue)).toBe(false);
    }
    test05(testValue) {
        alsatian_1.Expect(index_1.default.array(testValue)).toBe(true);
    }
    test06(testValue) {
        alsatian_1.Expect(index_1.default.array(testValue)).toBe(false);
    }
    test07(testValue) {
        alsatian_1.Expect(index_1.default.object(testValue)).toBe(true);
    }
    test08(testValue) {
        alsatian_1.Expect(index_1.default.object(testValue)).toBe(false);
    }
    test09(testValue) {
        alsatian_1.Expect(index_1.default.nonEmptyObject(testValue)).toBe(true);
    }
    test10(testValue) {
        alsatian_1.Expect(index_1.default.emptyObject(testValue)).toBe(true);
    }
    testIsumberTrue(testValue) {
        alsatian_1.Expect(index_1.default.number(testValue)).toBe(true);
    }
    testIsumberFalse(testValue) {
        alsatian_1.Expect(index_1.default.number(testValue)).toBe(false);
    }
};
__decorate([
    alsatian_1.TestCase(null),
    alsatian_1.TestCase(undefined),
    alsatian_1.TestCase(''),
    alsatian_1.TestCase(NaN),
    alsatian_1.TestCase(),
    alsatian_1.Test('Empty')
], Tests.prototype, "test01", null);
__decorate([
    alsatian_1.TestCase(0),
    alsatian_1.TestCase(1),
    alsatian_1.TestCase(-1),
    alsatian_1.TestCase('hello'),
    alsatian_1.TestCase(false),
    alsatian_1.Test('Not Empty')
], Tests.prototype, "test02", null);
__decorate([
    alsatian_1.TestCase(/"(?:\\["\\]|[^\n"\\])*"/),
    alsatian_1.TestCase(/[ \t]+/),
    alsatian_1.TestCase(/[-\/\\^$*+?.()|[\]{}]/g),
    alsatian_1.Test('RegExp')
], Tests.prototype, "test03", null);
__decorate([
    alsatian_1.TestCase(0),
    alsatian_1.TestCase(1),
    alsatian_1.TestCase(-1),
    alsatian_1.TestCase('hello'),
    alsatian_1.TestCase(false),
    alsatian_1.Test('Not RegExp')
], Tests.prototype, "test04", null);
__decorate([
    alsatian_1.TestCase([0, 1, -1, 'hello', false]),
    alsatian_1.Test('Array')
], Tests.prototype, "test05", null);
__decorate([
    alsatian_1.TestCase(0),
    alsatian_1.TestCase(1),
    alsatian_1.TestCase(-1),
    alsatian_1.TestCase('hello'),
    alsatian_1.TestCase(false),
    alsatian_1.Test('Not array')
], Tests.prototype, "test06", null);
__decorate([
    alsatian_1.TestCase({ property01: 'boo', property02: 3 }),
    alsatian_1.TestCase({ "property01": "boo", "property02": 3 }),
    alsatian_1.TestCase({}),
    alsatian_1.Test('Object')
], Tests.prototype, "test07", null);
__decorate([
    alsatian_1.TestCase(0),
    alsatian_1.TestCase(1),
    alsatian_1.TestCase(-1),
    alsatian_1.TestCase('hello'),
    alsatian_1.TestCase(false),
    alsatian_1.Test('Not object')
], Tests.prototype, "test08", null);
__decorate([
    alsatian_1.TestCase({ property01: 'boo', property02: 3 }),
    alsatian_1.Test('Non-empty object')
], Tests.prototype, "test09", null);
__decorate([
    alsatian_1.TestCase({}),
    alsatian_1.Test('Empty object')
], Tests.prototype, "test10", null);
__decorate([
    alsatian_1.TestCase(0),
    alsatian_1.TestCase(1),
    alsatian_1.TestCase(-1),
    alsatian_1.Test('Is number true')
], Tests.prototype, "testIsumberTrue", null);
__decorate([
    alsatian_1.TestCase('hello'),
    alsatian_1.TestCase(false),
    alsatian_1.Test('Is number false')
], Tests.prototype, "testIsumberFalse", null);
Tests = __decorate([
    alsatian_1.TestFixture('is Tests')
], Tests);
exports.Tests = Tests;
//# sourceMappingURL=index.test.js.map