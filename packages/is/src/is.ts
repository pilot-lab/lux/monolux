/**
 * is is completely dependency-free, so it won't create cross-dependencies with other LUX packages.
 */
export class is {
    static empty(value:any, isTestEmptyString:boolean = true):boolean {
        try {
            return (
                this.undefined(value) ||
                typeof value === 'undefined' ||
                value === null ||
                value === undefined ||
                (isTestEmptyString && value === '') ||
                this.NaN(value)
            );
        } catch(e) { return true; }
    }
    static notEmpty(value:any, isTestEmptyString:boolean = true):boolean { return !this.empty(value, isTestEmptyString); }


    static emptyAny(...values:any[]):boolean {
        let isAnyValueEmpty:boolean = false;

        values.forEach((value:any) => {
            if (this.empty(value)) {
                isAnyValueEmpty = true;
                return false;
            }

            return true;
        });

        return isAnyValueEmpty;
    }


    static notEmptyAny(...values:any[]):boolean { return !this.emptyAny(...values); }


    static emptyDeep(rootObject:any, deepChild:string):boolean {
        if (this.empty(rootObject) || this.empty(deepChild)) return true;
        let obj:any = rootObject;
        let isEmpty:boolean = false;

        deepChild.split('.').every((child:string) => {
            if (this.empty(obj)) {
                isEmpty = true;
                return false;
            }

            obj = obj[child];
            return true;
        });

        return isEmpty;
    }


    static notEmptyDeep(rootObject:any, deepChild:string):boolean {
        return !this.emptyDeep(rootObject, deepChild);
    }


    /**
     * Validate nested properties on an object.
     * For example: let isEmpty:boolean = g.emptyDeep(myObject, 'propertyLevel1.propertyLevel2.propertyLevel3');
     */
    static emptyDeepAny(rootObject:any, ...deepChildren:string[]):boolean {
        let isAnyDeepChildEmpty:boolean = false;

        deepChildren.forEach((deepChild:string) => {
            if (this.emptyDeep(rootObject, deepChild)) {
                isAnyDeepChildEmpty = true;
                return false;
            }

            return true;
        });

        return isAnyDeepChildEmpty;
    }


    static notEmptyDeepAny(rootObject:any, ...deepChildren:string[]):boolean {
        return !this.emptyDeepAny(rootObject, ...deepChildren);
    }


    static array(value:any):boolean { return Array.isArray(value); }
    static regExp(value:any):boolean { return this.notEmpty(value, true) && value !== 0 && value instanceof RegExp }
    static object(value:any):boolean {
        let isObject:boolean = this.notEmpty(value) &&
            typeof value === 'object' &&
            !this.regExp(value) &&
            !this.array(value);

        return isObject;
    }


    static emptyObject(value:any):boolean { return this.object(value) && Object.keys(value).length === 0;  }
    static nonEmptyObject(value:any):boolean { return this.object(value) && !this.emptyObject(value); }


    /**
     * A simple function to check if the given value is a number that is not equal to Infinity.
     */
    static number(value:any):boolean { return !isNaN(parseFloat(value)) && isFinite(value); }
    static notNumber(value:any):boolean { return !this.number(value); }


    /**
     * Tests specifically for a NaN value.
     * See: http://adripofjavascript.com/blog/drips/the-problem-with-testing-for-nan-in-javascript.html
     */
    static NaN(value:any):boolean {
        /// NaN is the only value in Javascript that is not equal to itself.
        return value !== value;
    }


    static undefined(value:any){ return value === void 0; }
} // End class


export default is;
