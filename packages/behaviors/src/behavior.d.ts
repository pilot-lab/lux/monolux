import { Initializable } from '@pilotlab/initializable';
import { Signal } from '@pilotlab/signals';
import IBehavior from "./iBehavior";
export declare abstract class Behavior<TBehaviorTarget> extends Initializable implements IBehavior<TBehaviorTarget> {
    constructor(data?: any);
    readonly key: string;
    readonly isActivated: boolean;
    protected p_isActivated: boolean;
    readonly target: TBehaviorTarget;
    protected p_activationTarget: TBehaviorTarget;
    protected p_initializationData: any;
    readonly activated: Signal<any>;
    protected p_activated: Signal<any>;
    activate(target: TBehaviorTarget, ...args: any[]): void;
    deactivate(...args: any[]): void;
    protected p_onActivationStarted(data: any, ...args: any[]): void;
    protected p_onActivated(data: any, ...args: any[]): void;
    protected p_onDeactivated(...args: any[]): void;
}
export default Behavior;
