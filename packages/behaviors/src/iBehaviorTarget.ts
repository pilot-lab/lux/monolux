import IBehaviors from './iBehaviors';
export interface IBehaviorTarget { behaviors:IBehaviors<any> } // End interface
export default IBehaviorTarget;
