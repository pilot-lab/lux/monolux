import { MapList } from '@pilotlab/collections';
import Behavior from './behavior';
import IBehavior from './iBehavior';
import IBehaviors from './iBehaviors';
export declare class Behaviors<TBehaviorTarget> implements IBehaviors<TBehaviorTarget> {
    constructor(target?: TBehaviorTarget, isActivateOnAdd?: boolean);
    protected p_collectionName: string;
    protected p_map: MapList<string, IBehavior<TBehaviorTarget>>;
    target: TBehaviorTarget;
    protected p_target: TBehaviorTarget;
    isActivateOnAdd: boolean;
    protected p_isActivateOnAdd: boolean;
    add(value: IBehavior<TBehaviorTarget>): void;
    get(key: string): IBehavior<TBehaviorTarget>;
    remove(key: string): void;
    clear(): void;
    forEach(callback: (key: string, value: IBehavior<TBehaviorTarget>) => boolean): void;
    activateAll(...args: any[]): void;
    deactivateAll(...args: any[]): void;
    log(): void;
}
export default Behavior;
