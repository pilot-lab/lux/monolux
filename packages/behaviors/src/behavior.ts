import is from '@pilotlab/is';
import { Initializable } from '@pilotlab/initializable';
import { Signal } from '@pilotlab/signals';
import IBehavior from "./iBehavior";


export abstract class Behavior<TBehaviorTarget>
extends Initializable
implements IBehavior<TBehaviorTarget> {
    constructor(data?:any) {
        super();
        if (is.notEmpty(data)) this.p_initializationData = data;
        this.p_activated = new Signal<any>();
    }


    /*====================================================================*
     START: Properties
     *====================================================================*/
    get key():string { return 'default'; }


    get isActivated():boolean { return this.p_isActivated; }
    protected p_isActivated:boolean = false;


    get target():TBehaviorTarget { return this.p_activationTarget; }
    protected p_activationTarget:TBehaviorTarget;


    protected p_initializationData:any;


    /*====================================================================*
     START: Signals
     *====================================================================*/
    get activated():Signal<any> { return this.p_activated; }
    protected p_activated:Signal<any>;


    /*====================================================================*
     START: Methods
     *====================================================================*/
    activate(target:TBehaviorTarget, ...args:any[]):void {
        if (is.empty(target)) return;
        this.p_activationTarget = target;

        this.p_onActivationStarted(this.p_initializationData, ...args);
        this.p_onActivated(this.p_initializationData, ...args);
        this.activated.dispatch(this.p_initializationData);
        this.p_isActivated = true;
    }


    deactivate(...args:any[]):void {
        this.p_onDeactivated(...args);
        this.p_isActivated = false;
    }


    protected p_onActivationStarted(data:any, ...args:any[]):void {}
    protected p_onActivated(data:any, ...args:any[]):void {}
    protected p_onDeactivated(...args:any[]):void {}
} // End class


export default Behavior;
