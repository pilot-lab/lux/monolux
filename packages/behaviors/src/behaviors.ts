import { is } from '@pilotlab/is';
import Debug from '@pilotlab/debug';
import { MapList } from '@pilotlab/collections';
import Behavior from './behavior';
import IBehavior from './iBehavior';
import IBehaviors from './iBehaviors';


export class Behaviors<TBehaviorTarget>
implements IBehaviors<TBehaviorTarget> {
    constructor(target?:TBehaviorTarget, isActivateOnAdd:boolean = true) {
        if (is.notEmpty(target)) this.p_target = target;
        this.p_isActivateOnAdd = isActivateOnAdd;
    }


    protected p_collectionName:string = 'behaviors'; // Used for debugging
    protected p_map:MapList<string, IBehavior<TBehaviorTarget>> = new MapList<string, IBehavior<TBehaviorTarget>>();


    get target():TBehaviorTarget { return this.p_target; }
    set target(value:TBehaviorTarget) { this.p_target = value; }
    protected p_target:TBehaviorTarget;


    get isActivateOnAdd():boolean { return this.p_isActivateOnAdd; }
    set isActivateOnAdd(value:boolean) { this.p_isActivateOnAdd = value; }
    protected p_isActivateOnAdd:boolean = true;


    add(value:IBehavior<TBehaviorTarget>):void {
        //----- Don't add the same value twice.
        if (this.p_map.has(value.key)) return;

        if (this.p_isActivateOnAdd) value.activate(this.p_target);
        this.p_map.set(value.key, value);
    }


    get(key:string):IBehavior<TBehaviorTarget> { return this.p_map.get(key); }


    remove(key:string):void {
        let value:IBehavior<TBehaviorTarget> = this.p_map.get(key);
        if (is.notEmpty(value)) value.deactivate();
        this.p_map.delete(key);
    }


    clear():void { this.p_map.clear(); }


    forEach(callback:(key:string, value:IBehavior<TBehaviorTarget>) => boolean):void {
        this.p_map.forEach((value2:IBehavior<TBehaviorTarget>, key2:string):boolean => {
            return callback(key2, value2);
        });
    }


    activateAll(...args:any[]):void {
        this.p_isActivateOnAdd = true;
        this.p_map.forEach((value:IBehavior<TBehaviorTarget>, key:string):boolean => {
            value.activate(this.p_target, ...args);
            return true;
        });
    }


    deactivateAll(...args:any[]):void {
        this.p_isActivateOnAdd = false;
        this.p_map.forEach((value:IBehavior<TBehaviorTarget>, key:string):boolean => {
            value.deactivate(...args);
            return true;
        });
    }


    log():void {
        Debug.log(this.p_collectionName + ':');
        this.p_map.forEach((value:IBehavior<TBehaviorTarget>, key:string):boolean => {
            Debug.log('    ' + key);
            return true;
        });
    }
} // End class


export default Behavior;
