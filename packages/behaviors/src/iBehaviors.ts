import IBehavior from './iBehavior';


export interface IBehaviors<TBehaviorTarget> {
    target:TBehaviorTarget;
    isActivateOnAdd:boolean;


    add(value:IBehavior<TBehaviorTarget>):void;
    get(key:string):IBehavior<TBehaviorTarget>;
    remove(key:string):void;
    clear():void;
    forEach(callback:(key:string, value:IBehavior<TBehaviorTarget>) => boolean):void;
    activateAll(...args:any[]):void;
    deactivateAll(...args:any[]):void;
    log():void;
} // End interface


export default IBehaviors;
