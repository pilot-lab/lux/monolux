import IBehaviors from './iBehaviors';
export interface IBehaviorTarget {
    behaviors: IBehaviors<any>;
}
export default IBehaviorTarget;
