import { Signal } from '@pilotlab/signals';


export interface IBehavior<TBehaviorTarget> {
    readonly key:string;
    readonly isActivated:boolean;
    readonly target:TBehaviorTarget;


    readonly activated:Signal<any>;


    activate(target:TBehaviorTarget, ...args:any[]):void;
    deactivate(...args:any[]):void;
} // End interface


export default IBehavior;
