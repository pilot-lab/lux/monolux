import IBehavior from './src/iBehavior';
import IBehaviors from './src/iBehaviors';
import Behavior from './src/behavior';
import Behaviors from './src/behaviors';
import IBehaviorTarget from './src/iBehaviorTarget';
export { IBehavior, IBehaviors, Behavior, Behaviors, IBehaviorTarget };
