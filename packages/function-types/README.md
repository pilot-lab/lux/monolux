# function-types

General function interfaces for commonly used function signatures.

## Install

sudo npm install --save @pilotlab/function-types

## Usage

```
import {IEqualsFunction, ICompareFunction, ILoopFunction} from '@pilotlab/function-types';
```
